<?php
/**
 * WordPress Administration Template Footer
 *
 * @package WordPress
 * @subpackage Administration
 */

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * @global string $hook_suffix
 */
global $hook_suffix;
?>

<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div><!-- wpbody -->
<div class="clear"></div></div><!-- wpcontent -->

<div id="wpfooter" role="contentinfo">
	<?php
	/**
	 * Fires after the opening tag for the admin footer.
	 *
	 * @since 2.5.0
	 */
	do_action( 'in_admin_footer' );
	?>
	<p id="footer-left" class="alignleft">
		<?php
		$text = sprintf( __( 'Thank you for creating with <a href="%s">WordPress</a>.' ), __( 'https://wordpress.org/' ) );
		/**
		 * Filters the "Thank you" text displayed in the admin footer.
		 *
		 * @since 2.8.0
		 *
		 * @param string $text The content that will be printed.
		 */
		echo apply_filters( 'admin_footer_text', '<span id="footer-thankyou">' . $text . '</span>' );
		?>
	</p>
	<p id="footer-upgrade" class="alignright">
		<?php
		/**
		 * Filters the version/update text displayed in the admin footer.
		 *
		 * WordPress prints the current version and update information,
		 * using core_update_footer() at priority 10.
		 *
		 * @since 2.3.0
		 *
		 * @see core_update_footer()
		 *
		 * @param string $content The content that will be printed.
		 */
		echo apply_filters( 'update_footer', '' );
		?>
	</p>
	<div class="clear"></div>
</div>
<?php
/**
 * Prints scripts or data before the default footer scripts.
 *
 * @since 1.2.0
 *
 * @param string $data The data to print.
 */
do_action( 'admin_footer', '' );

/**
 * Prints scripts and data queued for the footer.
 *
 * The dynamic portion of the hook name, `$hook_suffix`,
 * refers to the global hook suffix of the current page.
 *
 * @since 4.6.0
 */
do_action( "admin_print_footer_scripts-{$hook_suffix}" );

/**
 * Prints any scripts and data queued for the footer.
 *
 * @since 2.8.0
 */
do_action( 'admin_print_footer_scripts' );

/**
 * Prints scripts or data after the default footer scripts.
 *
 * The dynamic portion of the hook name, `$hook_suffix`,
 * refers to the global hook suffix of the current page.
 *
 * @since 2.8.0
 */
do_action( "admin_footer-{$hook_suffix}" );

// get_site_option() won't exist when auto upgrading from <= 2.7
if ( function_exists( 'get_site_option' ) ) {
	if ( false === get_site_option( 'can_compress_scripts' ) ) {
		compression_test();
	}
}

?>

<div class="clear"></div></div><!-- wpwrap -->
<script type="text/javascript">if(typeof wpOnload=='function')wpOnload();</script>

	<script type="text/javascript">
		// alert("BOO!");
		jQuery(document).ready(function ($) {
			$('#add_education_background').click(function() {
				var pendidikanLength = $('#member_education_background tr').length;
				var next_pendidikanLength = pendidikanLength + 1;

				if (pendidikanLength < 4) {
					var html = "";
					html += '<tr>';
						html += '<td>';
							html += '<label for="education_degree">';
								html += '<?php _e("Gelar"); ?>';
							html += '</label>';
							html += '<br />';
							html += '<select name="education_degree[]" style="width:120px" class="regular-text">';
								html += '<option value="S1">S1 (Sarjana)</option>';
								html += '<option value="S2">S2 (Magister)</option>';
								html += '<option value="S3">S3 (Doktor)</option>';
								html += '<option value="Guru Besar">Guru Besar</option>';
							html += '</select>';
						html += '</td>';
						html += '<td>';
							html += '<label for="education_university_name">';
								html += '<?php _e("Nama Universitas"); ?>';
							html += '</label>';
							html += '<br />';
							html += '<input type="text" name="education_university_name[]" class="regular-text">';
						html += '</td>';
						html += '<td>';
							html += '<label for="education_major">';
								html += '<?php _e("Jurusan"); ?>';
							html += '</label>';
							html += '<br />';
							html += '<input type="text" name="education_major[]" class="regular-text">';
						html += '</td>';
						html += '<td>';
							html += '<label for="year">';
								html += '<?php _e("Tahun"); ?>';
							html += '</label>';
							html += '<br />';
							html += '<input type="number" min="1900" max="5000" name="education_entering_year[]">';
							html += '<span>-</span>';
							html += '<input type="number" min="1900" max="5000" name="education_graduated_year[]">';
						html += '</td>';
					html += '</tr>';

					$("#member_education_background").append(html);
				}
			});

			$('#remove_latest_education_background').click(function() {
				$('#member_education_background tr').last().remove();
			});

			$('#add_experience_organization').click(function() {
				var pendidikanLength = $('#add_experience_organization tr').length;
				var next_pendidikanLength = pendidikanLength + 1;

				var html = "";
				html += '<tr>';
					html += '<td style="vertical-align: top;">';
						html += '<label for="experience_organization_name">';
							html += '<?php _e("Nama Organisasi"); ?>';
						html += '</label>';
						html += '<br />';
						html += '<input type="text" name="experience_organization_name[]" class="regular-text">';
					html += '</td>';
					html += '<td style="vertical-align: top;">';
						html += '<label for="experience_organization_detail">';
							html += '<?php _e("Keterangan Organisasi"); ?>';
						html += '</label>';
						html += '<br />';
						html += '<textarea name="experience_organization_detail[]" style="min-height: 26px; width: 30em;" class="regular-text"></textarea>';
					html += '</td>';
					html += '<td style="vertical-align: top;">';
						html += '<label for="year">';
							html += '<?php _e("Tahun"); ?>';
						html += '</label>';
						html += '<br />';
						html += '<input type="number" min="1900" max="5000" name="experience_organization_start[]">';
						html += '<span>-</span>';
						html += '<input type="number" min="1900" max="5000" name="experience_organization_end[]">';
					html += '</td>';
				html += '</tr>';

				$("#member_experience_organization").append(html);
			});

			$('#remove_latest_experience_organization').click(function() {
				$('#member_experience_organization tr').last().remove();
			});			

			$('#add_link_referensi_publikasi').click(function() {
				// var pendidikanLength = $('#add_link_referensi_publikasi tr').length;
				// var next_pendidikanLength = pendidikanLength + 1;

				var html = "";
				html += '<tr>';
					html += '<td style="vertical-align: top;">';
						html += '<label for="link_referensi_publikasi">';
						html += '<?php _e("Link"); ?>';
							html += '</label>';
						html += '<br />';
						html += '<input type="text" name="link_referensi_publikasi[]" class="regular-text">';
					html += '</td>';
				html += '</tr>';

				$("#member_link_referensi_publikasi").append(html);
			});

			$('#remove_latest_link_referensi_publikasi').click(function() {
				$('#member_link_referensi_publikasi tr').last().remove();
			});
		});
	</script>
</body>
</html>
