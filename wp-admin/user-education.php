<?php
/**
 * User administration panel
 *
 * @package WordPress
 * @subpackage Administration
 * @since 1.0.0
 */

global $wpdb;

/** WordPress Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

if ( ! current_user_can( 'list_users' ) ) {
	wp_die(
		'<h1>' . __( 'You need a higher level of permission.' ) . '</h1>' .
		'<p>' . __( 'Sorry, you are not allowed to list users.' ) . '</p>',
		403
	);
}

$wp_list_table = _get_list_table( 'WP_Users_List_Table' );
$pagenum       = $wp_list_table->get_pagenum();
$title         = __( 'Users' );
$parent_file   = 'users.php';

add_screen_option( 'per_page' );

// contextual help - choose Help on the top right of admin panel to preview this.
get_current_screen()->add_help_tab(
	array(
		'id'      => 'overview',
		'title'   => __( 'Overview' ),
		'content' => '<p>' . __( 'This screen lists all the existing users for your site. Each user has one of five defined roles as set by the site admin: Site Administrator, Editor, Author, Contributor, or Subscriber. Users with roles other than Administrator will see fewer options in the dashboard navigation when they are logged in, based on their role.' ) . '</p>' .
						'<p>' . __( 'To add a new user for your site, click the Add New button at the top of the screen or Add New in the Users menu section.' ) . '</p>',
	)
);

get_current_screen()->add_help_tab(
	array(
		'id'      => 'screen-content',
		'title'   => __( 'Screen Content' ),
		'content' => '<p>' . __( 'You can customize the display of this screen in a number of ways:' ) . '</p>' .
						'<ul>' .
						'<li>' . __( 'You can hide/display columns based on your needs and decide how many users to list per screen using the Screen Options tab.' ) . '</li>' .
						'<li>' . __( 'You can filter the list of users by User Role using the text links above the users list to show All, Administrator, Editor, Author, Contributor, or Subscriber. The default view is to show all users. Unused User Roles are not listed.' ) . '</li>' .
						'<li>' . __( 'You can view all posts made by a user by clicking on the number under the Posts column.' ) . '</li>' .
						'</ul>',
	)
);

$help = '<p>' . __( 'Hovering over a row in the users list will display action links that allow you to manage users. You can perform the following actions:' ) . '</p>' .
	'<ul>' .
	'<li>' . __( '<strong>Edit</strong> takes you to the editable profile screen for that user. You can also reach that screen by clicking on the username.' ) . '</li>';

if ( is_multisite() ) {
	$help .= '<li>' . __( '<strong>Remove</strong> allows you to remove a user from your site. It does not delete their content. You can also remove multiple users at once by using Bulk Actions.' ) . '</li>';
} else {
	$help .= '<li>' . __( '<strong>Delete</strong> brings you to the Delete Users screen for confirmation, where you can permanently remove a user from your site and delete their content. You can also delete multiple users at once by using Bulk Actions.' ) . '</li>';
}

$help .= '</ul>';

get_current_screen()->add_help_tab(
	array(
		'id'      => 'action-links',
		'title'   => __( 'Available Actions' ),
		'content' => $help,
	)
);
unset( $help );

get_current_screen()->set_help_sidebar(
	'<p><strong>' . __( 'For more information:' ) . '</strong></p>' .
	'<p>' . __( '<a href="https://codex.wordpress.org/Users_Screen">Documentation on Managing Users</a>' ) . '</p>' .
	'<p>' . __( '<a href="https://codex.wordpress.org/Roles_and_Capabilities">Descriptions of Roles and Capabilities</a>' ) . '</p>' .
	'<p>' . __( '<a href="https://wordpress.org/support/">Support</a>' ) . '</p>'
);

get_current_screen()->set_screen_reader_content(
	array(
		'heading_views'      => __( 'Filter users list' ),
		'heading_pagination' => __( 'Users list navigation' ),
		'heading_list'       => __( 'Users list' ),
	)
);

if ( empty( $_REQUEST ) ) {
	$referer = '<input type="hidden" name="wp_http_referer" value="' . esc_attr( wp_unslash( $_SERVER['REQUEST_URI'] ) ) . '" />';
} elseif ( isset( $_REQUEST['wp_http_referer'] ) ) {
	$redirect = remove_query_arg( array( 'wp_http_referer', 'updated', 'delete_count' ), wp_unslash( $_REQUEST['wp_http_referer'] ) );
	$referer  = '<input type="hidden" name="wp_http_referer" value="' . esc_attr( $redirect ) . '" />';
} else {
	$redirect = 'users.php';
	$referer  = '';
}

$update = '';

switch ( $wp_list_table->current_action() ) {

	/* Bulk Dropdown menu Role changes */
	case 'promote':
		check_admin_referer( 'bulk-users' );

		if ( ! current_user_can( 'promote_users' ) ) {
			wp_die( __( 'Sorry, you are not allowed to edit this user.' ), 403 );
		}

		if ( empty( $_REQUEST['users'] ) ) {
			wp_redirect( $redirect );
			exit();
		}

		$editable_roles = get_editable_roles();
		$role           = false;
		if ( ! empty( $_REQUEST['new_role2'] ) ) {
			$role = $_REQUEST['new_role2'];
		} elseif ( ! empty( $_REQUEST['new_role'] ) ) {
			$role = $_REQUEST['new_role'];
		}

		if ( ! $role || empty( $editable_roles[ $role ] ) ) {
			wp_die( __( 'Sorry, you are not allowed to give users that role.' ), 403 );
		}

		$userids = $_REQUEST['users'];
		$update  = 'promote';
		foreach ( $userids as $id ) {
			$id = (int) $id;

			if ( ! current_user_can( 'promote_user', $id ) ) {
				wp_die( __( 'Sorry, you are not allowed to edit this user.' ), 403 );
			}
			// The new role of the current user must also have the promote_users cap or be a multisite super admin
			if ( $id == $current_user->ID && ! $wp_roles->role_objects[ $role ]->has_cap( 'promote_users' )
			&& ! ( is_multisite() && current_user_can( 'manage_network_users' ) ) ) {
					$update = 'err_admin_role';
					continue;
			}

			// If the user doesn't already belong to the blog, bail.
			if ( is_multisite() && ! is_user_member_of_blog( $id ) ) {
				wp_die(
					'<h1>' . __( 'Something went wrong.' ) . '</h1>' .
					'<p>' . __( 'One of the selected users is not a member of this site.' ) . '</p>',
					403
				);
			}

			$user = get_userdata( $id );
			$user->set_role( $role );
		}

		wp_redirect( add_query_arg( 'update', $update, $redirect ) );
		exit();

	default:
		if ( ! empty( $_GET['_wp_http_referer'] ) ) {
			wp_redirect( remove_query_arg( array( '_wp_http_referer', '_wpnonce' ), wp_unslash( $_SERVER['REQUEST_URI'] ) ) );
			exit;
		}

		if ( $wp_list_table->current_action() && ! empty( $_REQUEST['users'] ) ) {
			$userids  = $_REQUEST['users'];
			$sendback = wp_get_referer();

			/** This action is documented in wp-admin/edit-comments.php */
			$sendback = apply_filters( 'handle_bulk_actions-' . get_current_screen()->id, $sendback, $wp_list_table->current_action(), $userids );

			wp_safe_redirect( $sendback );
			exit;
		}

		$wp_list_table->prepare_items();
		$total_pages = $wp_list_table->get_pagination_arg( 'total_pages' );
		if ( $pagenum > $total_pages && $total_pages > 0 ) {
			wp_redirect( add_query_arg( 'paged', $total_pages ) );
			exit;
		}

		include( ABSPATH . 'wp-admin/admin-header.php' );

		$messages = array();
		if ( isset( $_GET['update'] ) ) :
			switch ( $_GET['update'] ) {
				case 'del':
				case 'del_many':
					$delete_count = isset( $_GET['delete_count'] ) ? (int) $_GET['delete_count'] : 0;
					if ( 1 == $delete_count ) {
						$message = __( 'User deleted.' );
					} else {
						$message = _n( '%s user deleted.', '%s users deleted.', $delete_count );
					}
					$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . sprintf( $message, number_format_i18n( $delete_count ) ) . '</p></div>';
					break;
				case 'add':
					if ( isset( $_GET['id'] ) && ( $user_id = $_GET['id'] ) && current_user_can( 'edit_user', $user_id ) ) {
						/* translators: %s: edit page url */
						$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . sprintf(
							__( 'New user created. <a href="%s">Edit user</a>' ),
							esc_url(
								add_query_arg(
									'wp_http_referer',
									urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
									self_admin_url( 'user-edit.php?user_id=' . $user_id )
								)
							)
						) . '</p></div>';
					} else {
						$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . __( 'New user created.' ) . '</p></div>';
					}
					break;
				case 'promote':
					$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . __( 'Changed roles.' ) . '</p></div>';
					break;
				case 'err_admin_role':
					$messages[] = '<div id="message" class="error notice is-dismissible"><p>' . __( 'The current user&#8217;s role must have user editing capabilities.' ) . '</p></div>';
					$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . __( 'Other user roles have been changed.' ) . '</p></div>';
					break;
				case 'err_admin_del':
					$messages[] = '<div id="message" class="error notice is-dismissible"><p>' . __( 'You can&#8217;t delete the current user.' ) . '</p></div>';
					$messages[] = '<div id="message" class="updated notice is-dismissible"><p>' . __( 'Other users have been deleted.' ) . '</p></div>';
					break;
				case 'remove':
					$messages[] = '<div id="message" class="updated notice is-dismissible fade"><p>' . __( 'User removed from this site.' ) . '</p></div>';
					break;
				case 'err_admin_remove':
					$messages[] = '<div id="message" class="error notice is-dismissible"><p>' . __( "You can't remove the current user." ) . '</p></div>';
					$messages[] = '<div id="message" class="updated notice is-dismissible fade"><p>' . __( 'Other users have been removed.' ) . '</p></div>';
					break;
			}
		endif;
		?>

		<?php if ( isset( $errors ) && is_wp_error( $errors ) ) : ?>
		<div class="error">
			<ul>
			<?php
			foreach ( $errors->get_error_messages() as $err ) {
				echo "<li>$err</li>\n";
			}
			?>
			</ul>
		</div>
			<?php
	endif;

		if ( ! empty( $messages ) ) {
			foreach ( $messages as $msg ) {
				echo $msg;
			}
		}
		?>

	<div class="wrap">
	<h1 class="wp-heading-inline">
		<?php
		echo esc_html( $title );
		?>
</h1>

		<?php
		if ( current_user_can( 'create_users' ) ) {
			?>
	<a href="<?php echo admin_url( 'user-new.php' ); ?>" class="page-title-action"><?php echo esc_html_x( 'Add New', 'user' ); ?></a>
<?php } elseif ( is_multisite() && current_user_can( 'promote_users' ) ) { ?>
	<a href="<?php echo admin_url( 'user-new.php' ); ?>" class="page-title-action"><?php echo esc_html_x( 'Add Existing', 'user' ); ?></a>
			<?php
}

if ( strlen( $usersearch ) ) {
	/* translators: %s: search keywords */
	printf( '<span class="subtitle">' . __( 'Search results for &#8220;%s&#8221;' ) . '</span>', esc_html( $usersearch ) );
}
?>

<?php
  if(isset($_GET['education']) == false ){
    // wp_redirect('users.php');
  }
?>

<style>
#screen-options-link-wrap {
	display: none;
}
</style>

<?php
  $user_s1 = $wpdb->get_results(
    "SELECT ID FROM unsi_user_education WHERE degree = 'S1'",
    OBJECT
  );

  $user_s2 = $wpdb->get_results(
    "SELECT ID FROM unsi_user_education WHERE degree = 'S2'",
    OBJECT
  );

  $user_s3 = $wpdb->get_results(
    "SELECT ID FROM unsi_user_education WHERE degree = 'S3'",
    OBJECT
  );

  $user_guru_besar = $wpdb->get_results(
    "SELECT ID FROM unsi_user_education WHERE degree = 'Guru Besar'",
    OBJECT
  );

  $users = $wpdb->get_results(
    "SELECT ID FROM unsi_users",
    OBJECT
  );

	$order_user_e = "";
	if(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order'])) {
		$order_user_e = " ORDER BY user_login " . $_GET['order'];
	}
	if(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order'])) {
		$order_user_e = " ORDER BY user_email " . $_GET['order'];
	}

	$limit = 20;
	$limit_pagination = " LIMIT " . $limit;
	if(isset($_GET['hal'])) {
		$offset = ($_GET['hal'] - 1) * $limit;
		$limit_pagination = " LIMIT " . $offset . "," . $limit;
	}

	$get_education = $_GET['education'];

	$get_education_uppercase = strtoupper($get_education);
	if($get_education === "gurubesar") {
		$get_education_uppercase = "GURU BESAR";
	}

	if(isset($_GET["orderby"]) && $_GET["orderby"] === "sertifikat") {
		$user_e = $wpdb->get_results(
			"SELECT unsi_users.*, unsi_user_education.degree FROM unsi_users JOIN unsi_user_education ON unsi_user_education.user_id = unsi_users.ID LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) WHERE unsi_user_education.degree='" . $get_education_uppercase . "' AND alias.meta_key = 'upload_sertifikat' ORDER BY alias.meta_value " . $_GET["order"] . " " . $order_user_e . $limit_pagination,
			OBJECT
		);
	} else {
		$user_e = $wpdb->get_results(
			"SELECT unsi_users.*, unsi_user_education.degree FROM unsi_users JOIN unsi_user_education ON unsi_user_education.user_id = unsi_users.ID WHERE unsi_user_education.degree='" . $get_education_uppercase . "'" . $order_user_e . $limit_pagination,
			OBJECT
		);
	}

  // $user_e = $wpdb->get_results(
  //   "SELECT * FROM unsi_users" . $order_user_e . $limit_pagination,
  //   OBJECT
	// );
	
	$page = isset($_GET['hal'])? (int)$_GET['hal']:1;
	$loopPage = $wpdb->get_results(
		"SELECT unsi_users.*, unsi_user_education.degree FROM unsi_users JOIN unsi_user_education ON unsi_user_education.user_id = unsi_users.ID WHERE unsi_user_education.degree='" . $get_education_uppercase . "'" . $order_user_e,
		OBJECT
	);
	$total = count($loopPage);
	$pages = ceil($total/$limit);

	$is_sorted = (isset($_GET['orderby']) && isset($_GET['order'])) ? 'orderby=' . $_GET['orderby'] . '&order=' . $_GET['order'] . '&' : '';
?>

<div>
	<p style="float:left; margin:10px 10px 0px 0px;">Filter by Education: </p>
	<ul class="subsubsub" style="float:left;">
		<li>
			<a href="<?php echo admin_url( 'user-education.php' ); ?>?education=s1"<?php echo ($get_education ==='s1')? 'class="current"' : ''; ?>>
				S1 <span class="count">(<?php echo count($user_s1); ?>)</span>
			</a> |
		</li>
		<li>
			<a href="<?php echo admin_url( 'user-education.php' ); ?>?education=s2"<?php echo ($get_education ==='s2')? 'class="current"' : ''; ?>>
				S2 <span class="count">(<?php echo count($user_s2); ?>)</span>
			</a> |
		</li>
		<li>
			<a href="<?php echo admin_url( 'user-education.php' ); ?>?education=s3"<?php echo ($get_education ==='s3')? 'class="current"' : ''; ?>>
				S3 <span class="count">(<?php echo count($user_s3); ?>)</span>
			</a> |
		</li>
		<li>
			<a href="<?php echo admin_url( 'user-education.php' ); ?>?education=gurubesar"<?php echo ($get_education ==='gurubesar')? 'class="current"' : ''; ?>>
				Guru Besar <span class="count">(<?php echo count($user_guru_besar); ?>)</span>
			</a> |
		</li>
		<li>
			<a href="<?php echo admin_url( 'users.php' ); ?>">
				Show All User <span class="count">(<?php echo count($users); ?>)</span>
			</a>
		</li>
	</ul>
	<div class="clear"></div>
</div>

<div class="top">
	
	<?php
		wp_custom_pagination(
			array(
				'base'				=> admin_url() . 'user-education.php?education=' . $get_education . '&' . $is_sorted,
				'page'				=> $page,
				'pages' 			=> $pages,
				'total'				=> $total,
				'limit'				=> $limit,
				'key'					=> 'hal',
				'next_text'		=> '&rsaquo;',
				'prev_text'		=> '&lsaquo;',
				'first_text'	=> '&laquo;',
				'last_text'		=> '&raquo;'
			)
		);
	?>

  <table class="wp-list-table widefat fixed striped users">
    <thead>
      <tr>
        <th scope="col" id="username" class="manage-column column-username column-primary <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=login&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Username</span>
            <span class="sorting-indicator"></span>
          </a>
        </th>
        <th scope="col" id="name" class="manage-column column-name">Name</th>
        <th scope="col" id="pendidikan" class="manage-column column-pendidikan">Education</th>
        <th scope="col" id="sertifikat" class="manage-column column-sertifikat <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=sertifikat&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Sertifikat</span>
            <span class="sorting-indicator"></span>
          </a>
				</th>
        <th scope="col" id="email" class="manage-column column-email <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=email&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Email</span>
            <span class="sorting-indicator"></span>
          </a>
        </th>
        <th scope="col" id="role" class="manage-column column-role">Role</th>
        <th scope="col" id="posts" class="manage-column column-posts num">Posts</th>
      </tr>
    </thead>

    <tbody id="the-list" data-wp-lists="list:user">
			<?php if(!empty($user_e)) { ?>
				<?php foreach($user_e as $u) { ?>
					<?php $user_meta=get_userdata($u->ID); ?>
					<tr id="user-<?php echo $u->ID; ?>">
						<td class="username column-username has-row-actions column-primary" data-colname="Username">
							<?php /*<img alt="" src="http://1.gravatar.com/avatar/14e32abdb4404ab90790a35f33a02b95?s=32&d=mm&r=g" srcset="http://1.gravatar.com/avatar/14e32abdb4404ab90790a35f33a02b95?s=64&d=mm&r=g 2x" class="avatar avatar-32 photo" height="32" width="32">*/ ?>
							<strong>
								<a href="<?php echo admin_url(); ?>user-edit.php?user_id=<?php echo $u->ID; ?>&wp_http_referer=%2Fisnu%2Fwp-admin%2Fusers.php">
									<?php echo $u->user_login; ?>
								</a>
							</strong>
							<br />
							<div class="row-actions">
								<span class="edit"><a href="<?php echo admin_url(); ?>user-edit.php?user_id=<?php echo $u->ID; ?>&wp_http_referer=%2Fisnu%2Fwp-admin%2Fusers.php">Edit</a> | </span>
								<span class="delete"><a class="submitdelete" href="users.php?action=delete&user=<?php echo $u->ID; ?>&_wpnonce=eb32d220f3">Delete</a> | </span>
								<span class="view"><a href="<?php echo home_url(); ?>/author/<?php echo $u->user_login; ?>/" aria-label="View posts by <?php echo $u->display_name; ?>">View</a></span>
							</div>
							<button type="button" class="toggle-row">
								<span class="screen-reader-text">Show more details</span>
							</button>
						</td>
						<td class="name column-name" data-colname="Name"><?php echo $u->display_name; ?></td>
						<td class="pendidikan column-pendidikan" data-colname="Pendidikan Terakhir">
							<?php echo (!empty($u->degree)) ? $u->degree : '-'; ?>
						</td>
						<td class="email column-email" data-colname="Sertifikat">
							<?php
								$check_sertifikat = get_user_meta($u->ID, 'upload_sertifikat');

								if(!empty($check_sertifikat[0])) {
									echo "<span style='background-color:green;display:flex;width:15px;height:15px;border-radius:50%;font-size:1px;color:green;' title='Sudah melampirkan sertifikat'>yes</span>";
								} else {
									echo "<span style='background-color:red;display:flex;width:15px;height:15px;border-radius:50%;font-size:1px;color:red;' title='Belum melampirkan sertifikat'>no</span>";
								}
							?>
						</td>
						<td class="email column-email" data-colname="Email">
							<a href="mailto:<?php echo $u->user_email; ?>"><?php echo $u->user_email; ?></a>
						</td>
						<td class="role column-role" data-colname="Role">
							<?php echo ucwords($user_meta->roles[0]); ?>
						</td>
						<td class="posts column-posts num" data-colname="Posts">
							<?php if(count_user_posts($u->ID) != 0) { ?>
								<a href="<?php echo admin_url(); ?>edit.php?author=<?php echo $u->ID; ?>" class="edit">
									<span aria-hidden="true">
										<?php echo count_user_posts($u->ID); ?>
									</span>
									<span class="screen-reader-text">
										<?php echo count_user_posts($u->ID); ?> posts by this author
									</span>
								</a>
							<?php } else { ?>
								<?php echo count_user_posts($u->ID); ?>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr id="user">
					<td class="username column-username has-row-actions column-primary" data-colname="Username" colspan="6" style="text-align:center;">
						empty
					</td>
				</tr>
			<?php } ?>
    </tbody>

    <tfoot>
      <tr>
        <th scope="col" class="manage-column column-username column-primary <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=login&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'login' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Username</span>
            <span class="sorting-indicator"></span>
          </a>
        </th>
        <th scope="col" class="manage-column column-name">Name</th>
        <th scope="col" class="manage-column column-pendidikan">Education</th>
        <th scope="col" id="sertifikat" class="manage-column column-sertifikat <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=sertifikat&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'sertifikat' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Sertifikat</span>
            <span class="sorting-indicator"></span>
          </a>
				</th>
        <th scope="col" class="manage-column column-email <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order'])) ? 'sorted' : 'sortable'; ?> <?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order'])) ? $_GET['order'] : 'desc'; ?>">
          <a href="<?php echo admin_url(); ?>user-education.php?education=<?php echo $get_education; ?>&orderby=email&order=<?php echo(isset($_GET['orderby']) && $_GET['orderby'] === 'email' && isset($_GET['order']) && $_GET['order'] === 'asc') ? 'desc' : 'asc'; ?>">
            <span>Email</span>
            <span class="sorting-indicator"></span>
          </a>
        </th>
        <th scope="col" class="manage-column column-role">Role</th>
        <th scope="col" class="manage-column column-posts num">Posts</th>
      </tr>
    </tfoot>

  </table>

  <br class="clear" />
	
	<?php
		wp_custom_pagination(
			array(
				'base'				=> admin_url() . 'user-education.php?education=' . $get_education . '&' . $is_sorted,
				'page'				=> $page,
				'pages' 			=> $pages,
				'total'				=> $total,
				'limit'				=> $limit,
				'key'					=> 'hal',
				'next_text'		=> '&rsaquo;',
				'prev_text'		=> '&lsaquo;',
				'first_text'	=> '&laquo;',
				'last_text'		=> '&raquo;'
			)
		);
	?>

</div>

		<?php
		break;

} // end of the $doaction switch

include( ABSPATH . 'wp-admin/admin-footer.php' );
