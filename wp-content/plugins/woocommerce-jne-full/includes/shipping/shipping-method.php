<?php
/**
 * WooCommerce JNE Shipping
 *
 * Main file for the calculation and settings shipping
 *
 * @author AgenWebsite
 * @package WooCommerce JNE Shipping
 * @since 8.0.0
 */

if ( !defined( 'WOOCOMMERCE_JNE' ) ) { exit; } // Exit if accessed directly

if ( !class_exists( 'WC_JNE' ) ) :
    
/**
 * Class WooCommerce JNE
 *
 * @since 8.0.0
 *
 */
class WC_JNE extends WC_Shipping_Method{
            
    /**
     * Option name for save the settings
     *
     * @access private
     * @var string
     * @since 8.0.0
     *
     */
    private $option_layanan;
    
    /**
     * Notices
     *
     * @access private
     * @var array
     * @since 8.1.10
     **/
    private $notice;
    
    /**
     * Constructor
     *
     * @access public
     * @return void
     * @since 8.0.0
     **/
    public function __construct(){
        $this->id                     = 'jne_shipping';
        $this->method_title           = __('JNE Shipping', 'agenwebsite');
        $this->method_description     = __( 'Plugin JNE Shipping mengintegrasikan ongkos kirim dengan total belanja pelanggan Anda.', 'agenwebsite' );
        
        $this->option_layanan         = $this->plugin_id . $this->id . '_layanan';
        $this->option_license_code    = $this->plugin_id . $this->id . '_license_code';
        $this->option_api_location    = $this->plugin_id .'agenwebsite_api_location';

        add_filter( 'woocommerce_settings_api_sanitized_fields_' . $this->id, array( &$this, 'sanitize_fields' ) );
        add_filter( 'woocommerce_settings_api_form_fields_' . $this->id, array( &$this, 'set_form_fields' ) );

        $this->init();        
    }

    /**
     * Init JNE settings
     *
     * @access public
     * @return void
     * @since 8.0.0
     **/
    public function init(){
        // Load the settings API
        // Override the method to add JNE Shipping settings
        $this->form_fields = WC_JNE()->shipping->form_fields();
        // Loads settings you previously init.
        $this->init_settings();
        
        // Load default services options
        $this->load_default_services();

        // Load default settings options
        $this->load_default_settings();
        
        if( get_option( 'woocommerce_jne_shipping_license_code' ) ){
            // Define user set variables
            $this->enabled                    = ( array_key_exists( 'enabled', $this->settings ) ) ? $this->settings['enabled'] : '';
            $this->title                      = ( array_key_exists( 'title', $this->settings ) ) ? $this->settings['title'] : '';
            $this->default_weight             = ( array_key_exists( 'default_weight', $this->settings ) ) ? $this->settings['default_weight'] : '';
            
            // Tools
            $this->is_disable_transient    = ( array_key_exists( 'is_disable_transient', $this->settings ) ) ? $this->settings['is_disable_transient'] : '';            
        }

        // Save settings in admin
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( &$this, 'process_admin_options' ) );
        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( &$this, 'process_admin_jne' ) );
        
    }

    /**
     * Load default JNE services
     *
     * @access private
     * @return void
     * @since 8.0.0
     **/
    private function load_default_services(){
        $servives_options = get_option( $this->option_layanan );
        if( ! $servives_options ) {
        
            $data_to_save = WC_JNE()->shipping->default_service();
            
            update_option( $this->option_layanan, $data_to_save );
        }
    }

    /**
     * Load default settings fields
     *
     * @access private
     * @return void
     * @since 8.1.15
     **/
    private function load_default_settings(){
        $option_name = 'woocommerce_jne_shipping_settings';
        $options = get_option($option_name);
        $license_code = get_option('woocommerce_jne_shipping_license_code');
        $form_fields = WC_JNE()->shipping->get_form_fields();
        if( count( $options ) < 2 && $license_code != ''){
            $defaults = array();
            foreach($form_fields as $name => $data){
                foreach($data['fields'] as $name_fields => $value_fields){
                    $defaults[$name_fields] = $value_fields['default'];
                }
            }
            update_option($option_name, $defaults);
        }
    }
    
    /**
     * calculate_shipping function.
     *
     * @access public
     * @param mixed $package
     * @return void
     * @since 8.0.0
     **/
    public function calculate_shipping( $package = array() ){

        if( ! $this->enabled ) return false;

        $layanan_jne = get_option( $this->option_layanan );

        $country= WC()->customer->get_shipping_country();
        $state    = WC()->customer->get_shipping_state();
        $city    = WC()->customer->get_shipping_city();

        if( $country != 'ID' ) return false;

        $total_weight = WC_JNE()->shipping->calculate_weight( $package['contents'] );
        $jne_weight = WC_JNE()->shipping->calculate_jne_weight( $total_weight );
        $weight = $jne_weight;

        $cost = $this->_get_costs( $state, $city, $weight );

        $totalamount = floatval( preg_replace( '#[^\d]#', '', WC()->cart->get_cart_total() ) );

        if( empty( $cost ) ) return false;

        if( sizeof( $package ) == 0 ) return;

        foreach( $layanan_jne as $service ){
            $service_id = $service['id'];
            $service_name = $service['name'];
            $service_enable = $service['enable'];
            $service_extra_cost = $service['extra_cost'];
            $label = $this->title .' '. $service_name .' ';

            if( array_key_exists( $service_id, $cost) ){
                $etd = $cost[$service_id]['etd'];
                $tarif = $cost[$service_id]['harga'];
            }else{
                $tarif = 0;
            }
            
            if( ! empty($tarif) && $tarif != 0 && $service_enable == 1) {
                
                $tarif = $tarif * $weight;
                
                $label = $this->set_label( $label, $etd, $service_id );

                if( ! empty( $service_extra_cost ) ) $tarif += $service_extra_cost;

                // Hook filter for another plugin
                $rate = apply_filters( 'woocommerce_jne_tarif', array(
                    'id'    => $this->id . '_' . $service_id,
                    'label'    => $label,
                    'cost'    => $tarif
                ) );

                $this->add_rate( $rate );

            }

        }//end foreach layanan

    }
        
    /**
     * Calculate Total Weight
     * This function will calculated total weight for all product
     *
     * @access private
     * @param mixed $products
     * @return integer Total Weight in Kilograms
     * @since 8.0.0
     * @deprecated 8.0.17 No Longer used by internal code, only backup codes.
     **/
    private function calculate_weight( $products ){
        $options = get_option('woocommerce_jne_shipping_settings');
        $weight = 0;
        $weight_unit = WC_JNE()->get_woocommerce_weight_unit();
        $default_weight = $this->default_weight;

        // Default weight JNE settings is Kilogram
        // Change default weight settings to gram if woocommerce unit is gram
        if( $weight_unit == 'g' )
            $default_weight = $default_weight * 1000;

        foreach( $products as $item_id => $item ){
            $product = $item['data'];

            if( $product->is_downloadable() == false && $product->is_virtual() == false ) {
                $product_weight = $product->get_weight() ? $product->get_weight() : $default_weight;
                $product_weight = ( $product_weight == 0 ) ? $default_weight : $product_weight;

                /*
                 * Volumetrik
                 */
                $is_volumetrik_enable = ( $options['global_volumetrix'] ? $options['global_volumetrix'] : get_post_meta( $item['product_id'], '_is_jne_volumetrik_enable', TRUE ) );
                if( $product->length != '' && $product->width != '' && $product->height != '' ){
                    if( $is_volumetrik_enable == 'yes' ){
                        $unit = get_option('woocommerce_dimension_unit');
                        $length = $product->length;
                        $width = $product->width;
                        $height = $product->height;

                        if( $unit == 'mm' ) {
                            $length = $length / 10;
                            $width = $width / 10;
                            $height = $height / 10;
                        }

                        $volum_weight = $length * $width * $height / 6000;
                        if( $volum_weight > $product_weight ){
                            $product_weight = $volum_weight;
                        }
                    }
                }

                $product_weight = $product_weight * $item['quantity'];

                // Change product weight to kilograms
                if ($weight_unit == 'g')
                    $product_weight = $product_weight / 1000;

                $weight += $product_weight;

            }
        }

        $weight = number_format((float)$weight, 2, '.', '');
        $weight = ( $weight <= 0 ? 1 : $weight ); 

        return $weight;
    }

    /**
     * Set Label
     *
     * @access private
     * @param string $label
     * @param string $etd
     * @param string $service_id
     * @return string
     * @since 8.1.10
     */
    private function set_label( $label, $etd, $service_id ){
        $etd = sprintf( __( ' ( %s hari )', 'agenwebsite' ), $etd );
        
        $new_label = sprintf( '%s%s', $label, $etd );

        return apply_filters( 'woocommerce_jne_tarif_label', $new_label );
    }
    
    /**
     * Sanitize Fields
     *
     * @access public
     * @param array $sanitize_fields
     * @return array $new_sanitize_fields
     * @since 8.1.10
     */
    public function sanitize_fields( $sanitize_fields ){
        /*
          replace option settings with sanitize fields
         */
        $sanitize_fields = ( is_array( $sanitize_fields) ? array_replace( $this->settings, $sanitize_fields ) : '' );

        $new_sanitize_fields = $sanitize_fields;
        $options = get_option( $this->plugin_id . $this->id .'_settings' );
        $options_backup = get_option( $this->plugin_id . $this->id . '_settings_backup' );

        /*
        * jika license code kosong maka kosongkan post sanitize
        * dan lakukan update option ke settings backup
        * settings backup berfungsi untuk mengembalikan option ke option utama
        * jika license code ada dan option utama kosong maka sanitize field diisi dengan option settings backup
        */
        if( empty( $sanitize_fields['license_code'] ) ){
            $new_sanitize_fields = '';
            if( is_array( $options ) && ! empty( $options ) ){
                update_option( $this->plugin_id . $this->id . '_settings_backup', $options );
            }
        }else{
            if( $options_backup ){
                if( ! $options || empty( $options ) ){
                    $new_sanitize_fields = $options_backup;
                }
            }
        }

        return $new_sanitize_fields;
    }

    /**
     * Set form fields
     * before show fields, check license code exists or not
     *
     * @access public
     * @param array $sanitize_fields
     * @return array $new_sanitize_fields
     * @since 8.1.10
     */
    public function set_form_fields( $form_fields ){

        if( get_option( $this->option_license_code ) ){

            $current_tab = empty( $_GET['tab_jne'] ) ? 'general' : sanitize_title( $_GET['tab_jne'] );

            $form_field = WC_JNE()->shipping->get_form_fields();
                foreach( $form_field as $name => $data ){
                    if( $name == $current_tab ){
                        $form_fields = $data['fields'];
                    }
                }
        }
        return $form_fields;
    }

    /**
     * Settings Tab
     *
     * @access private 
     * @return HTML
     * @since 8.1.10
     */
    private function settings_tab(){

        $tabs = array();
        $section = WC_JNE()->get_section();

        if( get_option( $this->option_license_code ) ){

            foreach( WC_JNE()->shipping->get_form_fields() as $name => $data ){
                $tab[$name] = $data['label'];
                $tabs = array_merge( $tabs, $tab );
            }

        }

        $current_tab = empty( $_GET['tab_jne'] ) ? 'general' : sanitize_title( $_GET['tab_jne'] );

        $tab  = '<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">';

        foreach( $tabs as $name => $label ){
            $tab .= '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=shipping&section='. $section .'&tab_jne=' . $name ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . $label . '</a>';
        }

        $tab .= '</h2>';

        return $tab;
    }
    
    /**
     * Validate license code
     * check license code to api
     *
     * @access public
     * @param array $sanitize_fields
     * @return array $new_sanitize_fields
     * @since 8.1.10
     */
    public function validate_license_code_field( $key ){
        $text = $this->get_option( $key );
        $field = $this->get_field_key( $key );
        
        if( isset( $_POST[ $field ] ) ){
            $text = wp_kses_post( trim( stripslashes( $_POST[ $field ] ) ) ); 
            $api_location = wp_kses_post( trim( stripslashes( $_POST[$this->option_api_location] ) ) );
            $force = ( isset( $_POST['reactive'] ) && !empty( $_POST['reactive'] ) ? true : false );
            $args = array(
                'license_code' => $text,
                'api_location' => $api_location,
                'force' => $force
            );
            $valid_license = $this->validate_license_code( $args );
        
            update_option( $this->option_license_code, $valid_license );
            update_option( $this->option_api_location, $api_location );
        }
        
        return $valid_license;
    }

    /**
     * Process admin JNE shipping
     *
     * @access public
     * @return void
     * @since 8.0.0
     **/
    public function process_admin_jne(){
        $service_id = ( $_POST && isset( $_POST['service_id'] ) ) ? $_POST['service_id'] : '';
        $service_name = ( $_POST && isset( $_POST['service_name'] ) ) ? $_POST['service_name'] : '';
        $service_extra_cost = ( $_POST && isset( $_POST['service_extra_cost'] ) ) ? $_POST['service_extra_cost'] : '';
        $service_enable = ( $_POST && isset( $_POST['service_enable'] ) ) ? $_POST['service_enable'] : '';
        
        // Save order, enable, services field
        $save_layanan = array();
        if( ! empty( $service_id ) && is_array( $service_id ) ){
            foreach( $service_id as $i => $name ){
                if ( ! isset( $service_name[ $i ] ) ) {
                    continue;
                }
                if( ! isset( $service_enable[ $i ][ $name ] ) )
                    $service_enable[ $i ][ $name ] = 'no';    

                $save_layanan[] = array(
                    'id'        => $name,
                    'enable'    => $service_enable[ $i ][ $name ],
                    'name'        => $service_name[ $i ][ $name ],
                    'extra_cost'=> $service_extra_cost[ $i ][ $name ],
                );
            }

            // Update the option setting for layanan
            update_option( $this->option_layanan, $save_layanan );

        }

        if( $_POST && wp_verify_nonce( $_REQUEST['_wpnonce'], 'woocommerce-settings' ) ){
            // clear transients
            if( isset( $_POST['jne_shipping_transient'] ) ){
                $this->clear_jne_transient();
            }

            // clear all expired transients
            if( isset( $_POST['clear_expired_transients'] ) ){
                $this->clear_all_expired_transient();
            }

        }

    }

    /**
     * Clear JNE Transient
     * Will clear jne transient
     * 
     * @access private
     * @return HTML notice
     * @since 8.1.10
     */
    private function clear_jne_transient(){
        global $wpdb;
        $sql = "DELETE FROM $wpdb->options WHERE option_name LIKE %s";
        $result = $wpdb->query( $wpdb->prepare( $sql, $wpdb->esc_like( '_transient_wc_ship_jne' ) . '%' ) );
        $wpdb->query( $wpdb->prepare( $sql, $wpdb->esc_like( '_transient_timeout_wc_ship_jne' ) . '%' ) );
        echo '<div class="updated"><p><b>' . sprintf( __( '%d Transients Rows Cleared', 'agenwebsite' ), $result ) . '</b></p></div>';        
    }

    /**
     * Clear all expired transient
     * 
     * @access private
     * @return HTML notice
     * @since 8.1.03
     */
    private function clear_all_expired_transient(){
        global $wpdb;
        $sql = "DELETE a, b FROM $wpdb->options a, $wpdb->options b
        WHERE a.option_name LIKE %s
        AND a.option_name NOT LIKE %s
        AND b.option_name = CONCAT( '_transient_timeout_', SUBSTRING( a.option_name, 12 ) )
        AND b.option_value < %d";
        $rows = $wpdb->query( $wpdb->prepare( $sql, $wpdb->esc_like( '_transient_' ) . '%', $wpdb->esc_like( '_transient_timeout_' ) . '%', time() ) );

        $sql = "DELETE a, b FROM $wpdb->options a, $wpdb->options b
        WHERE a.option_name LIKE %s
        AND a.option_name NOT LIKE %s
        AND b.option_name = CONCAT( '_site_transient_timeout_', SUBSTRING( a.option_name, 17 ) )
        AND b.option_value < %d";
        $rows2 = $wpdb->query( $wpdb->prepare( $sql, $wpdb->esc_like( '_site_transient_' ) . '%', $wpdb->esc_like( '_site_transient_timeout_' ) . '%', time() ) );

        echo '<div class="updated"><p><b>' . sprintf( __( '%d Transients Rows Cleared', 'woocommerce' ), $rows + $rows2 ) . '</b></p></div>';        
    }

    /**
     * Reset option to default
     * Fungsi untuk tombol reset option pada halaman setting jne shipping
     *
     * @access private
     * @return array
     * @since 8.0.0
     **/
    private function _reset_option(){                
        $jne_settings = array();
        
        foreach( WC_JNE()->shipping->get_form_fields() as $line ){
            foreach( $line['fields'] as $key => $value ) {
                $jne_settings[$key] = ( !empty( $value['default'] ) ? $value['default'] : '' );
            }
        }
                
        $data['save_layanan'] = WC_JNE()->shipping->default_service();
        $data['save_settings'] = $jne_settings;
        return $data;
    }

    /**
     * Validate license code
     * Fungsi untuk memvalidasi kode lisensi
     *
     * @access private
     * @return array
     * @since 8.0.0
     **/
    private function validate_license_code( $args ){
        $code = $args['license_code'];
        $api_location = $args['api_location'];
        $force = $args['force'];
        $saved_license = get_option( $this->option_license_code );
        
        if( empty( $code ) || $saved_license == $code && $force == false ) return $code;
        
        WC_JNE()->api->license_code = $code;
        WC_JNE()->api->api_location = $api_location;
        
        $response = WC_JNE()->api->remote_put( 'license' );

        $this->notice['type'] = $response['status'];
        $this->notice['message'] = ( $response['status'] == 'success' ) ? $response['result']['message'] : $response['message'];

        if( $response['status'] == 'error' ){
            $code = '';
        }

        add_action( 'jne_admin_notices', array( &$this, 'notice' ) );

        return $code;
    }
            
    /**
     * JNE cost
     * Mendapatkan harga dari api
     *
     * @access private
     * @param string $state
     * @param string $city
     * @return array
     * @since 8.0.0
     **/
    private function _get_costs( $state, $city, $weight ){

        $tarif = '';

        if($this->is_disable_transient == 'yes'){
            // explode field city to kecamatan and kota
            // pattern is {kecamatan}, {kota}
            $explode_field_city = explode(', ', $city);
            if( count( $explode_field_city ) < 2 ) return false;

            $params = array(
                'provinsi'  => $this->_get_provinsi_name( $state ),
                'kecamatan' => $explode_field_city[0],
                'kota'      => $explode_field_city[1],
                'berat'     => $weight
            );

            // get data from API
            $response = WC_JNE()->api->remote_get( 'tarif', $params );

            // validate response status
            if( $response['status'] != 'error' ){

                $tarif = $response['result']['tarif'];

            }

        }

        if( ! empty( $city ) && $this->is_disable_transient == 'no' ){

            // Check if we need to recalculate shipping for this city
            $city_hash = 'wc_ship_jne_' . md5( $city ) . WC_Cache_Helper::get_transient_version( 'jne_shipping' );
            $stored_tarif = get_transient( $city_hash );

            if( false === $stored_tarif || $stored_tarif == '' || $stored_tarif == 0 ){

                // explode field city to kecamatan and kota
                // pattern is {kecamatan}, {kota}
                $explode_field_city = explode(', ', $city);

                $params = array(
                    'provinsi'  => $this->_get_provinsi_name( $state ),
                    'kecamatan' => $explode_field_city[0],
                    'kota'      => $explode_field_city[1],
                    'berat'     => $weight
                );

                // get data from API
                $response = WC_JNE()->api->remote_get( 'tarif', $params );

                // validate response status
                if( $response['status'] != 'error' ){

                    $tarif = $response['result']['tarif'];

                    // Store
                    if( !empty( $tarif ) )
                        WC_JNE()->set_transient( $city_hash, $tarif );

                }

            }else{

                $tarif = $stored_tarif;

            }

        }

        return $tarif;

    }
    
    /**
     * Get nama provinsi Indonesia
     * Mendapatkan nama provinsi berdasarkan id provinsi dari woocommerce
     *
     * @access private
     * @param string $id of provinsi
     * @return string
     * @since 8.0.0
     **/
    private function _get_provinsi_name( $id ){        
        $provinsi = '';
        $states = WC()->countries->get_states( 'ID' );
        
        foreach( $states as $id_provinsi => $nama_provinsi ){
            if( $id_provinsi == $id ){
                $provinsi = $nama_provinsi;
            }
        }
                    
        return $provinsi;
    }

    /**
     * Notice
     *
     * @access private 
     * @return HTML
     * @since 8.1.10
     */
    public function notice(){
        $type = ( $this->notice['type'] == 'error' ) ? 'error' : 'updated';
        echo '<div class="' . $type . '"><p><strong>' . $this->notice['message'] . '</strong></p></div>';
    }

    /**
     * Admin Options
     * Setup the gateway settings screen.
     *
     * @access public
     * @return HTML of the admin jne settings
     * @since 8.0.0
     */
    public function admin_options() {
        $class = empty( $_GET['tab_jne'] ) ? 'general' : sanitize_title( $_GET['tab_jne'] );

        $html  = '<div id="agenwebsite_woocommerce" class="' . $class . '">' . "\n";

            // AW head logo and links and table status
            ob_start();
            $this->aw_head();
            $html .= ob_get_clean();
    
            $html .= sprintf( '<h3>%s %s</h3>', $this->method_title, __( 'Settings', 'agenwebsite' ) ) . "\n";
            $html .= '<p>' . $this->method_description . '</p>' . "\n";            
            
            $html .= $this->settings_tab();
            
            $html .= '<div id="agenwebsite_notif">';
            ob_start();
            do_action( 'jne_admin_notices' );
            $html .= ob_get_clean();
            $html .= '</div>';
            
            $html .= '<table class="form-table hide-data">' . "\n";
    
                ob_start();
                $this->generate_settings_html();
                $html .= ob_get_clean();
    
            $html .= '</table>' . "\n";
            
        $html .= '</div>' . "\n";
        
        echo $html;
    }
    
    /**
     * AgenWebsite Head
     *
     * @access private static
     * @return HTML for the admin logo branding and usefull links.
     * @since 8.0.0
    */
    private function aw_head(){            
        $html  = '<div class="agenwebsite_head">';
        $html .= '<div class="logo">' . "\n";
        $html .= '<a href="' . esc_url( 'http://www.agenwebsite.com/' ) . '" target="_blank"><img id="logo" src="' . esc_url( apply_filters( 'aw_logo', WC_JNE()->plugin_url() . '/assets/images/logo.png' ) ) . '" /></a>' . "\n";
        $html .= '</div>' . "\n";
        $html .= '<ul class="useful-links">' . "\n";
            $html .= '<li class="documentation"><a href="' . esc_url( WC_JNE()->url_dokumen ) . '" target="_blank">' . __( 'Dokumentasi', 'agenwebsite' ) . '</a></li>' . "\n";
            $html .= '<li class="support"><a href="' . esc_url( WC_JNE()->url_support ) . '" target="_blank">' . __( 'Bantuan', 'agenwebsite' ) . '</a></li>' . "\n";
        $html .= '</ul>' . "\n";
        
        if( WC_JNE()->get_license_code() != '' ){
            ob_start();
                include_once( WC_JNE()->plugin_path() . '/views/html-admin-jne-settings-status.php' );
            $html .= ob_get_clean();
        }
            
        $html .= '</div>';
        echo $html;
    }

    /**
     * Field type button
     *
     * @access public
     * @return HTML
     * @since 8.1.10
     **/
    public function generate_button_html( $key, $data ){

        $field = $this->get_field_key( $key );
        $defaults = array(
            'title'             => '',
            'disabled'          => false,
            'class'             => '',
            'css'               => '',
            'placeholder'       => '',
            'desc_tip'          => false,
            'description'       => '',
            'custom_attributes' => array()
        );

        $data = wp_parse_args( $data, $defaults );

        ob_start();
        ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="<?php esc_attr( $field );?>"><?php echo wp_kses_post( $data['label'] );?></label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] );?></span></legend>
                        <button type="submit" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $field ); ?>" class="button <?php echo esc_attr( $data['class'] );?>" style="<?php echo esc_attr( $data['css'] ); ?>" <?php echo $this->get_custom_attribute_html( $data );?>><?php echo wp_kses_post( $data['placeholder'] );?></button>
                        <?php echo $this->get_description_html( $data ); ?>
                    </fieldset>
                </td>
            </tr>
        <?php
        return ob_get_clean();
    }

    /**
     * Field type license_code
     *
     * @access public
     * @return HTML
     * @since 8.1.10
     **/
    public function generate_license_code_html(){
        $license_code = get_option( $this->option_license_code );
        $api_location = get_option( $this->option_api_location );

        $html = '';
        if( ! $license_code && empty( $license_code ) ){
            $html .= sprintf('<div class="notice_wc_jne woocommerce-jne"><p><b>%s</b> &#8211; %s</p><p class="submit">%s %s</p></div>',
                __( 'Masukkan kode lisensi untuk mengaktifkan WooCommerce JNE', 'agenwebsite' ),
                __( 'anda bisa mendapatkan kode lisensi dari halaman akun AgenWebsite.', 'agenwebsite'  ),
                '<a href="http://www.agenwebsite.com/account" target="new" class="button-primary">' . __( 'Dapatkan kode lisensi', 'agenwebsite' ) . '</a>',
                '<a href="' . esc_url( WC_JNE()->url_dokumen ) . '" class="button-primary" target="new">' . __( 'Baca dokumentasi', 'agenwebsite' ) . '</a>' );
        }

        $html .= '<tr valid="top">';
            $html .= '<th scope="row" class="titledesc">';
                $html .= '<label for="' . $this->option_license_code . '">' . __( 'Kode Lisensi', 'agenwebsite' ) . '</label>';
            $html .= '</th>';
            $html .= '<td class="forminp">';
                $html .= '<fieldset>';
                    $html .= '<legend class="screen-reader-text"><span>' . __( 'Kode Lisensi', 'agenwebsite' ) . '</span></legend>';
                    $html .= '<input class="input-text regular-input " type="text" name="' . $this->option_license_code . '" id="' . $this->option_license_code . '" style="" value="' . esc_attr( get_option( $this->option_license_code ) ) . '" placeholder="' . __( 'Kode Lisensi', 'agenwebsite' ) . '">';
                    $html .= '<p class="description">' . __( 'Masukkan kode lisensi yang kamu dapatkan dari halaman akun agenwebsite.', 'agenwebsite' ) . '</p>';
                $html .= '</fieldset>';
            $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr valid="top">';
            $html .= '<th scope="row" class="titledesc">';
                $html .= '<label for="' . $this->option_api_location . '">' . __( 'Lokasi API', 'agenwebsite' ) . '</label>';
            $html .= '</th>';
            $html .= '<td class="forminp">';
                $html .= '<fieldset>';
                    $html .= '<legend class="screen-reader-text"><span>' . __( 'Lokasi API', 'agenwebsite' ) . '</span></legend>';
                    $html .= '<select name="'. $this->option_api_location.'" id="'. $this->option_api_location.'" class="wc-enhanced-select" style="min-width:300px;">';
                        $html .= '<option value="international" '. selected( $api_location, 'international', false ) .'>International</option>';
                        $html .= '<option value="indonesia" '. selected( $api_location, 'indonesia', false ) .'>Indonesia</option>';
                    $html .= '</select>';
                    $html .= '<p class="description">' . __( 'Pilih berdasarkan lokasi web server Anda.', 'agenwebsite' ) . '</p>';
                $html .= '</fieldset>';
            $html .= '</td>';
        $html .= '</tr>';

        return $html;
    }
                    
    /**
     * Field type jne_service
     *
     * @access public
     * @return HTML
     * @since 8.0.0
     **/
    public function generate_jne_service_html(){
        $html = '<tr valign="top">';
            $html .= '<th scope="row" class="titledesc">' . __( 'Layanan JNE', 'agenwebsite' ) . '</th>';
            $html .= '<td class="forminp">';
                $html .= '<table class="widefat wc_input_table sortable" cellspacing="0">';
                    $html .= '<thead>';
                        $html .= '<tr>';
                            $html .= '<th class="sort">&nbsp;</th>';
                            $html .= '<th>Nama Pengiriman ' . WC_JNE()->help_tip( 'Metode pengiriman yang digunakan.' ) . '</th>';
                            $html .= '<th>Tambahan Biaya ' . WC_JNE()->help_tip( 'Biaya tambahan, bisa disetting untuk tambahan biaya packing dan lain-lain.' ) . '</th>';
                            $html .= '<th style="width:14%;text-align:center;">Aktifkan</th>';
                        $html .= '</tr>';
                    $html .= '</thead>';
                    $html .= '<tbody>';
                        
                        $i = 0;
                        foreach( get_option( $this->option_layanan ) as $service ) :
                        
                            $html .= '<tr class="service">';
                                $html .= '<td class="sort"></td>';
                                $html .= '<td><input type="text" value="' . $service['name'] . '" name="service_name[' . $i . '][' . $service['id'] . ']" /></td>';
                                $html .= '<td><input type="number" value="' . $service['extra_cost'] . '" name="service_extra_cost[' . $i . '][' . $service['id'] . ']" /></td>';
                                $html .= '<td style="text-align:center;"><input type="checkbox" value="1" ' . checked( $service['enable'], 1, FALSE ) . ' name="service_enable[' . $i . '][' . $service['id'] . ']" /><input type="hidden" value="' . $service['id'] . '" name="service_id[' . $i . ']" /></td>';
                            $html .= '</tr>';
                                    
                            $i++;
                        endforeach;
                        
                    $html .= '</tbody>';
                $html .= '</table>';
            $html .= '</td>';
        $html .= '</tr>';
        
        return $html;
    }
        
}
    
endif;