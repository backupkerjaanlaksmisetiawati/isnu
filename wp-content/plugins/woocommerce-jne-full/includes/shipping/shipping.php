<?php
/**
 * WooCommerce JNE Shipping
 *
 * Main file for the calculation and settings shipping
 *
 * @author AgenWebsite
 * @package WooCommerce JNE Shipping
 * @since 8.0.0
 */

if ( !defined( 'WOOCOMMERCE_JNE' ) ) { exit; } // Exit if accessed directly

if ( !class_exists( 'WC_JNE_Shipping' ) ) :

/**
 * Class WooCommerce JNE
 *
 * @since 8.0.0
 **/
class WC_JNE_Shipping{

    /**
     * Constructor
     *
     * @return void
     * @since 8.0.0
     **/
    public function __construct(){
        /**
         * Initialise JNE shipping method.
         *
         * @since 8.0.0
         *
         */
        add_action( 'woocommerce_shipping_init', array( &$this, 'shipping_method' ) );

        /**
         * Add Shipping Method
         *
         * Tell method JNE shipping to woocommerce. Hey Woo AgenWebsite JNE is Here !! :D
         *
         * @since 8.0.0
         *
         */
        add_filter( 'woocommerce_shipping_methods', array( &$this, 'add_jne_shipping_method' ) );

        // filter default chosen shipping
        add_filter( 'woocommerce_shipping_chosen_method', array( &$this, 'get_default_method' ), 10, 2 );

        //Release the frontend
        if( $this->is_enable() ) new WC_JNE_Frontend();

        // Release volumetrik
        new WC_JNE_Volumetrik();
    }

    /**
     * Init Shipping method
     *
     * @access public
     * @return void
     * @since 8.0.0
     *
     */
    public function shipping_method(){
        include_once( 'shipping-method.php' );
    }

    /**
     * Add JNE shipping method
     *
     * @access public
     * @return void
     * @since 8.0.0
     *
     */
    public function add_jne_shipping_method( $methods ) {
        $methods[] = 'WC_JNE';
        return $methods;
    }

    /**
     * Check plugin is active
     *
     * @access public
     * @return bool
     * @since 8.0.0
     *
     */
    public function is_enable(){
        $settings = get_option( 'woocommerce_jne_shipping_settings' );
        if( $settings && is_array( $settings ) ){
            if( ! array_key_exists( 'enabled', $settings ) ) return false;

            return ( $settings['enabled'] == 'yes' ) ? TRUE : FALSE;
        }

        return false;
    }

    /**
     * Get the default method
     * @param  array  $available_methods
     * @param  boolean $current_chosen_method
     * @return string
     * @since 8.1.02
     */
    public function get_default_method( $default_method, $available_methods ) {
        $selection_priority = get_option( 'woocommerce_shipping_method_selection_priority', array() );

        if ( ! empty( $available_methods ) ) {

            // Is a method already chosen?
            if ( ! empty( $current_chosen_method ) && ! isset( $available_methods[ $current_chosen_method ] ) ) {
                foreach ( $available_methods as $method_id => $method ) {
                    if ( strpos( $method->id, $current_chosen_method ) === 0 ) {
                        return $method->id;
                    }
                }
            }

            // Order by priorities and costs
            $prioritized_methods = array();

            foreach ( $available_methods as $method_id => $method ) {
                $priority                         = isset( $selection_priority[ $method_id ] ) ? absint( $selection_priority[ $method_id ] ) : 1;
                if ( empty( $prioritized_methods[ $priority ] ) ) {
                    $prioritized_methods[ $priority ] = array();
                }
                $prioritized_methods[ $priority ][ $method_id ] = $method->cost;
            }

            $prioritized_methods = current( $prioritized_methods );

            return current( array_keys( $prioritized_methods ) );
        }

        return false;
    }

    /**
     * Change to kilograms
     *
     * @access public
     * @return string
     * @since 8.1.0
     *
     */
    public function change_to_kg(){
        return 'kg';
    }

    /**
     * Is Decimal
     * For check the number is decimal.
     *
     * @access public
     * @param integer
     * @return bool
     * @since 8.0.0
     *
     */
    private static function is_decimal( $num ){
        return is_numeric( $num ) && floor( $num ) != $num;
    }

    /**
     * Return the number of decimals after the decimal point.
     *
     * @access public
     * @return int
     * @since 8.0.1
     *
     */
    public function get_price_decimals(){
        if( function_exists( 'wc_get_price_decimals' ) )
            return wc_get_price_decimals();
        else
            return absint( get_option( 'woocommerce_price_num_decimals', 2 ) );
    }

    /**
     * Calculate JNE Weight
     * To calculate weight tolerance from jne.
     *
     * @link http://jne.co.id/share_article.php?id=2013020404274222
     * @access public
     * @param integer $weight
     * @return integer Total Weight in Kilograms
     * @since 8.0.0
     *
     */
    public function calculate_jne_weight( $weight ){
        if( WC_JNE_Shipping::is_decimal( $weight ) ){
            $desimal = explode( '.', $weight );
            $jne_weight = ( $desimal[0] == 0 || substr($desimal[1], 0, 1) > 3 || substr($desimal[1], 0, 2) > 30) ? ceil($weight) : floor($weight);
            $weight = ( $jne_weight == 0 ) ? 1 : $jne_weight;
        }

        return $weight;
    }

    /**
     * Calculate Total Weight
     * This function will calculated total weight for all product
     *
     * @access public
     * @param mixed $products
     * @return mixed Total Weight in Kilograms
     * @since 8.1.17
     **/
    public function calculate_weight( $products = NULL ){
        $options = get_option('woocommerce_jne_shipping_settings');
        $output = array();
        $return = 'int';
        $weight = 0;
        $weight_unit = WC_JNE()->get_woocommerce_weight_unit();
        $settings = get_option( 'woocommerce_jne_shipping_settings' );

        if(is_null($products)){
            $return = 'array';
            $default_weight = $settings['default_weight'];
            $products = WC()->cart->get_cart();
        }else{
            $default_weight = $settings['default_weight'];
        }

        // Default weight JNE settings is Kilogram
        // Change default weight settings to gram if woocommerce unit is gram
        if( $weight_unit == 'g' )
            $default_weight = $default_weight * 1000;

        foreach( $products as $item_id => $item ){
            $product = $item['data'];

            if( $product->is_downloadable() == false && $product->is_virtual() == false ) {
                $product_weight = $product->get_weight() ? $product->get_weight() : $default_weight;
                $product_weight = ( $product_weight == 0 ) ? $default_weight : $product_weight;

                /*
                 * Volumetrik
                 */
                $is_volumetrik_enable = ( $options['global_volumetrix'] ? $options['global_volumetrix'] : get_post_meta( $item['product_id'], '_is_jne_volumetrik_enable', TRUE ) );
                if( $product->get_length() != '' && $product->get_width() != '' && $product->get_height() != '' ){
                    if( $is_volumetrik_enable == 'yes' ){
                        $unit = get_option('woocommerce_dimension_unit');
                        $length = $product->get_length();
                        $width = $product->get_width();
                        $height = $product->get_height();

                        if( $unit == 'mm' ) {
                            $length = $length / 10;
                            $width = $width / 10;
                            $height = $height / 10;
                        }

                        $volum_weight = $length * $width * $height / 6000;
                        if( $volum_weight > $product_weight ){
                            $product_weight = $volum_weight;
                        }
                    }
                }

                $product_weight = $product_weight * $item['quantity'];

                // Change product weight to kilograms
                if ($weight_unit == 'g')
                    $product_weight = $product_weight / 1000;

                $weight += $product_weight;

                $output['volumetrik'] = 'yes';
            }
        }

        $weight = number_format((float)$weight, 0, '.', '');
        $weight = ( $weight <= 0 ? 1 : $weight );

        $output['weight'] = $weight;
        return ($return == 'array') ? $output : $weight;
    }

    /**
     * Get total weight
     *
     * @access public
     * @return integer Total weight
     * @since 8.0.0
     * @deprecated 8.0.17 No Longer used by internal code, only backup codes.
     *
     */
    public function get_total_weight_checkout(){
        $settings = get_option( 'woocommerce_jne_shipping_settings' );
        $default_weight = $settings['default_weight'];
        $weight = 0;
        $weight_unit = WC_JNE()->get_woocommerce_weight_unit();

        foreach ( WC()->cart->cart_contents as $cart_item_key => $values ) {
            $_product = $values['data'];
            if( $_product->is_downloadable() == false && $_product->is_virtual() == false ) {
                $_product_weight = $_product->get_weight();

                if( $_product_weight == '' ){
                    if( $weight_unit == 'g' ){
                        $default_weight *= 1000;
                    }
                    $_product_weight = $default_weight;
                }

                $weight += $_product_weight * $values['quantity'];

                $output['virtual'] = 'yes';
            }
        }

        if( $weight_unit == 'g' ){
            if( $weight > 1000 ){
                $weight = $weight / 1000;
                $weight = number_format((float)$weight, 2, '.', '');
                add_filter( 'weight_unit_total_weight', array( &$this, 'change_to_kg' ) );
            }
        }

        $output['weight'] = $weight;

        return $output;
    }

    /**
     * Shipping service option default
     *
     * @access public
     * @return array
     * @since 8.0.0
     *
     */
    public function default_service(){
        return array(
            array(
                'id'        => 'oke',
                'enable'    => 1,
                'name'      => 'OKE',
                'extra_cost'=> 0
            ),
            array(
                'id'        => 'reg',
                'enable'    => 1,
                'name'      => 'REG',
                'extra_cost'=> 0
            ),
            array(
                'id'        => 'yes',
                'enable'    => 1,
                'name'      => 'YES',
                'extra_cost'=> 0
            )
        );
    }

    /**
     * Shipping form fields settings
     *
     * @access public
     * @return array
     * @since 8.0.0
     *
     */
    public function form_fields(){
        $form_fields = array(
            'license_code'  => array(
                'type'          => 'license_code',
                'default'       => '',
            )
        );

        return apply_filters( 'woocommerce_jne_form_fields_settings', $form_fields );
    }

    /**
     * Shipping form fields settings
     *
     * @access public
     * @return array
     * @since 8.0.0
     *
     */
    public function get_form_fields(){
        $form_fields = array(
            'general'   => array(
                'label' => __( 'General', 'agenwebsite' ),
                'fields'    => array(
                    'enabled' => array(
                        'title'         => __( 'Aktifkan JNE Shipping', 'agenwebsite' ),
                        'type'          => 'checkbox',
                        'label'         => __( 'Aktifkan WooCommerce JNE Shipping', 'agenwebsite' ),
                        'default'       => 'no',
                    ),
                    'title' => array(
                        'title'         => __( 'Label', 'agenwebsite' ),
                        'description'     => __( 'Ubah label untuk fitur pengiriman kamu.', 'agenwebsite' ),
                        'type'          => 'text',
                        'default'       => __( 'JNE Shipping', 'agenwebsite' ),
                    ),
                    'default_weight' => array(
                        'title'         => __( 'Berat default ( kg )', 'agenwebsite' ),
                        'description'     => __( 'Otomatis setting berat produk jika kamu tidak setting pada masing-masing produk.', 'agenwebsite' ),
                        'type'          => 'number',
                        'custom_attributes' => array(
                            'step'    =>    'any',
                            'min'    => '0'
                        ),
                        'placeholder'    => '0.00',
                        'default'        => '1',
                    ),
                    'license_code'  => array(
                        'type'          => 'license_code',
                        'default'       => '',
                    ),
                    'enabler_total_weight' => array(
                        'title'         => __( 'Aktifkan Total Weight', 'agenwebsite'),
                        'type'          => 'checkbox',
                        'label'         => __( 'Aktifkan Total Weight di Checkout Table', 'agenwebsite' ),
                        'default'       => 'yes'
                    ),
                    'global_volumetrix' => array(
                        'title'         => __( 'Aktifkan Global Volumetrik', 'agenwebsite'),
                        'type'          => 'checkbox',
                        'label'         => __( 'Aktifkan jika ingin semua produk Anda menggunakan volumetrik', 'agenwebsite' ),
                        'default'       => 'no'
                    ),
                    'jne_service' => array(
                        'type'          => 'jne_service',
                        'default'        => 'yes',
                    ),
                )
            ),
            'asuransi' => array(
                'label' => __( 'Asuransi', 'agenwebsite' ),
                'fields'=> array(
                    'jne_asuransi' => array(
                        'title'            => __( 'Asuransi', 'agenwebsite' ),
                        'type'          => 'title',
                        'description'   => __( 'Fitur ini berfungsi untuk memberika biaya asuransi pada setiap produk atau pesanan.', 'agenwebsite' ),
                        'default'        => 'yes'
                    ),
                    'asuransi_overide' => array(
                        'title'            => __( 'Aktifkan ke semua produk', 'agenwebsite' ),
                        'type'          => 'checkbox',
                        'description'   => __( 'Dengan mencentang ini, maka semua produk yang ada akan diasuransikan dan pengaturan di metapost akan diabaikan.', 'agenwebsite' ),
                        'default'        => 'no',
                    ),
                    'asuransi_teks' => array(
                        'title'            => __( 'Teks asuransi', 'agenwebsite' ),
                        'type'          => 'textarea',
                        'description'   => __( 'Teks ini akan muncul dihalaman produk yang diasuransikan. gunakan <code>{harga}</code> untuk menampilkan harga asuransi.', 'agenwebsite' ),
                        'default'        => 'Asuransikan produk ini seharga {harga} ?'
                    ),
                    'asuransi_teks_required' => array(
                        'title'            => __( 'Teks asuransi yang diwajibkan', 'agenwebsite' ),
                        'type'          => 'textarea',
                        'description'   => __( 'Teks ini akan muncul dihalaman produk yang <code>wajib</code> diasuransikan. gunakan <code>{harga}</code> untuk menampilkan harga asuransi.', 'agenwebsite' ),
                        'default'        => 'Produk ini di asuransikan seharga {harga}.'
                    ),
                )
            ),
            'tools' => array(
                'label' => __( 'Tools', 'agenwebsite' ),
                'fields'=> array(
                    'is_disable_transient' => array(
                        'title'        => __('Disable JNE transients', 'agenwebsite'),
                        'type'        => 'checkbox',
                        'label'        => __('Disable JNE transients', 'agenwebsite'),
                        'description' => __('Disable JNE transients will get every shipping price from our server.', 'agenwebsite'),
                        'default' => 'yes'
                    ),
                    'jne_shipping_transient' => array(
                        'title'         => __( 'JNE Transients', 'agenwebsite' ),
                        'type'          => 'button',
                        'label'         => __( 'JNE Transients', 'agenwebsite' ),
                        'description'   => __( 'Tool ini akan menghapus semua transient harga jne shipping.', 'agenwebsite' ),
                        'placeholder'   => __( 'Clear JNE shipping transient', 'agenwebsite' ),
                        'class'         => 'clear_jne_transient',
                        'default'       => ''
                    ),
                    'clear_expired_transients' => array(
                        'title'         => __( 'Expired Transients', 'agenwebsite' ),
                        'type'          => 'button',
                        'label'         => __( 'Clear expired transients', 'agenwebsite' ),
                        'description'   => __( 'Tool ini akan menghapus semua transient kadaluarsa dari WordPress.', 'agenwebsite' ),
                        'placeholder'   => __( 'Clear expired transient', 'agenwebsite' ),
                        'class'         => 'clear_expired_transient',
                        'default'       => ''
                    )
                ),
            ),
        );

        return $form_fields;
    }

}

endif;
