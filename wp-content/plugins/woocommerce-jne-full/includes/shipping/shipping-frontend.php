<?php
/**
 * Frontend JNE Shipping
 *
 * Handles for the request to frontend
 *
 * @author AgenWebsite
 * @package WooCommerce JNE Shipping
 * @since 8.0.0
 */

if ( !defined( 'WOOCOMMERCE_JNE' ) ) { exit; } // Exit if accessed directly

if ( !class_exists( 'WC_JNE_Frontend' ) ) :

/**
 * Class WooCommerce JNE Frontend
 *
 * @since 8.0.0
 **/
class WC_JNE_Frontend{

    /**
     * Constructor
     *
     * @return void
     * @since 8.0.0
     **/
    public function __construct(){        
        // JNE reorder fields option
        add_filter( 'woocommerce_checkout_fields', array( &$this, 'JNE_reorder_fields' ), 15 );

        // JNE reorder billing address
        add_filter( 'woocommerce_billing_fields', array( &$this, 'JNE_reorder_billing_fields' ), 15 );

        // JNE reorder shipping address
        add_filter( 'woocommerce_shipping_fields', array( &$this, 'JNE_reorder_shipping_fields' ), 15 );

        // Enable city shipping calculator
        add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_true' );

        // Show total weight in review order table
        add_action( 'woocommerce_review_order_before_shipping', array( &$this, 'show_total_weight' ) );

        // Calculate chosen shipping method jne
        add_action( 'woocommerce_calculate_totals', array( &$this, 'on_calculate_totals' ) );

        // remove order item meta per chosen shipping method jne
        add_action( 'woocommerce_add_order_item_meta', array( &$this, 'remove_order_item_meta' ), 15, 2 );

        // add total weight to order post meta
        add_action( 'woocommerce_checkout_update_order_meta', array( &$this, 'add_total_weight_item_meta' ), 10, 2 );

        // display total weight on admin order data after shipping
        add_action( 'woocommerce_admin_order_data_after_shipping_address', array(&$this, 'display_total_weight_item_meta'), 10, 1 );

        // add column to show total weight on my account orders columns
        add_filter( 'woocommerce_my_account_my_orders_columns', array(&$this, 'add_total_weight_column_to_my_account_table') );

        // display total weight on my account orders columns
        add_action( 'woocommerce_my_account_my_orders_column_order_total_weight', array(&$this, 'display_total_weight_my_account_columns') );
        
        // filter country locale to reorder state and city
        add_filter('woocommerce_get_country_locale', array( &$this, 'sorting_ID_locale') );
    }
    
    /**
     * Locale ID
     * Sorting fields address by priority for indonesia
     *
     * @access public
     * @param array $locales
     * @return array
     * @since 8.1.19
     */
    public function sorting_ID_locale($locales){
        $wc_version = WC_JNE()->get_woocommerce_version();
		
        $fields = array(
			'state' => array(
				'type'         => 'state',
				'label'        => __( 'State / County', 'woocommerce' ),
				'required'     => true,
				'class'        => array( 'form-row-wide', 'address-field' ),
				'validate'     => array( 'state' ),
				'autocomplete' => 'address-level1',
				'priority'     => 70,
			),
			'city' => array(
				'label'        => __( 'Town / City', 'woocommerce' ),
				'required'     => true,
				'class'        => array( 'form-row-wide', 'address-field' ),
				'autocomplete' => 'address-level2',
				'priority'     => 80,
			),
		);

        if($wc_version[0] >= '3'){
            $locales['ID'] = $fields;
        }
        
        return $locales;
    }

    /**
     * JNE reorder fields option billing
     *
     * @access public
     * @param array $fields
     * @return array
     * @since 8.0.0
     **/
    public function JNE_reorder_billing_fields($fields) {
        $AW_fields['billing_country']               = $fields['billing_country'];
        $AW_fields['billing_first_name']            = $fields['billing_first_name'];
        $AW_fields['billing_last_name']             = $fields['billing_last_name'];
        $AW_fields['billing_company']               = $fields['billing_company'];
        $AW_fields['billing_address_1']             = $fields['billing_address_1'];
        $AW_fields['billing_address_2']             = $fields['billing_address_2'];
        $AW_fields['billing_postcode']              = $fields['billing_postcode'];
        $AW_fields['billing_state']                 = $fields['billing_state'];

        // Tambah custom field kota untuk billing field
        $AW_fields['billing_kota']                  = $this->get_field_kota();

        $AW_fields['billing_city']                  = $fields['billing_city'];
        $AW_fields['billing_city']['class']         = array( 'form-row-wide', 'address-field', 'update_totals_on_change' );

        $AW_fields['billing_email']                 = $fields['billing_email'];
        $AW_fields['billing_phone']                 = $fields['billing_phone'];

        return apply_filters( 'woocommerce_jne_billing_fields', $AW_fields );
    }

    /**
     * JNE reorder fields option shipping
     *
     * @access public
     * @param array $fields
     * @return array
     * @since 8.0.0
     **/
    public function JNE_reorder_shipping_fields($fields) {
        $AW_fields['shipping_country']              = $fields['shipping_country'];
        $AW_fields['shipping_first_name']           = $fields['shipping_first_name'];
        $AW_fields['shipping_last_name']            = $fields['shipping_last_name'];
        $AW_fields['shipping_company']              = $fields['shipping_company'];
        $AW_fields['shipping_address_1']            = $fields['shipping_address_1'];
        $AW_fields['shipping_address_2']            = $fields['shipping_address_2'];
        $AW_fields['shipping_postcode']             = $fields['shipping_postcode'];
        $AW_fields['shipping_state']                = $fields['shipping_state'];

        // Tambah custom field kota untuk shipping field
        $AW_fields['shipping_kota']                 = $this->get_field_kota();

        $AW_fields['shipping_city']                 = $fields['shipping_city'];
        $AW_fields['shipping_city']['class']        = array( 'form-row-wide','address-field', 'update_totals_on_change' );

        return apply_filters( 'woocommerce_jne_shipping_fields', $AW_fields );
    }

    /**
     * JNE reorder fields option
     *
     * @access public
     * @param array $fields
     * @return array
     * @since 8.0.0
     **/
    public function jne_reorder_fields($fields) {
        $reorder_field = array(
            'billing' => array(
                'billing_country',
                'billing_first_name',
                'billing_last_name',
                'billing_company',
                'billing_address_1',
                'billing_address_2',
                'billing_postcode',
                'billing_state',
                'billing_kota',
                'billing_city',
                'billing_email',
                'billing_phone'
            ),
            'shipping' => array(
                'shipping_country',
                'shipping_first_name',
                'shipping_last_name',
                'shipping_company',
                'shipping_address_1',
                'shipping_address_2',
                'shipping_postcode',
                'shipping_state',
                'shipping_kota',
                'shipping_city'
            )
        );
        
        foreach($reorder_field as $field_id => $field_data){
            $fields[$field_id][$field_id.'_kota'] = $this->get_field_kota();
            foreach($field_data as $field_name){
                $AW_fields[$field_id][$field_name] = $fields[$field_id][$field_name];
            }
        }

        $AW_fields['account']     = $fields['account'];
        $AW_fields['order']     = $fields['order'];

        return apply_filters( 'woocommerce_jne_checkout_fields', $AW_fields );
    }

    /**
     * Add total weight to review order table
     *
     * @access public
     * @return HTML
     * @since 8.0.0
     */
    public function show_total_weight(){
        $options = get_option('woocommerce_jne_shipping_settings');
        $total_weight_is_display = $options['enabler_total_weight'];
        $total_weight = WC_JNE()->shipping->calculate_weight();
        $total_weight['weight'] = ( WC_JNE()->get_woocommerce_weight_unit() == 'g' ? ( $total_weight['weight'] * 1000 ) : $total_weight['weight'] );
        
        if( ( array_key_exists( 'virtual', $total_weight ) && $total_weight['virtual'] !== 'yes' ) || $total_weight['weight'] !== 0 && $total_weight_is_display == 'yes' ){
            echo '<tr>' . "\n";
            echo '<th>' . __( 'Total weight', 'agenwebsite' ) . '</th>' . "\n";
            echo '<td>' . $total_weight['weight'] . apply_filters( 'woocommerce_jne_weight_unit', WC_JNE()->get_woocommerce_weight_unit() ) .'</td>' . "\n";
            echo '</tr>' . "\n";
        }
    }
    
    /**
     * Field Kota
     *
     * @access public
     * @return array $field_kota
     * @since 8.1.19
     */
    public function get_field_kota(){
        $field_kota = array(
            'type' => 'select',
            'label' => __( 'Kota / Kabupaten', 'agenwebsite' ),
            'class' => array( 'form-row-wide','address-field', 'update_totals_on_change' ),
            'options' => array('' => ''),
            'required' => TRUE,
            'placeholder' => __( 'Pilih Kota / Kabupaten', 'agenwebsite' )
        );
        
        return $field_kota;
    }

    /**
     * Add total weight to post meta when order placed
     *
     * @access public
     * @return void
     * @since 8.1.17
     */
    public function add_total_weight_item_meta( $order_id ){
        $meta_key = '_total_weight';
        $meta_value = WC_JNE()->shipping->calculate_weight();
        update_post_meta($order_id, $meta_key, $meta_value);
    }

    /**
     * Display total weight on admin order data after shipping
     *
     * @access public
     * @return HTML
     * @since 8.1.17
     */
    public function display_total_weight_item_meta($order){
        $wc_version = WC_JNE()->get_woocommerce_version();
        $order_id = ($wc_version >= '3') ? $order->get_id() : $order->id;
        $total_weight = get_post_meta( $order_id, '_total_weight', true );
        if(is_array($total_weight) && array_key_exists('weight', $total_weight)){
            printf('<p><strong style="display:block">%s</strong>%s %s</p>', __('Total Weight', 'agenwebsite'), $total_weight['weight'], WC_JNE()->get_woocommerce_weight_unit());
        }
    }

    /**
     * Display total weight on my account columns
     *
     * @access public
     * @return HTML
     * @since 8.1.17
     */
    public function display_total_weight_my_account_columns($order){
        $wc_version = WC_JNE()->get_woocommerce_version();
        $order_id = ($wc_version >= '3') ? $order->get_id() : $order->id;
        $total_weight = get_post_meta( $order_id, '_total_weight', true );
        if(is_array($total_weight) && array_key_exists('weight', $total_weight)){
            printf('%s %s', $total_weight['weight'], WC_JNE()->get_woocommerce_weight_unit());
        }
    }

    /**
     * Add total weight columns to my account columns
     *
     * @access public
     * @return HTML
     * @since 8.1.17
     */
    public function add_total_weight_column_to_my_account_table($columns){
        $columns_total_weight = array('order_total_weight' => __('Weight', 'agenwebsite'));
        $new_columns = array();
        $after = 'order-total';
        foreach($columns as $column_id => $column_name){
            $new_columns[$column_id] = $column_name;

            if($column_id == $after){
                $new_columns = $new_columns + $columns_total_weight;
            }
        }
        return $new_columns;
    }

    /**
     * Action on calculate totals woocommerce
     *
     * @access public
     * @return HTML
     * @since 8.1.02
     */
    public function on_calculate_totals( $cart ){

        $shipping_method = WC()->session->get( 'chosen_shipping_methods' );
        $shipping_total = $cart->shipping_total;

        $cart = WC()->cart->get_cart();
        // cek shipping method sudah terpilih
        if( ! empty( $shipping_method[0] ) ){
             // cek shipping method bukan jne
            if( strpos($shipping_method[0], 'jne') === false ){
                // lihat isi cart
                foreach( $cart as $id => $data ){
                     //jika di cart ada data jne asuransi
                    if( array_key_exists( 'jne_asuransi', $data ) ){
                        // ubah jne asuransi jadi false
                        $data['jne_asuransi'] = false;
                        // buat harga jne asuransi jadi minus
                        $harga_asuransi = '-' . $data['jne_asuransi_price'];
                        // update harga total item
                        $data['data']->adjust_price( $harga_asuransi );
                    }

                    // jika di cart ada data jne asuransi
                    if( array_key_exists( 'jne_asuransi', $data ) ){
                        // filter woocommerce get data untuk menghapus variation jne asuransi
                        add_filter( 'woocommerce_get_item_data', array( &$this, 'remove_item_data' ), 10, 2 );
                    }
                }
            }else{ //jika shipping method adalah jne
                // lihat isi cart
                foreach( $cart as $id => $data ){
                    // jika di cart ada data jne asuransi
                    if( array_key_exists( 'jne_asuransi', $data ) ){
                        // jika asuransi false
                        // false artinya sudah di kurangi pada saat shipping method terpilih bukan jne
                        if( $data['jne_asuransi'] == false ){
                            // ubah jne asuransi jadi true
                            $data['jne_asuransi'] = 1;
                            // harga jne asuransi
                            $harga_asuransi = $data['jne_asuransi_price'];
                            // update harga total item
                            $data['data']->adjust_price( $harga_asuransi );
                        }
                    }
                }
            }
        }
    }

    /**
     * Remove order item meta
     *
     * @access public
     * @return HTML
     * @since 8.1.02
     */
    public function remove_order_item_meta( $item_id, $cart_item ){
        $shipping_method = WC()->session->get( 'chosen_shipping_methods' );

        if( strpos($shipping_method[0], 'jne') === false ){
            if( ! empty( $cart_item['jne_asuransi'] ) ){
            woocommerce_delete_order_item_meta( $item_id, __( 'JNE Asuransi', 'woocommerce-jne' ) );
            }
        }
    }

    /**
     * remove item data asuransi
     *
     * @access public
     * @return HTML
     * @since 8.1.02
     */
    public function remove_item_data( $item_data, $cart_item ){
        foreach( $item_data as $key => $data ){
            $name = explode( ' x' , $data['name'] );
            $name = $name[0];
            if($name == 'JNE Asuransi'){
                unset( $item_data[$key] );
            }
        }

        return $item_data;
    }

}

endif;
