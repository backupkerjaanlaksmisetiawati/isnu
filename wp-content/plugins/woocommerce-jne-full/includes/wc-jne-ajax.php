<?php
/**
 * Main AJAX Handles
 *
 * Handles for all request file
 *
 * @author AgenWebsite
 * @package WooCommerce JNE Shipping
 * @since 8.0.0
 */

if ( !defined( 'WOOCOMMERCE_JNE' ) ) { exit; } // Exit if accessed directly

if ( ! class_exists( 'WC_JNE_AJAX' ) ):

class WC_JNE_AJAX{
    
    /* @var string */
    private static $nonce_admin = 'woocommerce_jne_admin';
    
    /**
     * Hook in methods
     */
    public static function init(){
        
        // ajax_event => nopriv
        $ajax_event = array(
            'get_provinsi_bykeyword'        => false,
            'get_city_bykeyword'                => false,
            'check_version'                    => false,
            'check_status'                        => false,
            'shipping_get_kota'                => true,
            'shipping_get_kecamatan'        => true,
        );
            
        foreach( $ajax_event as $ajax_event => $nopriv ){
            add_action( 'wp_ajax_woocommerce_jne_' . $ajax_event, array( __CLASS__, $ajax_event ) );
            
            if( $nopriv ){
                add_action( 'wp_ajax_nopriv_woocommerce_jne_' . $ajax_event, array( __CLASS__, $ajax_event ) );    
            }
        }
            
    }
    
    /**
     * AJAX Get provinsi indonesia by keyword on jne shipping settings page
     *
     * @access public
     * @return json
     * @since 8.0.0
     **/
    public static function get_provinsi_bykeyword(){
                        
        check_ajax_referer( self::$nonce_admin );
        
        $wc_version = WC_JNE()->get_woocommerce_version();
        $keyword = ($wc_version[0] >= 3) ? $_POST['q']['term'] : $_POST['q'];

        $args = array(
            'action' => 'get_provinsi_bykeyword',
            'keyword' => $keyword
        );

        $response = WC_JNE()->api->remote_get( 'kota', $args );

        if( $response['status'] == 'success' ){
            wp_send_json( $response['result'] );
        }

        wp_die();            
    }

    /**
     * AJAX Get city by keyword on jne shipping settings page
     *
     * @access public
     * @return json
     * @since 8.0.0
     **/
    public static function get_city_bykeyword(){

        check_ajax_referer( self::$nonce_admin );

        $wc_version = WC_JNE()->get_woocommerce_version();
        $keyword = ($wc_version[0] >= 3) ? $_POST['q']['term'] : $_POST['q'];
        
        $args = array(
            'action' => 'get_city_bykeyword',
            'keyword' => $keyword,
        );

        $response = WC_JNE()->api->remote_get( 'kota', $args );

        if( $response['status'] == 'success' ){
            wp_send_json( $response['result'] );
        }

        wp_die();
    }
    
    /**
     * AJAX Check version to API AgenWebsite
     *
     * @access public
     * @return json
     * @since 8.0.0
     **/
    public static function check_version(){
        
        check_ajax_referer( self::$nonce_admin );
        
        $url = 'api.agenwebsite.com/cek-versi/0.0.1/?action=cek_versi&product=woocommerce_jne_shipping&version=' . WOOCOMMERCE_JNE_VERSION;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => 30
        ));
        $result = curl_exec($curl);
        if(curl_errno($curl)) {
            curl_close($curl);
            return false;
        }
        curl_close($curl);
        
        $data = json_decode( $result, TRUE );
        
        $output['result'] = $data['result'];
        $output['version'] = WOOCOMMERCE_JNE_VERSION;
        if( $data['result'] != 1 ){
            $output['update_url'] = $data['update_url'];
            $output['latest_version'] = $data['versi'];                
        }
        
        wp_send_json( $output );
        
        wp_die();

    }
    
    /**
     * AJAX Checking status
     *
     * @access public
     * @return json
     * @since 4.0.0
     **/
    public static function check_status(){

        check_ajax_referer( self::$nonce_admin );

        $license = ( ! empty($_POST['license_code']) ) ? $_POST['license_code'] : WC_JNE()->get_license_code();

        WC_JNE()->api->license_code = $license;
        $result = WC_JNE()->api->remote_get( 'license' );

        $message = ( $result['status'] == 'success' ) ? $result['result']['message'] : $result['message'];

        ob_start();
        wc_get_template( 'html-aw-product-status.php', array(
            'status' => $result['status'],
            'message' => $message,
            'data' => $result['result'],
        ), 'woocommerce-pos', untrailingslashit( WC_JNE()->plugin_path() ) . '/views/' );
        $output['message'] = ob_get_clean();        

        wp_send_json( $output );

        wp_die();

    }
    
    /**
     * AJAX Get data kota
     *
     * @access public
     * @return json
     * @since 8.0.0
     **/
    public static function shipping_get_kota(){

        check_ajax_referer( WC_JNE()->get_nonce() );

        // Check if we need to recalculate shipping for this city
        $city_hash = 'wc_ship_jne_kota_' . md5( $_POST['provinsi'] ) . WC_Cache_Helper::get_transient_version( 'jne_shipping_kota' );

        if( $_POST['kota'] == 'Select an option…' ) wp_die();

        if( false === ( $stored_city = get_transient( $city_hash ) ) ){

            $method = 'Using API';

            $args = array(
                'action' => 'get_kota',
                'keyword' => $_POST['provinsi'],
            );

            $response = WC_JNE()->api->remote_get( 'kota', $args );

            if( $response['status'] == 'success' && $response['result'] != '' ){
                // store transient
                WC_JNE()->set_transient( $city_hash, $response['result'] );
            }

        }else{

            $method = 'Using transients';

            $response['status'] = 'success';
            $response['result'] = $stored_city;

        }

        if( $response['status'] == 'success' ){
            if( WC_JNE()->debug ) $response['result']['debug'] = $method;
            wp_send_json( $response['result'] );
        } else {
            // Gagal terhubung / error
            wp_send_json( $response );
        }

        wp_die();

    }
        
    /**
     * AJAX Get data kecamatan
     *
     * @access public
     * @return json
     * @since 8.0.0
     **/
    public static function shipping_get_kecamatan(){

        check_ajax_referer( WC_JNE()->get_nonce() );

        // Check if we need to recalculate shipping for this kecamatan
        $kec_hash = 'wc_ship_jne_kec_' . md5( $_POST['kota'] ) . WC_Cache_Helper::get_transient_version( 'jne_shipping_kec' );

        if( $_POST['kota'] == 'Pilih Kota / Kabupaten' ) wp_die();

        if( false === ( $stored_kec = get_transient( $kec_hash ) ) ){

            $method = 'Using API';

            $args = array(
                'action' => 'get_kecamatan',
                'keyword' => $_POST['provinsi'] .'|'. $_POST['kota'],
            );

            $response = WC_JNE()->api->remote_get( 'kota', $args );

            if( $response['status'] == 'success' && $response['result'] != '' ){
                // store transient
                WC_JNE()->set_transient( $kec_hash, $response['result'] );
            }

        }else{

            $method = 'Using transients';

            $response['status'] = 'success';
            $response['result'] = $stored_kec;

        }

        if( $response['status'] == 'success' ){
            if( WC_JNE()->debug ) $response['result']['debug'] = $method;
            wp_send_json( $response['result'] );
        } else {
            // Gagal terhubung / error
            wp_send_json( $response );
        }

        wp_die();
    }

}

WC_JNE_AJAX::init();

endif;