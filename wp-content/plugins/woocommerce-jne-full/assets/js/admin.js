jQuery(function($) {
    
    // agenwebsite_jne_admin_params is required to continue, ensure the object exists
    if ( typeof agenwebsite_jne_admin_params === 'undefined' ) {
        return false;
    }
    
    jQuery(window).on('load', function($) {
    
        var aw_status_version = jQuery('#aw_status_version');
        var aw_status_version_help = jQuery('#aw_status_version_help');
    
        jQuery.ajax({
            url: agenwebsite_jne_admin_params.ajax_url,
            type: "POST",
            dataType:"json",
            data:{ action: 'woocommerce_jne_check_version', _wpnonce: agenwebsite_jne_admin_params.jne_admin_wpnonce },
            success: function( data ){
                if( data != 0 ){
                    var mark = jQuery( aw_status_version ).find('mark')
                    var status;
                    if( data.result == 1 ){
                        status = data.version;
                    }else if( data.result == 0 ){
                        jQuery( mark ).removeClass( 'yes' ).addClass( 'no' );
                        jQuery( mark ).before( data.version );
                        jQuery( aw_status_version_help ).find('a').attr('href', data.update_url);
                        jQuery( aw_status_version_help ).show();
                        status = ' - ' + data.latest_version + ' ' + agenwebsite_jne_admin_params.i18n_is_available;
                    }
                    jQuery( mark ).text( status );
                }
            }
        });
        
        /**
         * Load status
         */
        jQuery.post( agenwebsite_jne_admin_params.ajax_url, { action: 'woocommerce_jne_check_status', license_code: agenwebsite_jne_admin_params.license, _wpnonce: agenwebsite_jne_admin_params.jne_admin_wpnonce }, function(response){
            jQuery('#wc_jne_status tbody').html(response.message);
        });
        
    });

    jQuery(document).ready(function($) {
        /*
         * Add reset button
         */
        if( agenwebsite_jne_admin_params.tab !== 'shortcodes' ){
            $('.woocommerce .submit').append('<input name="reset_default" type="submit" value="Kembali ke Settingan Awal" class="button button_secondary" id="reset_default">');
        }else{
            $('input[type="submit"]').hide();
        }

        if( agenwebsite_jne_admin_params.license == '' ) {
            $('input[name="reset_default"]').hide();
        }
        
        /*
         * Event click reset default
         */
        $("#reset_default").click(function(){
            var e = confirm( agenwebsite_jne_admin_params.i18n_reset_default );
            return e?void 0:!1
        });
        
        // WC Version
        var wc_version = agenwebsite_jne_admin_params.wc_version.split('.')[0];
        
    });

});