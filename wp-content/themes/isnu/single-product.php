<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

<?php 
global $product;
$product_id = $post->ID;

$attch_id = $product->get_gallery_image_ids();





?>
	
<div class="row row_singlePro">
	<div class="col-md-12 col_singlePro">
				
		<div class="ht_proCategory"> 
				<a href="<?php echo home_url(); ?>" class="a_proCategory"><i class="fab fa-fort-awesome"></i></a>
				<span><i class="fas fa-chevron-right"></i></span>
				<a class="a_proCategory">Elektronik & Handphone</a>
				<span><i class="fas fa-chevron-right"></i></span>
				<a class="a_proCategory">Gaming</a>
				<span><i class="fas fa-chevron-right"></i></span>
				<a class="a_proCategory"><?php echo get_the_title($post->ID); ?></a>
		</div>	

		<div class="row row_top_singlePro">
			<div class="col-md-5 col_left_singlePro">
				
				<div id="slide_bigProduct" class="owl-carousel owl-theme popup-gallery">
					<?php 
						foreach ($attch_id as $key => $value) {
							$id_poto = $value;
							$linkpoto = wp_get_attachment_image_src($id_poto,'large');
							$linkpoto_full = wp_get_attachment_image_src($id_poto,'full');
							if($linkpoto){
								$large_photo = $linkpoto['0'];
								$full_photo = $linkpoto_full[0];
							}else{
								$large_photo = get_template_directory_uri().'/library/images/sorry.jpg';
								$full_photo = get_template_directory_uri().'/library/images/sorry.jpg';
							}
							$alt = get_post_meta($id_thumb, '_wp_attachment_image_alt', true);
							if(!isset($alt)){
								$alt = 'Milenial Mall Gallery';
							}
					?>
								<div class="item" data-hash="photo-<?php echo $id_poto; ?>">
									<a href="<?php echo $full_photo; ?>" rel="<div class='tx_magnific'><?php echo $t_poto; ?></div>" title="View Photo Gallery">
								    	<div class="mg_slider_homeBanner">
								    		<img src="<?php echo $large_photo; ?>" alt="<?php echo $alt; ?>" title="klik untuk memperbesar foto.">
								    	</div>
							    	</a>
							    </div>

					<?php
							
						}
					 ?>
				</div>				

				<div class="box_bannerThumb"> 
					<?php 
						foreach ($attch_id as $key => $value) {
							$id_poto = $value;
							$linkpoto = wp_get_attachment_image_src($id_poto,'thumbnail');
							$linkpoto_full = wp_get_attachment_image_src($id_poto,'full');
							if($linkpoto){
								$thumb_photo = $linkpoto[0];
								$full_photo = $linkpoto_full[0];
							}else{
								$thumb_photo = get_template_directory_uri().'/library/images/sorry.jpg';
								$full_photo = get_template_directory_uri().'/library/images/sorry.jpg';
							}
							$alt = get_post_meta($id_thumb, '_wp_attachment_image_alt', true);
							if(!isset($alt)){
								$alt = 'Milenial Mall Gallery';
							}
					?>	
								<a href="#photo-<?php echo $id_poto; ?>" title="klik untuk melihat foto.">
									<div class="mg_bannerThumb">
										<img src="<?php echo $thumb_photo; ?>" alt="<?php echo $alt; ?>">
									</div>	
								</a>
					<?php
						}
					 ?>
				</div>


			</div>
			<div class="col-md-7 col_right_singlePro">
				<h1 class="title_singlePro"><?php echo get_the_title($post->ID); ?></h1>

				<div class="row row_procont">
					<div class="col-md-8 col_procont_r1">
						
						<div class="bx_pro_price">
							<!-- <div class="ht_normal_product ht_price_product">Harga</div> -->
							<div class="xt_price_product">
								<span class="pro_basePrice">Harga awal : <i>Rp 2.199.000</i></span>
							</div>
							<div class="xt_price_product">
								<span class="pro_finalPrice"><span>Rp</span> 1.479.000</span>
								<span class="sale_off">Hemat 32%</span>
							</div>

						</div>

						<div class="bx_mid_singlePro">
							<div class="col_req_singlePro">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/brand.svg">
								Brand : <b>Flower Kids</b>
							</div>
							<div class="col_req_singlePro">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/shirt.svg">
								Tersedia Ukuran : <span>6,9,12,18,24M.3T</span>
							</div>
							<div class="col_req_singlePro">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/box.svg">
								Berat : <span>200</span> gram /pcs.
							</div>
							<div class="col_req_singlePro">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/medal.svg">
								poin Milenial Rewards : <span>+148</span>
							</div>
							
							<div class="col_req_singlePro">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/guarantee.svg">
								Asuransi Pengiriman : Opsional
							</div>
						</div>


						<div class="box_shippingInfo">
							
							<div class="f_shippingInfo">
								<button class="btn_ship_prize">Gratis!</button> <label>7 hari pengembalian produk cacat</label> 
								<a href="#freeongkir" title="Lihat detail promo ini."><span class="check"><i class="fas fa-info-circle"></i></span></a>
							</div>

							<div class="f_shippingInfo">
								<button class="btn_ship_prize">Gratis Ongkir</button> <label>Pengiriman nominal <b>Rp 10.000</b></label> 
								<a href="#freeongkir" title="Lihat detail promo ini."><span class="check"><i class="fas fa-info-circle"></i></span></a>
							</div>

							
							<div class="f_shippingInfo normal">
								<div class="detail_shippingInfo">
									<label>ESTIMASI PENGIRIMAN :</label>
									<span>Jabodetabek 2-5 hari kerja,</span>
									<span>luar Jabodetabek 4-8 hari kerja.</span>
								</div>
							</div>

						</div>


					</div>
					<div class="col-md-4 col_procont_r2">
						<div class="box_procont_shipping">

							<div class="top_procont">
								<div class="col_req_toprate">
									<img src="<?php bloginfo('template_directory'); ?>/library/images/rating.svg">
									100% Produk Terbaik 
								</div>

								<div class="info_seller"><label>dijual oleh :</label> <span>Milenial Mall Original</span></div>
							</div>

							<div class="mid_procont">

								<div class="ht_ukuran">Pilih Ukuran</div>
								<a class="a_ukuran" href="#detailukuran">
									<div class="hts_ukuran ht_margin">Bantu aku menghitung ukuran <span><i class="fas fa-info-circle"></i></span></div>
								</a>
								
								<div class="_fc_variantproduct">
									<label>Ukuran</label>
									<select name="ukuran" class="sel_variantSize">
										<option value="" selected>Kids</option>
									</select>
								</div>

								<div class="_fc_variantproduct">
									<label>Nomor</label>
									<select name="ukuran" class="sel_variantSize">
										<option>9T (90cm)</option>
										<option>10T (100cm)</option>
										<option>11T (110cm)</option>
										<option>12T (120cm)</option>
										<option>13T (130cm)</option>
										<option>14T (140cm)</option>
									</select>
								</div>

								<div class="_fc_stock">
									<span class="_ready">Produk ini tersedia</span>
									<span class="_sisa">Tersisa 2 pcs saja</span>
									<span class="_soldout">Maaf, stock variant ini Habis</span>
								</div>

								<input type="hidden" name="qty" value="1">


								<input type="submit" class="sub_addtoCart" value="Pesan Sekarang">

								<button class="but_addtoWishlist"><i class="fas fa-heart love"></i> Tambah ke Wishlist</button>

								<button class="but_addtoAfiliasi"><i class="fas fa-plus add_afiliasi"></i> Tambah ke Estalase Afiliasi</button>

							</div>

						</div>
					</div>
				</div>

			</div>
		</div>



		<div class="row row_singleProTabs">
			<ul class="proTabs">
				<li class="act" data-id="tabs1"><a href="javascript:;">Rincian Produk</a></li>
				<li data-id="tabs2"><a href="javascript:;">Detail Ukuran</a></li>
				<li data-id="tabs3"><a href="javascript:;">Ulasan (234)</a></li>
				<li data-id="tabs4"><a href="javascript:;">Diskusi</a></li>
			</ul>
		</div>
		<div class="row row_singleProContent">
			<?php // ============= TABS 1 ============ ?>
			<div id="tabs1" class="col-md-12 box_detailProduct act animated fadeIn">
				
				<div class="row row_tabsinfo">
					<div class="col-md-6 box_tabsinfo1">
						
						<div class="f_tabsInfo">
							<label>SKU</label>
							<span>6175899781444</span>
						</div>

						<div class="f_tabsInfo">
							<label>Warna</label>
							<span>Merah, Biru, Hijau</span>
						</div>

						<div class="f_tabsInfo">
							<label>Bahan</label>
							<span>100% Cotton</span>
						</div>

						<div class="f_tabsInfo">
							<label>Ukuran</label>
							<span>Kids</span>
						</div>	
						
					</div>
					<div class="col-md-6"></div>
				</div>
				
				<?php 
					$content_post = get_post($product_id);
					$content = $content_post->post_content;
					// $content = apply_filters('the_content', $content);
					// $content = str_replace(']]>', ']]&gt;', $content);
				 ?>
				<div class="box_tabsinfo2">
					<?php echo $content; ?>
				</div>
			
			</div>
			<?php // ============= TABS 2 ============ ?>
			<div id="tabs2" class="col-md-12 box_detailProduct  animated fadeIn">

				<div class="row">
					<div class="col-md-6 col_sizeDetail">
					
							<?php $get_size = get_post_meta($product_id,'pro_ukuran_pakaian',true);
								if(isset($get_size) AND $get_size != ''){
									echo $get_size;
								}	
							?>
						
					</div>
					<div class="col-md-6 col_sizeDetail">
						<div class="box_sizeDetail">
							<div class="info_sizeDetail">Lihat ukuran badan Anda :</div>
							<div class="mg_sizeDetail">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/size-child.jpg">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/size-woman.jpg">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/size-man.jpg">
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<?php // ============= TABS 3 ============ ?>
			<div id="tabs3" class="col-md-12 box_detailProduct  animated fadeIn">
				
				<div class="box_allulasan">
					<h2 class="ht_ulasanProduk">Ulasan Produk</h2>

					<div class="row row_rateUlasan">
						<div class="col-md-3 col_rateUlasasn">
							<?php 
							// $rate_count = 0.5;
							$ulasan_count = 234;
							$rate5 = 120;
							$rate4 = 80;
							$rate3 = 12;
							$rate2 = 12;
							$rate1 = 10;

							$val_rate5 = 5*(int)$rate5;
							$val_rate4 = 4*(int)$rate4;
							$val_rate3 = 3*(int)$rate3;
							$val_rate2 = 2*(int)$rate2;
							$val_rate1 = 1*(int)$rate1;

							$rate_count = ($val_rate5+$val_rate4+$val_rate3+$val_rate2+$val_rate1)/$ulasan_count;
							$rate_count = round($rate_count,1);

							$per_rate5 = (int)$rate5/(int)$ulasan_count*100;
							$per_rate4 = (int)$rate4/(int)$ulasan_count*100;
							$per_rate3 = (int)$rate3/(int)$ulasan_count*100;
							$per_rate2 = (int)$rate2/(int)$ulasan_count*100;
							$per_rate1 = (int)$rate1/(int)$ulasan_count*100;


							if($rate_count == 0){
							?>
								<div class="ct_rateCount"><?php echo $rate_count; ?></div>
								<div class="rateStar ">
							<?php
							}else if($rate_count > 0 AND $rate_count <= 1){
							?>
								<div class="ct_rateCount give_rate1"><?php echo $rate_count; ?></div>
								<div class="rateStar give_rate1">
							<?php
							}else if($rate_count > 1 AND $rate_count <= 2){
							?>
								<div class="ct_rateCount give_rate2"><?php echo $rate_count; ?></div>
								<div class="rateStar give_rate2">
							<?php
							}else if($rate_count > 2 AND $rate_count <= 3){
							?>
								<div class="ct_rateCount give_rate3"><?php echo $rate_count; ?></div>
								<div class="rateStar give_rate3">
							<?php
							}else if($rate_count > 3 AND $rate_count <= 4){
							?>
								<div class="ct_rateCount give_rate4"><?php echo $rate_count; ?></div>
								<div class="rateStar give_rate4">
							<?php
							}else{
							?>
								<div class="ct_rateCount give_rate5"><?php echo $rate_count; ?></div>
								<div class="rateStar give_rate5">
							<?php
							}

							$hascomma = 0;
							if(is_decimal($rate_count) == 1){
								$hascomma = 1;
							}
							$play_total = $rate_count;
							for ($i=0; $i < 5; $i++) { 
								
								if($hascomma == 1){
									$play_total = (float)$play_total-1;

									if($i < $rate_count AND $play_total > 0){
									?>
										<span><i class="fas fa-star"></i></span>
									<?php 
									}else if($i < $rate_count AND $play_total < 0){ 
									?>
										<span><i class="fas fa-star-half-alt"></i></span>
									<?php
									}else{
									?>
										<span><i class="far fa-star"></i></span>
									<?php
									}

								}else{

									if($i < $rate_count){
									?>
										<span><i class="fas fa-star"></i></span>
									<?php
									}else{
									?>
										<span><i class="far fa-star"></i></span>
									<?php
									}
								}

							} 
							?>
							
							</div>
							<div class="ct_rateDetail"><?php echo $ulasan_count; ?> ulasan</div>
						</div>
						<div class="col-md-8 col_rateUlasasn2">
								
							<div class="box_rateStarTotal_1">
								<div class="progress_value">5 Bintang</div>
								<div class="progress_value">4 Bintang</div>
								<div class="progress_value">3 Bintang</div>
								<div class="progress_value">2 Bintang</div>
								<div class="progress_value">1 Bintang</div>
							</div>

							<div class="box_rateStarTotal_2">
								<div class="progress">
								  <div class="progress-bar b_rate5" role="progressbar" style="width: <?php echo $per_rate5; ?>%" aria-valuenow="<?php echo $per_rate5; ?>" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="progress">
								  <div class="progress-bar b_rate4" role="progressbar" style="width: <?php echo $per_rate4; ?>%" aria-valuenow="<?php echo $per_rate4; ?>" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="progress">
								  <div class="progress-bar b_rate3" role="progressbar" style="width: <?php echo $per_rate3; ?>%" aria-valuenow="<?php echo $per_rate3; ?>" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="progress">
								  <div class="progress-bar b_rate2" role="progressbar" style="width: <?php echo $per_rate2; ?>%" aria-valuenow="<?php echo $per_rate2; ?>" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="progress">
								  <div class="progress-bar b_rate1" role="progressbar" style="width: <?php echo $per_rate1; ?>%" aria-valuenow="<?php echo $per_rate1; ?>" aria-valuemin="0" aria-valuemax="100"></div>
								</div>

							</div>

							<div class="box_rateStarTotal_3">
								<div class="progress_value"><?php echo $rate5; ?> ulasan</div>
								<div class="progress_value"><?php echo $rate4; ?> ulasan</div>
								<div class="progress_value"><?php echo $rate3; ?> ulasan</div>
								<div class="progress_value"><?php echo $rate2; ?> ulasan</div>
								<div class="progress_value"><?php echo $rate1; ?> ulasan</div>
							</div>


						</div>
					</div>
				</div>


				<div class="row box_content_ulasan">

					<div class="col-md-12">
						<h3 class="pro_userRate"><?php echo get_the_title($product_id); ?></h3>
					</div>
					
					<?php for ($a=0; $a < 9 ; $a++) { ?>
					
						<div class="col-md-6 col_userRate">

							<div class="box_left_ulasan">
								<div class="ulasan_rateUser">
									<?php 
										$user_rate = 4;
										for ($i=0; $i < 5; $i++) { 
											if($i < $user_rate){
											?>
												<span><i class="fas fa-star"></i></span>
											<?php
											}else{
											?>
												<!-- <span><i class="far fa-star"></i></span> -->
											<?php
											}
										}
									 ?>
								</div>
								<div class="ht_rateUser">Oleh <b>Edi Susanto</b> <br/> 08 Nov 2019</div>
							</div>
							<div class="box_right_ulasan">
								<div class="comment_rateuser">
									Luar biasa produk ini, sangat bermanfaat!! Main game jadi gampang banget.
									Terima Kasih Milenial Mall. Suka Deh !!!
								</div>
							</div>
						</div>

					<?php } ?>


				</div>

			</div>
			<?php // ============= TABS 4 ============ ?>
			<div id="tabs4" class="col-md-12 box_detailProduct  animated fadeIn">
				<h1>hellow tabs 4</h1>
			</div>
		</div>

		<div class="row row_homeProduct">

			<div class="col-md-12 col_filt_homeProduct">
				<div class="ptc_homeProduct">Rekomendasi :</div>
				<ul class="cat_homeProduct">
					<li class="act"><a href="javascript:;">Paling Sesuai</a></li>
				</ul>
			</div>

		</div>

		<div class="row">
			
			<div id="slide_relatedProduct" class="owl-carousel owl-theme">
			
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<a href="#" title="Lihat Xiaomi Mi Air Purifier 3">
								<div class="mg_homeProduct">
									<!-- <img class="lazy" data-src="https://jakmall.id/2019/08/images/products/d37c5d/thumbnail/xiaomi-mi-air-purifier-3.jpg?s=e8fc8bf107b7ef1ed1c269ab383f6229"> -->
									<img src="<?php bloginfo('template_directory'); ?>/library/images/wifi-loading.gif">
								</div>
								<div class="body_homeProduct">
									<h4 class="pi_title">Xiaomi Mi Air Purifier 3</h4>
									<div class="pi_price">Rp 2.762.000 <span class="pi_sale">Rp.3.356.000</span></div>
								</div>
							</a>
						</div>
					</div>
			
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium/logitech_logitech-f310-gaming-pad--940-000112-_full03.jpg?output-format=webp">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Logitech - F310 Gaming Pad Wired</h4>
								<div class="pi_price">Rp 359.000 <span class="pi_sale">Rp 631.500</span></div>
							</div>
						</div>
					</div>
			
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://jakmall.id/2019/10/images/products/518e18/thumbnail/oneup-travel-toiletries-kit-tempat-sabun-sikat-gigi-handuk-etravel.jpg?s=59de5c75be7366bc04e4543a58ccc4bd">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">ONEUP Etravel Travel Toiletries Kit Tempat Sabun Sikat Handuk</h4>
								<div class="pi_price">Rp 111.800 <span class="pi_sale">Rp 145.300</span></div>
							</div>
						</div>
					</div>
		
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//89/MTA-3131765/joyful-_joyfyul-charger-pompa-galon-elektrik-otomatis_full09.jpg?output-format=webp">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Joyful Charger Pompa Galon Elektrik Otomatis</h4>
								<div class="pi_price">Rp 37.000 <span class="pi_sale">Rp 50.000</span></div>
							</div>
						</div>
					</div>
				
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://jakmall.id/2019/10/images/products/347300/thumbnail/wk-earphone-earphone-hifi-35mm-with-mic-y10.jpg?s=85577d922a916ae033ecf2d81ef338a9">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">WK Earphone Earpods HiFi 3.5mm with Mic - Y10</h4>
								<div class="pi_price">Rp 32.300 <span class="pi_sale">Rp 52.000</span></div>
							</div>
						</div>
					</div>
				
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://jakmall.id/images/products/9d2e5b/thumbnail/xiaomi-car-air-purifier.jpg?s=120e8ca2534feca98ef55d6f9715b791">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Jo & Nic Charlote Lace Christmas Dress - Red</h4>
								<div class="pi_price">Rp 847.700 <span class="pi_sale">Rp 1.168.800</span></div>
							</div>
						</div>
					</div>
				
					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//91/MTA-3368430/joyful-_joyful--yellow-duck-set-baju-anak_full02.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Joyful. Yellow Duck Set Baju Anak</h4>
								<div class="pi_price">Rp 65.000 <span class="pi_sale">Rp 90.000</span></div>
							</div>
						</div>
					</div>

					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//104/MTA-3669485/chicle_chicle-setelan-baju-anak-perempuan_full04.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">CHICLE Setelan Baju Anak Perempuan</h4>
								<div class="pi_price">Rp 105.000 <span class="pi_sale">Rp 185.000</span></div>
							</div>
						</div>
					</div>

					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//98/MTA-3499810/joyful-_joyful--motif-doraemon-set-baju-anak_full02.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Joyful. Motif Doraemon Set Baju Anak</h4>
								<div class="pi_price">Rp 65.800 <span class="pi_sale">Rp 90.300</span></div>
							</div>
						</div>
					</div>

					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//89/MTA-4087041/chicle_chicle_baju_anak_import_setelan_set_full03.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">CHICLE Set Baju Anak Perempuan</h4>
								<div class="pi_price">Rp 37.000 <span class="pi_sale">Rp 50.000</span></div>
							</div>
						</div>
					</div>

					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//86/MTA-2813162/jo---nic_jo---nic-charlote-lace-dress---dress-christmas---all-size_full03.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Jo & Nic Charlote Lace Christmas Dress - Red</h4>
								<div class="pi_price">Rp 113.000 <span class="pi_sale">Rp 140.000</span></div>
							</div>
						</div>
					</div>

					<div class="item col_homeProduct">
						<div class="box_homeProduct">
							<div class="mg_homeProduct">
								<img src="https://www.static-src.com/wcsstore/Indraprastha/images/catalog/medium//99/MTA-3419990/joyful-_joyful--sailboat-set-baju-anak_full02.jpg">
							</div>
							<div class="body_homeProduct">
								<h4 class="pi_title">Joyful. Sailboat Set Baju Anak</h4>
								<div class="pi_price">Rp 65.000 <span class="pi_sale">Rp 92.000</span></div>
							</div>
						</div>
					</div>

			</div>

		</div>


	</div>
</div>




<?php // ============= Pop up Gallery ================== ?>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<?php /*
<div class="row">
	<h2>Contoh Gallery Photoswipe : </h2>

	<div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">

		<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
		  <a href="https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg" itemprop="contentUrl" data-size="1024x768">
		      <img src="https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_m.jpg" itemprop="thumbnail" alt="Image description" />
		  </a>
		  <figcaption itemprop="caption description">Image caption 2.1</figcaption>
		</figure>

		<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
		  <a href="https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg" itemprop="contentUrl" data-size="1024x768">
		      <img src="https://farm7.staticflickr.com/6175/6176698785_7dee72237e_m.jpg" itemprop="thumbnail" alt="Image description" />
		  </a>
		  <figcaption itemprop="caption description">Image caption 2.2</figcaption>
		</figure>

		<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
		  <a href="https://farm6.staticflickr.com/5023/5578283926_822e5e5791_b.jpg" itemprop="contentUrl" data-size="1024x768">
		      <img src="https://farm6.staticflickr.com/5023/5578283926_822e5e5791_m.jpg" itemprop="thumbnail" alt="Image description" />
		  </a>
		  <figcaption itemprop="caption description">Image caption 2.3</figcaption>
		</figure>

	</div>
	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

	    <!-- Background of PhotoSwipe. 
	         It's a separate element, as animating opacity is faster than rgba(). -->
	    <div class="pswp__bg"></div>

	    <!-- Slides wrapper with overflow:hidden. -->
	    <div class="pswp__scroll-wrap">

	        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
	        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
	        <div class="pswp__container">
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	            <div class="pswp__item"></div>
	        </div>

	        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
	        <div class="pswp__ui pswp__ui--hidden">

	            <div class="pswp__top-bar">

	                <!--  Controls are self-explanatory. Order can be changed. -->

	                <div class="pswp__counter"></div>

	                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

	                <button class="pswp__button pswp__button--share" title="Share"></button>

	                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

	                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

	                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
	                <!-- element will get class pswp__preloader--active when preloader is running -->
	                <div class="pswp__preloader">
	                    <div class="pswp__preloader__icn">
	                      <div class="pswp__preloader__cut">
	                        <div class="pswp__preloader__donut"></div>
	                      </div>
	                    </div>
	                </div>
	            </div>

	            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
	                <div class="pswp__share-tooltip"></div> 
	            </div>

	            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
	            </button>

	            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
	            </button>

	            <div class="pswp__caption">
	                <div class="pswp__caption__center"></div>
	            </div>

	          </div>

	        </div>

	</div>
</div>
*/ ?>
<?php // ============= Pop up Gallery ================== ?>


</article>
<?php endwhile; ?>
<?php else : ?>
<article id="post-not-found" class="hentry clearfix">
		<header class="article-header">
			<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
		</header>
		<section class="entry-content">
			<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
		</section>
		<footer class="article-footer">
				<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
		</footer>
</article>
<?php endif; ?>
<?php get_footer(); ?>