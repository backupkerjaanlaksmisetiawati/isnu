<?php
/*
Author: Drife 2019
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/*********************************** INCLUDE NEEDED FILES ***********************************/
	/*
	1. library/bones.php
		- head cleanup (remove rsd, uri links, junk css, ect)
		- enqueueing scripts & styles
		- theme support functions
		- custom menu output & fallbacks
		- related post function
		- page-navi function
		- removing <p> from around images
		- customizing the post excerpt
		- custom google+ integration
		- adding custom fields to user profiles
	*/
	require_once( 'library/bones.php' ); // if you remove this, bones will break
	/*
	2. library/custom-post-type.php
		- an example custom post type
		- example custom taxonomy (like categories)
		- example custom taxonomy (like tags)
	*/
	// require_once( 'library/custom-post-type.php' ); // you can disable this if you like
	/*
	3. library/admin.php
		- removing some default WordPress dashboard widgets
		- an example custom dashboard widget
		- adding custom login css
		- changing text in footer of admin
	*/
	// require_once( 'library/admin.php' ); // this comes turned off by default
	/*
	4. library/translation/translation.php
		- adding support for other languages
	*/
	// require_once( 'library/translation/translation.php' ); // this comes turned off by default
	/*
	5. custom/taxonomy/(any).php
		- add custom taxonomy without WCK
		- please create file first in 'custom/taxonomy' folder
		- change (any).php to your custom taxonomy file e.g custom/taxonomy/example.php

		please don't delete this
	*/
	require_once( 'custom/taxonomy/publikasi/tipe-publikasi.php' );
	require_once( 'custom/taxonomy/publikasi/tahun-publikasi.php' );
	require_once( 'custom/taxonomy/video/kategori-video.php' );
	require_once( 'custom/taxonomy/program/kategori-program.php' );
	require_once( 'custom/taxonomy/kepengurusan/posisi-kepengurusan.php' );
	require_once( 'custom/taxonomy/opini/kategori-opini.php' );
	require_once( 'custom/taxonomy/opini/tag-opini.php' );
	/*
	6. custom/post-type/(any).php
		- add custom post-type without WCK
		- please create file first in 'custom/post-type' folder
		- change (any).php to your custom post-type file e.g custom/post-type/example.php

		please don't delete this
	*/
	require_once( 'custom/post-type/banner.php' );
	require_once( 'custom/post-type/kepengurusan.php' );
	require_once( 'custom/post-type/advertising.php' );
	/*
	7. custom/meta/(any).php
		- add custom meta without Custom Fields
		- please create file first in 'custom/meta' folder
		- change (any).php to your custom meta file e.g custom/meta/example.php

		please don't delete this
	*/
	// require_once( 'custom/meta/example.php' );
	/*
	8. other functions
	*/
	get_template_part( 'function', 'ajax' );
	get_template_part( 'function', 'crons' );
	get_template_part( 'function', 'custom-wpadmin' );
	get_template_part( 'function', 'datas' );
	get_template_part( 'function', 'migrations' );
/*********************************** INCLUDE NEEDED FILES **********************************/


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select 
the new images sizes you have just created from within the media manager 
when you add media to your content blocks. If you add more image sizes, 
duplicate one of the lines in the array and name it according to your 
new image size.
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<?php // custom gravatar call ?>
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<?php /*
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				*/ ?>	
				<?php // end custom gravatar call ?>
				<b><?php printf(__( '<cite class="fn">%s</cite>', 'bonestheme' ), get_comment_author_link()) ?></b> |

				<time class="comment_time" datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>
				<?php // edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<?php // </li> is added by WordPress automatically ?>
<?php } // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __( 'Search for:', 'bonestheme' ) . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search the Site...', 'bonestheme' ) . '" />
	<input type="submit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" />
	</form>';
	return $form;
} // don't remove this bracket!

//---------------- start ----------------------------
function main_init_js(){
	if (!is_admin()){
		// $url = 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'; // the URL to check against
		$url = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';
		$test_url = @fopen($url,'r'); // test parameters
		if($test_url !== false){ // test if the URL exists
			wp_deregister_script('jquery'); // initiate the function  
			wp_register_script('jquery', $url, false, '1.10.2',true);
			wp_enqueue_script('jquery');
		}
	// don't remove this
	wp_enqueue_script('jquery-1', get_template_directory_uri().'/library/js/jquery-1.11.3.min.js', array('jquery'), '3',true);

	//add stylesheet below
	wp_enqueue_style('bootstrap-css', get_template_directory_uri().'/library/css/bootstrap.css', false, '3', 'all' );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/library/css/all.min.css');
	wp_enqueue_style('animate-css', get_template_directory_uri().'/library/css/animate.css', false, '3', 'all' );
	wp_enqueue_style('carousel-css', get_template_directory_uri().'/owlcarousel/assets/owl.carousel.min.css', false, '3', 'all' );
	wp_enqueue_style('carousel-theme-css', get_template_directory_uri().'/owlcarousel/assets/owl.theme.default.min.css', false, '3', 'all' );

	wp_enqueue_style('photoswipe-css', get_template_directory_uri().'/library/css/photoswipe.css', false, '3', 'all' );
	wp_enqueue_style('photoswipe-skin-css', get_template_directory_uri().'/library/css/default-skin/default-skin.css', false, '3', 'all' );

	//add javascript below
	wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/library/js/bootstrap.min.js', array('jquery'), '3',true);
	wp_enqueue_script('font-awesome-js', get_template_directory_uri().'/library/js/all.min.js', array('jquery'), '3',true);
	// wp_enqueue_script('smoothscroll-js', get_template_directory_uri().'/library/js/smoothscroll.js', array('jquery'), '3',true);
	wp_enqueue_script('carousel-js', get_template_directory_uri().'/owlcarousel/owl.carousel.min.js', array('jquery'), '3',true);

	wp_enqueue_style('magnific-css', get_template_directory_uri().'/library/css/magnific-popup.css', false, '3', 'all' );
	wp_enqueue_script('magnific-js', get_template_directory_uri().'/library/js/jquery.magnific-popup.js', array('jquery'), '3',true);

	wp_enqueue_script('photoswipe-js', get_template_directory_uri().'/library/js/photoswipe.min.js', array('jquery'), '3',true);
	wp_enqueue_script('photoswipe-ui-js', get_template_directory_uri().'/library/js/photoswipe-ui-default.min.js', array('jquery'), '3',true);

		wp_enqueue_style(
			'jqueryui-css',
			get_template_directory_uri().'/library/css/jquery-ui/v1.12.1/jquery-ui.css',
			false,
			'1.12.1',
			'all'
		);
		wp_enqueue_style(
			'jqueryui-style',
			get_template_directory_uri().'/library/css/jquery-ui/v1.12.1/style-jquery-ui.css',
			false,
			'1.12.1',
			'all'
		);
		wp_enqueue_script(
			'jqueryui-js',
			get_template_directory_uri().'/library/js/jquery-ui/v1.12.1/jquery-ui.js',
			array('jquery'),
			'1.12.1',
			true
		);

		wp_enqueue_style(
			'select2-css',
			get_template_directory_uri().'/library/css/select2.min.css',
			false,
			'4.0.10',
			'all'
		);
		wp_enqueue_script(
			'select2-js',
			get_template_directory_uri().'/library/js/select2.min.js',
			array('jquery'),
			'4.0.10',
			true
		);

		wp_enqueue_style(
			'fancybox-css', 
			get_template_directory_uri().'/library/css/jquery.fancybox.min.css', 
			false, 
			'3.5.7', 
			'all'
		);
		wp_enqueue_script(
			'fancybox-js',
			get_template_directory_uri().'/library/js/jquery.fancybox.min.js',
			array('jquery'),
			'3.5.7',
			true
		);

	}
}
add_action('wp_enqueue_scripts', 'main_init_js');



//----------------------------------------custom--------------------------------------------------------//
require_once('library/project_posttype.php');


add_filter( 'show_admin_bar', '__return_false' );
function hide_abar() {
?>
	<style type="text/css">
		.show-admin-bar {
			display: none;
		}
	</style>
<?php
}

function hide_a_bar() {
    add_filter( 'show_admin_bar', '__return_false' );
    add_action( 'admin_print_scripts-profile.php', 
         'hide_abar' );
}
add_action( 'init', 'hide_a_bar' , 9 );

add_action('init', 'script_js');
function script_js() {
	if (!is_admin()) {
		wp_enqueue_script('script_js', get_bloginfo('template_url') . '/library/js/myajax.js', array('jquery'), '1',true);
		$protocol = isset( $_SERVER["HTTPS"]) ? 'https://' : 'http://';
		$params = array(
			'ajaxurl'=>admin_url('admin-ajax.php', $protocol)
		);
		wp_localize_script('script_js', 'ajaxscript', $params);
	}
}

function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

function my_login_logo() { ?>
    <style type="text/css">
    	body.login{
    		background: transparent linear-gradient(108deg, #007E27 0%, #006714 100%) 0% 0% no-repeat padding-box;
    	}
        #login h1 a, .login h1 a {
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/main_logo.png);
        height: 65px;
        width: auto;
        background-size: auto 90px;
        background-repeat: no-repeat;
        padding-bottom: 30px;
        }
       /* #nav,#loginform{
          display: none !important;
        }*/
        .login #backtoblog, .login #nav{
          text-align: center;
        }
        #wp-submit{
            background: #FFCE00;
		    width: 100%;
		    line-height: 30px;
		    font-size: 13px;
		    color: #000;
		    font-weight: 600;
		    text-transform: uppercase;
		    letter-spacing: .3px;
		    text-align: center;
		    border: 0;
		    border-radius: 20px;
		    text-shadow: none;
    		box-shadow: none;
    		margin-top: 15px;
        }
        .login label {
		    font-size: 13px;
		    font-weight: 600;
		}
		.login form .input, .login input[type=text]{
			line-height: 35px;
			font-size: 14px !important;
			padding: 0px 10px 0px 10px !important;
		}
        #login{

        	padding-top: 2% !important;
        }
        .login #backtoblog a, .login #nav a {
		    text-decoration: none;
		    color: #fff !important;
		}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Redirect Login Error
add_action( 'wp_login_failed', 'my_front_end_login_fail' );
function my_front_end_login_fail( $username ) {
     $referrer = $_SERVER['HTTP_REFERER'];
     if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')     ) {
      $linkParts = explode('?', $referrer);
      // $new_link = $linkParts[0]; // no use anymore
      $new_link = home_url(); // give the base url https://revamp.iqos.id/ 
          wp_redirect( $new_link . '/login/?login=failed' );
          exit();
     }
}

function wpse_lost_password_redirect() {
     $referrer = $_SERVER['HTTP_REFERER'];
    //  if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')     ) {
    //   $linkParts = explode('?', $referrer);
    //   // $new_link = $linkParts[0]; // no use anymore
    //   $new_link = home_url(); // give the base url https://revamp.iqos.id/ 
    //       wp_redirect( $new_link . '/login/?login=failed' );
    //       exit();
		//  }
		return home_url();
}
add_action('lostpassword_redirect', 'wpse_lost_password_redirect');

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_regex($post) {
	// if ( !preg_match('/^(?=[a-z]{2})(?=.{4,26})(?=[^.]*\.?[^.]*$)(?=[^_]*_?[^_]*$)[\w.]+$/iD', $post) ) {
	if( !preg_match('/^[a-z0-9 .\-]+$/i', $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_regex', 'check_regex', 0 );

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_regex_address($post) {
	if( !preg_match('/^[a-z0-9 .\/-]+$/i', $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_regex_address', 'check_regex_address', 0 );

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_url_regex($post) {
	if( !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_url_regex', 'check_url_regex', 0 );

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_date_regex($post) {
	if( !preg_match('/[\d\/]+/', $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_regex', 'check_regex', 0 );

/**
 * Debug Array or Object
 * supaya g usah nulis echo print_r blablabla
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function ddbug($array, $is_exit=true, $type="print_r") {
	echo '<pre>';
	if( $type === 'print_r' ) {
		print_r($array);
	}
	if( $type === 'var_dump' ) {
		var_dump($array);
	}
	echo'</pre>';
	if( $is_exit === true ) {
		exit;
	}
}
add_action( 'ddbug', 'ddbug', 0 );

/**
 * for log, by create log file
 * dont forget to set WP_DEBUG_LOG on wp-config to true first
 * 
 * @param string  $arMsg
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function ddlog($arMsg) {
	$arLogData['event_datetime'] = '['.date('Y-m-d h:i:s A').'] [client '.$_SERVER['REMOTE_ADDR'].']';

	$stEntry = "";
	$stEntry .= $arLogData['event_datetime']." ".$arMsg."\n";  

	$stCurLogFileName = date('Ymd').'.log';

	if(WP_DEBUG_LOG == true) {
		$dirname = "log";

		if (!file_exists($dirname)) {
			mkdir($dirname, 0777);
		}

		$stCurLogFileName = $dirname . "/" . $stCurLogFileName;

		$fHandler = fopen($stCurLogFileName,'a+');
		fwrite($fHandler,$stEntry);
		fclose($fHandler);
	}
}
add_action( "ddlog", "ddlog", 0 );

/**
 * Pagination
 * 
 * param reference example
 * $param = array(
 * 		'base'				=> 'http://localhost/isnu/news/',
 * 		'page'				=> $page,
 * 		'pages' 			=> $pages,
 * 		'key'					=> 'hal',
 * 		'next_text'		=> 'Next',
 * 		'prev_text'		=> 'Prev',
 * 		'first_text'	=> 'First',
 * 		'last_text'		=> 'Last',
 * 		'show_dots'		=> false
 * );
 * 
 * param detail:
 * base				: (string - mandatory)	current page
 * page				: (integer - mandatory)	current page
 * pages			: (integer - mandatory)	total amount of pages
 * key				: (string - optional)		parameters name, default value 'hal'.
 * 																		please don't use 'page' or 'paged' it'll replaced by wordpress (yea a.n.n.o.y.i.n.g)
 * next_text	: (string - optional)		default value is 'Next'
 * prev_text	: (string - optional)		default value is 'Prev'
 * first_text	: (string - optional)		default value is 'First'
 * last_text	: (string - optional)		default value is 'Last'
 * show_dots	: (boolean - optional)	send "true" if you want to shows "..." dots, by default its false
 *
 * @param array		$param		parameters list
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function pagination($param) {
	$key = (isset($param['key'])) ? $param['key'] : 'hal';
	$next_text = (isset($param['next_text'])) ? $param['next_text'] : 'Next';
	$prev_text = (isset($param['prev_text'])) ? $param['prev_text'] : 'Prev';
	$first_text = (isset($param['first_text'])) ? $param['first_text'] : 'First';
	$last_text = (isset($param['last_text'])) ? $param['last_text'] : 'Last';
	$page = $param['page'];
	$pages = $param['pages'];
	$base = $param['base'];
	$prevs = array();
	for ($p=1; $p<=4; $p++){
		$prevs[] = $page-$p;
	}sort($prevs);
	$nexts = array();
	for ($n=1; $n<=2; $n++){
		$nexts[] = $page+$n;
	}sort($nexts);
	$html = '';
	if($pages>5 && $page!=1) {
		$html .= '<a class="first" href="' . $base . $key . '=1">' . $first_text . '</a> ';
	}
	if($page!=1) {
		$html .= '<a class="prev" href="' . $base . $key . '=' . $prevs[3] . '">' . $prev_text . '</a> ';
	}
	if($pages>5) {
		if($page<5) {
			for ($i=1; $i<=5 ; $i++){
				$active = ($page==$i) ? 'color:#007E27':'';
				$html .= '<a href="' . $base . $key . '=' . $i . '" style="'. $active . '">'. $i . '</a> ';
			}
			if(isset($param["show_dots"]) && $param["show_dots"] === true) {
				$html .= '<span>...</span> ';
			}
		} else {
			if(isset($param["show_dots"]) && $param["show_dots"] === true) {
				if($page!=1 || $page==$pages) {
					$html .= '<span>...</span> ';
				}
			}
			if($page!=$pages) {
				unset($prevs[0]);
				unset($prevs[1]);
			}
			foreach($prevs as $prev) {
				if($page!=1 || $page==$pages) {
					$html .= '<a href="' . $base . $key . '=' . $prev . '">' . $prev . '</a> ';
				}
			}
			$html .= '<a href="' . $base . $key . '=' . $page . '" style="color:#007E27">' . $page . '</a> ';
			if($page!=$pages) {
				foreach($nexts as $next) {
					if($next<=$pages) {
						$html .= '<a href="' . $base . $key . '=' . $next . '">' . $next . '</a> ';
					}
				}
			}
			if(isset($param["show_dots"]) && $param["show_dots"] === true) {
				if($page!=$pages) {
					$html .= '<span>...</span> ';
				}
			}
		}
	} else {
		for ($i=1; $i<=$pages ; $i++){
			$active = ($page==$i) ? 'active':'';
			$html .= '<a href="' . $base . $key . '='. $i . '" class="'. $active . '">'. $i . '</a> ';
		}
	}
	if($page!=$pages && $nexts[0]<=$pages) {
		$html .= '<a class="next" href="' . $base . $key . '='. $nexts[0] . '">' . $next_text . '</a> ';
	}
	if($pages>5 && $page!=$pages && $nexts[0]<=$pages) {
		$html .= '<a class="last" href="' . $base . $key . '='. $pages . '">' . $last_text . '</a> ';
	}
	echo $html;
}

/**
 * Template for email
 * 
 * @param string		$message		message content of email
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function template_email($message) {
	$html = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\" ";
		$html .= "style=\"border-collapse:collapse;border-spacing:0;vertical-align:top;height:100%!important;width:100%!important;min-width:100%;box-sizing:border-box;background-color:#f1f1f1;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;margin:0;Margin:0;text-align:left;font-size:14px;line-height:140%\">";
		$html .= "<tbody>";
			$html .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
				$html .= "<td align=\"center\" valign=\"top\"";
					$html .= "style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:50px 0px;margin:0;Margin:0;font-size:14px;line-height:140%;text-align:center\">";

					$html .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" ";
						$html .= "style=\"border-collapse:collapse;border-spacing:0;padding:0;vertical-align:top;width:600px;margin:0 auto 0 auto;Margin:0 auto 0 auto;text-align:inherit\">";

						$html .= "<tbody>";
							$html .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
								$html .= "<td align=\"center\" valign=\"middle\" ";
									$html .= "style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;font-size:14px;line-height:140%;text-align:center;padding:30px 30px 22px 30px">
									$html .= "<img width=\"250\" alt=\"WP Mail SMTP Logo\"";
										$html .= "style=\"outline:none;text-decoration:none;max-width:100%;clear:both;display:inline-block!important;width:150px\" src=\"" . get_stylesheet_directory_uri() . "/library/images/main_logo.png\">";
								$html .= "</td>";
							$html .= "</tr>";

							$html .= $message;

						$html .= "</tbody>";
					$html .= "</table>";
				$html .= "</td>";
			$html .= "</tr>";
		$html .= "</tbody>";
	$html .= "</table>";
	
	return $html;
}
add_action( "template_email", "template_email", 0 );

/**
 * sending email notification after member approved
 *
 * @param object		$dataUser			[description of param]
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function send_email_wpadmin($dataUser) {
	$username = str_replace(' ', '', $dataUser->display_name); //untuk di user_login dan user_nicename
	$password = $username.'ISNU100%'; //hardcoded

	$email = $dataUser->user_email;
	
	/* member */
		$html_message_member = "";
		$html_message_member .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
			$html_message_member .= "<td align=\"left\" valign=\"top\" style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;text-align:left;font-size:14px;line-height:140%;background-color:#ffffff;padding:60px 75px 45px 75px;border-right:1px solid #ddd;border-bottom:1px solid #ddd;border-left:1px solid #ddd;border-top:3px solid #809eb0\">";
				$html_message_member .= "<div style=\"text-align:center\">";
					$html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;line-height:140%;font-size:20px;text-align:center;margin:0 0 20px 0;Margin:0 0 20px 0\">";
						$html_message_member .= "Selamat! Akun Anda telah di approve";
					$html_message_member .= "</p>";
					$html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;text-align:left;line-height:140%;margin:0 0 15px 0;Margin:0 0 15px 0;font-size:16px\">";
						$html_message_member .= "Akun membership Anda telah di approve dan akun Anda adalah sebagai berikut:";
					$html_message_member .= "</p>";
					$html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;text-align:left;line-height:140%;margin:0 0 15px 0;Margin:0 0 15px 0;font-size:16px\">";
						$html_message_member .= "Email: " . $email . "<br />";
						$html_message_member .= "Password: " . $password;
					$html_message_member .= "</p>";
					$html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;text-align:left;line-height:140%;margin:0 0 15px 0;Margin:0 0 15px 0;font-size:16px\">";
						$html_message_member .= "Anda sudah dapat berbagi publikasi ilmiah.";
					$html_message_member .= "</p>";
				$html_message_member .= "</div>";
			$html_message_member .= "</td>";
		$html_message_member .= "</tr>";

		$html_message_member .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
			$html_message_member .= "<td align=\"left\" valign=\"top\" style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;font-size:14px;line-height:140%;background-color:#f8f8f8;border-top:1px solid #dddddd;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;border-left:1px solid #dddddd;text-align:center!important;padding:30px 75px 25px 75px\">";
				$html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;line-height:140%;font-size:13px;text-align:center;margin:0 0 10px 0\">";
					$html_message_member .= "Segera ubah password Anda dan Anda dapat memulai untuk publish ilmiah Anda dengan meng-klik button di bawah ini";
				$html_message_member .= "</p>";
				$html_message_member .= "<div style=\"width:100%;text-align:center;\">";
					$html_message_member .= "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;vertical-align:top;text-align:left;color:#e27730;width:100%!important\">";
						$html_message_member .= "<tbody>";
							$html_message_member .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
								$html_message_member .= "<td tyle=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;text-align:left;font-size:14px;line-height:100%;padding:20px 0 20px 0\">";
									$html_message_member .= "<table style=\"border-collapse:collapse;border-spacing:0;padding:0;vertical-align:top;text-align:left;width:100%!important\">";
										$html_message_member .= "<tbody>";
											$html_message_member .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
												$html_message_member .= "<td style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:14px 20px 12px 20px;margin:0;Margin:0;font-size:14px;text-align:center;color:#ffffff;background:#007E27;border:1px solid #093c1a;border-bottom:3px solid #093c1a;line-height:100%\">";
													$html_message_member .= "<a href=\"" . home_url() . "/wp-admin\" style=\"margin:0;Margin:0;font-family:Helvetica,Arial,sans-serif;font-weight:bold;color:#ffffff;text-decoration:none;display:block;border:0 solid #093c1a;line-height:100%;padding:14px 20px 12px 20px;font-size:18px;text-align:center;width:100%;padding-left:0;padding-right:0;display:block;\">";
														$html_message_member .= "Publish Ilmiah Sekarang";
													$html_message_member .= "</a>";
												$html_message_member .= "</td>";
											$html_message_member .= "</tr>";
										$html_message_member .= "</tbody>";
									$html_message_member .= "</table>";
								$html_message_member .= "</td>";
							$html_message_member .= "</tr>";
						$html_message_member .= "</tbody>";
					$html_message_member .= "</table>";
				$html_message_member .= "</div>";
			$html_message_member .= "</td>";
		$html_message_member .= "</tr>";

		$message_member = template_email($html_message_member);
		$rephead_member = array();
		$rephead_member[] = 'MIME-Version: 1.0' . "\r\n";
		$rephead_member[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$rephead_member[] = "From: ". "Ikatan Sarjana Nahdlatul Ulama (ISNU) <noreply@pp-isnu.or.id>" . "\r\n";
		
		wp_mail(trim(strip_tags($email)), "[ISNU] Membership ISNU Anda telah kami approve.", $message_member, $rephead_member);
	/* member */

	// echo 'Sukses';
	// die();
}
add_action( "send_email_wpadmin", "send_email_wpadmin", 0 );


/***************** FILE UPLOAD *****************/
	/**
	 * Upload User's Sertifikat file
	 *
	 * @param array		$file			[description of param]
	 * @return void
	 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
	 */
	function upload_user_file( $file = array() ) {    
			require_once( ABSPATH . 'wp-admin/includes/admin.php' );
			$file_return = wp_handle_upload( $file, array('test_form' => false ) );
			if( isset( $file_return['error'] ) || isset( $file_return['upload_error_handler'] ) ) {
					return false;
			} else {
					$filename = $file_return['file'];
					$attachment = array(
							'post_mime_type' => $file_return['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => $file_return['url']
					);
					$attachment_id = wp_insert_attachment( $attachment, $file_return['url'] );
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attachment_data = wp_generate_attachment_metadata( $attachment_id, $filename );
					wp_update_attachment_metadata( $attachment_id, $attachment_data );
					if( 0 < intval( $attachment_id ) ) {
						return $attachment_id;
					}
			}
			return false;
	}
/***************** FILE UPLOAD *****************/

// function register_team_show_case_setting() {
// 	//register our settings
// 	register_setting('my_team_show_case_setting', 'my_file_upload');
// }
// add_action('admin_init', 'register_team_show_case_setting');

// function logout_without_confirm($action)
// {
// 	/**
// 	 * Allow logout without confirmation
// 	 */
// 	if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
// 		// $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : 'url-you-want-to-redirect';
// 		$location = str_replace('&amp;', '&', wp_logout_url(get_home_url()));
// 		// header("Location: $location");
// 		echo $location;
// 	}
// 	die();
// }
// add_action('check_admin_referer', 'logout_without_confirm', 10, 2);