<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom/taxonomy/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_taxonomy(
  'kategori-opini',
  array('opini'),
  array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Kategori Opini', 'bonestheme' ),
      'singular_name' => __( 'Kategori Opini', 'bonestheme' ),
      'search_items' =>  __( 'Search Kategori Opini', 'bonestheme' ),
      'all_items' => __( 'All Kategori Opini', 'bonestheme' ),
      'parent_item' => __( 'Parent Kategori Opini', 'bonestheme' ),
      'parent_item_colon' => __( 'Parent Kategori Opini:', 'bonestheme' ),
      'edit_item' => __( 'Edit Kategori Opini', 'bonestheme' ),
      'update_item' => __( 'Update Kategori Opini', 'bonestheme' ),
      'add_new_item' => __( 'Add New Kategori Opini', 'bonestheme' ),
      'new_item_name' => __( 'New Kategori Opini Name', 'bonestheme' )
    ),
    'show_admin_column' => true, 
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
      'slug' => 'kategori-opini',
      'with_front' => true
    ),
  )
);
?>