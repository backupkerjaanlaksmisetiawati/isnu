<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom/taxonomy/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_taxonomy(
  'posisi-kepengurusan',
  array('kepengurusan'),
  array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Posisi Kepengurusan', 'bonestheme' ),
      'singular_name' => __( 'Posisi Kepengurusan', 'bonestheme' ),
      'search_items' =>  __( 'Search Posisi Kepengurusan', 'bonestheme' ),
      'all_items' => __( 'All Posisi Kepengurusan', 'bonestheme' ),
      'parent_item' => __( 'Parent Posisi Kepengurusan', 'bonestheme' ),
      'parent_item_colon' => __( 'Parent Posisi Kepengurusan:', 'bonestheme' ),
      'edit_item' => __( 'Edit Posisi Kepengurusan', 'bonestheme' ),
      'update_item' => __( 'Update Posisi Kepengurusan', 'bonestheme' ),
      'add_new_item' => __( 'Add New Posisi Kepengurusan', 'bonestheme' ),
      'new_item_name' => __( 'New Posisi Kepengurusan Name', 'bonestheme' )
    ),
    'show_admin_column' => true, 
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
      'slug' => 'posisi-kepengurusan',
      'with_front' => true
    ),
  )
);
?>