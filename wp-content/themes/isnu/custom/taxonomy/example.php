<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom-taxonomy/(any).php' untuk melihat cara men-include
 * 
 * thanks Eddie Machado for develop this
 * 
 * - Ryu Amy -
 */

// let's add new taxonomy (these act like categories)
// example:
// register_taxonomy( 'tipe_publikasi', /* slug of the custom taxonomy */ 
//   array('publikasi'), /* slug of post type where you want to put this taxonomy. make sure post type is available */
//   array('hierarchical' => true,     /* if this is true, it acts like categories */
//     'labels' => array(
//       'name' => __( 'Tipe Publikasi', 'bonestheme' ), /* name of the custom taxonomy */
//       'singular_name' => __( 'Tipe Publikasi', 'bonestheme' ), /* single taxonomy name */
//       'search_items' =>  __( 'Search Tipe Publikasi', 'bonestheme' ), /* search title for taxomony */
//       'all_items' => __( 'All Tipe Publikasi', 'bonestheme' ), /* all title for taxonomies */
//       'parent_item' => __( 'Parent Tipe Publikasi', 'bonestheme' ), /* parent title for taxonomy */
//       'parent_item_colon' => __( 'Parent Tipe Publikasi:', 'bonestheme' ), /* parent taxonomy title */
//       'edit_item' => __( 'Edit Tipe Publikasi', 'bonestheme' ), /* edit custom taxonomy title */
//       'update_item' => __( 'Update Tipe Publikasi', 'bonestheme' ), /* update title for taxonomy */
//       'add_new_item' => __( 'Add New Tipe Publikasi', 'bonestheme' ), /* add new title for taxonomy */
//       'new_item_name' => __( 'New Tipe Publikasi Name', 'bonestheme' ) /* name title for taxonomy */
//     ),
//     'show_admin_column' => true, 
//     'show_ui' => true,
//     'query_var' => true,
//     'rewrite' => array( 'slug' => 'custom-slug' ),
//   )
// );
	
/*
  looking for custom meta boxes?
  check out this fantastic tool:
  https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
*/	

?>