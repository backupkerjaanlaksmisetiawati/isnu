<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom/taxonomy/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_taxonomy(
  'tahun-publikasi',
  array('publikasi'),
  array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Tahun Publikasi', 'bonestheme' ),
      'singular_name' => __( 'Tahun Publikasi', 'bonestheme' ),
      'search_items' =>  __( 'Search Tahun Publikasi', 'bonestheme' ),
      'all_items' => __( 'All Tahun Publikasi', 'bonestheme' ),
      'parent_item' => __( 'Parent Tahun Publikasi', 'bonestheme' ),
      'parent_item_colon' => __( 'Parent Tahun Publikasi:', 'bonestheme' ),
      'edit_item' => __( 'Edit Tahun Publikasi', 'bonestheme' ),
      'update_item' => __( 'Update Tahun Publikasi', 'bonestheme' ),
      'add_new_item' => __( 'Add New Tahun Publikasi', 'bonestheme' ),
      'new_item_name' => __( 'New Tahun Publikasi Name', 'bonestheme' )
    ),
    'show_admin_column' => true, 
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
      'slug' => 'tahun-publikasi',
      'with_front' => true
    ),
  )
);
?>