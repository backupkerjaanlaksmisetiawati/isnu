<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom/taxonomy/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_taxonomy(
  'kategori-video',
  array('video'),
  array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Kategori Video', 'bonestheme' ),
      'singular_name' => __( 'Kategori Video', 'bonestheme' ),
      'search_items' =>  __( 'Search Kategori Video', 'bonestheme' ),
      'all_items' => __( 'All Video Categories', 'bonestheme' ),
      'parent_item' => __( 'Parent Kategori Video', 'bonestheme' ),
      'parent_item_colon' => __( 'Parent Kategori Video:', 'bonestheme' ),
      'edit_item' => __( 'Edit Kategori Video', 'bonestheme' ),
      'update_item' => __( 'Update Kategori Video', 'bonestheme' ),
      'add_new_item' => __( 'Add New Kategori Video', 'bonestheme' ),
      'new_item_name' => __( 'New Kategori Video Name', 'bonestheme' )
    ),
    'show_admin_column' => true, 
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
      'slug' => 'kategori-video',
      'with_front' => true
    ),
  )
);
?>