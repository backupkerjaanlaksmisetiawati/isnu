<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom/taxonomy/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_taxonomy(
  'kategori-program',
  array('program'),
  array(
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Kategori Program', 'bonestheme' ),
      'singular_name' => __( 'Kategori Program', 'bonestheme' ),
      'search_items' =>  __( 'Search Kategori Program', 'bonestheme' ),
      'all_items' => __( 'All Program Categories', 'bonestheme' ),
      'parent_item' => __( 'Parent Kategori Program', 'bonestheme' ),
      'parent_item_colon' => __( 'Parent Kategori Program:', 'bonestheme' ),
      'edit_item' => __( 'Edit Kategori Program', 'bonestheme' ),
      'update_item' => __( 'Update Kategori Program', 'bonestheme' ),
      'add_new_item' => __( 'Add New Kategori Program', 'bonestheme' ),
      'new_item_name' => __( 'New Kategori Program Name', 'bonestheme' )
    ),
    'show_admin_column' => true, 
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array(
      'slug' => 'kategori-program',
      'with_front' => true
    ),
  )
);
?>