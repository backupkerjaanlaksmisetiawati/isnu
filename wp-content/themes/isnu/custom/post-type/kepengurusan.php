<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '6. custom/post-type/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */


register_post_type( 'kepengurusan', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	// let's now add all the options for this post type
	array( 'labels' => array(
		'name' => __( 'Kepengurusan', 'bonestheme' ), /* This is the Title of the Group */
		'singular_name' => __( 'Kepengurusan', 'bonestheme' ), /* This is the individual type */
		'all_items' => __( 'All Kepengurusan', 'bonestheme' ), /* the all items menu item */
		'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
		'add_new_item' => __( 'Add New Kepengurusan', 'bonestheme' ), /* Add New Display Title */
		'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
		'edit_item' => __( 'Edit Kepengurusan', 'bonestheme' ), /* Edit Display Title */
		'new_item' => __( 'New Kepengurusan', 'bonestheme' ), /* New Display Title */
		'view_item' => __( 'View Kepengurusan', 'bonestheme' ), /* View Display Title */
		'search_items' => __( 'Search Kepengurusan', 'bonestheme' ), /* Search Custom Type Title */ 
		'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
		'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
		'parent_item_colon' => ''
		), /* end of arrays */
		'description' => __( 'This is the example custom Kepengurusan', 'bonestheme' ), /* Custom Type Description */
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */ 
		// 'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
		'menu_icon' => 'dashicons-networking',
		'rewrite'	=> array( 'slug' => 'kepengurusan', 'with_front' => false ), /* you can specify its url slug */
		'has_archive' => 'kepengurusan', /* you can rename the slug here */
		'capability_type' => 'post',
		'hierarchical' => false,
		/* the next one is important, it tells what's enabled in the post editor */
		'supports' => array( 'title', 'author', 'thumbnail', 'custom-fields', 'revisions')
	) /* end of options */
); /* end of register post type */

/* this adds your post categories to your custom post type */
register_taxonomy_for_object_type( 'category', 'kepengurusan' );
/* this adds your post tags to your custom post type */
register_taxonomy_for_object_type( 'post_tag', 'kepengurusan' );
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/
	

?>
