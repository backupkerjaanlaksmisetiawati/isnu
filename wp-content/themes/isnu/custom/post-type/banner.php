<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '6. custom/post-type/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_post_type( 'banner',
	array(
    'labels' => array(
      'name' => __( 'Banner', 'bonestheme' ),
      'singular_name' => __( 'Banner', 'bonestheme' ),
      'all_items' => __( 'All Banner', 'bonestheme' ),
      'add_new' => __( 'Add New', 'bonestheme' ),
      'add_new_item' => __( 'Add New Banner', 'bonestheme' ),
      'edit' => __( 'Edit', 'bonestheme' ),
      'edit_item' => __( 'Edit Banner', 'bonestheme' ),
      'new_item' => __( 'New Banner', 'bonestheme' ),
      'view_item' => __( 'View Banner', 'bonestheme' ),
      'search_items' => __( 'Search Banner', 'bonestheme' ), 
      'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ),
      'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), 
      'parent_item_colon' => ''
    ),
		'description' => __( 'This is the example custom Banner', 'bonestheme' ),
		'public' => false,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' => 11, 
		'menu_icon' => 'dashicons-images-alt2',
		'rewrite'	=> false,
		'has_archive' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array( 'title', 'author', 'thumbnail', 'custom-fields', 'revisions')
	)
);

register_taxonomy_for_object_type( 'category', 'kepengurusan' );
register_taxonomy_for_object_type( 'post_tag', 'kepengurusan' );

?>
