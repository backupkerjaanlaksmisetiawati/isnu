<?php
	$per_page = '6';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
	$total_uri = count($req_uri);
	$slug = $req_uri[$total_uri-2];
  $current_category = get_term_by('slug', $slug, 'category');

	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$loopPage = new WP_Query(array( 'post_type'=>'post',
		'posts_per_page'=> '-1',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
    'category__in' => $current_category->term_id
	));
	$total = count($loopPage->posts);
	$pages = ceil($total/$per_page);

	$categories = getPostsCategories();
	$ads = getAds();
	$beritaTerbaru = getBerita('5');

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

  $loop = new WP_Query(array( 'post_type'=>'post',
    'posts_per_page'=> $per_page,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => $page,
    'category__in' => $current_category->term_id
  ));
?>

<div class="row_globalPage row_homeBerita">
  <h1 class="ht_home ht_homeRubrik">
    <?php echo $current_category->name; ?>
  </h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">JELAJAH KATEGORI</div>
		<select class="select-option select-basic-hide-search">
			<option value="<?php echo home_url() . '/category/'; ?>">Berita Terbaru</option>
			<?php foreach ( $categories as $category ) { ?>
        <option value="<?php echo home_url() . '/category/' . $category->slug; ?>"
          <?php echo ($current_category->slug == $category->slug) ? ' selected' : ''; ?>><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>

	<?php if(!empty($ads['ads_top'])) { ?>
		<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
			<img src="<?php echo $ads['ads_top']->banner; ?>">
		</a>
	<?php } ?>

  <div class="row row_listRubrikCategory">
    <div class="col-xs-12 col-md-12 col_bx_vrubrik">
      <?php if(!empty($loop->posts)) { ?>
        <div class="row box_v_listRubrik act">
          <div class="row">
            <?php
            while ( $loop->have_posts() ){
              $loop->the_post();
              $id_post = get_the_ID();
              $title_post = get_the_title($id_post);
              $short_name = get_the_title($id_post);
              if(strlen($short_name) > 38) $short_name = substr($short_name, 0, 38).'...';
              $link_post = get_the_permalink($id_post);
              $date_post = get_the_date('d F Y', $id_post);
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'medium' );
              if($thumb){
                $urlphoto = $thumb['0'];
              }else{
                $urlphoto = get_template_directory_uri().'/library/images/sorry.png';
              }
              $alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);
              $category_post = get_the_category($id_post);
            ?>
              <div class="col-xs-12 col-md-12 nopadding_left nopadding_right col_v_listRubrik">
                <div class="bxsm_listRubrik">
                  <div class="left_listRubrik">
                    <div class="mg_sm_rubrik">
                      <img src="<?php echo $urlphoto; ?>" style="<?php echo $style_emptyphoto; ?>" alt="<?php echo $alt; ?>">
                    </div>
                  </div>
                  <div class="right_listRubrik">
                    <a href="<?php echo $link_post; ?>" title="Lihat <?php echo $title_post; ?>">
                      <h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
                    </a>
                    <div class="info_sm_listRubrik">
                      <?php echo substr(get_the_excerpt($id_post), 0, 80); ?>...
                    </div>
                    <a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
                  </div>
                  <div class="bx_sm_rubrikCat">
                    <span class="l_cat"><?php echo $date_post; ?></span>
                    <span class="r_cat"><?php echo $category_post[0]->name; ?></span>
                  </div>
                </div>
              </div>
            <?php }wp_reset_postdata();  ?>
          </div>

          <?php if($total > $per_page) { ?>
            <div class="pagination">
              <?php
                pagination(
                  array(
							      'base'				=> home_url() . '/category/' . $current_category->slug . '?',
                    'page'				=> $page,
                    'pages' 			=> $pages,
                    'key'					=> 'hal',
                    'next_text'		=> '&rsaquo;',
                    'prev_text'		=> '&lsaquo;',
                    'first_text'	=> '&laquo;',
                    'last_text'		=> '&raquo;'
                  )
                );
              ?>
            </div>
          <?php } ?>
        </div>
      <?php } else { ?>
        <div id="post-not-found" class="hentry clearfix">
          <div class="article-header">
            <h4><?php _e( 'Tidak ada berita untuk kategori ini.', 'bonestheme' ); ?></h4>
          </div>
        </div>
      <?php } ?>

			<?php if(!empty($ads['ads_right'])) { ?>
				<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" style="margin:30px 0px 0px 0px;" target="_blank">
					<img src="<?php echo $ads['ads_right']->banner; ?>">
				</a>
			<?php } ?>
    </div>
    
    <div class="col-xs-12 col-md-12 col_bx_vrubrik">
      <?php if(!empty($beritaPopuler)) { ?>
        <div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
          <div class="row_articleTitle">Berita Populer</div>
          <ul class="listBeritaPopuler listBeritaPopulerRight">
            <?php foreach ( $beritaPopuler as $key => $berita ) { ?>
              <li>
                <a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
                  <span><?php echo $key+1; ?></span>
                  <span><?php echo $berita->post_title; ?></span>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      <?php } ?>
      
      <?php if(!empty($beritaTerbaru)) { ?>
        <div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
          <div class="row_articleTitle">Berita Terbaru</div>
          <ul class="listBeritaPopuler listBeritaPopulerRight">
            <?php foreach ( $beritaTerbaru as $key => $berita ) { ?>
              <li>
                <a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
                  <span><?php echo $key+1; ?></span>
                  <span><?php echo $berita->post_title; ?></span>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      <?php } ?>

			<?php if(!empty($ads['ads_bottom'])) { ?>
				<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" style="margin:30px 0px 0px 0px;" target="_blank">
					<img src="<?php echo $ads['ads_bottom']->banner; ?>">
				</a>
			<?php } ?>
    </div>
  </div>
</div>