<?php
	global $wpdb;

	$id_page = $wpdb->get_results(
		"SELECT ID FROm unsi_posts WHERE post_name = 'tentang-isnu'",
		OBJECT
	);
	$id_page = $id_page[0]->ID;

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
  $total_uri = count($req_uri);

  $slug = $req_uri[$total_uri-2];
  
	$current_category = get_term_by('slug', $slug, 'posisi-kepengurusan');
	// ddbug($current_category,true);
	
	$posisiCabangUtama = getPosisiKepengurusan(
		array(
			'is_pusat' => TRUE
		)
	);
	
	if($posisiCabangUtama != null) {
		$categoryPosisi = getPosisiKepengurusan(
			array(
				'parent' => $posisiCabangUtama->term_id
			)
		);
	}
?>

<?php /*
<div class="row row_globalPage row_tentangPage">

	<div class="mg_back_tentangPage">
		<img class="" src="<?php bloginfo('template_directory'); ?>/library/images/mg_tentang.png">
	</div>

	<div class="col-xs-3 nopadding_right col_left_tentang">

		<h1 class="ht_globalPage hide">Tentang ISNU</h1>

		<ul class="tabsTentang">
			<li class="act" data-id="tabs1-mobile"><a href="javascript:;" ><span class="glyphicon glyphicon-triangle-right choose_arrow"></span> Visi</a></li>
			<li class="" data-id="tabs2-mobile"><a href="javascript:;" ><span class="glyphicon glyphicon-triangle-right choose_arrow"></span> Misi</a></li>
		</ul>
		
	</div>
	<div id="tabs1-mobile" class="col-xs-9 nopadding_right col_content_tentang act animated fadeIn">
		
		<div class="row">
			<?php 
				$daftar = get_post_meta($id_page,'daftarvisi',true);
				if(isset($daftar) AND $daftar != ''){

					if(count($daftar) > 1){

						$no = 1;
						foreach ($daftar as $key => $value) {
			?>
							<div class="col-md-6 col_listTentang">
								<div class="numb_tentang"><?php echo $no; ?></div>
								<div class="content_tentang">
									<?php echo $value['visi-isnu'] ?>
								</div>
							</div>
				<?php
						$no++;
						}
					}else{
				?>
						<div class="box_cont_onevisi">
							<?php echo $daftar[0]['visi-isnu'] ?>
						</div>
				<?php
					}
				 } ?>
		</div>
		
	</div>
	<div id="tabs2-mobile" class="col-xs-9 nopadding_right col_content_tentang animated fadeIn">
		
		<div class="row">
			<?php 
				$daftar = get_post_meta($id_page,'daftarmisi',true);
				if(isset($daftar) AND $daftar != ''){
					$no = 1;
					foreach ($daftar as $key => $value) {
			?>
						<div class="col-xs-12 col_listTentang">
							<div class="numb_tentang"><?php echo $no; ?></div>
							<div class="content_tentang">
								<?php echo $value['misi-isnu'] ?>
							</div>
						</div>
				<?php
					$no++;
					}
				 } ?>
		</div>
		
	</div>
</div>
*/ ?>

<div class="row_globalPage" id="kepengurusan" style="border-top:none;">
	<h2 class="ht_kepengurusanTitle fira">KEPENGURUSAN ISNU</h2>
	<p class="ht_kepengurusanDescription">
		Berikut ini merupakan struktur organisasi kepengurusan ISNU tahun periode 2019 - 2024.
	</p>

	<p class="breadcrumb_kepengurusan">
		<a href="<?php echo home_url() . '/tentang-isnu'; ?>">Kepengurusan</a>
		<span>&rsaquo;</span>
		<a href="<?php echo home_url() . '/tentang-isnu/kepengurusan-isnu/'; ?>">Semua Pengurus</a>
		<span>&rsaquo;</span>
		<a class="active" href="<?php echo home_url() . '/posisi-kepengurusan/' . $current_category->slug; ?>">
			<?php echo ucwords(strtolower($current_category->name)); ?>
		</a>
	</p>

	<?php if($posisiCabangUtama != null) { ?>		
		<div class="newwrap_posisiKepengurusan">
			<div class="title_posisiKepengurusan">
				<div>
					<?php echo strtoupper($current_category->name) ?>
				</div>
			</div>

			<?php
				$categorySubPosisi = getPosisiKepengurusan(
					array(
						'parent' => $current_category->term_id,
						'limit' => 1,
						'noanggota' => TRUE
					)
				);
			?>

			<?php if($categorySubPosisi != null) { ?>
				<div class="newwrap_subPosisiKepengurusan">
					<?php foreach($categorySubPosisi as $sk => $catSub) { ?>
						<?php
							$listPengurus = getListKepengurusan(
								array(
									'category' => $catSub->term_id
								)
							);
						?>

						<?php if($listPengurus != null) { ?>
							<?php foreach($listPengurus as $pengurus) { ?> 
								<?php
									$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pengurus->ID), 'medium' );
									if($thumb) {
										$urlphoto = $thumb['0'];
										$urlphoto_empty = false;
									} else{
										$urlphoto = get_template_directory_uri().'/library/images/user.svg';
										$urlphoto_empty = true;
									}
								?>

								<div class="wrap_newProfileKepengurusan">
									<div class="newProfileKepengurusan clearfix">
										<div class="foto <?php echo ($urlphoto_empty === true)?'empty':''; ?>">
											<img src="<?php echo $urlphoto; ?>"
												alt="<?php echo $pengurus->post_title; ?>">
										</div>
										
										<div class="nama full">
											<div>
												<h6><?php echo $pengurus->post_title; ?></h6>
												<p><?php echo $catSub->name; ?></p>
											</div>
										</div>
									</div>

									<div class="newwrap_subPengurus" style="display:block">
										<?php
											$subPengurus = getPosisiKepengurusan(
												array(
													'parent' => $catSub->term_id
												)
											);
										?>

										<?php if($subPengurus != null) { ?>
											<?php foreach($subPengurus as $subPengurus) { ?> 
												<?php
													$listSubPengurus = getListKepengurusan(
														array(
															'category' => $subPengurus->term_id
														)
													);
												?>
												<?php if($listSubPengurus != null) { ?>
													<?php foreach($listSubPengurus as $listSubPengurus) { ?> 
														<?php
															$thumb2 = wp_get_attachment_image_src( get_post_thumbnail_id($listSubPengurus->ID), 'medium' );
															if($thumb2) {
																$urlphoto2 = $thumb2['0'];
																$urlphoto2_empty = false;
															} else{
																$urlphoto2 = get_template_directory_uri().'/library/images/user.svg';
																$urlphoto2_empty = true;
															}
														?>
														<div class="newProfileKepengurusan">
															<div class="foto <?php echo ($urlphoto2_empty === true)?'empty':''; ?>">
																<img src="<?php echo $urlphoto2; ?>"
																	alt="<?php echo $subPengurus->post_title; ?>">
															</div>
															
															<div class="nama full">
																<div>
																	<h6><?php echo $listSubPengurus->post_title; ?></h6>
																	<p><?php echo $subPengurus->name; ?></p>
																</div>
															</div>
														</div>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										<?php } ?>

										<?php
											$otherPosition = $wpdb->get_results(
												"SELECT unsi_terms.*,
														unsi_term_taxonomy.taxonomy,
														unsi_term_taxonomy.count
													FROM unsi_term_taxonomy
													JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
													WHERE unsi_term_taxonomy.taxonomy = 'posisi-kepengurusan' 
														AND unsi_terms.name NOT LIKE '%anggota%' 
														AND unsi_terms.sort != '1'
														AND unsi_term_taxonomy.parent = ".$current_category->term_id."
													ORDER BY unsi_terms.sort ASC",
												OBJECT
											);
										?>

										<?php if($otherPosition != null) { ?>
											<?php foreach($otherPosition as $subPengurus) { ?> 
												<?php
													$listSubPengurus = getListKepengurusan(
														array(
															'category' => $subPengurus->term_id
														)
													);
												?>
												<?php if($listSubPengurus != null) { ?>
													<?php foreach($listSubPengurus as $listSubPengurus) { ?> 
														<?php
															$thumb2 = wp_get_attachment_image_src( get_post_thumbnail_id($listSubPengurus->ID), 'medium' );
															if($thumb2) {
																$urlphoto2 = $thumb2['0'];
																$urlphoto2_empty = false;
															} else{
																$urlphoto2 = get_template_directory_uri().'/library/images/user.svg';
																$urlphoto2_empty = true;
															}
														?>
														<div class="newProfileKepengurusan">
															<div class="foto <?php echo ($urlphoto2_empty === true)?'empty':''; ?>">
																<img src="<?php echo $urlphoto2; ?>"
																	alt="<?php echo $subPengurus->post_title; ?>">
															</div>
															
															<div class="nama full">
																<div>
																	<h6><?php echo $listSubPengurus->post_title; ?></h6>
																	<p><?php echo $subPengurus->name; ?></p>
																</div>
															</div>
														</div>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										<?php } ?>

										<?php
											$anggota = $wpdb->get_results(
												"SELECT unsi_terms.*,
														unsi_term_taxonomy.taxonomy,
														unsi_term_taxonomy.parent,
														unsi_term_taxonomy.count
													FROM unsi_term_taxonomy
													JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
													WHERE unsi_term_taxonomy.taxonomy = 'posisi-kepengurusan' 
														AND unsi_terms.name LIKE '%anggota%'
														AND unsi_term_taxonomy.parent = ".$current_category->term_id."",
												OBJECT
											);
										?>
										
										<?php if(($listSubPengurus != null || $otherPosition != null) && $anggota != null) { ?>
											<div class="newwrap_subPengurus" style="display:block">
												<?php foreach($anggota as $anggotaPengurus) { ?> 
													<?php
														$listAnggota = getListKepengurusan(
															array(
																'category' => $anggotaPengurus->term_id
															)
														);
													?>
													<?php if($listAnggota != null) { ?>
														<?php foreach($listAnggota as $list_anggota) { ?> 
															<?php
																$thumb2 = wp_get_attachment_image_src( get_post_thumbnail_id($list_anggota->ID), 'medium' );
																if($thumb2) {
																	$urlphoto2 = $thumb2['0'];
																	$urlphoto2_empty = false;
																} else{
																	$urlphoto2 = get_template_directory_uri().'/library/images/user.svg';
																	$urlphoto2_empty = true;
																}
															?>
															<div class="newProfileKepengurusan">
																<div class="foto <?php echo ($urlphoto2_empty === true)?'empty':''; ?>">
																	<img src="<?php echo $urlphoto2; ?>"
																		alt="<?php echo $anggotaPengurus->post_title; ?>">
																</div>
																
																<div class="nama full">
																	<div>
																		<h6><?php echo $list_anggota->post_title; ?></h6>
																		<p><?php echo $anggotaPengurus->name; ?></p>
																	</div>
																</div>
															</div>
														<?php } ?>
													<?php } ?>
												<?php } ?>
											</div>
										<?php } elseif($anggota != null) { ?>
											<?php foreach($anggota as $anggotaPengurus) { ?> 
												<?php
													$listAnggota = getListKepengurusan(
														array(
															'category' => $anggotaPengurus->term_id
														)
													);
												?>
												<?php if($listAnggota != null) { ?>
													<?php foreach($listAnggota as $list_anggota) { ?> 
														<?php
															$thumb2 = wp_get_attachment_image_src( get_post_thumbnail_id($list_anggota->ID), 'medium' );
															if($thumb2) {
																$urlphoto2 = $thumb2['0'];
																$urlphoto2_empty = false;
															} else{
																$urlphoto2 = get_template_directory_uri().'/library/images/user.svg';
																$urlphoto2_empty = true;
															}
														?>
														<div class="newProfileKepengurusan">
															<div class="foto <?php echo ($urlphoto2_empty === true)?'empty':''; ?>">
																<img src="<?php echo $urlphoto2; ?>"
																	alt="<?php echo $anggotaPengurus->post_title; ?>">
															</div>
															
															<div class="nama full">
																<div>
																	<h6><?php echo $list_anggota->post_title; ?></h6>
																	<p><?php echo $anggotaPengurus->name; ?></p>
																</div>
															</div>
														</div>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>
			
			<?php /*						
			<a class="btn_detailKepengurusan">
				Lihat Anggota Lainnya <span>&rsaquo;</span>
			</a>
			*/ ?>
		</div>
	<?php } ?>
</div>