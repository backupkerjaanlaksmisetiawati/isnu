<?php
	$per_page = '10';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
  $total_uri = count($req_uri);
  $slug = $req_uri[$total_uri-2];
  
  $current_category = get_term_by('slug', $slug, 'kategori-opini');
	$opiniTerbaru = getOpini('5');
	$ads = getAds();
	$categories = getKategoriOpini();
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(
		getOpini(
			'-1',
			array(
				'cat' => $current_category->term_id
			)
		)
	);
	$pages = ceil($total/$per_page);
	
	$getOpini = getOpini(
		$per_page,
		array(
			'paged' => $page,
			'cat' => $current_category->term_id
		)
	);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
?>

<div class="row_globalPage row_homeBerita allow_ajax_opini">
	<h1 class="ht_home ht_homeRubrik">
		OPINI <?php echo $current_category->name; ?>
	</h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">JELAJAH KATEGORI</div>
		<select class="select-option select-basic-hide-search">
			<?php foreach ( $categories as $category ) { ?>
        <option value="<?php echo home_url() . '/kategori-opini/' . $category->slug; ?>"
          <?php echo ($current_category->slug == $category->slug) ? ' selected' : ''; ?>><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>

	<?php if(!empty($ads['ads_top'])) { ?>
		<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
			<img src="<?php echo $ads['ads_top']->banner; ?>">
		</a>
	<?php } ?>
		
	<div class="row row_listRubrikCategory">
		<div class="col-xs-12 col-md-12 col_bx_vrubrik">
			<?php if( !empty($getOpini) ) { ?>
				<div class="row_archiveArticle row">
					<?php foreach($getOpini as $val) { ?>
						<?php
							$link_post = get_the_permalink($val->ID);
							$short_name = (strlen($val->post_title) > 38) ? substr($val->post_title, 0, 38).'...' : $val->post_title;
						?>
						<div class="col-xs-12 col-md-12 nopadding_left nopadding_right col_v_listRubrik">
							<div class="bxsm_listRubrik">
								<div class="left_listRubrik">
									<div class="mg_sm_rubrik">
										<img src="<?php echo $val->foto; ?>" style="<?php echo $val->style_emptyphoto; ?>"
											alt="<?php echo $val->alt_foto; ?>">
									</div>
								</div>
								<div class="right_listRubrik">
									<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $title_post; ?>">
										<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
									</a>
									<div class="info_sm_listRubrik">
										<?php echo strip_tags(substr(get_the_excerpt($val->ID), 0,80)); ?>...
									</div>
									<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
								</div>
								<div class="bx_sm_rubrikCat">
									<span class="l_cat">
										<?php echo date('d F Y', strtotime($val->post_date)); ?>
									</span>
									<span class="r_cat">
										<?php echo get_comments_number($val->ID); ?> Komentar
									</span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>

				<?php if($total > $per_page) { ?>
					<div class="pagination">
						<?php
							pagination(
								array(
									'base'				=> home_url() . '/kategori-opini/' . $current_category->slug . '?',
									'page'				=> $page,
									'pages' 			=> $pages,
									'key'					=> 'hal',
									'next_text'		=> '&rsaquo;',
									'prev_text'		=> '&lsaquo;',
									'first_text'	=> '&laquo;',
									'last_text'		=> '&raquo;'
								)
							);
						?>
					</div>
				<?php } ?>
			<?php } else { ?>
				<div id="post-not-found" class="hentry clearfix">
					<div class="article-header">
						<h4><?php _e( 'Tidak ada opini untuk kategori ini.', 'bonestheme' ); ?></h4>
					</div>
				</div>
			<?php } ?>

			<?php if(!empty($ads['ads_right'])) { ?>
				<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" style="margin:30px 0px 0px 0px;" target="_blank">
					<img src="<?php echo $ads['ads_right']->banner; ?>">
				</a>
			<?php } ?>
		</div>

		<div class="col-xs-12 col-md-12 col_bx_vrubrik">
			<?php if(!empty($beritaPopuler)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Berita Populer</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $berita->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

			<?php if(!empty($opiniTerbaru)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Opini Terbaru</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $opiniTerbaru as $key => $val ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $val->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

			<?php if(!empty($ads['ads_bottom'])) { ?>
				<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" style="margin:30px 0px 0px 0px;" target="_blank">
					<img src="<?php echo $ads['ads_bottom']->banner; ?>">
				</a>
			<?php } ?>
		</div>
	</div>
</div>
