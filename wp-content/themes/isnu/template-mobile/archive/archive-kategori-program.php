<?php
	$per_page = '10';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
  $total_uri = count($req_uri);
  $slug = $req_uri[$total_uri-2];
  $current_category = get_term_by('slug', $slug, 'kategori-program');
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(
		getPrograms(
			'-1',
			array(
				'category'	=> $current_category->term_id
			)
		)
	);
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
	
	$ads = getAds();

	$getProgram = getPrograms(
		$per_page,
		array(
			'paged'			=> $page,
			'category'	=> $current_category->term_id
		)
	);
	$categories = get_terms(
		array(
			'taxonomy'    => 'kategori-program',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);
	$terbaru = getPrograms('5');
?>

<div class="row_globalPage row_homeBerita">
	<h1 class="ht_home ht_homeRubrik">
		<?php echo $current_category->name; ?>
	</h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">Jelajah Kategori</div>
		<select class="select-option select-basic-hide-search">
			<?php foreach ( $categories as $category ) { ?>
				<option value="<?php echo home_url(); ?>/kategori-program/<?php echo $category->slug; ?>"
          <?php echo ($current_category->slug == $category->slug) ? ' selected' : ''; ?>><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>
	<p>&nbsp;</p>
	
	<?php if( !empty($getProgram) ) { ?>
		<div id="wrap_load_program_mobile">
			<?php foreach($getProgram as $program) { ?>
				<div class="row wrap_progList">
					<div class="wrap_progImg">
						<div class="prog_img">
							<img src="<?php echo $program->photo; ?>" alt="<?php echo $program->alt_photo; ?>" />
						</div>
					</div>

					<div class="wrap_progTxt">
						<a href="<?php echo home_url(); ?>/program/<?php echo $program->post_name; ?>" class="prog_title">
							<?php
								$check_len = strlen( strip_tags($program->post_title) );
								if( $check_len > 50 ) {
									echo substr(strip_tags($program->post_title), 0, 50)  . '...';
								} else {
									echo strip_tags($program->post_title);
								}
							?>
						</a>

						<div class="prog_excerpt">
							<?php echo $program->cropped_content; ?>
						</div>
						
						<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
					</div>

					<div class="bx_sm_rubrikCat">
						<span class="l_cat"><?php echo date('d F Y', strtotime($program->post_date)); ?></span>
						<span class="r_cat"><?php echo $program->category_name; ?></span>
					</div>
				</div>
			<?php } ?>
		</div>

		<?php if($total > $per_page) { ?>
			<div class="pagination">
				<?php
					pagination(
						array(
							'base'				=> home_url() . '/kategori-program/' . $current_category->slug . '?',
							'page'				=> $page,
							'pages' 			=> $pages,
							'key'					=> 'hal',
							'next_text'		=> '&rsaquo;',
							'prev_text'		=> '&lsaquo;',
							'first_text'	=> '&laquo;',
							'last_text'		=> '&raquo;'
						)
					);
				?>
			</div>
		<?php } ?>
	<?php } else { ?>
		<div id="post-not-found" class="hentry clearfix">
			<div class="article-header">
				<h4><?php _e( 'Tidak ada program untuk kategori ini.', 'bonestheme' ); ?></h4>
			</div>
		</div>
	<?php } ?>

	<?php if(!empty($beritaPopuler)) { ?>
		<div class="row_articleCategories row_artikelPopuler">
			<div class="row_articleTitle">Berita Populer</div>
			<ul class="listBeritaPopuler listBeritaPopulerRight">
				<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
					<li>
						<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
							<span><?php echo $key+1; ?></span>
							<span><?php echo $berita->post_title; ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_right'])) { ?>
		<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" style="margin:0px 0px 30px 0px;" target="_blank">
			<img src="<?php echo $ads['ads_right']->banner; ?>">
		</a>
	<?php } ?>

	<?php if(!empty($terbaru)) { ?>
		<div class="row_articleCategories row_artikelPopuler">
			<div class="row_articleTitle">Program Terbaru</div>
			<ul class="listBeritaPopuler listBeritaPopulerRight">
				<?php foreach ( $terbaru as $key => $val ) { ?>
					<li>
						<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
							<span><?php echo $key+1; ?></span>
							<span><?php echo $val->post_title; ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<a href="<?php echo $ads['ads_bottom']->url; ?>" style="margin:30px 0px 0px 0px;" class="ik ik-bottom" target="_blank">
			<img src="<?php echo $ads['ads_bottom']->banner; ?>">
		</a>
	<?php } ?>
</div>