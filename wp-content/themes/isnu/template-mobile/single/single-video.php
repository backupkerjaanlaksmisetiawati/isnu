<?php // code here for single page 
	$id_post = $post->ID;

	$video_source = get_post_meta($post->ID, 'video_source', true);
	$upload_video = get_post_meta($post->ID, 'upload_video', true);
	if(!empty($upload_video)) {
		$upload_video = wp_get_attachment_url($upload_video);
	}
	$upload_video_ogg = get_post_meta($post->ID, 'upload_video_ogg', true);
	if(!empty($upload_video_ogg)) {
		$upload_video_ogg = wp_get_attachment_url($upload_video_ogg);
	}
	$video_youtube_id = get_post_meta($post->ID, 'video_youtube_id', true);

	$date_post = get_the_date('d F Y', $id_post);

	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'full' );
	if($thumb){
		$urlphoto = $thumb['0'];
	}else{
		$urlphoto = '';
	}
	$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);

	$rekomendasiVideos_category = get_the_terms($id_post, 'kategori-video');
	$rekomendasiVideos_params = array();
	if(isset($rekomendasiVideos_category[0])) {
		$rekomendasiVideos_params['cat'] = array($rekomendasiVideos_category[0]->term_id);
		$rekomendasiVideos_params['exclude'] = array($post->ID);
	}
	$rekomendasiVideos = getVideos(
		3,
		$rekomendasiVideos_params
	);
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	<div class="row_globalPage row_postDetail">
		<div class="wrap_videoAttachment">
			<?php if($video_source === 'upload_video' && (!empty($upload_video) || !empty($upload_video_ogg))){ ?>
				<video controls>
					<source src="<?php echo $upload_video; ?>" type="video/mp4">
					<?php if(!empty($upload_video_ogg)) { ?>
						<source src="<?php echo $upload_video_ogg; ?>" type="video/ogg">
					<?php } ?>
					Your browser does not support the video tag.
				</video>
			<?php } elseif($video_source === 'youtube' && !empty($video_youtube_id)) { ?>
				<iframe src="https://www.youtube.com/embed/<?php echo $video_youtube_id; ?>"
					frameborder="0"
					allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
					allowfullscreen></iframe>
			<?php } ?>
		</div>

		<h1 class="ht_htpostDetail">
			<?php echo get_the_title($post->ID); ?>
		</h1>

		<div class="bx_contentPost"><?php the_content(); ?></div>
		
		<h2 class="ht_rekomandasiVideo">Rekomendasi Video</h2>
		<?php foreach($rekomendasiVideos as $vid) { ?>
			<a href="<?php echo get_the_permalink($vid->ID); ?>"
				title="Lihat <?php echo $vid->post_title; ?>"
				class="row media_highlightSmall">
				<div class="col-xs-6 col-md-6 wrap_mediaHighlightSmallThumb">
					<div class="iconplay">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
					</div>
					<div class="media_highlightSmallThumb">
						<img src="<?php echo $vid->foto; ?>" 
							alt="<?php echo $vid->alt_foto; ?>" />
					</div>
				</div>

				<div class="col-xs-6 col-md-6 media_highlightSmallTxt">
					<h5>
						<?php
							echo (strlen($vid->post_title) > 50) ? 
								substr($vid->post_title, 0, 50).'...' : 
								$vid->post_title;
						?>
					</h5>
					<p>
						<?php echo date('d F Y', strtotime($vid->post_date)); ?>
					</p>
				</div>
			</a>
		<?php } ?>

		<div class="bx_commentPost">
			<h2 class="ht_relatedPost">Komentar</h2>
			<div class="sin_comment"><?php comments_template(); ?></div> 
		</div>
	</div>
</div>