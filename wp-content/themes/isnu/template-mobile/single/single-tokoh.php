<?php // code here for single page 
  $id_post = $post->ID;
  $tokoh = getProfileTokoh($id_post, false);
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>
  role="article" itemscope
  itemtype="http://schema.org/BlogPosting">
  <div class="row row_globalPage row_postDetail">
    <h3 class="tkh_profileTitle">
      <?php echo strtoupper($tokoh->post_title); ?>
    </h3>

    <?php if(!empty($tokoh->jabatan_di_indonesia)) { ?>
      <div>
        <h3 class="tkh_profileJabatan">
          <?php echo $tokoh->jabatan_di_indonesia ?>
        </h3>
      </div>
    <?php } ?>

    <div class="profile_tkh">
      <div class="foto">
        <img src="<?php echo $tokoh->foto; ?>" alt="<?php echo $tokoh->alt_foto; ?>">
      </div>

      <div class="biodata">
        <div>
          <?php if(!empty($tokoh->nama_lengkap_tokoh)) { ?>
            <p class="tkh_namaLengkap">
              <strong>Nama Lengkap : </strong>
              <span>
                <?php echo $tokoh->nama_lengkap_tokoh; ?>
              </span>
            </p>
          <?php } ?>

          <?php if(!empty($tokoh->tanggal_lahir) || !empty($tokoh->tempat_lahir)) { ?>
            <p class="tkh_ttl">
              <strong>Lahir : </strong>
              <span>
                <?php echo date('d F Y', strtotime($tokoh->tanggal_lahir)); ?>
                <?php echo (!empty($tokoh->tempat_lahir)) ? 'di ' . $tokoh->tempat_lahir : '' ; ?>
              </span>
            </p>
          <?php } ?>

          <?php if(!empty($tokoh->tanggal_meninggal_dunia) || !empty($tokoh->tempat_meninggal_dunia)) { ?>
            <p class="tkh_tglM">
              <strong>Meninggal Dunia : </strong>
              <span>
                <?php echo date('d F Y', strtotime($tokoh->tanggal_meninggal_dunia)); ?>
                <?php echo (!empty($tokoh->tempat_meninggal_dunia)) ? 'di ' . $tokoh->tempat_meninggal_dunia : '' ; ?>
              </span>
            </p>
          <?php } ?>
          
          <?php if(!empty($tokoh->cuplikan_tokoh)) { ?>
            <p class="tkh_cuplikan">
              <?php echo $tokoh->cuplikan_tokoh; ?>
            </p>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="bx_contentPost">
      <?php echo $tokoh->post_content; ?>
    </div>

    <div class="bx_commentPost">
      <h2 class="ht_relatedPost">Komentar</h2>
      <div class="sin_comment"><?php comments_template(); ?></div> 
    </div>
  </div>
</div>