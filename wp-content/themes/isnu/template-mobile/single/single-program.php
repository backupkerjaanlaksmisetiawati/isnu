<?php
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$ads = getAds('detail_article');
	$id_post = $post->ID;
	$current_category = get_the_terms( $id_post, 'kategori-program' );
	$category_id = ( !empty($current_category[0]) ) ? $current_category[0]->term_id : '';
	$category_name = ( !empty($current_category[0]) ) ? $current_category[0]->name : '';

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

	$categories = get_terms(
		array(
			'taxonomy'    => 'kategori-program',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);

	$related = getPrograms('5');

	$programTerbaru = getPrograms('3');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
<?php

$date_post = get_the_date('d F Y', $id_post);

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'full' );
if($thumb){
	$urlphoto = $thumb['0'];
}else{
	// $urlphoto = get_template_directory_uri().'/library/images/sorry.png';
	$urlphoto = '';
}
$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);

$allcategory = array();
$category_post = get_the_category($id_post);
foreach ($category_post as $key => $value) {
	$cat_parent = $value->category_parent;
	if($cat_parent == 4){ // 4 = berita-isnu ID
		$cat_id = $value->term_id;
		$cat_name = $value->name;
		$cat_slug = $value->slug;
		array_push($allcategory, $cat_id);
	}
}


$get_pdfpublikasi = get_post_meta( $id_post , 'pdfpublikasi' );
$pdfpublikasi = array();
if( !empty($get_pdfpublikasi[0]) ) {
  foreach( $get_pdfpublikasi as $get_pdfpublikasi ) {
    if( !empty($get_pdfpublikasi[0]) ) {
      foreach( $get_pdfpublikasi as $get_pdfpublikasi ) {
        $pdfpublikasi = $get_pdfpublikasi;
      }
    }
  }
}

?>
	
	<div class="row_globalPage row_postDetail">
		<div class="col_mobileCategories">
			<div class="sub_ht_rubrik">Jelajah Kategori</div>
			<select class="select-option select-basic-hide-search">
				<?php foreach ( $categories as $category ) { ?>
					<option <?php echo ($current_category[0]->term_id == $category->term_id) ? 'selected' : ''; ?>
						value="<?php echo home_url() . '/kategori-opini/'.$category->slug.'/'; ?>"><?php echo $category->name; ?></option>
				<?php } ?>
			</select>
		</div>
		<p>&nbsp;</p>

		<div class="bx_htpost">
			<?php if(isset($urlphoto) AND $urlphoto != ''){ ?>
				<div class="mg_postDetail">
					<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
				</div>
			<?php } ?>
			<h1 class="ht_htpostDetail"><?php echo get_the_title($post->ID); ?></h1>
			<div class="row">
				<div class="col-md-12 col_rightcat_post">
					<span class="sp_rightpost"><?php echo $date_post; ?></span>
					<span class="sp_greenpost"><?php echo $category_name; ?></span>
				</div>
			</div>
		</div>

		<?php if(!empty($ads['ads_top'])) { ?>
			<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
				<img src="<?php echo $ads['ads_top']->banner; ?>">
			</a>
		<?php } ?>

		<div class="bx_contentPost">
			<?php the_content(); ?>
		</div>
		
		<?php if(isset($u_id) AND $u_id != '' AND $u_id != 0){ ?>
			<?php if( !empty($pdfpublikasi) ) { ?>
				<div class="bx_contentPost bx_contentPostPDF">
					<h4>
						<strong>UNDUH TEXT LENGKAP</strong>
					</h4>
					<?php foreach( $pdfpublikasi as $pdf) { ?>
						<p>
							<a href="<?php echo wp_get_attachment_url($pdf['pdf-file']); ?>">
								<?php //echo $pdf['pdf-title']; ?>
								<?php
									$file_path = explode('/', get_attached_file($pdf['pdf-file']));
									echo $file_path[count($file_path) - 1];
								?>
							</a>
						</p>
					<?php } ?>
				</div>
			<?php } ?>
		<?php } ?>

		<?php if(!empty($ads['ads_bottom'])) { ?>
			<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
				<img src="<?php echo $ads['ads_bottom']->banner; ?>">
			</a>
		<?php } ?>
		
		<?php if(!empty($programTerbaru)) { ?>
			<div class="bx_relatedPost">
				<h2 class="ht_relatedPost">Program Terbaru</h2>
				<div id="default_berita" class="row box_global_berita">
					<?php foreach($programTerbaru as $val) { ?>
						<?php
							$link_post = get_the_permalink($val->ID);
							$short_name = $val->post_title;
							if(strlen($short_name) > 38) $short_name = substr($short_name, 0, 38).'...';
						?>
						<div class="col-md-6 col_v_listRubrik">
							<div class="bxsm_listRubrik">
								<div class="left_listRubrik">
									<div class="mg_sm_rubrik">
										<img src="<?php echo $val->photo; ?>" alt="<?php echo $val->alt_foto; ?>">
									</div>
								</div>
								<div class="right_listRubrik">
									<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
										<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
									</a>
									<div class="info_sm_listRubrik">
										<?php echo substr(get_the_excerpt($val->ID), 0, 80); ?>...
									</div>
									<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
								</div>
								<div class="bx_sm_rubrikCat detailPost">
									<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
									<span class="r_cat"><?php echo $val->category_name; ?></span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

		<?php if(!empty($beritaPopuler)) { ?>
			<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
				<div class="row_articleTitle">Berita Populer</div>
				<ul class="listBeritaPopuler listBeritaPopulerRight">
					<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
						<li>
							<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
								<span><?php echo $key+1; ?></span>
								<span><?php echo $berita->post_title; ?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>

		<?php if(!empty($related)) { ?>
			<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
				<div class="row_articleTitle">Program Terkait</div>
				<ul class="listBeritaPopuler listBeritaPopulerRight">
					<?php foreach ( $related as $key => $val ) { ?>
						<li>
							<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
								<span><?php echo $key+1; ?></span>
								<span><?php echo $val->post_title; ?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>

		<?php if(!empty($ads['ads_right'])) { ?>
			<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
				<img src="<?php echo $ads['ads_right']->banner; ?>">
			</a>
		<?php } ?>

		<?php get_template_part( 'content', 'rightvideo' ); ?>

		<div class="bx_commentPost">
			<h2 class="ht_relatedPost">Komentar</h2>
			<div class="sin_comment"><?php comments_template(); ?></div> 
		</div>

	</div>

</div>