<?php
	$id_post = $post->ID;
	$per_page = '6';
	$ads = getAds('detail_article');
	$categories = getPostsCategories();
	$current_category = get_the_category($id_post);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita('5');

	$beritaTerbaru = getBerita(
		'3',
		array(
			'exclude' => array($id_post)
		)
	);

	$related = getBerita(
		'5',
		array(
			'cat' => $current_category[0]->term_id,
			'exclude' => array($id_post)
		)
	);

	$date_post = get_the_date('d F Y', $id_post);

	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'full' );
	if($thumb){
		$urlphoto = $thumb['0'];
	}else{
		$urlphoto = '';
	}
	$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	<div class="row_globalPage row_postDetail">
		<div class="">
			<div class="col_mobileCategories">
				<div class="sub_ht_rubrik">Jelajah Kategori</div>
				<select class="select-option select-basic-hide-search">
					<?php foreach ( $categories as $category ) { ?>
						<option <?php echo ($current_category[0]->term_id == $category->term_id) ? 'selected' : ''; ?>
							value="<?php echo home_url() . '/category/'.$category->slug.'/'; ?>"><?php echo $category->name; ?></option>
					<?php } ?>
				</select>
			</div>
			<p>&nbsp;</p>

			<div class="bx_htpost">
				<?php if(isset($urlphoto) AND $urlphoto != ''){ ?>
					<div class="mg_postDetail">
						<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
					</div>
				<?php } ?>
				<h1 class="ht_htpostDetail"><?php echo get_the_title($post->ID); ?></h1>
				<div class="row">
					<div class="col-md-12 col_rightcat_post">
						<span class="sp_rightpost"><?php echo $date_post; ?></span>
						<span class="sp_greenpost"><?php echo $current_category[0]->name; ?></span>
					</div>
				</div>
			</div>

			<?php if(!empty($ads['ads_top'])) { ?>
				<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
					<img src="<?php echo $ads['ads_top']->banner; ?>">
				</a>
			<?php } ?>

			<div class="bx_contentPost">
				<?php the_content(); ?>
			</div>

			<?php if(!empty($ads['ads_bottom'])) { ?>
				<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
					<img src="<?php echo $ads['ads_bottom']->banner; ?>">
				</a>
			<?php } ?>

			<?php if(!empty($beritaTerbaru)) { ?>
				<div class="bx_relatedPost">
					<h2 class="ht_relatedPost">Berita Terbaru</h2>
					<div id="default_berita" class="row box_global_berita">
						<?php foreach($beritaTerbaru as $berita) { ?>
							<?php
								$short_name = $berita->post_title;
								// if(strlen($short_name) > 38) $short_name = substr($short_name, 0, 38).'...';
							?>

							<div class="col-xs-12 col-md-12 nopadding_left nopadding_right col_v_listRubrik">
								<div class="bxsm_listRubrik">
									<div class="left_listRubrik">
										<div class="mg_sm_rubrik">
											<img src="<?php echo $berita->foto; ?>" style="<?php echo $berita->style_emptyphoto; ?>" alt="<?php echo $berita->alt_foto; ?>">
										</div>
									</div>
									<div class="right_listRubrik">
										<a href="<?php echo $berita->link_post; ?>" title="Lihat <?php echo $berita->link_post; ?>">
											<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
										</a>
										<a class="a_nextRubrik a_detailRubrik" href="<?php echo $berita->link_post; ?>">Selengkapnya »</a>
									</div>
									<div class="bx_sm_rubrikCat detailPost">
										<span class="l_cat"><?php echo date('d F Y', strtotime($berita->post_date)); ?></span>
										<span class="r_cat"><?php echo $berita->category_name; ?></span>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>

			<?php if(!empty($ads['ads_right'])) { ?>
				<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
					<img src="<?php echo $ads['ads_right']->banner; ?>">
				</a>
			<?php } ?>

			<?php if(!empty($beritaPopuler)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Berita Populer</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $berita->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

			<?php if(!empty($related)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Berita Terkait</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $related as $key => $berita ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $berita->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

			<?php get_template_part( 'content', 'rightvideo' ); ?>

			<div class="bx_commentPost">
				<h2 class="ht_relatedPost">Komentar</h2>
				<div class="sin_comment"><?php comments_template(); ?></div> 
			</div>
		</div>

	</div>
</article>