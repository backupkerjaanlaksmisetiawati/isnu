<?php $highlightTokoh = getHighlightTokoh('banner'); ?>
<?php if($highlightTokoh !== null) { ?>
	<div class="row row_mobileHomeTokoh">
		<div class="col-md-12 nopadding">
			<div class="mg_homeTokoh">
				<img src="<?php echo $highlightTokoh->footer_banner_image; ?>">
			</div>
			<div class="box_quotesTokoh">
				<h3 class="ht_quotesTokoh"><?php echo $highlightTokoh->footer_banner_title1; ?></h3>
				<div class="cont_quotes">
					<?php echo $highlightTokoh->footer_banner_title2; ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>