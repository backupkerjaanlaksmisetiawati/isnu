<?php
	$beritaPopuler = getHighlightBerita('5');
	$berita = getBerita('3');

	//sementara ambil berita populer dari field 'Berita Populer' 
	$populer = getHighlightBerita();
	$categories = getPostsCategories();
	$ads = getAds();
?>

<?php if(!empty($ads['ads_top'])) { ?>
	<style>
	.row_homeRubrik {
		padding-top: 40px; /*130px;*/
	}
	</style>
<?php } ?>

<div class="row_globalPage row_mobileHomeRubrik">
	<?php if(!empty($ads['ads_top'])) { ?>
		<div class="col_homeTopSmallBanner">
			<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
				<img src="<?php echo $ads['ads_top']->banner; ?>">
			</a>
		</div>
	<?php } ?>

	<h2 class="mobileTitle mobileTitleGreen">BERITA ISNU</h2>
	<div class="mobileSubTitle">Populer</div>
	
	<div id="slide_rubrikMobile" class="owl-carousel owl-theme">
		<?php foreach($beritaPopuler as $val) { ?>
			<?php $link_post = get_the_permalink($val->ID); ?>
			<div class="col_mobileHomeSlideBerita">				
				<div class="mg_rubrik">
					<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
				</div>

				<div class="box_rubrikContent">
					<a href="<?php echo $link_post; ?>"
						title="Lihat <?php echo $val->post_title; ?>">
						<h4 class="ht_rubrikPost">
							<?php
								if(strlen($val->post_title) > 60) {
										echo substr($val->post_title, 0, 60).'...';
									} else {
										echo $val->post_title;
									}
							?>
						</h4>
					</a>

					<div class="content_rubrik">
						<?php echo substr(get_the_excerpt($val->ID), 0,150); ?>...
					</div>

					<p class="a_nextRubrik">
						<a href="<?php echo $link_post; ?>">Selengkapnya »</a>
					</p>
				</div>

				<div class="bx_sm_rubrikCat">
					<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
					<span class="r_cat"><?php echo $val->category_name; ?></span>
				</div>
			</div>
		<?php } ?>
	</div>
			
	<?php if(!empty($ads['ads_right'])) { ?>
		<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
			<img src="<?php echo $ads['ads_right']->banner; ?>">
		</a>
	<?php } ?>

	<div class="row_mobileHomeBerita">
		<div class="">
			<?php foreach($berita as $val) { ?>
				<?php
					$check_len = strlen( strip_tags($val->post_title) );
					if( $check_len > 50 ) {
						$post_title = substr($val->post_title, 0, 50)  . '...';
					} else {
						$post_title = $val->post_title;
					}
				?>
				<div class="col_mobileListHomeBerita">
					<div class="mobileListHomeBerita">
						<div class="left_mobileListHomeBerita">
							<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>" <?php if($val->have_img === false) { ?> style="width: auto; height: 100%; opacity: 0.2;"<?php } ?>>
						</div>

						<div class="right_mobileListHomeBerita">
							<a href="<?php echo $val->link_post; ?>" title="Lihat <?php echo $val->foto; ?>">
								<h5 class="mobileTitleListRubrik">
									<?php echo $post_title; ?>
								</h5>
							</a>
							<div class="info_sm_listRubrik">
								<?php echo substr(get_the_excerpt($val->ID), 0,90); ?>...
							</div>
							<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
						</div>
					</div>

					<div class="bx_sm_rubrikCat">
						<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
						<span class="r_cat"><?php echo $val->category_name; ?></span>
					</div>
				</div>
			<?php }  ?>
		</div>
		
		<div class="bx_loadmore">
			<a href="<?php echo home_url(); ?>/berita/">
				<input type="button" class="btn_loadmore btn_moreRubrik" value="Lihat Lainnya">
			</a>
		</div>
	</div>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
			<img src="<?php echo $ads['ads_bottom']->banner; ?>">
		</a>
	<?php } ?>
</div>
