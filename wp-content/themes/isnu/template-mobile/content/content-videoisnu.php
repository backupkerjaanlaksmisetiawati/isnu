<?php
	$highlightVideos = getFooterHighlightVideos();
	$big_video_information = get_field('big_video_information', $highlightVideos[0]->ID);
?>

<div class="row_globalPage row_mobileVdeoHome">
	<h2 class="ht_home">Video ISNU</h2>

	<a href="<?php echo get_the_permalink($highlightVideos[0]->ID); ?>"
		title="Lihat <?php echo $highlightVideos[0]->post_title; ?>"
		class="frame_video mg_bigvideo">
		<div class="iconplay">
			<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
		</div>
		<img src="<?php echo $highlightVideos[0]->foto; ?>"
			alt="<?php echo $highlightVideos[0]->alt_foto; ?>" />
	</a>

	<a href="<?php echo get_the_permalink($highlightVideos[0]->ID); ?>"
		title="Lihat <?php echo $highlightVideos[0]->post_title; ?>">
		<h3 class="ht_videoisnu">
			<?php echo $highlightVideos[0]->post_title; ?>
		</h3>
	</a>

	<div class="info_videoisnu"><?php echo $big_video_information; ?></div>

	<div class="wrap_daftarisnu">
		<a href="<?php echo home_url(); ?>/media/video/">
			<input type="button" class="btn_loadmore btn_moreRubrik" value="Lihat Selengkapnya">
		</a>
	</div>
</div>