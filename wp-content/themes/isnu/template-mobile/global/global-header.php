<?php
	$page_id = get_the_ID();
	$page_post = get_post($page_id);

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
?>

<?php if(is_page(array('2'))){ ?>
<header class="header_mobile" role="banner">
<?php }else{ ?>
<header class="header_mobile" role="banner">
<?php } ?>

  <div class="row">
    <div class="col-xs-3 col-md-3">
      <a href="<?php echo home_url(); ?>" title="Beranda - ISNU">
        <div class="mg_topLogo mobile_topLogo">
          <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
        </div>
      </a>

    </div>
    <div class="col-xs-9 col-md-9 col_mobile_topRightMenu">
      <div class="btn_mobileTop clearfix">
        <button type="button" class="" data-target="#mobile_topMenu">
          <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-menu.svg" />
        </button>
        <span>
          <?php if(isset($u_id) AND $u_id != '' AND $u_id != 0){ ?>
            <?php $nama = explode(" ", $current_user->display_name); ?>
            <a href="<?php echo home_url('/wp-admin/profile.php'); ?>">
              Halo, <?php echo $nama[0]; ?>
            </a>
          <?php } else { ?>
            <a href="<?php echo home_url(); ?>/daftar/" class="a_registNow">Daftar</a> / 
            <a href="<?php echo home_url(); ?>/login/" class="a_loginNow">Masuk</a>
          <?php } ?>
        </span>
      </div>
    </div>
  </div>

  <ul class="mobile_topMenu">
    <?php
      $menu_id = 20; // menu id footer 1
      $menu_list='';
      $menu_items = wp_get_nav_menu_items($menu_id);
      foreach ( $menu_items as $key => $menu_item ) {
        $title = $menu_item->title;
        $url = $menu_item->url;
        $explode_url = explode('/', $url);
        $last_url_path = count($explode_url)-2;
        $class_active = (!empty($page_post) && $explode_url[$last_url_path] == $page_post->post_name) ? 'active' : 'inactive';
        $menu_list .= '<li class="' . $class_active . '">';
          $menu_list .= '<a href="'.$url.'" title="view detail '.$title.'">';
            $menu_list .= $title;
          $menu_list .= '</a>';
        $menu_list .= '</li>';
      }
      
      if(isset($u_id) AND $u_id != '' AND $u_id != 0){
        $menu_list .= '<li class="btn_mobileTopLogout">';
          $menu_list .= '<img src="' . get_template_directory_uri() . '/library/images/icon-logout.svg" />';
          $menu_list .= '<a class="logout" href="'.wp_logout_url(home_url('/')).'" title="Keluar akun Anda">Keluar</a>';
        $menu_list .= '</li>';
      } 
      echo $menu_list;
    ?>
  </ul>
</header>