<?php
	$per_page = 12;
	$page_id = get_the_ID();
	$page_post = get_post($page_id);
	$videos = getVideos();
?>

<div class="row row_globalPage row_homeBerita ajax_load_video">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">New Release Media ISNU</h1>
		<p>&nbsp;</p>
		
		<div id="wrap_load_video" class="row wrap_mediaVideo wrap_mediaVideoArchive">
			<?php foreach($videos as $k => $vid) { ?>
				<a href="<?php echo get_the_permalink($vid->ID); ?>"
					title="Lihat <?php echo $vid->post_title; ?>"
					class="col-xs-3 col-md-3 media_video">
					<div class="wrap_videoList">
						<div class="wrap_mediaVideoThumb">
							<div class="iconplay">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
							</div>
							
							<div class="media_videoThumb">
								<img src="<?php echo $vid->foto; ?>" 
									alt="<?php echo $vid->alt_foto; ?>" />
							</div>
						</div>

						<div class="media_videoTxt">
							<h5>
								<?php
									echo (strlen($vid->post_title) > 55) ? substr($vid->post_title, 0, 55).'...' : $vid->post_title;
								?>
							</h5>
							<p>
								<?php echo date('d F Y', strtotime($vid->post_date)); ?>
							</p>
						</div>
					</div>
				</a>
			<?php } ?>
		</div>

		<div class="row box_v_listRubrik act">
			<div class="row load_ajax_video"
				data-next-page="2"
				data-per-page="<?php echo $per_page; ?>"
				data-category="0"></div>
			<div class="mg_pleasewait">
				<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
				<p>Mohon menunggu...</p>
			</div>
		</div>
	</div>
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>