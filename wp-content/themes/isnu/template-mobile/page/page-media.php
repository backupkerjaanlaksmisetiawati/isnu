<?php
	$firstVideos = getVideos('1');
	$videos = getVideos(
		'3',
		array(
			'exclude' => array($firstVideos[0]->ID)
		)
	);

	$photos = getGaleri('10');
?>

<style>
	#bigFotoMobile.owl-theme {
		position: relative;
	}
	.owl-theme .owl-nav [class*='owl-'] {
		-webkit-transition: all .3s ease;
		transition: all .3s ease;
	}
	.owl-theme .owl-nav [class*='owl-'].disabled:hover {
		background-color: #D6D6D6;
	}
</style>

<div id="pageMedia" class="row_globalPage row_homeBerita">
	<?php if(!empty($photos)) { ?>
		<div class="wrap_mediaKategoriTitle clearfix">
			<div class="media_kategoriTitle">
				<h1 class="ht_home ht_homeRubrik">Foto ISNU</h1>
				<div class="sub_ht_rubrik">Kumpulan Foto-Foto Kegiatan ISNU</div>
			</div>
		</div>

		<div class="outerFoto">
			<div id="bigFotoMobile" class="owl-carousel owl-theme">
				<?php foreach($photos as $photo) { ?>
					<div class="item">
						<div>
							<img src="<?php echo $photo->foto; ?>"
							alt="<?php echo $photo->alt_foto; ?>" />
						</div>
						<h4>
							<?php echo $photo->post_title; ?>
						</h4>
					</div>
				<?php } ?>
			</div>

			<div id="thumbsFotoMobile" class="owl-carousel owl-theme">
				<?php foreach($photos as $photo) { ?>
					<div class="item">
						<img src="<?php echo $photo->foto; ?>"
							alt="<?php echo $photo->alt_foto; ?>" />
					</div>
				<?php } ?>
			</div>

			<div class="box_btnMediaKategori">
				<a href="<?php echo home_url() . '/media/foto/'; ?>"
					class="btn_mediaKategori">
					Lihat Selengkapnya
				</a>
			</div>
		</div>
	<?php } ?>

	<?php if(!empty($firstVideos) || !empty($videos)) { ?>
		<div class="wrap_mediaKategoriTitle clearfix">
			<div class="media_kategoriTitle">
				<h2 class="ht_home ht_homeRubrik">Video ISNU</h2>
				<div class="sub_ht_rubrik">Kumpulan Video Kegiatan ISNU</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 wrap_mediaHighlight">
				<a href="<?php echo get_the_permalink($firstVideos[0]->ID); ?>"
					title="Lihat <?php echo $firstVideos[0]->post_title; ?>"
					class="media_highlightWrapper">
					<div class="media_highlight">
						<div class="iconplay">
							<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
						</div>
						<img src="<?php echo $firstVideos[0]->foto; ?>"
							alt="<?php echo $firstVideos[0]->alt_foto; ?>" />
					</div>

					<h5>
						<?php
							echo (strlen($firstVideos[0]->post_title) > 100) ? 
								substr($firstVideos[0]->post_title, 0, 100).'...' : 
								$firstVideos[0]->post_title;
						?>
					</h5>
					<p>
						<?php 
							echo (strlen(strip_tags($firstVideos[0]->post_content)) > 200) ? 
								substr(strip_tags($firstVideos[0]->post_content), 0, 200).'...' : 
								strip_tags($firstVideos[0]->post_content);
						?>
					</p>
				</a>
			</div>

			<?php if(!empty($videos)) { ?>
				<div class="col-xs-12 wrap_mediaHighlightSmall">
					<br />
					<br />
					<div class="sub_ht_rubrik">Rekomendasi</div>
					<br />
					<?php foreach($videos as $key => $vid) { ?>
						<a href="<?php echo get_the_permalink($vid->ID); ?>"
							title="Lihat <?php echo $vid->post_title; ?>"
							class="row media_highlightSmall">
							<div class="col-xs-6 col-md-6 wrap_mediaHighlightSmallThumb">
								<div class="iconplay">
									<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
								</div>
								<div class="media_highlightSmallThumb">
									<img src="<?php echo $vid->foto; ?>" 
										alt="<?php echo $vid->alt_foto; ?>" />
								</div>
							</div>

							<div class="col-xs-6 col-md-6 media_highlightSmallTxt">
								<h5>
									<?php
										echo (strlen($vid->post_title) > 45) ? substr($vid->post_title, 0, 45).'...' : $vid->post_title;
									?>
								</h5>
								<p>
									<?php echo date('d M Y', strtotime($vid->post_date)); ?>
								</p>
							</div>
						</a>
					<?php } ?>
				</div>
			<?php } ?>
		</div>

		<div class="box_btnMediaKategori">
			<a href="<?php echo home_url() . '/media/video/'; ?>"
				class="btn_mediaKategori">
				Lihat Selengkapnya
			</a>
		</div>
	<?php } ?>
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>