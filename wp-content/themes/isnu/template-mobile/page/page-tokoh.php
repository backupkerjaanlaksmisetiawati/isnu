<?php
  $per_page = '4';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getTokohList('-1'));
  $pages = ceil($total/$per_page);
  
  $highlightTokoh = getHighlightTokoh('banner');
  $listTokoh = getTokohList(
    $per_page,
    array(
			'paged' => $page
    )
  );
?>

<?php if(!empty($highlightTokoh)) { ?>
  <div id="bnr_tokoh" class="row_globalPage"
    style="background-image: url(<?php echo $highlightTokoh->banner_image; ?>);">
    <div class="bnr_tokohTitle">
      <h4 class="fira">
        <?php echo $highlightTokoh->banner_title; ?>
      </h4>
      <h5>
        &ldquo;<?php echo $highlightTokoh->banner_text; ?>&rdquo;
      </h5>
    </div>
  </div>
<?php } ?>

<div class="row row_globalPage row_homeBerita">
  <h1 class="ht_home ht_homeRubrik">TOKOH</h1>
  <div class="sub_ht_rubrik">TELADAN YANG DIJIWAI NAHDLATUL ULAMA</div>
  <p>&nbsp;</p>

  <div id="wrap_load_tokoh_mobile">
    <?php foreach($listTokoh as $tokoh) { ?>
      <div class="col_tokohList">
        <div class="row">
          <div class="wrap_tkhFoto col-xs-12 col-md-12">
            <div class="tkh_foto">
              <img src="<?php echo $tokoh->foto; ?>" alt="<?php echo $tokoh->alt_foto; ?>" />
            </div>
          </div>

          <div class="wrap_tkhProfile col-xs-12 col-md-12">
            <h4 class="tkh_nama">
              <strong>
                <?php echo $tokoh->post_title; ?>
              </strong>
            </h4>

            <?php if(!empty($tokoh->jabatan_di_indonesia)) { ?>
              <div>
                <p class="tkh_jabatan">
                  <?php echo $tokoh->jabatan_di_indonesia; ?>
                </p>
              </div>
            <?php } ?>

            <?php if(!empty($tokoh->nama_lengkap_tokoh)) { ?>
              <p class="tkh_namaLengkap">
                <strong>Nama Lengkap : </strong>
                <span>
                  <?php echo $tokoh->nama_lengkap_tokoh; ?>
                </span>
              </p>
            <?php } ?>

            <?php if(!empty($tokoh->tanggal_lahir) || !empty($tokoh->tempat_lahir)) { ?>
              <p class="tkh_ttl">
                <strong>Lahir : </strong>
                <span>
                  <?php echo date('d F Y', strtotime($tokoh->tanggal_lahir)); ?>
                  <?php echo (!empty($tokoh->tempat_lahir)) ? ' di ' . $tokoh->tempat_lahir : '' ; ?>
                </span>
              </p>
            <?php } ?>

            <?php if(!empty($tokoh->tanggal_meninggal_dunia) || !empty($tokoh->tempat_meninggal_dunia)) { ?>
              <p class="tkh_tglM">
                <strong>Meninggal Dunia : </strong>
                <span>
                  <?php echo date('d F Y', strtotime($tokoh->tanggal_meninggal_dunia)); ?>
                  <?php echo (!empty($tokoh->tempat_meninggal_dunia)) ? ' di ' . $tokoh->tempat_meninggal_dunia : '' ; ?>
                </span>
              </p>
            <?php } ?>
          </div>
        </div>

        <?php if(!empty($tokoh->cuplikan_tokoh)) { ?>
          <p class="tkh_cuplikan">
            <?php echo $tokoh->cuplikan_tokoh; ?>
          </p>
        <?php } ?>

        <div class="box_btnMediaKategori">
          <a href="<?php echo home_url() . '/tokoh/' . $tokoh->post_name . '/'; ?>"
            class="btn_mediaKategori">
            Baca Selengkapnya
          </a>
        </div>
      </div>
    <?php } ?>
  </div>

  <?php if($total > $per_page) { ?>
    <div class="pagination">
      <?php
        pagination(
          array(
						'base'				=> home_url() . '/tokoh?',
            'page'				=> $page,
            'pages' 			=> $pages,
            'key'					=> 'hal',
            'next_text'		=> '&rsaquo;',
            'prev_text'		=> '&lsaquo;',
            'first_text'	=> '&laquo;',
            'last_text'		=> '&raquo;'
          )
        );
      ?>
    </div>
  <?php } ?>
</div>