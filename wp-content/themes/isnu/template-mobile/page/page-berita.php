<?php
	$per_page = '10';

	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$loopPage = new WP_Query(array( 'post_type'=>'post',
		'posts_per_page'=> '-1',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC'
	));
	$total = count($loopPage->posts);
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$populer = getHighlightBerita('5');

	$categories = getPostsCategories();
	$ads = getAds();

  $loop = new WP_Query(array( 'post_type'=>'post',
		'posts_per_page'=> $per_page,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'paged' => $page
  ));
?>

<div class="row_globalPage row_homeBerita">
	<h1 class="ht_home ht_homeRubrik">BERITA ISNU</h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">Kategori</div>
		<select class="select-option select-basic-hide-search">
			<option value="<?php echo home_url() . '/category/'; ?>" selected>Berita Terbaru</option>
			<?php foreach ( $categories as $category ) { ?>
				<option value="<?php echo home_url() . '/category/' . $category->slug; ?>"><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>
	<p>&nbsp;</p>

	<div class="row_articleTitle">Populer</div>

	<div class="row">
		<div id="slide_rubrikBeritaMobile" class="owl-carousel owl-theme">
			<?php foreach($populer as $val) { ?>
				<?php $link_post = get_the_permalink($val->ID); ?>
				<div class="col_mobileHomeSlideBerita">					
					<div class="mg_rubrik">
						<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
					</div>

					<div class="box_rubrikContent">
						<a href="<?php echo $link_post; ?>"
							title="Lihat <?php echo $val->post_title; ?>">
							<h4 class="ht_rubrikPost">
								<?php echo $val->post_title; ?>
							</h4>
						</a>
						<div class="content_rubrik">
							<?php echo substr(get_the_excerpt($val->ID), 0,150); ?>...
						</div>
						<p class="a_nextRubrik">
							<a href="<?php echo $link_post; ?>">Selengkapnya »</a>
						</p>
					</div>

					<div class="bx_sm_rubrikCat">
						<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
						<span class="r_cat"><?php echo $val->category_name; ?></span>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

	<?php if(!empty($ads['ads_top'])) { ?>
		<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
			<img src="<?php echo $ads['ads_top']->banner; ?>">
		</a>
	<?php } ?>

	<p>&nbsp;</p>
	<div class="sub_ht_rubrik sub_ht_beritaLainnya">Berita Lainnya</div>
	<div class="">
		<?php if(!empty($loop->posts)) { ?>
			<div class="row">
				<?php
				while ( $loop->have_posts() ){
					$loop->the_post();
					$id_post = get_the_ID();
					$title_post = get_the_title($id_post);
					$short_name = get_the_title($id_post);
					if(strlen($short_name) > 38) $short_name = substr($short_name, 0, 38).'...';
					$link_post = get_the_permalink($id_post);
					$date_post = get_the_date('d F Y', $id_post);
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'medium' );
					if($thumb){
						$urlphoto = $thumb['0'];
						$style_emptyphoto = '';
					}else{
						$urlphoto = get_template_directory_uri().'/library/images/main_logo.png';
						$style_emptyphoto = 'width: auto; height: 100%; opacity: 0.2;';
					}
					$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);
					// if(count($alt));
					$category_post = get_the_category($id_post);
				?>
					<div class="col-xs-12 col-md-12 nopadding_left nopadding_right col_v_listRubrik">
						<div class="bxsm_listRubrik">
							<div class="left_listRubrik">
								<div class="mg_sm_rubrik">
									<img src="<?php echo $urlphoto; ?>" style="<?php echo $style_emptyphoto; ?>" alt="<?php echo $alt; ?>">
								</div>
							</div>
							<div class="right_listRubrik">
								<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $title_post; ?>">
									<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
								</a>
								<div class="info_sm_listRubrik">
									<?php echo substr(get_the_excerpt($id_post), 0, 80); ?>...
								</div>
								<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
							</div>
							<div class="bx_sm_rubrikCat">
								<span class="l_cat"><?php echo $date_post; ?></span>
								<span class="r_cat"><?php echo $category_post[0]->name; ?></span>
							</div>
						</div>
					</div>
				<?php }wp_reset_postdata();  ?>
			</div>

			<div class="pagination">
				<?php
					pagination(
						array(
							'base'				=> home_url() . '/berita?',
							'page'				=> $page,
							'pages' 			=> $pages,
							'key'					=> 'hal',
							'next_text'		=> '&rsaquo;',
							'prev_text'		=> '&lsaquo;',
							'first_text'	=> '&laquo;',
							'last_text'		=> '&raquo;'
						)
					);
				?>
			</div>
		<?php } ?>
	</div>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
			<img src="<?php echo $ads['ads_bottom']->banner; ?>">
		</a>
	<?php } ?>
</div>

<?php
  get_template_part(
    'template-mobile/content/content',
    'tokoh'
  );
?>