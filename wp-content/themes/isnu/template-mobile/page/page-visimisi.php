<?php
	$posisiCabangUtama = getPosisiKepengurusan(
		array(
			'is_pusat' => TRUE
		)
	);
	
	if($posisiCabangUtama != null) {
		$categoryPosisi = getPosisiKepengurusan(
			array(
				'parent' => $posisiCabangUtama->term_id,
				'limit' => 4
			)
		);
	}
?>

<div class="row row_globalPage row_tentangPage">

	<div class="mg_back_tentangPage">
		<img class="" src="<?php bloginfo('template_directory'); ?>/library/images/mg_tentang.png">
	</div>

	<div class="col-xs-3 nopadding_right col_left_tentang">

		<h1 class="ht_globalPage hide">Tentang ISNU</h1>

		<ul class="tabsTentang">
			<li class="act" data-id="tabs1-mobile"><a href="javascript:;" ><span class="glyphicon glyphicon-triangle-right choose_arrow"></span> Visi</a></li>
			<li class="" data-id="tabs2-mobile"><a href="javascript:;" ><span class="glyphicon glyphicon-triangle-right choose_arrow"></span> Misi</a></li>
		</ul>
		
	</div>
	<div id="tabs1-mobile" class="col-xs-9 nopadding_right col_content_tentang act animated fadeIn">
		
		<div class="row">
			<?php 
				$daftar = get_post_meta($post->ID,'daftarvisi',true);
				if(isset($daftar) AND $daftar != ''){

					if(count($daftar) > 1){

						$no = 1;
						foreach ($daftar as $key => $value) {
			?>
							<div class="col-md-6 col_listTentang">
								<div class="numb_tentang"><?php echo $no; ?></div>
								<div class="content_tentang">
									<?php echo $value['visi-isnu'] ?>
								</div>
							</div>
				<?php
						$no++;
						}
					}else{
				?>
						<div class="box_cont_onevisi">
							<?php echo $daftar[0]['visi-isnu'] ?>
						</div>
				<?php
					}
				 } ?>
		</div>
		
	</div>
	<div id="tabs2-mobile" class="col-xs-9 nopadding_right col_content_tentang animated fadeIn">
		
		<div class="row">
			<?php 
				$daftar = get_post_meta($post->ID,'daftarmisi',true);
				if(isset($daftar) AND $daftar != ''){
					$no = 1;
					foreach ($daftar as $key => $value) {
			?>
						<div class="col-xs-12 col_listTentang">
							<div class="numb_tentang"><?php echo $no; ?></div>
							<div class="content_tentang">
								<?php echo $value['misi-isnu'] ?>
							</div>
						</div>
				<?php
					$no++;
					}
				 } ?>
		</div>
		
	</div>
</div>

<div class="row_globalPage" id="kepengurusan">
	<h2 class="ht_kepengurusanTitle fira">KEPENGURUSAN ISNU</h2>
	<p class="ht_kepengurusanDescription">
		Berikut ini merupakan struktur organisasi kepengurusan ISNU tahun periode 2019 - 2024.
	</p>

	<?php if($posisiCabangUtama != null) { ?>
		<?php if($categoryPosisi != null) { ?>
			<?php foreach($categoryPosisi as $cat) { ?>
				<div class="newwrap_posisiKepengurusan">
					<div class="title_posisiKepengurusan">
						<div>
							<?php echo strtoupper($cat->name) ?>
						</div>
					</div>

					<?php
						$categorySubPosisi = getPosisiKepengurusan(
							array(
								'parent' => $cat->term_id,
								// 'limit' => 4,
								'noanggota' => TRUE
							)
						);
					?>

					<?php if($categorySubPosisi != null) { ?>
						<div class="newwrap_subPosisiKepengurusan">
							<?php foreach($categorySubPosisi as $sk => $catSub) { ?>
								<?php
									$listPengurus = getListKepengurusan(
										array(
											'limit' => 2,
											'category' => $catSub->term_id
										)
									);
								?>

								<?php if($listPengurus != null) { ?>
									<?php foreach($listPengurus as $pengurus) { ?> 
										<?php
											$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($pengurus->ID), 'medium' );
											if($thumb) {
												$urlphoto = $thumb['0'];
												$urlphoto_empty = false;
											} else{
												$urlphoto = get_template_directory_uri().'/library/images/user.svg';
												$urlphoto_empty = true;
											}
										?>

										<div class="wrap_newProfileKepengurusan">
											<?php
												$subPengurus = getPosisiKepengurusan(
													array(
														'parent' => $catSub->term_id
													)
												);
											?>

											<div class="newProfileKepengurusan clearfix <?php if($subPengurus != null) { ?>newProfileKepengurusanParent<?php } ?>">
												<div class="foto <?php echo ($urlphoto_empty === true)?'empty':''; ?>">
													<img src="<?php echo $urlphoto; ?>"
														alt="<?php echo $pengurus->post_title; ?>">
												</div>
												
											<div class="nama <?php if($subPengurus == null) { ?>full<?php } ?>">
													<div>
														<h6><?php echo $pengurus->post_title; ?></h6>
														<p><?php echo $catSub->name; ?></p>
													</div>
												</div>

												<?php if($subPengurus != null) { ?>
													<div class="selengkapnya btn_kepengurusan_mobile inactive" data-btnslug="sub-<?php echo $catSub->slug; ?>">
														<div>
															Lihat Selengkapnya<br />
															<span>&rsaquo;</span>
														</div>
													</div>
												<?php } ?>
											</div>

											<?php if($subPengurus != null) { ?>
												<div class="newwrap_subPengurus_mobile" data-slug="sub-<?php echo $catSub->slug; ?>">
													<?php foreach($subPengurus as $subPengurus) { ?> 
														<?php
															$listSubPengurus = getListKepengurusan(
																array(
																	'category' => $subPengurus->term_id
																)
															);
														?>
														<?php if($listSubPengurus != null) { ?>
															<?php foreach($listSubPengurus as $listSubPengurus) { ?> 
																<?php
																	$thumb2 = wp_get_attachment_image_src( get_post_thumbnail_id($listSubPengurus->ID), 'medium' );
																	if($thumb2) {
																		$urlphoto2 = $thumb2['0'];
																		$urlphoto2_empty = false;
																	} else{
																		$urlphoto2 = get_template_directory_uri().'/library/images/user.svg';
																		$urlphoto2_empty = true;
																	}
																?>
																<div class="newProfileKepengurusan">
																	<div class="foto <?php echo ($urlphoto2_empty === true)?'empty':''; ?>">
																		<img src="<?php echo $urlphoto2; ?>"
																			alt="<?php echo $subPengurus->post_title; ?>">
																	</div>
																	
																	<div class="nama">
																		<div>
																			<h6><?php echo $listSubPengurus->post_title; ?></h6>
																			<p><?php echo $subPengurus->name; ?></p>
																		</div>
																	</div>
																</div>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</div>
											<?php } ?>
										</div>
									<?php } ?>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			<?php } ?>

			<div class="box_btnKepengurusan">
				<a href="<?php echo home_url() . '/tentang-isnu/kepengurusan-isnu/'; ?>"
					class="btn_yellow">
					Lihat Semua
				</a>
			</div>
		<?php } ?>
	<?php } ?>
</div>