<?php
	$per_page = '10';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getOpini('-1'));
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$populer = getHighlightOpini();

	$categories = getKategoriOpini();
	$ads = getAds();
	$opini = getOpini(
		$per_page,
		array(
			'paged' => $page
		)
	);
	
	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
?>

<div class="row_globalPage row_homeBerita">
	<h1 class="ht_home ht_homeRubrik">OPINI ISNU</h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">Kategori</div>
		<select class="select-option select-basic-hide-search">
			<option value="<?php echo home_url() . '/kategori-opini/'; ?>" selected>Opini Terbaru</option>
			<?php foreach ( $categories as $category ) { ?>
				<option value="<?php echo home_url() . '/' . $category->taxonomy . '/' . $category->slug; ?>"><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>
	<p>&nbsp;</p>

	<div class="row_articleTitle">Opini Populer</div>

	<?php if(!empty($populer)) { ?>
		<div class="row">
			<div id="slide_rubrikBeritaMobile" class="owl-carousel owl-theme">
				<?php foreach($populer as $val) { ?>
					<?php $link_post = get_the_permalink($val->ID); ?>
					<div class="col_mobileHomeSlideBerita">					
						<div class="mg_rubrik">
							<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
						</div>

						<div class="box_rubrikContent">
							<a href="<?php echo $link_post; ?>"
								title="Lihat <?php echo $val->post_title; ?>">
								<h4 class="ht_rubrikPost">
									<?php echo $val->post_title; ?>
								</h4>
							</a>
							<div class="content_rubrik">
								<?php echo substr(get_the_excerpt($val->ID), 0,150); ?>...
							</div>
							<p class="a_nextRubrik">
								<a href="<?php echo $link_post; ?>">Selengkapnya »</a>
							</p>
						</div>

						<div class="bx_sm_rubrikCat">
							<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
							<span class="r_cat">
								<?php echo get_comments_number($val->ID); ?> Komentar
							</span>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_top'])) { ?>
		<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
			<img src="<?php echo $ads['ads_top']->banner; ?>">
		</a>
	<?php } ?>

	<p>&nbsp;</p>

	<div class="row_articleTitle row_articleTitleContent">Opini Lainnya</div>
	<div class="row">
		<?php foreach($opini as $val) { ?>
			<?php
				$link_post = get_the_permalink($val->ID);
				$short_name = (strlen($val->post_title) > 38) ? substr($val->post_title, 0, 38).'...' : $val->post_title;
			?>
			<div class="col-xs-12 col-md-12 nopadding_left nopadding_right col_v_listRubrik">
				<div class="bxsm_listRubrik">
					<div class="left_listRubrik">
						<div class="mg_sm_rubrik">
							<img src="<?php echo $val->foto; ?>" style="<?php echo $val->style_emptyphoto; ?>" alt="<?php echo $val->alt_foto; ?>">
						</div>
					</div>
					
					<div class="right_listRubrik">
						<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
							<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
						</a>
						<div class="info_sm_listRubrik">
							<?php echo strip_tags(substr(get_the_excerpt($val->ID), 0,80)); ?>...
						</div>
						<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
					</div>
					<div class="bx_sm_rubrikCat">
						<span class="l_cat">
							<?php echo date('d F Y', strtotime($val->post_date)); ?>
						</span>
						<span class="r_cat">
							<?php echo get_comments_number($val->ID); ?> Komentar
						</span>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

	<?php if($total > $per_page) { ?>
		<div class="pagination">
			<?php
				pagination(
					array(
						'base'				=> home_url() . '/opini?',
						'page'				=> $page,
						'pages' 			=> $pages,
						'key'					=> 'hal',
						'next_text'		=> '&rsaquo;',
						'prev_text'		=> '&lsaquo;',
						'first_text'	=> '&laquo;',
						'last_text'		=> '&raquo;'
					)
				);
			?>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
			<img src="<?php echo $ads['ads_bottom']->banner; ?>">
		</a>
	<?php } ?>

	<?php if(!empty($beritaPopuler)) { ?>
		<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
			<div class="row_articleTitle">Berita Populer</div>
			<ul class="listBeritaPopuler listBeritaPopulerRight">
				<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
					<li>
						<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
							<span><?php echo $key+1; ?></span>
							<span><?php echo $berita->post_title; ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_right'])) { ?>
		<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
			<img src="<?php echo $ads['ads_right']->banner; ?>">
		</a>
	<?php } ?>	
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>