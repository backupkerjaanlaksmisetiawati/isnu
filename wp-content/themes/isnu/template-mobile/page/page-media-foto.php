<?php
	$per_page = '16';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getGaleri('-1'));
	$pages = ceil($total/$per_page);

	$galleries = getGaleri(
		$per_page,
		array(
			'paged' => $page
		)
	);
?>

<style>
	/* .fancybox-slide--image {
		padding-top: 81px;
	} */
	/* .fancybox-content {
		transform: translate(349px, 80px);
	} */
	.fancybox-caption {
		/* top: 0; */
		background: none;
	}
	.fancybox-caption__body > div {
		display: flex;
		align-items: center;
		justify-content: space-evenly;
	}
</style>

<div class="row_globalPage row_homeBerita ajax_load_foto">
	<h1 class="ht_home ht_homeRubrik">Galeri ISNU</h1>
	<p>&nbsp;</p>
	
	<div id="wrap_load_foto_mobile" class="row wrap_mediaFoto wrap_mediaVideo wrap_mediaVideoArchive">
		<?php foreach($galleries as $k => $ft) { ?>
			<?php
				$html_caption = "<div>";
					$html_caption .= "<p>".$ft->post_title."</p>";
					$html_caption .= "<p>".date('d F Y', strtotime($ft->post_date))."</p>";
				$html_caption .= "</div>";
			?>
			<a href="<?php echo $ft->foto; ?>"
				data-fancybox="gallery-foto"
				data-options='{"caption" : "<?php echo $html_caption; ?>"}'
				title="Lihat <?php echo $ft->post_title; ?>"
				class="col-xs-4 col-md-4 media_video">
				<div class="wrap_videoList">
					<div class="wrap_mediaVideoThumb">
						<div class="media_videoThumb">
							<img src="<?php echo $ft->foto; ?>" 
								alt="<?php echo $ft->alt_foto; ?>" />
						</div>
					</div>
				</div>
			</a>
		<?php } ?>
	</div>

	<div class="row box_v_listRubrik act">
		<div class="row load_ajax_foto_mobile"
			data-next-page="2"
			data-per-page-mobile="<?php echo $per_page; ?>"></div>
		<div class="mg_pleasewait">
			<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
			<p>Mohon menunggu...</p>
		</div>
	</div>
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>