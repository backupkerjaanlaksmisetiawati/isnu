<?php
  $data_banner = get_post_meta($post->ID,'homebanner',true);
	$data_mimpi = get_post_meta($post->ID,'mimpiisnu',true);
?>

<div class="row_mobileHomepage">
  <?php if(isset($data_banner) AND $data_banner != ''){ ?>
    <?php
			$photo_id = $data_banner[0]['home-banner'];
			$linkpoto = wp_get_attachment_image_src($photo_id,'full');
			if($linkpoto){
				$urlphoto = $linkpoto['0'];
			}else{
				$urlphoto = get_template_directory_uri().'/library/images/temp/home_banner.jpg';
			}
    ?>
    <div class="mobile_homeBanner">
      <img src="<?php echo $urlphoto; ?>" alt="Banner ISNU">
    </div>

    <div class="mobile_contentBanner">
      <div class="">
        <h1 class="ht_homeBanner"><?php echo $data_banner[0]['title-banner']; ?></h1>
        <div class="info_homeBanner"><?php echo $data_banner[0]['info-banner']; ?></div>

        <a href="<?php echo $data_banner[0]['link-banner']; ?>" title="Lihat detail...">
          <input type="button" class="btn_homeBanner" value="<?php echo $data_banner[0]['button-banner']; ?>">
        </a> 
      </div>
    </div>	
	<?php } ?>
</div>

<div class="row_mobileMimpi">
	<div class="col_mimpiHome">
    <?php /*
    //temporary hide
    */ ?>
		<h2 class="mobile_mimpiTitle">Mimpi Kami</h2>
		<div class="mobile_mimpiDesc">
      Kami berupaya meningkatkan kualitas hidup masyarakat dimulai dengan beberapa langkah:
    </div>

		<div id="mobileListMimpi" class="row_listMimpi owl-carousel owl-theme">
			<?php if(isset($data_mimpi) AND !empty($data_mimpi)){ ?>
			  <?php foreach ($data_mimpi as $key => $value) { ?>
					<?php 
            $photo_mimpi = $value['image-mimpi'];
            $url_photo = wp_get_attachment_image_src($photo_mimpi,'full');
            if($url_photo){
              $urlphoto_mimpi = $url_photo['0'];
            }else{
              $urlphoto_mimpi = get_template_directory_uri().'/library/images/banner/home_banner.jpg';
            }

            $title_mimpi = $value['title-mimpi'];
            $deskripsi_mimpi = $value['deskripsi-mimpi'];
            $link_mimpi = $value['link-mimpi'];
          ?>

          <div class="box_mobileListMimpi" style="width:255px">
            <div class="imgMobileListMimpi">
              <img src="<?php echo $urlphoto_mimpi; ?>">
            </div>

            <h4 class="titleMobileListMimpi">
              <?php echo $title_mimpi; ?>
            </h4>

            <div class="descMobileListMimpi">
              <?php echo $deskripsi_mimpi; ?>
            </div>

            <a class="a_detailMimpi" href="<?php echo $link_mimpi; ?>">
              Lihat Selengkapnya
              <span><i class="fas fa-chevron-right"></i></span>
            </a>
          </div>
			  <?php } ?>
			<?php } ?>
		</div>
	</div>
    
  <div class="row_mobileBergabungHome">
    <div class="col_mobileBergabungHome">
      <div class="">
        <div class="ht_bergabung">Mari Bergabung!</div>
        <div class="info_bergabung">
          Tunjukkan bakti bagi negeri, berjuang bersama kami untuk Indonesia yang lebih baik.
        </div>
        <a href="<?php echo home_url(); ?>/daftar">
          <input type="button" class="btn_mobileBergabung" value="Gabung Sekarang">
        </a>
      </div>	
    </div>
  </div>
</div>

<?php
  get_template_part(
    'template-mobile/content/content',
    'berita'
  );
?>

<?php
  get_template_part(
    'template-mobile/content/content',
    'videoisnu'
  );
?>

<?php
  get_template_part(
    'template-mobile/content/content',
    'tokoh'
  );
?>