<?php
	$per_page = '10';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getPrograms('-1'));
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

	$getProgram = getPrograms(
		$per_page,
		array(
			'paged' => $page
		)
	);
	$halfProgram = round(count($getProgram)/2);

	$ads = getAds();

	$categories = get_terms(
		array(
			'taxonomy'    => 'kategori-program',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);
?>

<div class="row_globalPage row_homeBerita">
	<h1 class="ht_home ht_homeRubrik">PROGRAM</h1>

	<p>&nbsp;</p>
	<div class="col_mobileCategories">
		<div class="sub_ht_rubrik">Kategori</div>
		<select class="select-option select-basic-hide-search">
			<option value="<?php echo home_url() . '/kategori-program/'; ?>" selected>Opini Terbaru</option>
			<?php foreach ( $categories as $category ) { ?>
				<option value="<?php echo home_url(); ?>/kategori-program/<?php echo $category->slug; ?>"><?php echo $category->name; ?></option>
			<?php } ?>
		</select>
	</div>
	<p>&nbsp;</p>

	<div class="row_articleTitle">PROGRAM KERJA ISNU</div>

	<?php if( !empty($getProgram) ) { ?>
		<?php foreach($getProgram as $k => $program) { ?>
			<?php if($k < $halfProgram) { ?>
				<div class="row wrap_progList">
					<div class="wrap_progImg">
						<div class="prog_img">
							<img src="<?php echo $program->photo; ?>" alt="<?php echo $program->alt_photo; ?>" />
						</div>
					</div>

					<div class="wrap_progTxt">
						<a href="<?php echo home_url(); ?>/program/<?php echo $program->post_name; ?>" class="prog_title">
							<?php
								$check_len = strlen( strip_tags($program->post_title) );
								if( $check_len > 50 ) {
									echo substr(strip_tags($program->post_title), 0, 50)  . '...';
								} else {
									echo strip_tags($program->post_title);
								}
							?>
						</a>

						<div class="prog_excerpt">
							<?php echo $program->cropped_content; ?>
						</div>
						
						<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
					</div>

					<div class="bx_sm_rubrikCat">
						<span class="l_cat"><?php echo date('d F Y', strtotime($program->post_date)); ?></span>
						<span class="r_cat"><?php echo $program->category_name; ?></span>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php } ?>

	<?php if(!empty($ads['ads_top'])) { ?>
		<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top ik_program" target="_blank">
			<img src="<?php echo $ads['ads_top']->banner; ?>">
		</a>
	<?php } ?>

	<?php if( !empty($getProgram) ) { ?>
		<?php foreach($getProgram as $k => $program) { ?>
			<?php if($k >= $halfProgram) { ?>
				<div class="row wrap_progList">
					<div class="wrap_progImg">
						<div class="prog_img">
							<img src="<?php echo $program->photo; ?>" alt="<?php echo $program->alt_photo; ?>" />
						</div>
					</div>

					<div class="wrap_progTxt">
						<a href="<?php echo home_url(); ?>/program/<?php echo $program->post_name; ?>" class="prog_title">
							<?php
								$check_len = strlen( strip_tags($program->post_title) );
								if( $check_len > 50 ) {
									echo substr(strip_tags($program->post_title), 0, 50)  . '...';
								} else {
									echo strip_tags($program->post_title);
								}
							?>
						</a>

						<div class="prog_excerpt">
							<?php echo $program->cropped_content; ?>
						</div>
						
						<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
					</div>

					<div class="bx_sm_rubrikCat">
						<span class="l_cat"><?php echo date('d F Y', strtotime($program->post_date)); ?></span>
						<span class="r_cat"><?php echo $program->category_name; ?></span>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	<?php } ?>

	<?php if($total > $per_page) { ?>
		<div class="pagination">
			<?php
				pagination(
					array(
						'base'				=> home_url() . '/program?',
						'page'				=> $page,
						'pages' 			=> $pages,
						'key'					=> 'hal',
						'next_text'		=> '&rsaquo;',
						'prev_text'		=> '&lsaquo;',
						'first_text'	=> '&laquo;',
						'last_text'		=> '&raquo;'
					)
				);
			?>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_right'])) { ?>
		<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" style="margin:0px 0px 30px 0px;" target="_blank">
			<img src="<?php echo $ads['ads_right']->banner; ?>">
		</a>
	<?php } ?>

	<?php if(!empty($beritaPopuler)) { ?>
		<div class="row_articleCategories row_artikelPopuler">
			<div class="row_articleTitle">Berita Populer</div>
			<ul class="listBeritaPopuler listBeritaPopulerRight">
				<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
					<li>
						<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
							<span><?php echo $key+1; ?></span>
							<span><?php echo $berita->post_title; ?></span>
						</a>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
			<img src="<?php echo $ads['ads_bottom']->banner; ?>">
		</a>
	<?php } ?>
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>