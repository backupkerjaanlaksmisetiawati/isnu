<?php
	$videoCategories = getVideoCategories();
	$highlightVideos = getHighlightVideos(4);

	$excludeVid = array();
	foreach($highlightVideos as $idHighlight) {
		array_push($excludeVid, $idHighlight->ID);
	}
	if(!empty($excludeVid)) {
		$newReleaseVideos = getVideos(
			'3',
			array(
				'exclude' => $excludeVid
			)
		);
	} else {
		$newReleaseVideos = getVideos('3');
	}
?>

<div class="row_globalPage row_homeBerita">
	<h1 class="ht_home ht_homeRubrik">VIDEO ISNU</h1>
	<p>&nbsp;</p>

	<?php /*<img src="https://img.youtube.com/vi/t5WmoR9hnY8/maxresdefault.jpg" />*/ ?>
	<?php if(!empty($highlightVideos)) { ?>
		<div class="row">
			<div class="col-xs-12 col-md-12 wrap_mediaHighlight wrap_mediaHighlightMobileVideo">
				<a href="<?php echo get_the_permalink($highlightVideos[0]->ID); ?>"
					title="Lihat <?php echo $highlightVideos[0]->post_title; ?>"
					class="media_highlightWrapper">
					<div class="media_highlight">
						<div class="iconplay">
							<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
						</div>
						<img src="<?php echo $highlightVideos[0]->foto; ?>"
							alt="<?php echo $highlightVideos[0]->alt_foto; ?>" />
					</div>

					<h5>
						<?php
							echo (strlen($highlightVideos[0]->post_title) > 100) ? 
								substr($highlightVideos[0]->post_title, 0, 100).'...' : 
								$highlightVideos[0]->post_title;
						?>
					</h5>
					<p>
						<?php 
							echo (strlen(strip_tags($highlightVideos[0]->post_content)) > 200) ? 
								substr(strip_tags($highlightVideos[0]->post_content), 0, 200).'...' : 
								strip_tags($highlightVideos[0]->post_content);
						?>
					</p>
				</a>
			</div>

			<div class="col-xs-12 col-md-12 wrap_mediaHighlightSmall">
				<?php foreach($highlightVideos as $key => $vid) { ?>
					<?php if($key != 0) { ?>
						<a href="<?php echo get_the_permalink($vid->ID); ?>"
							title="Lihat <?php echo $vid->post_title; ?>"
							class="row media_highlightSmall">
							<div class="col-xs-6 col-md-6 wrap_mediaHighlightSmallThumb">
								<div class="iconplay">
									<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
								</div>

								<div class="media_highlightSmallThumb">
									<img src="<?php echo $vid->foto; ?>" 
										alt="<?php echo $vid->alt_foto; ?>" />
								</div>
							</div>

							<div class="col-xs-6 col-md-6 media_highlightSmallTxt">
								<h5>
									<?php
										echo (strlen($vid->post_title) > 60) ? 
											substr($vid->post_title, 0, 60).'...' : 
											$vid->post_title;
									?>
								</h5>
								<p>
									<?php echo date('d F Y', strtotime($vid->post_date)); ?>
								</p>
							</div>
						</a>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	
	<div class="wrap_mediaKategori">
		<div class="wrap_mediaKategoriTitle clearfix">
			<h5 class="media_kategoriTitle">Video Terbaru</h5>
		</div>

		<?php if(!empty($newReleaseVideos) || !empty($newReleaseVideos[0])) { ?>
			<div class="row wrap_mediaVideo">
				<?php foreach($newReleaseVideos as $vid) { ?>
					<a href="<?php echo get_the_permalink($vid->ID); ?>"
						title="Lihat <?php echo $vid->post_title; ?>"
						class="row media_highlightSmall">
						<div class="col-xs-6 col-md-6 wrap_mediaHighlightSmallThumb">
							<div class="iconplay">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
							</div>

							<div class="media_highlightSmallThumb">
								<img src="<?php echo $vid->foto; ?>" 
									alt="<?php echo $vid->alt_foto; ?>" />
							</div>
						</div>

						<div class="col-xs-6 col-md-6 media_highlightSmallTxt">
							<h5>
								<?php
									echo (strlen($vid->post_title) > 60) ? 
										substr($vid->post_title, 0, 60).'...' : 
										$vid->post_title;
								?>
							</h5>
							<p>
								<?php echo date('d F Y', strtotime($vid->post_date)); ?>
							</p>
						</div>
					</a>
				<?php } ?>
			</div>
		<?php } ?>
		
		<div class="box_btnMediaKategori">
			<a href="<?php echo home_url() . '/media/video/new-release/'; ?>"
				class="btn_mediaKategori">
				Lihat Semua
			</a>
		</div>
	</div>
	
	<?php foreach($videoCategories as $category) { ?>
		<?php
			$vidCat = getVideos(
				'3',
				array(
					'cat' => array($category->term_id)
				)
			);
		?>

		<?php if(!empty($vidCat)) { ?>
			<div class="wrap_mediaKategori">
				<div class="wrap_mediaKategoriTitle clearfix">
					<h5 class="media_kategoriTitle">
						<?php echo $category->name; ?>
					</h5>
				</div>

				<div class="row wrap_mediaVideo">
					<?php foreach($vidCat as $vid) { ?>
						<a href="<?php echo get_the_permalink($vid->ID); ?>"
							title="Lihat <?php echo $vid->post_title; ?>"
							class="row media_highlightSmall">
							<div class="col-xs-6 col-md-6 wrap_mediaHighlightSmallThumb">
								<div class="iconplay">
									<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
								</div>

								<div class="media_highlightSmallThumb">
									<img src="<?php echo $vid->foto; ?>" 
										alt="<?php echo $vid->alt_foto; ?>" />
								</div>
							</div>

							<div class="col-xs-6 col-md-6 media_highlightSmallTxt">
								<h5>
									<?php
										echo (strlen($vid->post_title) > 60) ? 
											substr($vid->post_title, 0, 60).'...' : 
											$vid->post_title;
									?>
								</h5>
								<p>
									<?php echo date('d F Y', strtotime($vid->post_date)); ?>
								</p>
							</div>
						</a>
					<?php } ?>
				</div>
		
				<div class="box_btnMediaKategori">
					<a href="<?php echo home_url() . '/' . $category->taxonomy . '/' . $category->slug; ?>"
						class="btn_mediaKategori">
						Lihat Semua
					</a>
				</div>
			</div>
		<?php } ?>
	<?php } ?>
</div>

<?php
	get_template_part(
		'template-mobile/content/content',
		'tokoh'
	);
?>