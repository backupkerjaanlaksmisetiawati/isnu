<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php echo get_bloginfo(); ?> <?php wp_title(); ?></title>

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	<?php // or, set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<?php // Put you url link font here....  ?>
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700,800,900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,700i,900&display=swap" rel="stylesheet">

	<?php // Insert your css & jss only at function.php ?>

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>

	<?php // drop Google Analytics Here ?>


	<?php // end analytics ?>

</head>

<body <?php body_class(); ?>>

<?php
	$page_id = get_the_ID();
	$page_post = get_post($page_id);

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
?>

<?php //check if logged user already active or not ?>
<?php if(isset($u_id) AND $u_id != '' && (!empty($page_post) && $page_post->post_name !== 'login')){ ?>
	<?php
		$all_meta_for_user = get_user_meta( $u_id );
		$is_active = (isset($all_meta_for_user['userstatus'])) ? $all_meta_for_user['userstatus'][0] : 0;
	?>
	<?php if($is_active == 0) { ?>
		<div id="redirect_login" data-hurl="<?php echo home_url(); ?>/login"></div>
		<style>
			#container {
				display: none;
			}
		</style>
		<script>
			var plant = document.getElementById('redirect_login');
			var hurl = plant.getAttribute('data-hurl'); 
				setTimeout(function(){
						location.replace(hurl); 
					}, 5);
		</script>
	<?php } ?>
<?php } ?>

<article id="container">
	
	<div class="desktop-template">
		<?php
			get_template_part(
				'template-desktop/global/global',
				'header'
			);
		?>
	</div>

	<div class="mobile-template">
		<?php
			get_template_part(
				'template-mobile/global/global',
				'header'
			);
		?>
	</div>

<?php // next to body and footer.php ?>