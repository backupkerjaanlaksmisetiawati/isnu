<?php
	$id_post = $post->ID;
	$date_post = get_the_date('d F Y', $id_post);
	$categories = getKategoriOpini();
	$ads = getAds('detail_article');
	$current_category = get_the_terms( $id_post, 'kategori-opini' );

	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'full' );
	if($thumb){
		$urlphoto = $thumb['0'];
	}else{
		$urlphoto = '';
	}
	$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

	$opiniTerbaru = getOpini('4');

	$related = getOpini('5');

	/*
	$opiniTerbaru = getOpini(
		'4',
		// array(
		// 	'exclude' => array($id_post)
		// )
	);

	$related = getOpini(
		'5',
		// array(
		// 	'cat' => $current_category[0]->term_id,
		// 	'exclude' => array($id_post)
		// )
	);
	*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>
	role="article" itemscope
	itemtype="http://schema.org/BlogPosting">
	
<div class="row row_globalPage row_postDetail">
	<div class="col-md-8">
		<div class="bx_htpost">
			<?php if(isset($urlphoto) AND $urlphoto != ''){ ?>
				<div class="mg_postDetail">
					<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
				</div>
			<?php } ?>
			<h1 class="ht_htpostDetail"><?php echo get_the_title($post->ID); ?></h1>
			<div class="row">
				<div class="col-md-12 col_rightcat_post">
					<span class="sp_rightpost"><?php echo $date_post; ?></span>
					<span class="sp_greenpost"><?php echo $current_category[0]->name; ?></span>
				</div>
			</div>
		</div>

		<?php if(!empty($ads['ads_top'])) { ?>
			<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
				<img src="<?php echo $ads['ads_top']->banner; ?>">
			</a>
		<?php } ?>

		<div class="bx_contentPost">
			<?php the_content(); ?>
		</div>
		
		<?php if(!empty($opiniTerbaru)) { ?>
			<div class="bx_relatedPost">
				<h2 class="ht_relatedPost">Opini Terbaru</h2>
				<div id="default_berita" class="row box_global_berita">
					<?php foreach($opiniTerbaru as $val) { ?>
						<?php
							$link_post = get_the_permalink($val->ID);
							$short_name = $val->post_title;
							if(strlen($short_name) > 60) $short_name = substr($short_name, 0, 60).'...';
						?>

						<div class="col-md-6 col_v_listRubrik">
							<div class="bxsm_listRubrik">
								<div class="left_listRubrik">
									<div class="mg_sm_rubrik">
										<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
									</div>
								</div>
								<div class="right_listRubrik">
									<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
										<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
									</a>
									<div class="info_sm_listRubrik">
										<?php echo substr(get_the_excerpt($val->ID), 0, 130); ?>...
									</div>
									<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
								</div>
								<div class="bx_sm_rubrikCat detailPost">
									<span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
									<span class="r_cat"><?php echo $val->category_name; ?></span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

		<?php if(!empty($ads['ads_bottom'])) { ?>
			<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
				<img src="<?php echo $ads['ads_bottom']->banner; ?>">
			</a>
		<?php } ?>

		<div class="bx_commentPost">
			<h2 class="ht_relatedPost">Komentar</h2>
			<div class="sin_comment"><?php comments_template(); ?></div> 
		</div>
		
	</div>
	<div class="col-md-4">
		<?php if(!empty($beritaPopuler)) { ?>
			<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
				<div class="row_articleTitle">Berita Populer</div>
				<ul class="listBeritaPopuler listBeritaPopulerRight">
					<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
						<li>
							<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
								<span><?php echo $key+1; ?></span>
								<span><?php echo $berita->post_title; ?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>

		<?php if(!empty($related)) { ?>
			<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
				<div class="row_articleTitle">Opini Terkait</div>
				<ul class="listBeritaPopuler listBeritaPopulerRight">
					<?php foreach ( $related as $key => $val ) { ?>
						<li>
							<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
								<span><?php echo $key+1; ?></span>
								<span><?php echo $val->post_title; ?></span>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
		
		<div class="bx_right_jelajah">
			<h2 class="ht_relatedPost row_articleTitle">Jelajah Kategori</h2>

			<ul class="listBeritaCat">
				<?php foreach ( $categories as $category ) { ?>
					<li class="<?php echo ($current_category[0]->term_id == $category->term_id) ? 'act' : ''; ?>">
						<a href="<?php echo home_url() . '/kategori-opini/'.$category->slug.'/'; ?>">
							<?php echo $category->name; ?>
						</a>
					</li>
				<?php } ?>
			</ul>

		</div>

		<?php if(!empty($ads['ads_right'])) { ?>
			<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
				<img src="<?php echo $ads['ads_right']->banner; ?>">
			</a>
		<?php } ?>

		<?php get_template_part( 'content', 'rightvideo' ); ?>

	</div>

</div>

</article>