<?php
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$ads = getAds('detail_article');
	$id_post = $post->ID;
	$current_category = get_the_terms( $id_post, 'kategori-program' );
	$category_id = ( !empty($current_category[0]) ) ? $current_category[0]->term_id : '';
	$category_name = ( !empty($current_category[0]) ) ? $current_category[0]->name : '';

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

	$categories = get_terms(
		array(
			'taxonomy'    => 'kategori-program',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);

	$related = getOpini('5');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
<?php

$date_post = get_the_date('d F Y', $id_post);

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'full' );
if($thumb){
	$urlphoto = $thumb['0'];
}else{
	// $urlphoto = get_template_directory_uri().'/library/images/sorry.png';
	$urlphoto = '';
}
$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);

$allcategory = array();
$category_post = get_the_category($id_post);
foreach ($category_post as $key => $value) {
	$cat_parent = $value->category_parent;
	if($cat_parent == 4){ // 4 = berita-isnu ID
		$cat_id = $value->term_id;
		$cat_name = $value->name;
		$cat_slug = $value->slug;
		array_push($allcategory, $cat_id);
	}
}


$get_pdfpublikasi = get_post_meta( $id_post , 'pdfpublikasi' );
$pdfpublikasi = array();
if( !empty($get_pdfpublikasi[0]) ) {
  foreach( $get_pdfpublikasi as $get_pdfpublikasi ) {
    if( !empty($get_pdfpublikasi[0]) ) {
      foreach( $get_pdfpublikasi as $get_pdfpublikasi ) {
        $pdfpublikasi = $get_pdfpublikasi;
      }
    }
  }
}

?>
	
	<div class="row row_globalPage row_postDetail">
		<div class="col-md-8">

			<div class="bx_htpost">
				<?php if(isset($urlphoto) AND $urlphoto != ''){ ?>
					<div class="mg_postDetail">
						<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
					</div>
				<?php } ?>
				<h1 class="ht_htpostDetail"><?php echo get_the_title($post->ID); ?></h1>
				<div class="row">
					<div class="col-md-12 col_rightcat_post">
						<span class="sp_rightpost"><?php echo $date_post; ?></span>
						<span class="sp_greenpost"><?php echo $category_name; ?></span>
					</div>
				</div>
			</div>

			<?php if(!empty($ads['ads_top'])) { ?>
				<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
					<img src="<?php echo $ads['ads_top']->banner; ?>">
				</a>
			<?php } ?>

			<div class="bx_contentPost">
				<?php the_content(); ?>
			</div>
			
			<?php if(isset($u_id) AND $u_id != '' AND $u_id != 0){ ?>
				<?php if( !empty($pdfpublikasi) ) { ?>
					<div class="bx_contentPost bx_contentPostPDF">
						<h4>
							<strong>UNDUH TEXT LENGKAP</strong>
						</h4>
						<?php foreach( $pdfpublikasi as $pdf) { ?>
							<p>
								<a href="<?php echo wp_get_attachment_url($pdf['pdf-file']); ?>">
									<?php //echo $pdf['pdf-title']; ?>
									<?php
										$file_path = explode('/', get_attached_file($pdf['pdf-file']));
										echo $file_path[count($file_path) - 1];
									?>
								</a>
							</p>
						<?php } ?>
					</div>
				<?php } ?>
			<?php } ?>

			<?php if(!empty($ads['ads_bottom'])) { ?>
				<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
					<img src="<?php echo $ads['ads_bottom']->banner; ?>">
				</a>
			<?php } ?>

			<div class="bx_commentPost">
				<h2 class="ht_relatedPost">Komentar</h2>
				<div class="sin_comment"><?php comments_template(); ?></div> 
			</div>
			
		</div>

		<div class="col-md-4">
			<?php if(!empty($beritaPopuler)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Berita Populer</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $berita->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

			<?php if(!empty($related)) { ?>
				<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
					<div class="row_articleTitle">Program Terkait</div>
					<ul class="listBeritaPopuler listBeritaPopulerRight">
						<?php foreach ( $related as $key => $val ) { ?>
							<li>
								<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
									<span><?php echo $key+1; ?></span>
									<span><?php echo $val->post_title; ?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
			
			<div class="bx_right_jelajah">
				<h2 class="row_articleTitle">Jelajah Kategori</h2>
				<ul class="listBeritaCat">
					<?php foreach ( $categories as $category ) { ?>
						<li class="<?php echo ($category_name == $category->name) ? 'act' : 'ac'; ?>">
							<a href="<?php echo home_url(); ?>/kategori-program/<?php echo $category->slug; ?>">
								<?php echo $category->name; ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<?php if(!empty($ads['ads_right'])) { ?>
				<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
					<img src="<?php echo $ads['ads_right']->banner; ?>">
				</a>
			<?php } ?>

			<?php get_template_part( 'content', 'rightvideo' ); ?>

		</div>

	</div>

</div>

<?php
  get_template_part(
    'template-desktop/content/content',
    'videoisnu'
  );
?>