<?php
	$per_page = '10';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
  $total_uri = count($req_uri);
  $slug = $req_uri[$total_uri-2];
  
	$current_category = get_term_by('slug', $slug, 'kategori-opini');
	$opiniTerbaru = getOpini('5');
	$categories = getKategoriOpini();
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(
		getOpini(
			'-1',
			array(
				'cat' => $current_category->term_id
			)
		)
	);
	$pages = ceil($total/$per_page);
	
	$getOpini = getOpini(
		$per_page,
		array(
			'paged' => $page,
			'cat' => $current_category->term_id
		)
	);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
?>

<article class="row row_globalPage row_homeBerita allow_ajax_opini">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">
			OPINI <?php echo $current_category->name; ?>
		</h1>
		<p>&nbsp;</p>
		
		<div class="row">
			<div class="col-xs-8 col-md-8 nopadding_left">
				<?php if( !empty($getOpini) ) { ?>
					<div class="row_archiveArticle row">
						<?php foreach($getOpini as $val) { ?>
							<?php $link_post = get_the_permalink($val->ID); ?>
							<div class="col-xs-6 col-md-6 col_article">
								<div class="list_article">
									<div class="article_shortContent">
										<div class="left_articleShortContent">
											<div class="imgArticle">
												<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
											</div>
										</div>
										
										<div class="right_articleShortContent">
											<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
												<h5 class="titleArticle">
													<?php echo $val->post_title; ?>
												</h5>
											</a>
											<div class="shortDescription">
												<?php echo strip_tags(substr(get_the_excerpt($val->ID), 0,150)); ?>...
											</div>
											<p class="linkDetail">
												<a href="<?php echo $link_post; ?>">Selengkapnya &raquo;</a>
											</p>
										</div>
									</div>

									<div class="article_categoryName opini_date clearfix">
										<span class="l_cat">
											<?php echo date('d F Y', strtotime($val->post_date)); ?>
										</span>
										<span class="r_cat">
											<?php echo get_comments_number($val->ID); ?> Komentar
										</span>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>

					<?php if($total > $per_page) { ?>
						<div class="pagination">
							<?php
								pagination(
									array(
										'base'				=> home_url() . '/kategori-opini/' . $current_category->slug . '?',
										'page'				=> $page,
										'pages' 			=> $pages,
										'key'					=> 'hal',
										'next_text'		=> '&rsaquo;',
										'prev_text'		=> '&lsaquo;',
										'first_text'	=> '&laquo;',
										'last_text'		=> '&raquo;'
									)
								);
							?>
						</div>
					<?php } ?>
				<?php } else { ?>
					<div id="post-not-found" class="hentry clearfix">
						<div class="article-header">
							<h4><?php _e( 'Tidak ada opini untuk kategori ini.', 'bonestheme' ); ?></h4>
						</div>
					</div>
				<?php } ?>
			</div>

			<div class="col-xs-4 col-md-4">
				<?php if(!empty($beritaPopuler)) { ?>
					<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
						<div class="row_articleTitle">Berita Populer</div>
						<ul class="listBeritaPopuler listBeritaPopulerRight">
							<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $berita->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>

				<?php if(!empty($opiniTerbaru)) { ?>
					<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
						<div class="row_articleTitle">Opini Terbaru</div>
						<ul class="listBeritaPopuler listBeritaPopulerRight">
							<?php foreach ( $opiniTerbaru as $key => $val ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $val->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $val->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>
				
				<div class="bx_right_jelajah">
					<h2 class="row_articleTitle">Jelajah Kategori</h2>
					<ul class="listBeritaCat">
						<?php foreach ( $categories as $category ) { ?>
							<li class="<?php echo ($current_category->name == $category->name) ? 'act' : 'ac'; ?>">
								<a href="<?php echo home_url(); ?>/kategori-opini/<?php echo $category->slug; ?>">
									<?php echo $category->name; ?>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</article>
