<?php
	$per_page = '10';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
	$total_uri = count($req_uri);
	$slug = $req_uri[$total_uri-2];
  $current_category = get_term_by('slug', $slug, 'tipe-publikasi');

	$publikasiTerbaru = getPublikasi('5');

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
  
	//pagination
	$page = isset($_GET['hal']) ? (int)$_GET["hal"]:1;
	$total = count(
    getPublikasi(
      '-1', 
      array(
        'kategori' => $current_category->slug
      )
    )
  );
  $pages = ceil($total/$per_page);
  
	$publikasi = getPublikasi(
    $per_page,
    array(
      'kategori'  => $current_category->slug,
      'paged'     => $page
    )
  );
?>

<div class="row row_globalPage row_homeBerita">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">
			<?php echo $current_category->name; ?>
		</h1>

		<div class="row row_listRubrikCategory">
			<div class="col-xs-8 col-md-8 col_bx_vrubrik">
        <?php if(!empty($publikasi)) { ?>
          <div id="default_berita" class="row box_v_listRubrik act">
            <div id="show_publikasi" class="row">
              <?php foreach($publikasi as $val) { ?>
                <?php
									$id_post = $val->ID;
                  $link_post = get_the_permalink($id_post);
                  $short_name = get_the_title($id_post);
                  if(strlen($short_name) > 60) $short_name = substr($short_name, 0, 60).'...';
                ?>

                <div class="col-xs-6 col-md-6 col_v_listRubrik">
                  <div class="bxsm_listRubrik">
                    <div class="left_listRubrik">
                      <div class="mg_sm_rubrik">
                        <img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
                      </div>
                    </div>
                    <div class="right_listRubrik">
                      <a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
                        <h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
                      </a>
                      <div class="info_sm_listRubrik">
                        <?php echo substr(get_the_excerpt($val->ID), 0,140); ?>...
                      </div>
                      <a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
                    </div>
                    <div class="bx_sm_rubrikCat">
                      <span class="l_cat"><?php echo date('d F Y', strtotime($val->post_date)); ?></span>
                      <span class="r_cat"><?php echo $val->category_name; ?></span>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
				  </div>

          <?php if($total > $per_page) { ?>
            <div class="pagination">
              <?php
                pagination(
                  array(
                    'base'				=> home_url() . '/tipe-publikasi/' . $current_category->slug . '?',
                    'page'				=> $page,
                    'pages' 			=> $pages,
                    'key'					=> 'hal',
                    'next_text'		=> '&rsaquo;',
                    'prev_text'		=> '&lsaquo;',
                    'first_text'	=> '&laquo;',
                    'last_text'		=> '&raquo;'
                  )
                );
              ?>
            </div>
          <?php } ?>
        <?php } else { ?>
					<div id="post-not-found" class="hentry clearfix">
						<div class="article-header">
							<h4><?php _e( 'Tidak ada berita untuk kategori ini.', 'bonestheme' ); ?></h4>
						</div>
					</div>
				<?php } ?>
      </div>
      
      <div class="col-xs-4 col-md-4">
				<?php if(!empty($beritaPopuler)) { ?>
					<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
						<div class="row_articleTitle">Berita Populer</div>
						<ul class="listBeritaPopuler listBeritaPopulerRight">
							<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $berita->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
        <?php } ?>
        
				<?php if(!empty($publikasiTerbaru)) { ?>
					<div class="row_articleCategories row_artikelPopuler row_artikelPopulerRight">
						<div class="row_articleTitle">Publikasi Terbaru</div>
						<ul class="listBeritaPopuler listBeritaPopulerRight">
							<?php foreach ( $publikasiTerbaru as $key => $berita ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $berita->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
        <?php } ?>

        <div class="bx_right_jelajah">
          <h2 class="row_articleTitle">Jelajah Kategori</h2>
          <ul class="listBeritaCat">
            <?php 
              $categories = get_terms(
                array(
                  'taxonomy'    => 'tipe-publikasi',
                  'orderby'     => 'name',
                  'hide_empty'  => false,
                )
              );
            ?>
            <?php foreach ( $categories as $category ) { ?>
              <li class="<?php echo ($current_category->name == $category->name) ? 'act' : 'ac'; ?>">
                <a href="<?php echo home_url() . '/category/'.$category->slug.'/'; ?>">
                  <?php echo $category->name; ?>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
		</div>
	</div>
</div>