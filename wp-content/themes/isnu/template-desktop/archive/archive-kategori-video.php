<?php
	$per_page = '12';

	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
	$total_uri = count($req_uri);

	$slug = $req_uri[$total_uri-2];

	$current_category = get_term_by('slug', $slug, 'kategori-video');
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(
		getVideos(
			'-1',
			array(
				'paged' => $page,
				'cat' => array($current_category->term_id)
			)
		)
	);
	$pages = ceil($total/$per_page);

	$videos = getVideos(
		$per_page,
		array(
			'paged' => $page,
			'cat' => array($current_category->term_id)
		)
	);
?>

<div class="row row_globalPage row_homeBerita">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">
			<?php echo $current_category->name; ?>
		</h1>
		<p>&nbsp;</p>
		
		<div id="wrap_load_video" class="row wrap_mediaVideo wrap_mediaVideoArchive">
			<?php foreach($videos as $k => $vid) { ?>
				<a href="<?php echo get_the_permalink($vid->ID); ?>"
					title="Lihat <?php echo $vid->post_title; ?>"
					class="col-xs-3 col-md-3 media_video">
					<div class="wrap_videoList">
						<div class="wrap_mediaVideoThumb">
							<div class="iconplay">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
							</div>
							
							<div class="media_videoThumb">
								<img src="<?php echo $vid->foto; ?>" 
									alt="<?php echo $vid->alt_foto; ?>" />
							</div>
						</div>

						<div class="media_videoTxt">
							<h5>
								<?php
									echo (strlen($vid->post_title) > 55) ? substr($vid->post_title, 0, 55).'...' : $vid->post_title;
								?>
							</h5>
							<p>
								<?php echo date('d F Y', strtotime($vid->post_date)); ?>
							</p>
						</div>
					</div>
				</a>
			<?php } ?>
		</div>

		<?php if($total > $per_page) { ?>
			<div class="pagination">
				<?php
					pagination(
						array(
							'base'				=> home_url() . '/kategori-video/' . $current_category->slug . '?',
							'page'				=> $page,
							'pages' 			=> $pages,
							'key'					=> 'hal',
							'next_text'		=> '&rsaquo;',
							'prev_text'		=> '&lsaquo;',
							'first_text'	=> '&laquo;',
							'last_text'		=> '&raquo;'
						)
					);
				?>
			</div>
		<?php } ?>
	</div>
</div>
