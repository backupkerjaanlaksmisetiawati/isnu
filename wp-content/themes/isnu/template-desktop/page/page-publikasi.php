<?php
	$per_page = '10';

	$publikasi_params = array();
	
	if(isset($_GET['search_by'])) {
		$publikasi_params['search_by'] = $_GET['search_by'];
	}
	if(isset($_GET['search'])) {
		$publikasi_params['search'] = $_GET['search'];
	}
	if(isset($_GET['kategori'])) {
		$publikasi_params['kategori'] = $_GET['kategori'];
	}
	if(isset($_GET['from_tahun'])) {
		$publikasi_params['from_tahun'] = $_GET['from_tahun'];
	}
	if(isset($_GET['to_tahun'])) {
		$publikasi_params['to_tahun'] = $_GET['to_tahun'];
	}

	//pagination
	$page = isset($_GET['hal']) ? (int)$_GET["hal"]:1;
	if(!empty($publikasi_params))	{
		$total = count(getPublikasi('-1', $publikasi_params));
	} else {
		$total = count(getPublikasi('-1'));
	}
	$pages = ceil($total/$per_page);

	$publikasi_params['paged'] = $page;
	$listPublikasi = getPublikasi(
		$per_page,
		$publikasi_params
	);

	$get_tipe_publikasi = get_terms(
		array(
			'taxonomy'    => 'tipe-publikasi',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);

	$get_tahun_publikasi = get_terms(
		array(
			'taxonomy'    => 'tahun-publikasi',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);
	
	$class_trigger_thn = ( isset($_GET['search']) ) ? 'thnSearch' : '';
	$class_trigger_tipe = ( isset($_GET['search']) ) ? 'tipeSearch' : '';
?>

<style>
.box_btnKepengurusan {
  margin-top: 30px;
}
</style>

<div class="row row_globalPage row_homeBerita row_publikasiPage">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">Telurusi Publikasi Ilmiah</h1>

		<form class="row" method="get" id="searchform" action="<?php bloginfo('url'); ?>/publikasi-ilmiah/">
			<h5 class="hd_search col-xs-12 col-md-12">Cari berdasarkan</h5>
			
			<div class="dd_Filter col-xs-2 col-md-2">
				<select name="search_by" class="search_by select-basic-hide-search">
					<option value="semua" <?php echo (isset($_GET['search_by']) && $_GET['search_by'] == 'semua') ? 'selected' : ""; ?>>Semua</option>
					<option value="judul" <?php echo (isset($_GET['search_by']) && $_GET['search_by'] == 'judul') ? 'selected' : ""; ?>>Judul</option>
					<option value="konten" <?php echo (isset($_GET['search_by']) && $_GET['search_by'] == 'konten') ? 'selected' : ""; ?>>Konten</option>
				</select>
			</div>

			<div class="col-xs-9 col-md-9 search_Publikasi">
				<input type="text" name="search" <?php echo (isset($_GET['search'])) ? 'value="' . strip_tags($_GET['search']) . '"' : ""; ?> autocomplete="off" class="fieldSearch" placeholder="Telusuri publikasi" />
			</div>
			
			<div class="col-xs-1 col-md-1 submit_Publikasi">
				<button type="submit" class="">
					<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-search-white.svg">
				</button>
			</div>

			<div>
				<?php if(isset($_GET['from_tahun']) && isset($_GET['kategori'])) { ?>
					<input type="hidden" name="from_tahun" value="<?php echo $_GET['from_tahun']; ?>" />
					<input type="hidden" name="to_tahun" value="<?php echo $_GET['to_tahun']; ?>" />
					<input type="hidden" name="kategori" value="<?php echo $_GET['kategori']; ?>" />
				<?php } ?>
			</div>
		</form>

		<div class="row">
			<div class="col-xs-3 col-md-3">
				<form class="box_v_listRubrik act" method="get" action="<?php bloginfo('url'); ?>/publikasi-ilmiah/">
					<h4 class="hd_filter">FILTER</h4>

					<div class="filter_tahunPublikasi">
						<p class="txt_title">
							<label for="amount">Tahun</label>
							<input type="text" 
								class="thn-publikasi <?php echo $class_trigger_thn; ?>" 
								id="amount" 
								name="from_tahun"
								<?php if(isset($_GET['from_tahun'])) { ?>value="<?php echo $_GET['from_tahun']; ?>"<?php } ?>
								style="border: 0; background: none; width:30px;" 
							/>
							 - 
							<input type="text" 
								class="thn-publikasi <?php echo $class_trigger_thn; ?>" 
								id="amount_to" 
								name="to_tahun"
								<?php if(isset($_GET['to_tahun'])) { ?>value="<?php echo $_GET['to_tahun']; ?>"<?php } ?>
								style="border: 0; background: none; width:30px;" 
							/>
						</p>

						<div class="thn clearfix">
							<div class="pull-left">
								<?php echo ( isset($get_tahun_publikasi[0]) ) ? $get_tahun_publikasi[0]->name : ''; ?>
							</div>
							<div class="pull-right">
								<?php echo ( isset($get_tahun_publikasi[count($get_tahun_publikasi) - 1]) ) ? $get_tahun_publikasi[count($get_tahun_publikasi) - 1]->name : ''; ?>
							</div>
						</div>
						<div id="filterTahunPublikasi"></div>
					</div>

					<?php if( !empty($get_tipe_publikasi) ) { ?>
						<div class="filter_tipePublikasi">
							<p class="txt_title">
								<label for="amount">TIPE PUBLIKASI</label>
							</p>

							<div class="clearfix" style="margin: 0px -7px;">
								<?php foreach ( $get_tipe_publikasi as $tipe ) { ?>
									<label class="pull-left tipe-publikasi tipeSearch <?php if(isset($_GET['kategori']) && $_GET['kategori'] === $tipe->slug) { ?>active<?php } ?>" data-id="<?php echo $tipe->term_id; ?>">
										<input type="radio" name="kategori" <?php if(isset($_GET['kategori']) && $_GET['kategori'] === $tipe->slug) { ?>checked<?php } ?> value="<?php echo $tipe->slug; ?>" />
										<?php echo $tipe->name; ?>
									</label>
								<?php } ?>
							</div>
						</div>						
					<?php } ?>

					<div>
						<?php if(isset($_GET['search_by']) && isset($_GET['search'])) { ?>
							<input type="hidden" name="search_by" value="<?php echo $_GET['search_by']; ?>" />
							<input type="hidden" name="search" value="<?php echo $_GET['search']; ?>" />
						<?php } ?>
					</div>

					<div class="box_btnKepengurusan">
						<button type="submit" class="btn_yellow">
							Terapkan
						</button>
						<a href="<?php echo home_url() . '/publikasi-ilmiah/'; ?>" class="btn_yellow">
							Lihat Semua
						</a>
					</div>
				</form>
			</div>

			<div class="col-xs-9 col-md-9 col_bx_vrubrik">
				<div class="row box_v_listRubrik act">
					<div>
						<?php if( !empty($listPublikasi) ) { ?>
							<?php foreach($listPublikasi as $publikasi) { ?>
								<?php
									$id_post = $publikasi->ID;

									$title_post = get_the_title($id_post);
									$link_post = get_the_permalink($id_post);
									$date_post = get_the_date('d F Y', $id_post);

									$tahun_publikasi = wp_get_object_terms( $id_post, 'tahun-publikasi' );
									$tipe_publikasi = wp_get_object_terms( $id_post, 'tipe-publikasi' );

									$category_post = get_the_category($id_post);
								?>

								<div class="col-xs-12 col-md-12 col_v_listRubrik">
									<div class="bxsm_listRubrik">
										<div class="listPublikasi">
											<div class="tipe_listPublikasi clearfix">
												<?php foreach( $tipe_publikasi as $tThn => $tipe) { ?>
													<div class="pull-left">
														<?php echo $tipe->name; ?>
													</div>
												<?php } ?>
											</div>

											<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $title_post; ?>">
												<h5 class="ht_sm_listPublikasi"><?php echo $title_post; ?></h5>
											</a>
											<p class="author_listPublikasi">
												Penulis: <?php echo get_the_author_meta( 'display_name', $publikasi->post_author ); ?>
											</p>
											<p class="thn_listPublikasi">
												Tahun: 
												<?php foreach( $tahun_publikasi as $kThn => $thn) { ?>
													<?php 
														echo ( $kThn == count($tahun_publikasi) - 1 ) ? $thn->name : $thn->name.", ";
													?>
												<?php } ?>
											</p>
										</div>
									</div>
								</div>
							<?php } ?>

							<?php if($total > $per_page) { ?>
								<?php
									$url_base = home_url() . '/publikasi-ilmiah?';

									if(isset($_GET)) {
										if(isset($_GET['hal'])) {
											unset($_GET["hal"]);
										}
										foreach($_GET as $k => $s) {
											$url_base .= $k.'='.$s.'&';
										}
									}
								?>

								<div class="pagination">
									<?php
										pagination(
											array(
												'base'				=> $url_base,
												'page'				=> $page,
												'pages' 			=> $pages,
												'key'					=> 'hal',
												'next_text'		=> '&rsaquo;',
												'prev_text'		=> '&lsaquo;',
												'first_text'	=> '&laquo;',
												'last_text'		=> '&raquo;'
											)
										);
									?>
								</div>
							<?php } ?>
						<?php } else { ?>
							<h5>Publikasi yang Anda cari tidak ditemukan.</h5>
						<?php } ?>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>