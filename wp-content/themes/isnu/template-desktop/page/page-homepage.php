<?php 
	$ads = getAds();
?>

<div class="row row_hompage">
	<?php 
		$data_banner = get_post_meta($post->ID,'homebanner',true);
		if(isset($data_banner) AND $data_banner != ''){
			$photo_id = $data_banner[0]['home-banner'];
			$linkpoto = wp_get_attachment_image_src($photo_id,'full');
			if($linkpoto){
				$urlphoto = $linkpoto['0'];
			}else{
				$urlphoto = get_template_directory_uri().'/library/images/temp/home_banner.jpg';
			}
	?>
			<div class="mg_homeBanner">
				<img src="<?php echo $urlphoto; ?>" alt="Banner ISNU">
			</div>
			<div class="box_homeBanner">
				<h1 class="ht_homeBanner"><?php echo $data_banner[0]['title-banner']; ?></h1>
				<div class="info_homeBanner"><?php echo $data_banner[0]['info-banner']; ?></div>

				<a href="<?php echo $data_banner[0]['link-banner']; ?>" title="Lihat detail...">
					<input type="button" class="btn_homeBanner" value="<?php echo $data_banner[0]['button-banner']; ?>">
				</a> 
			</div>	
	<?php
		}
	?>	
</div>

<?php /*
//temporary hide
<div class="row row_globalPage row_mimpiHome">
	<div class="col-md-12 col_mimpiHome">
		<h2 class="ht_globalPage ht_mimpiHome">Mimpi Kami</h2>
		<div class="info_mimpiHome">Kami berupaya meningkatkan kualitas hidup masyarakat dimulai dengan beberapa langkah:</div>
		<div class="row row_listMimpi">
			<?php 
				$data_mimpi = get_post_meta($post->ID,'mimpiisnu',true);

				if(isset($data_mimpi) AND !empty($data_mimpi)){
					foreach ($data_mimpi as $key => $value) {
					
					$photo_mimpi = $value['image-mimpi'];
					$url_photo = wp_get_attachment_image_src($photo_mimpi,'full');
					if($url_photo){
						$urlphoto_mimpi = $url_photo['0'];
					}else{
						$urlphoto_mimpi = get_template_directory_uri().'/library/images/banner/home_banner.jpg';
					}

					$title_mimpi = $value['title-mimpi'];
					$deskripsi_mimpi = $value['deskripsi-mimpi'];
					$link_mimpi = $value['link-mimpi'];
			?>
					<div class="col-md-4 col_listMimpi">
						<div class="box_listMimpi">
							<div class="mg_listMimpi">
								<img src="<?php echo $urlphoto_mimpi; ?>">
							</div>
							<h4 class="ht_listMimpi"><?php echo $title_mimpi; ?></h4>
							<div class="info_listMimpi"><?php echo $deskripsi_mimpi; ?></div>

							<a class="a_detailMimpi" href="<?php echo $link_mimpi; ?>">Lebih Lanjut <span><i class="fas fa-chevron-right"></i></span></a>
						</div>
					</div>
			<?php 
					}
				}
			?>
		</div>
    
		<div class="row row_bergabungHome">
			<div class="col-md-2"></div>
			<div class="col-md-8 col_bergabungHome">
				<div class="left_bergabungHome">
					<div class="ht_bergabung">Mari Bergabung!</div>
					<div class="info_bergabung">Tunjukkan bakti bagi negeri, berjuang bersama kami untuk Indonesia yang lebih baik.</div>
				</div>
				<div class="right_bergabungHome">
					<a href="<?php echo home_url(); ?>/daftar">
						<input type="button" class="btn_bergabung" value="Gabung Sekarang">
					</a>
				</div>	
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</div>
*/ ?>

<?php
	get_template_part(
		'template-desktop/content/content',
		'berita'
	);
?>

<?php
	get_template_part(
		'template-desktop/content/content',
		'videoisnu'
	);
?>

<?php
  get_template_part(
    'template-desktop/content/content',
    'tokoh'
  );
?>