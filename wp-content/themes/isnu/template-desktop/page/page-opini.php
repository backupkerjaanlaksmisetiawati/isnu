<?php
	$per_page = '10';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getOpini('-1'));
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$populer = getHighlightOpini();

	$categories = getKategoriOpini();
	$ads = getAds();
	$opini = getOpini(
		$per_page,
		array(
			'paged' => $page
		)
	);
	
	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);
?>

<div class="row row_globalPage row_homeBerita">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">OPINI ISNU</h1>

		<div class="row_articleTitle">Opini Populer</div>

		<div class="row row_listRubrik">
			<?php if(!empty($populer)) { ?>
				<div id="slide_rubrik" class="owl-carousel owl-theme">
					<?php foreach($populer as $val) { ?>
						<?php $link_post = get_the_permalink($val->ID); ?>
						<div class="item col_bx_homeRubrik">
							<div class="left_rubrik_post">
								<div class="mg_rubrik">
									<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
								</div>
							</div>
							<div class="right_rubrik_post">
								<div class="box_rubrikCategory">
									<?php echo $val->category_name; ?>
								</div>
								<a href="<?php echo $link_post; ?>"
									title="Lihat <?php echo $val->post_title; ?>">
									<h4 class="ht_rubrikPost">
										<?php echo $val->post_title; ?>
									</h4>
								</a>
								<div class="content_rubrik">
									<?php echo substr(get_the_excerpt($val->ID), 0,150); ?>...
								</div>
								<a class="a_nextRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
							</div>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>

		<?php if(!empty($ads['ads_top'])) { ?>
			<div class="col_homeTopSmallBanner">
				<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
					<img src="<?php echo $ads['ads_top']->banner; ?>">
				</a>
			</div>
		<?php } ?>
		
		<div class="row row_article">
			<div class="col-xs-3 col-md-3">
				<?php if(!empty($beritaPopuler)) { ?>
					<div class="row_articleCategories row_artikelPopuler">
						<div class="row_articleTitle">Berita Populer</div>
						<ul class="listBeritaPopuler">
							<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $berita->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>

				<div class="row_articleCategories">
					<div class="row_articleTitle">Kategori</div>
					<ul class="listRubrikCat">
						<li class="act">
							<a>Opini Terbaru</a>
						</li>
						<?php foreach ( $categories as $category ) { ?>
							<li>
								<a href="<?php echo home_url() . '/' . $category->taxonomy . '/' . $category->slug; ?>">
									<?php echo $category->name; ?>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			
				<?php if(!empty($ads['ads_right'])) { ?>
					<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
						<img src="<?php echo $ads['ads_right']->banner; ?>">
					</a>
				<?php } ?>
			</div>

			<div class="col-xs-9 col-md-9 col_padding_10">
				<div class="row_articleTitle row_articleTitleContent">Opini</div>
				<div class="row">
					<?php foreach($opini as $val) { ?>
						<?php $link_post = get_the_permalink($val->ID); ?>
						<div class="col-xs-6 col-md-6 col_article">
							<div class="list_article">
								<div class="article_shortContent">
									<div class="left_articleShortContent">
										<div class="imgArticle">
											<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
										</div>
									</div>
									
									<div class="right_articleShortContent">
										<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $val->post_title; ?>">
											<h5 class="titleArticle">
												<?php echo $val->post_title; ?>
											</h5>
										</a>
										<div class="shortDescription">
											<?php echo strip_tags(substr(get_the_excerpt($val->ID), 0,150)); ?>...
										</div>
										<p class="linkDetail">
											<a href="<?php echo $link_post; ?>">Selengkapnya &raquo;</a>
										</p>
									</div>
								</div>

								<div class="article_categoryName opini_date clearfix">
									<span class="l_cat">
										<?php echo date('d F Y', strtotime($val->post_date)); ?>
									</span>
									<span class="r_cat">
										<?php echo get_comments_number($val->ID); ?> Komentar
									</span>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>

        <?php if($total > $per_page) { ?>
					<div class="pagination">
						<?php
							pagination(
								array(
									'base'				=> home_url() . '/opini?',
									'page'				=> $page,
									'pages' 			=> $pages,
									'key'					=> 'hal',
									'next_text'		=> '&rsaquo;',
									'prev_text'		=> '&lsaquo;',
									'first_text'	=> '&laquo;',
									'last_text'		=> '&raquo;'
								)
							);
						?>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	<p>&nbsp;</p>
	
	<?php if(!empty($ads['ads_bottom'])) { ?>
		<div class="col_homeBottomSmallBanner">
			<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
				<img src="<?php echo $ads['ads_bottom']->banner; ?>">
			</a>
		</div>
	<?php } ?>
</div>

<?php
  get_template_part(
    'template-desktop/content/content',
    'tokoh'
  );
?>