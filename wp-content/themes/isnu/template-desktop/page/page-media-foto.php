<?php
	$per_page = '16';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getGaleri('-1'));
	$pages = ceil($total/$per_page);

	$galleries = getGaleri(
		$per_page,
		array(
			'paged' => $page
		)
	);
?>

<style>
	/* .fancybox-slide--image {
		padding-top: 81px;
	} */
	/* .fancybox-content {
		transform: translate(349px, 80px);
	} */
	.fancybox-caption {
		background: none;
	}
	.fancybox-caption__body > div {
		display: flex;
		align-items: center;
		justify-content: space-evenly;
	}
</style>

<div class="row row_globalPage row_homeBerita">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">Galeri ISNU</h1>
		<p>&nbsp;</p>
		
		<div id="wrap_load_foto" class="row wrap_mediaFoto wrap_mediaVideo wrap_mediaVideoArchive">
			<?php foreach($galleries as $k => $ft) { ?>
				<?php
					$html_caption = "<div>";
						$html_caption .= "<p>".$ft->post_title."</p>";
						$html_caption .= "<p>".date('d F Y', strtotime($ft->post_date))."</p>";
					$html_caption .= "</div>";
				?>
				<a href="<?php echo $ft->foto; ?>"
					data-fancybox="gallery-foto-desktop"
					data-options='{"caption" : "<?php echo $html_caption; ?>"}'
					title="Lihat <?php echo $ft->post_title; ?>"
					class="col-xs-3 col-md-3 media_video">
					<div class="wrap_videoList">
						<div class="wrap_mediaVideoThumb">
							<div class="media_videoThumb">
								<img src="<?php echo $ft->foto; ?>" 
									alt="<?php echo $ft->alt_foto; ?>" />
							</div>
						</div>

						<div class="media_videoTxt">
							<p>
								<?php echo date('d F Y', strtotime($ft->post_date)); ?>
							</p>
							<h5>
								<?php
									echo (strlen($ft->post_title) > 55) ? substr($ft->post_title, 0, 55).'...' : $ft->post_title;
								?>
							</h5>
						</div>
					</div>
				</a>
			<?php } ?>
		</div>

		<?php if($total > $per_page) { ?>
			<div class="pagination">
				<?php
					pagination(
						array(
							'base'				=> home_url() . '/media/foto?',
							'page'				=> $page,
							'pages' 			=> $pages,
							'key'					=> 'hal',
							'next_text'		=> '&rsaquo;',
							'prev_text'		=> '&lsaquo;',
							'first_text'	=> '&laquo;',
							'last_text'		=> '&raquo;'
						)
					);
				?>
			</div>
		<?php } ?>
	</div>
</div>

<?php
  get_template_part(
    'template-desktop/content/content',
    'tokoh'
  );
?>