<?php
	$per_page = '10';
	
	//pagination
	$page = isset($_GET['hal'])? (int)$_GET["hal"]:1;
	$total = count(getPrograms('-1'));
	$pages = ceil($total/$per_page);

	//sementara ambil berita populer dari field 'Berita Populer'
	$beritaPopuler = getHighlightBerita(5);

	$getProgram = getPrograms(
		$per_page,
		array(
			'paged' => $page
		)
	);
	$halfProgram = round(count($getProgram)/2);
	
	$ads = getAds();

	$getProgramCategories = get_terms(
		array(
			'taxonomy'    => 'kategori-program',
			'orderby'     => 'name',
			'hide_empty'  => false,
		)
	);
?>

<div class="row row_globalPage row_homeBerita">
	<div class="col-md-12">
		<h1 class="ht_home ht_homeRubrik">PROGRAM</h1>
		<div class="row_articleTitle">PROGRAM KERJA ISNU</div>
		<p>&nbsp;</p>

		<div class="row">
			<div class="col-xs-8 col-md-8 nopadding_left">
				<?php if( !empty($getProgram) ) { ?>
					<?php foreach($getProgram as $k => $program) { ?>
						<?php if($k < $halfProgram) { ?>
							<div class="row wrap_progList">
								<div class="col-xs-5 col-md-5 wrap_progImg">
									<div class="prog_img">
										<img src="<?php echo $program->photo; ?>" alt="<?php echo $program->alt_photo; ?>" />
									</div>
								</div>

								<div class="col-xs-7 col-md-7 wrap_progTxt">
									<div class="prog_cat"><?php echo $program->category_name; ?></div>

									<a href="<?php echo home_url(); ?>/program/<?php echo $program->post_name; ?>" class="prog_title">
										<?php
											$check_len = strlen( strip_tags($program->post_title) );
											if( $check_len > 50 ) {
												echo substr(strip_tags($program->post_title), 0, 50)  . '...';
											} else {
												echo strip_tags($program->post_title);
											}
										?>
									</a>

									<p class="prog_date">
										<?php echo date('d F Y', strtotime($program->post_date)); ?>
									</p>

									<div class="prog_excerpt">
										<?php echo $program->cropped_content; ?>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>

				<?php if(!empty($ads['ads_top'])) { ?>
					<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top ik_program" target="_blank">
						<img src="<?php echo $ads['ads_top']->banner; ?>">
					</a>
				<?php } ?>

				<?php if( !empty($getProgram) ) { ?>
					<?php foreach($getProgram as $k => $program) { ?>
						<?php if($k >= $halfProgram) { ?>
							<div class="row wrap_progList">
								<div class="col-xs-5 col-md-5 wrap_progImg">
									<div class="prog_img">
										<img src="<?php echo $program->photo; ?>" alt="<?php echo $program->alt_photo; ?>" />
									</div>
								</div>

								<div class="col-xs-7 col-md-7 wrap_progTxt">
									<div class="prog_cat"><?php echo $program->category_name; ?></div>

									<a href="<?php echo home_url(); ?>/program/<?php echo $program->post_name; ?>" class="prog_title">
										<?php
											$check_len = strlen( strip_tags($program->post_title) );
											if( $check_len > 50 ) {
												echo substr(strip_tags($program->post_title), 0, 50)  . '...';
											} else {
												echo strip_tags($program->post_title);
											}
										?>
									</a>

									<p class="prog_date">
										<?php echo date('d F Y', strtotime($program->post_date)); ?>
									</p>

									<div class="prog_excerpt">
										<?php echo $program->cropped_content; ?>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				<?php } ?>

        <?php if($total > $per_page) { ?>
					<div class="pagination">
						<?php
							pagination(
								array(
									'base'				=> home_url() . '/program?',
									'page'				=> $page,
									'pages' 			=> $pages,
									'key'					=> 'hal',
									'next_text'		=> '&rsaquo;',
									'prev_text'		=> '&lsaquo;',
									'first_text'	=> '&laquo;',
									'last_text'		=> '&raquo;'
								)
							);
						?>
					</div>
				<?php } ?>

				<?php if(!empty($ads['ads_bottom'])) { ?>
					<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
						<img src="<?php echo $ads['ads_bottom']->banner; ?>">
					</a>
				<?php } ?>
			</div>

			<div class="col-xs-4 col-md-4">
				<?php if(!empty($beritaPopuler)) { ?>
					<div class="row_articleCategories row_artikelPopuler">
						<div class="row_articleTitle">Berita Populer</div>
						<ul class="listBeritaPopuler listBeritaPopulerRight">
							<?php foreach ( $beritaPopuler as $key => $berita ) { ?>
								<li>
									<a class="clearfix" href="<?php echo home_url() . '/' . $berita->post_name; ?>">
										<span><?php echo $key+1; ?></span>
										<span><?php echo $berita->post_title; ?></span>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
				<?php } ?>

				<?php if(!empty($ads['ads_right'])) { ?>
					<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
						<img src="<?php echo $ads['ads_right']->banner; ?>">
					</a>
				<?php } ?>

				<div class="bx_right_jelajah">
					<h2 class="row_articleTitle">Jelajah Kategori</h2>
					<ul class="listBeritaCat">
						<?php foreach ( $getProgramCategories as $category ) { ?>
							<?php
								$id_cat = $category->term_id;
								$name_cat = $category->name;
							?>
							<li class="<?php echo ($current_category->name == $name_cat) ? 'act' : 'ac'; ?>">
								<a href="<?php echo home_url(); ?>/kategori-program/<?php echo $category->slug; ?>">
									<?php echo $name_cat; ?>
								</a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
  get_template_part(
    'template-desktop/content/content',
    'tokoh'
  );
?>