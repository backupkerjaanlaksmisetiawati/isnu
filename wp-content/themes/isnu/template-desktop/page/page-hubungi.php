<?php
	$setting = getSetting();
?>

<div class="row row_globalPage row_hubungiPage">
	<div class="col-md-8 col_Hubungi">
		
		<h1 class="ht_globalPage">Hubungi Kami</h1>
		<div class="content_hubungi">
			<?php the_content($post->ID); ?>
		</div>

		<div class="box_listHubungi">
			<?php if(!empty($setting) && isset($setting['setting_phone'])) { ?>
				<div class="f_listHubungi">
					<img src="<?php bloginfo('template_directory'); ?>/library/images/mg_call.svg">
					<label><?php echo $setting['setting_phone']; ?></label>
				</div>
			<?php } ?>

			<?php if(!empty($setting) && isset($setting['setting_address'])) { ?>
				<div class="f_listHubungi">
					<img src="<?php bloginfo('template_directory'); ?>/library/images/mg_location.svg">
					<label><?php echo $setting['setting_address']; ?></label>
				</div>
			<?php } ?>

			<?php if(!empty($setting) && isset($setting['setting_facebook_link'])) { ?>
				<a target="_blank" href="<?php echo $setting['setting_facebook_link']; ?>">
					<div class="f_listHubungi">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/mg_facebook.svg">
						<label><?php echo $setting['setting_facebook_link']; ?></label>
					</div>
				</a>
			<?php } ?>

			<?php if(!empty($setting) && isset($setting['setting_instagram_link'])) { ?>
				<a target="_blank" href="<?php echo $setting['setting_instagram_link']; ?>">
					<div class="f_listHubungi">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/mg_instagram.svg">
						<label><?php echo $setting['setting_instagram_link']; ?></label>
					</div>
				</a>
			<?php } ?>

			<?php if(!empty($setting) && isset($setting['setting_youtube_link'])) { ?>
				<a target="_blank" href="<?php echo $setting['setting_youtube_link']; ?>">
					<div class="f_listHubungi">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/mg_youtube.svg">
						<label><?php echo $setting['setting_youtube_link']; ?></label>
					</div>
				</a>
			<?php } ?>
		</div>
	</div>

	<div class="col-md-4 col_formHubungi">
		<?php echo do_shortcode( '[contact-form-7 id="5" title="Hubungi ISNU"]' ); ?>
	</div>
</div>

<?php
  // get_template_part(
  //   'template-desktop/content/content',
  //   'tokoh'
  // );
?>