<?php
	//sementara ambil berita populer dari field 'Berita Populer' 
	$populer = getHighlightBerita();
	$categories = getPostsCategories();
	$ads = getAds();
?>

<?php if(!empty($ads['ads_top'])) { ?>
	<style>
	.row_homeRubrik {
		padding-top: 40px; /*130px;*/
	}
	</style>
<?php } ?>

<div class="row_globalPage row_homeRubrik">
	<?php if(!empty($ads['ads_top'])) { ?>
		<div class="col_homeTopSmallBanner">
			<a href="<?php echo $ads['ads_top']->url; ?>" class="ik ik-top" target="_blank">
				<img src="<?php echo $ads['ads_top']->banner; ?>">
			</a>
		</div>
	<?php } ?>

	<div class="">
		<h2 class="ht_home ht_homeRubrik">BERITA ISNU</h2>
		<div class="sub_ht_rubrik">Populer</div>
		<div class="row row_listRubrik">
			<div id="slide_rubrik" class="owl-carousel owl-theme">
				<?php foreach($populer as $val) { ?>
					<?php $link_post = get_the_permalink($val->ID); ?>
					<div class="item col_bx_homeRubrik">
						<div class="left_rubrik_post">
							<div class="mg_rubrik">
								<img src="<?php echo $val->foto; ?>" alt="<?php echo $val->alt_foto; ?>">
							</div>
						</div>
						<div class="right_rubrik_post">
							<div class="box_rubrikCategory">
								<?php echo $val->category_name; ?>
							</div>
							<a href="<?php echo $link_post; ?>"
								title="Lihat <?php echo $val->post_title; ?>">
								<h4 class="ht_rubrikPost">
									<?php echo $val->post_title; ?>
								</h4>
							</a>
							<div class="content_rubrik">
								<?php echo substr(get_the_excerpt($val->ID), 0,150); ?>...
							</div>
							<a class="a_nextRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="row row_listRubrikCategory">
			<div class="col-md-3 col_bx_vrubrik">
				<div class="sub_ht_rubrik">Kategori</div>
				<ul class="listRubrikCat">
					<li class="act" data-id="default_berita" data-catid=""><a>Berita Terbaru</a></li>
					<?php foreach ( $categories as $category ) { ?>
						<li data-id="custom_berita" data-catid="<?php echo $category->term_id; ?>">
							<a href="<?php echo home_url(); ?>/category/<?php echo $category->slug; ?>">
								<?php echo $category->name; ?>
							</a>
						</li>
					<?php } ?>
				</ul>
				
				<?php if(!empty($ads['ads_right'])) { ?>
					<a href="<?php echo $ads['ads_right']->url; ?>" class="ik ik-right" target="_blank">
						<img src="<?php echo $ads['ads_right']->banner; ?>">
					</a>
				<?php } ?>
			</div>
			<div class="col-md-9 col_bx_vrubrik col_contentBerita">
				<div id="default_berita" class="row box_v_listRubrik act">
					<?php
					$loop = new WP_Query(array( 'post_type'=>'post',
							'posts_per_page'=> 6,
							'post_status' => 'publish',
							'orderby' => 'date',
							'order' => 'DESC'
							));
					while ( $loop->have_posts() ){
						$loop->the_post();
						$id_post = get_the_ID();
						$title_post = get_the_title($id_post);
						$short_name = get_the_title($id_post);
						if(strlen($short_name) > 60) $short_name = substr($short_name, 0, 60).'...';
						$link_post = get_the_permalink($id_post);
						$date_post = get_the_date('d F Y', $id_post);
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'medium' );
						if($thumb){
							$urlphoto = $thumb['0'];
							$have_img = true;
						}else{
							$urlphoto = get_template_directory_uri().'/library/images/main_logo.png';
							$have_img = false;
						}
						$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);
						// if(count($alt));
						$category_post = get_the_category($id_post);
					?>
						<div class="col-md-6 col_v_listRubrik">
							<div class="bxsm_listRubrik">
								<div class="left_listRubrik">
									<div class="mg_sm_rubrik">
										<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>" <?php if($have_img === false) { ?> style="width: auto; height: 100%; opacity: 0.2;"<?php } ?>>
									</div>
								</div>
								<div class="right_listRubrik">
									<a href="<?php echo $link_post; ?>" title="Lihat <?php echo $title_post; ?>">
										<h5 class="ht_sm_listRubrik"><?php echo $short_name; ?></h5>
									</a>
									<div class="info_sm_listRubrik">
										<?php echo substr(get_the_excerpt($id_post), 0,150); ?>...
									</div>
									<a class="a_nextRubrik a_detailRubrik" href="<?php echo $link_post; ?>">Selengkapnya »</a>
								</div>
								<div class="bx_sm_rubrikCat">
									<span class="l_cat"><?php echo $date_post; ?></span>
									<span class="r_cat"><?php echo $category_post[0]->name; ?></span>
								</div>
							</div>
						</div>
					<?php }wp_reset_postdata();  ?>
				</div>

				<div id="custom_berita" class="row box_v_listRubrik">
					<div id="show_berita"></div>
					<h2 class="ht_notfound">Maaf, berita untuk kategori ini belum tersedia.</h2>
					<div class="mg_pleasewait">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
						<p>Mohon menunggu...</p>
					</div>
				</div>
				<div class="bx_loadmore">
					<a href="<?php echo home_url(); ?>/berita/">
						<input type="button" class="btn_loadmore btn_moreRubrik" value="Lihat Lainnya">
					</a>
				</div>
			</div>
		</div>
	</div>

	<?php if(!empty($ads['ads_bottom'])) { ?>
		<div class="col_homeBottomSmallBanner">
			<a href="<?php echo $ads['ads_bottom']->url; ?>" class="ik ik-bottom" target="_blank">
				<img src="<?php echo $ads['ads_bottom']->banner; ?>">
			</a>
		</div>
	<?php } ?>
</div>
