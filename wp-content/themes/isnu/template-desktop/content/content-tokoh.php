<?php $highlightTokoh = getHighlightTokoh('banner'); ?>
<?php if($highlightTokoh !== null) { ?>
	<div class="row row_homeTokoh">
		<div class="col-md-8 col_homeTokoh1">
			<div class="mg_homeTokoh">
				<img src="<?php echo $highlightTokoh->footer_banner_image; ?>">
			</div>
			<div class="box_quotesTokoh">
				<h3 class="ht_quotesTokoh"><?php echo $highlightTokoh->footer_banner_title1; ?></h3>
				<img class="mg_quotes" src="<?php bloginfo('template_directory'); ?>/library/images/qoutes.svg">
				<div class="cont_quotes">
					<?php echo $highlightTokoh->footer_banner_title2; ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 col_homeTokoh2">
			<div class="content_homeTokoh">
				<?php echo $highlightTokoh->footer_banner_text; ?>
			</div>
		</div>
	</div>
<?php } ?>