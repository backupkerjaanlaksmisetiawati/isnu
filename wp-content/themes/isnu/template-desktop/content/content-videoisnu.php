<?php $highlightVideos = getFooterHighlightVideos(); ?>

<div class="row row_globalPage row_videoHome">

	<h2 class="ht_home">Video ISNU</h2>

	<div class="col-md-8 col_videoisnu">
		<?php $big_video_information = get_field('big_video_information', $highlightVideos[0]->ID); ?>

		<a href="<?php echo get_the_permalink($highlightVideos[0]->ID); ?>"
			title="Lihat <?php echo $highlightVideos[0]->post_title; ?>"
			class="frame_video mg_bigvideo">
			<div class="iconplay">
				<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
			</div>
			<img src="<?php echo $highlightVideos[0]->foto; ?>"
				alt="<?php echo $highlightVideos[0]->alt_foto; ?>" />
		</a>
		<a href="<?php echo get_the_permalink($highlightVideos[0]->ID); ?>"
			title="Lihat <?php echo $highlightVideos[0]->post_title; ?>">
			<h3 class="ht_videoisnu">
				<?php echo $highlightVideos[0]->post_title; ?>
			</h3>
		</a>
		<div class="info_videoisnu"><?php echo $big_video_information; ?></div>
	</div>

	<div class="col-md-3 col_videoisnu2">
		<?php foreach($highlightVideos as $key => $vid) { ?>
			<?php if($key != 0) { ?>
				<a href="<?php echo get_the_permalink($vid->ID); ?>"
					title="Lihat <?php echo $vid->post_title; ?>"
					class="frame_video mg_smallvideo">
					<div class="iconplay">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
					</div>
					<div class="thumb">
						<img src="<?php echo $vid->foto; ?>"
							alt="<?php echo $vid->alt_foto; ?>" />
					</div>
				</a>
			<?php } ?>
		<?php } ?>
	</div>

</div>