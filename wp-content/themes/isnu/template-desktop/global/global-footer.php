<?php 
  $page_id = get_the_ID();
  $page_post = get_post($page_id);
  $setting = getSetting();
?>

<footer class="footer" role="contentinfo">

  <div id="inner-footer" class="wrap clearfix">
    <div class="row row_footer">
      <div class="col-md-12 col_menuFooter">
        <div class="row">
          <div class="col-md-2 col_footerLogo">
            <div class="mg_footerLogo">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
            </div>
          </div>

          <?php if(!empty($setting) && isset($setting['site_description'])) { ?>
            <div class="col-md-4">
              <h3 class="pt_menuFooter">Tentang Kami</h3>
              <div class="cont_menuFooter">
                <?php echo nl2br($setting['site_description']); ?>
              </div>
            </div>
          <?php } ?>

          <div class="col-md-3">
            <h3 class="pt_menuFooter">Kontak</h3>
            <div class="cont_menuFooter cont_menuFooterKontak">
              <?php if(!empty($setting) && isset($setting['setting_phone'])) { ?>
                <p>
                  <a href="tel:<?php echo $setting['setting_phone']; ?>" class="phone clearfix">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-phone.svg" />
                    <span><?php echo $setting['setting_phone']; ?></span>
                  </a>
                </p>
              <?php } ?>
              
              <?php if(!empty($setting) && isset($setting['setting_email'])) { ?>
                <p>
                  <a href="mailto:<?php echo $setting['setting_email']; ?>" class="clearfix">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-email.svg" />
                    <span><?php echo $setting['setting_email']; ?></span>
                  </a>
                </p>
              <?php } ?>
              
              <?php if(!empty($setting) && isset($setting['siteurl'])) { ?>
                <p>
                  <a href="<?php echo $setting['siteurl']; ?>" class="web clearfix">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-web.svg" />
                    <span><?php echo str_replace(array('http://', 'https://'), '', $setting['siteurl']); ?></span>
                  </a>
                </p>
              <?php } ?>
              
              <?php if(!empty($setting) && isset($setting['setting_address'])) { ?>
                <p>
                  <a class="address clearfix">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-map-point.svg" />
                    <span><?php echo nl2br($setting['setting_address']); ?></span>
                  </a>
                </p>
              <?php } ?>
            </div>
          </div>

          <div class="col-md-3">
            <h3 class="pt_menuFooter">Ikuti Kami</h3>
            
            <div class="medsos_menuFooter clearfix">
              <?php if(!empty($setting) && isset($setting['setting_facebook_link'])) { ?>
                <a target="_blank" href="<?php echo $setting['setting_facebook_link']; ?>">
                  <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-facebook.svg" />
                </a>
              <?php } ?>
              
              <?php if(!empty($setting) && isset($setting['setting_instagram_link'])) { ?>
                <a target="_blank" href="<?php echo $setting['setting_instagram_link']; ?>">
                  <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-instagram.svg" />
                </a>
              <?php } ?>
              
              <?php if(!empty($setting) && isset($setting['setting_twitter_link'])) { ?>
                <a target="_blank" href="<?php echo $setting['setting_twitter_link']; ?>">
                  <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-twitter.svg" />
                </a>
              <?php } ?>
            </div>
            
            <?php if(!empty($setting)) { ?>
              <p>&nbsp;</p>
            <?php } ?>

            <p>Atau hubungi kami untuk mengetahui informasi selengkapnya.</p>

            <a href="<?php echo home_url(); ?>/hubungi-isnu/" class="btn_yellow a_daftarNU">
              Hubungi ISNU
            </a>

          </div>
        </div>
      </div>

    </div>

    <div class="row row_copyright">
      <div class="col-md-12 col_copyright">
        <a href="<?php echo home_url(); ?>">
          <div class="pt_copyright">
            ©2019 Ikatan Sarjana Nahdlatul Ulama | All Rights Reserved
          </div>
        </a>
      </div>
    </div>

  </div>

</footer>