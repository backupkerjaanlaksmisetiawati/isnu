<?php
	$page_id = get_the_ID();
	$page_post = get_post($page_id);

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
?>

<?php if(is_page(array('2'))){ ?>
<header class="header" role="banner">
<?php }else{ ?>
<header class="header" role="banner">
<?php } ?>

  <div class="row row_topMenu">
    <div class="col-xs-3 col-md-3 col_left_topMenu">
      <a href="<?php echo home_url(); ?>" title="Beranda - ISNU">
        <div class="mg_topLogo">
          <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
        </div>
      </a>

    </div>
    <div class="col-xs-9 col-md-9 col_right_topMenu">
      
      <?php if(isset($u_id) AND $u_id != '' AND $u_id != 0){ 
          $user_name = $current_user->display_name;
      ?>
        <div class="box_topRegist">
          <a href="<?php echo home_url('/wp-admin/profile.php'); ?>" class="a_loginNow">
            Selamat Datang, <?php echo $user_name; ?>
          </a>
          <div class="a_spaceTop">
            <div class="greenspace"></div>
            <a href="<?php echo wp_logout_url(home_url('/')); ?>" title="Keluar akun Anda">Keluar</a>
          </div>
        </div>
      <?php }else{ ?>
        <div class="box_topRegist">
          <a href="<?php echo home_url(); ?>/daftar/" class="a_registNow">Daftar</a>
          <a href="<?php echo home_url(); ?>/login/" class="a_loginNow">Masuk</a>
          <div class="a_spaceTop"><div class="greenspace"></div></div>
        </div>
      <?php } ?>

      <ul class="listTopMenu">
        <?php
        $menu_id = 20; // menu id footer 1
        $menu_list='';
        $menu_items = wp_get_nav_menu_items($menu_id);
        foreach ( $menu_items as $key => $menu_item ) {
          $title = $menu_item->title;
          $url = $menu_item->url;
          $explode_url = explode('/', $url);
          $last_url_path = count($explode_url)-2;
          $class_active = (!empty($page_post) && $explode_url[$last_url_path] == $page_post->post_name) ? 'active' : 'inactive';
          $menu_list .= '<li class="' . $class_active . '"><a href="'.$url.'" title="view detail '.$title.'">'.$title.'</a></li>';
        }
        echo $menu_list;
        ?>
      </ul>

    </div>
  </div>
  
</header>