<?php
/**
 * funtion for ajax loading, ajax submit, etc
 */

/**
 * Ajax user registration from website
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function ajax_daftar_user() {
  global $wpdb;

  require_once( ABSPATH . 'wp-admin/includes/image.php' );
  require_once( ABSPATH . 'wp-admin/includes/file.php' );
  require_once( ABSPATH . 'wp-admin/includes/media.php' );

  /* validate input */
    $error = array();

    if(empty($_POST['namalengkap'])) {
      $error['namalengkap'] = 'Nama Lengkap wajib diisi.';
    }
    if( !empty($_POST['namalengkap']) && check_regex($_POST['namalengkap']) === false ) {
      $error['namalengkap'] = 'Invalid input.';
    }
    
    if(empty($_POST['no_sertifikat_mknu'])) {
      $error['no_sertifikat_mknu'] = 'Nomor Sertifikat MKNU wajib diisi.';
    }
    if( !empty($_POST['no_sertifikat_mknu']) && is_numeric($_POST['no_sertifikat_mknu']) != 1 ) {
      $error['no_sertifikat_mknu'] = 'Hanya masukan no MKNU. Contoh 3202000000001000.';
    }

    if(empty($_POST['email'])) {
      $error['email'] = 'Email wajib diisi.';
    }
    if( !empty($_POST['email']) ) {
      if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        // echo("$email is a valid email address");
      } else {
        $error['email'] = 'Format email salah.';
      }

      $check_email = $wpdb->get_row("SELECT ID, user_email, display_name FROM unsi_users
        WHERE user_email = '" . $_POST['email'] . "'
        ORDER BY ID DESC 
        LIMIT 1", OBJECT);
      
      if(!empty($check_email)) {
        $error['email'] = 'Email sudah terdaftar.';
      }
    }
    
    if(empty($_POST['phone'])) {
      $error['phone'] = 'Telepon wajib diisi.';
    }
    if( !empty($_POST['phone']) && is_numeric($_POST['phone']) != 1 ) {
      $error['phone'] = 'Hanya masukan angka telepon. Contoh 081312345678.';
    }
    
    if(empty($_POST['tempatlahir'])) {
      $error['tempatlahir'] = 'Tempat Lahir wajib diisi.';
    }
    if( !empty($_POST['tempatlahir']) && check_regex($_POST['tempatlahir']) === false ) {
      $error['tempatlahir'] = 'Invalid input.';
    }
    
    if(empty($_POST['tanggallahir'])) {
      $error['tanggallahir'] = 'Tanggal Lahir wajib diisi.';
    }
    
    if(empty($_POST['alamat'])) {
      $error['alamat'] = 'Alamat wajib diisi.';
    }
    if( !empty($_POST['alamat']) && check_regex_address($_POST['alamat']) === false ) {
      $error['alamat'] = 'Alamat hanya boleh diisi dengan alphabet dan numeric.';
    }

    if(empty($_POST['link_referensi_publikasi'][0])) {
      $error['link_referensi_publikasi_0'] = 'Link Referensi Publikasi wajib diisi.';
    }
    if( !empty($_POST['link_referensi_publikasi'][0]) && check_url_regex($_POST['link_referensi_publikasi'][0]) === false ) {
      $error['link_referensi_publikasi_0'] = 'Format URL referensi publikasi salah. Harus diawali dengan http:// atau https://';
    }

    foreach($_POST['link_referensi_publikasi'] as $k_rp => $rp) {
      $er_rp = $k_rp+1;      
      if(empty($_POST['link_referensi_publikasi'][$k_rp])) {
        $error['link_referensi_publikasi_'.$er_rp] = 'Link Referensi Publikasi wajib diisi.';
      }
      if( !empty($_POST['link_referensi_publikasi'][$k_rp]) && check_url_regex($_POST['link_referensi_publikasi'][$k_rp]) === false ) {
        $error['link_referensi_publikasi_'.$er_rp] = 'Format URL referensi publikasi salah. Harus diawali dengan http:// atau https://';
      }
    }

    if(empty($_POST['statuspernikahan'])) {
      $error['statuspernikahan'] = 'Status Pernikahan wajib dipilih.';
    }

    if(empty($_POST['jeniskelamin'])) {
      $error['jeniskelamin'] = 'Jenis Kelamin wajib dipilih.';
    }

    if(empty($_POST['pekerjaan'])) {
      $error['pekerjaan'] = 'Pekerjaan wajib dipilih.';
    }

    if(empty($_POST['universitass1'])) {
      // $error['pendidikan']['s1']['universitass1'] = 'Universitas wajib diisi.';
      $error['universitass1'] = 'Universitas wajib diisi.';
    }
    if( !empty($_POST['universitass1']) && check_regex($_POST['universitass1']) === false ) {
      $error['universitass1'] = 'Invalid input.';
    }

    if(empty($_POST['jurusans1'])) {
      $error['jurusans1'] = 'Jurusan wajib diisi.';
    }
    if( !empty($_POST['jurusans1']) && check_regex($_POST['jurusans1']) === false ) {
      $error['jurusans1'] = 'Invalid input.';
    }

    if(empty($_POST['tahuns1from']) || empty($_POST['tahuns1to'])) {
      $error['tahuns1'] = 'Tahun wajib diisi.';
    }
    if( (!empty($_POST['tahuns1from']) && is_numeric($_POST['tahuns1from']) != 1) || (!empty($_POST['tahuns1to']) && is_numeric($_POST['tahuns1to']) != 1) ) {
      $error['tahuns1'] = 'Hanya masukan angka tahun. Contoh 2020.';
    }
    
    if( isset($_POST['pendidikans2']) &&  empty($_POST['universitass2']) ) {
      $error['universitass2'] = 'Universitas wajib diisi.';
    }
    if( isset($_POST['pendidikans2']) && !empty($_POST['universitass2']) && check_regex($_POST['universitass2']) === false ) {
      $error['universitass2'] = 'Invalid input.';
    }

    if( isset($_POST['pendidikans2']) && empty($_POST['jurusans2'])) {
      $error['jurusans2'] = 'Jurusan wajib diisi.';
    }
    if( isset($_POST['pendidikans2']) && !empty($_POST['jurusans2']) && check_regex($_POST['jurusans2']) === false ) {
      $error['jurusans2'] = 'Invalid input.';
    }

    if( isset($_POST['pendidikans2']) && (empty($_POST['tahuns2from']) || empty($_POST['tahuns2to']))) {
      $error['tahuns2'] = 'Tahun wajib diisi.';
    }
    if( isset($_POST['pendidikans2']) && (!empty($_POST['tahuns2from']) && is_numeric($_POST['tahuns2from']) != 1) || (!empty($_POST['tahuns2to']) && is_numeric($_POST['tahuns2to']) != 1) ) {
      $error['tahuns2'] = 'Hanya masukan angka tahun. Contoh 2020.';
    }
    
    if( isset($_POST['pendidikans3']) &&  empty($_POST['universitass3']) ) {
      $error['universitass3'] = 'Universitas wajib diisi.';
    }
    if( isset($_POST['pendidikans3']) && !empty($_POST['universitass3']) && check_regex($_POST['universitass3']) === false ) {
      $error['universitass3'] = 'Invalid input.';
    }

    if( isset($_POST['pendidikans3']) && empty($_POST['jurusans3'])) {
      $error['jurusans3'] = 'Jurusan wajib diisi.';
    }
    if( isset($_POST['pendidikans3']) && !empty($_POST['jurusans3']) && check_regex($_POST['jurusans3']) === false ) {
      $error['jurusans3'] = 'Invalid input.';
    }

    if( isset($_POST['pendidikans3']) && (empty($_POST['tahuns3from']) || empty($_POST['tahuns3to']))) {
      $error['tahuns3'] = 'Tahun wajib diisi.';
    }
    if( isset($_POST['pendidikans3']) && (!empty($_POST['tahuns3from']) && is_numeric($_POST['tahuns3from']) != 1) || (!empty($_POST['tahuns3to']) && is_numeric($_POST['tahuns3to']) != 1) ) {
      $error['tahuns3'] = 'Hanya masukan angka tahun. Contoh 2020.';
    }

    if(empty($_POST['organisasi_name'][0])) {
      $error['organisasi_name_0'] = 'Nama Organisasi wajib diisi.';
    }
    foreach($_POST['organisasi_name'] as $k_ou => $ou) {
      $er_ou = $k_ou+1;      
      if(empty($_POST['organisasi_name'][$k_ou])) {
        $error['organisasi_name_'.$er_ou] = 'Nama Organisasi wajib diisi.';
      }
    }

    if(empty($_POST['organisasi_detail'][0])) {
      $error['organisasi_detail_0'] = 'Keterangan Organisasi wajib diisi.';
    }
    foreach($_POST['organisasi_detail'] as $k_ou => $ou) {
      $er_ou = $k_ou+1;
      if(empty($_POST['organisasi_detail'][$k_ou])) {
        $error['organisasi_detail_'.$er_ou] = 'Keterangan Organisasi wajib diisi.';
      }
    }

    if(empty($_POST['organisasi_from'][0]) || empty($_POST['organisasi_to'][0])) {
      $error['organisasi_tahun_0'] = 'Tahun wajib diisi.';
    }
    foreach($_POST['organisasi_from'] as $k_ou => $ou) {
      $er_ou = $k_ou+1;
      if(empty($_POST['organisasi_from'][$k_ou]) || empty($_POST['organisasi_to'][$k_ou])) {
        $error['organisasi_tahun_'.$er_ou] = 'Tahun wajib diisi.';
      }
    }

    if( !empty($_POST['organisasi_name']) ) {
      foreach($_POST['organisasi_name'] as $k_ou => $ou) {        
        if( !empty($_POST['organisasi_name'][$k_ou]) && check_regex($_POST['organisasi_name'][$k_ou]) === false ) {
          $error['organisasi_name_'.$k_ou] = 'Invalid input.';
        }
        
        if( !empty($_POST['organisasi_detail'][$k_ou]) && check_regex($_POST['organisasi_detail'][$k_ou]) === false ) {
          $error['organisasi_detail_'.$k_ou] = 'Invalid input.';
        }

        if( (!empty($_POST['organisasi_from'][$k_ou]) && is_numeric($_POST['organisasi_from'][$k_ou]) != 1) || (!empty($_POST['organisasi_to'][$k_ou]) && is_numeric($_POST['organisasi_to'][$k_ou]) != 1) ) {
          $error['organisasi_tahun_'.$k_ou] = 'Hanya masukan angka tahun. Contoh 3030.';
        }
      }
    }

    $maxsize = 2097152;

    $filename_sertifikat = $_FILES['filesertifikat']['name'];
    if($filename_sertifikat != null) {
      $ext_sertifikat = pathinfo($filename_sertifikat, PATHINFO_EXTENSION);
      if (!in_array($ext_sertifikat, array('jpeg', 'jpg', 'pdf'))) {
        $error['scan_sertifikat'] = 'Format file hanya boleh JPG dan PDF.';
      }

      if(($_FILES['filesertifikat']['size'] >= $maxsize) || ($_FILES["filesertifikat"]["size"] == 0)) {
        $error['scan_sertifikat'] = 'Maksimal size file adalah 2MB.';
      }
    }

    $filename_foto = $_FILES['filefoto']['name'];
    if($filename_foto != null) {
      $ext_foto = pathinfo($filename_foto, PATHINFO_EXTENSION);
      if (!in_array($ext_foto, array('jpeg', 'jpg', 'png'))) {
        $error['pas_foto'] = 'Format file hanya boleh JPG, JPEG dan PNG.';
      }

      if(($_FILES['filefoto']['size'] >= $maxsize) || ($_FILES["filefoto"]["size"] == 0)) {
        $error['pas_foto'] = 'Maksimal size file adalah 2MB.';
      }
    }

    if( !empty($error) ) {
      $return = array(
        'status'  => 400,
        'data'    => array(
          'message' => $error
        )
      );
      echo json_encode($return);
      die();
    }
  /* validate input */

  $username = str_replace(' ', '', $_POST['namalengkap']); //untuk di user_login dan user_nicename
  $password = md5($username.'ISNU100%'); //hardcoded

  $data_user = array(
    'user_login' => $username,
    'user_pass' => $password,
    'user_nicename' => $username,
    'user_email' => $_POST['email'],
    'user_registered' => date('Y-m-d H:i:s'),
    'display_name' => $_POST['namalengkap']
  );
  $wpdb->insert( 'unsi_users', $data_user );

  /* get new user */
  $query_new_user = "SELECT ID, user_email, display_name FROM unsi_users
    WHERE user_email = '" . $_POST['email'] . "'
    ORDER BY ID DESC 
    LIMIT 1";
  $get_new_user = $wpdb->get_row($query_new_user, OBJECT);

  $sertifikat_attachment_id = media_handle_upload('filesertifikat', $get_new_user->ID);
  $foto_attachment_id = media_handle_upload('filefoto', $get_new_user->ID);
  
  /* insert meta user */
    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'nickname',
        'meta_value' => $username
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'first_name',
        'meta_value' => $_POST['namalengkap']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'last_name',
        'meta_value' => ''
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'description',
        'meta_value' => ''
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'jenis_kelamin',
        'meta_value' => $_POST['jeniskelamin']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_jenis_kelamin',
        'meta_value' => 'field_5de9e110887a1'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'identity_id',
        'meta_value' => $_POST['no_sertifikat_mknu']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_identity_id',
        'meta_value' => 'field_5de9e23ed83a1'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'telepon',
        'meta_value' => $_POST['phone']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_telepon',
        'meta_value' => 'field_5de9e26ad83a2'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'tempat_lahir',
        'meta_value' => $_POST['tempatlahir']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_tempat_lahir',
        'meta_value' => 'field_5de9e279d83a3'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'tanggal_lahir',
        'meta_value' => date("Y-m-d", strtotime($_POST['tanggallahir']))
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_tanggal_lahir',
        'meta_value' => 'field_5de9e286d83a4'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'status_pernikahan',
        'meta_value' => $_POST['statuspernikahan']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_status_pernikahan',
        'meta_value' => 'field_5de9e2a0d83a5'
      )
    );

    // $wpdb->insert(
    // 	'unsi_usermeta',
    // 	array(
    // 		'user_id' => $get_new_user->ID,
    // 		'meta_key' => 'pendidikan_terakhir',
    // 		'meta_value' => $_POST['pendidikan']
    // 	)
    // );

    // $wpdb->insert(
    // 	'unsi_usermeta',
    // 	array(
    // 		'user_id' => $get_new_user->ID,
    // 		'meta_key' => '_pendidikan_terakhir',
    // 		'meta_value' => 'field_5de9f15279418'
    // 	)
    // );

    // $wpdb->insert(
    // 	'unsi_usermeta',
    // 	array(
    // 		'user_id' => $get_new_user->ID,
    // 		'meta_key' => 'gelar_pendidikan',
    // 		'meta_value' => $_POST['gelar']
    // 	)
    // );

    // $wpdb->insert(
    // 	'unsi_usermeta',
    // 	array(
    // 		'user_id' => $get_new_user->ID,
    // 		'meta_key' => '_gelar_pendidikan',
    // 		'meta_value' => 'field_5de9f1d24a98d'
    // 	)
    // );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'alamat',
        'meta_value' => $_POST['alamat']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_alamat',
        'meta_value' => 'field_5de9f1e24a98e'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'upload_sertifikat',
        'meta_value' => $sertifikat_attachment_id
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'pas_foto',
        'meta_value' => $foto_attachment_id
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_upload_sertifikat',
        'meta_value' => 'field_5de9f2054a98f'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'userstatus',
        'meta_value' => '0'
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'approved_flag',
        'meta_value' => '0'
      )
    );

    /* riwayat pendidikan */
    for($p=1;$p<=3;$p++) {
      if($_POST['pendidikans'.$p] === 'S'.$p) {
        if(
          (isset($_POST['universitass'.$p]) && !empty($_POST['universitass'.$p])) && 
          (isset($_POST['jurusans'.$p]) && !empty($_POST['jurusans'.$p])) && 
          (isset($_POST['tahuns'.$p.'from']) && !empty($_POST['tahuns'.$p.'from'])) && 
          (isset($_POST['tahuns'.$p.'to']) && !empty($_POST['tahuns'.$p.'to']))
        ) {
          $wpdb->insert('unsi_user_education', array(
            'user_id'					=> $get_new_user->ID,
            'degree'					=> $_POST['pendidikans'.$p],
            'university_name'	=> $_POST['universitass'.$p],
            'major'						=> $_POST['jurusans'.$p],
            'entering_year'		=> $_POST['tahuns'.$p.'from'],
            'graduated_year'	=> $_POST['tahuns'.$p.'to']
          ));
        }
      }
    }
    /* riwayat pendidikan */

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => 'pekerjaan_sekarang',
        'meta_value' => $_POST['pekerjaan']
      )
    );

    $wpdb->insert(
      'unsi_usermeta',
      array(
        'user_id' => $get_new_user->ID,
        'meta_key' => '_pekerjaan_sekarang',
        'meta_value' => 'field_5f0bcb39dcc4c'
      )
    );
  /* insert meta user */
  
  /* pengalaman organisasi */
    if(!empty($_POST['organisasi_name'][0])) {
      foreach($_POST['organisasi_name'] as $exp_key => $experience) {
        $wpdb->insert('unsi_user_experience_organization', array(
          'user_id'							=> $get_new_user->ID,
          'organization_name'		=> $_POST['organisasi_name'][$exp_key],
          'organization_detail'	=> $_POST['organisasi_detail'][$exp_key],
          'organization_start'	=> $_POST['organisasi_from'][$exp_key],
          'organization_end'		=> $_POST['organisasi_to'][$exp_key]
        ));
      }
    }
  /* pengalaman organisasi */
  
  /* link referensi publikasi */
    if(!empty($_POST['link_referensi_publikasi'][0])) {
      foreach($_POST['link_referensi_publikasi'] as $rep_key => $referensi_publikasi) {
        $wpdb->insert('unsi_user_referensi_publikasi', array(
          'user_id' => $get_new_user->ID,
          'link'    => $_POST['link_referensi_publikasi'][$rep_key]
        ));
      }
    }
    /* link referensi publikasi */

  /* send email */
    $member_email = $get_new_user->user_email;
    $member_name = $get_new_user->display_name;

    // $_SESSION["member_email"] = $member_email;
    // $_SESSION["member_name"] = $member_name;
  /* send email */

  $return = array(
    'status'  => 200,
    'data'    => array(
      'message_type'  => "success",
      'message'		    => "Terima kasih sudah mendaftar. Admin kami akan memvalidasi akun Anda.",
      'member_email'  => $member_email,
      'member_name'   => $member_name,
      // 'new_user' => $get_new_user
    )
  );
  
  echo json_encode($return);
  die();
}
add_action('wp_ajax_ajax_daftar_user','ajax_daftar_user');
add_action('wp_ajax_nopriv_ajax_daftar_user', 'ajax_daftar_user');

/**
 * Ajax user registration from website
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function ajax_email_daftar_user() {
  global $wpdb;
  
  /* send email */
    $member_email = $_POST['email'];
    $member_name = $_POST['name'];

    /* member */
      $html_message_member = "";
      $html_message_member .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
        $html_message_member .= "<td align=\"left\" valign=\"top\" style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;text-align:left;font-size:14px;line-height:140%;background-color:#ffffff;padding:60px 75px 45px 75px;border-right:1px solid #ddd;border-bottom:1px solid #ddd;border-left:1px solid #ddd;border-top:3px solid #809eb0\">";
          $html_message_member .= "<div style=\"text-align:center\">";
            $html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;line-height:140%;font-size:20px;text-align:center;margin:0 0 20px 0;Margin:0 0 20px 0\">";
              $html_message_member .= "Selamat datang di ISNU!";
            $html_message_member .= "</p>";
            $html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;text-align:left;line-height:140%;margin:0 0 15px 0;Margin:0 0 15px 0;font-size:16px\">";
              $html_message_member .= "Terima kasih sudah mendaftar sebagai member ISNU.";
            $html_message_member .= "</p>";
            $html_message_member .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;text-align:left;line-height:140%;margin:0 0 15px 0;Margin:0 0 15px 0;font-size:16px\">";
              $html_message_member .= "Anda dapat berbagi publikasi ilmiah Anda setelah akun Anda kami approve.";
            $html_message_member .= "</p>";
          $html_message_member .= "</div>";
        $html_message_member .= "</td>";
      $html_message_member .= "</tr>";

      $message_member = template_email($html_message_member);
      $rephead_member = array();
      $rephead_member[] = 'MIME-Version: 1.0' . "\r\n";
      $rephead_member[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $rephead_member[] = "From: ". "Ikatan Sarjana Nahdlatul Ulama (ISNU) <noreply@pp-isnu.or.id>" . "\r\n";
      
      wp_mail(trim(strip_tags($member_email)), "[ISNU] Anda telah mendaftar di ISNU.", $message_member, $rephead_member);
    /* member */
    
    /* admin */
      $html_message_admin = "";
      $html_message_admin .= "<tr style=\"padding:0;vertical-align:top;text-align:left\">";
        $html_message_admin .= "<td align=\"left\" valign=\"top\" style=\"word-wrap:break-word;border-collapse:collapse!important;vertical-align:top;color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;margin:0;Margin:0;text-align:left;font-size:14px;line-height:140%;background-color:#ffffff;padding:60px 75px 45px 75px;border-right:1px solid #ddd;border-bottom:1px solid #ddd;border-left:1px solid #ddd;border-top:3px solid #809eb0\">";
          $html_message_admin .= "<div style=\"text-align:center\">";
            $html_message_admin .= "<p style=\"color:#444;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-weight:normal;padding:0;line-height:140%;font-size:20px;text-align:center;margin:0 0 20px 0;Margin:0 0 20px 0\">";
              $html_message_admin .= "Member " . $member_email . " baru saja mendaftar sebagai member.";
            $html_message_admin .= "</p>";
          $html_message_admin .= "</div>";
        $html_message_admin .= "</td>";
      $html_message_admin .= "</tr>";
      
      $message_admin = template_email($html_message_admin);
      $rephead_admin = array();
      $rephead_admin[] = 'MIME-Version: 1.0' . "\r\n";
      $rephead_admin[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $rephead_admin[] = "From: ". "Ikatan Sarjana Nahdlatul Ulama (ISNU) <noreply@pp-isnu.or.id>" . "\r\n";
      
      wp_mail("ppisnuorid@gmail.com", "[ISNU] Member baru di ISNU.", $message_admin, $rephead_admin);
    /* admin */
  /* send email */

  // unset($_SESSION["member_name"]);
  // unset($_SESSION["member_email"]);
  
  echo 'Sukses';
  // die();
}
add_action('wp_ajax_ajax_email_daftar_user', 'ajax_email_daftar_user');
add_action('wp_ajax_nopriv_ajax_email_daftar_user', 'ajax_email_daftar_user');

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function ajax_showBerita(){

	$cat_id = $_REQUEST['cat_id'];
	$status = $_REQUEST['status'];
	$html = '';

	if(isset($status) AND $status == 'page'){
		$total_post = -1;
	}else{
		$total_post = 6;
	}

	$loop = new WP_Query(array( 'post_type'=>'post',
								'posts_per_page'=> $total_post,
								'post_status' => 'publish',
							    'orderby' => 'date',
							    'order' => 'DESC',
									'paged' => 1,
							    'category__in' => $cat_id // slug=berita-isnu
			));
	while ( $loop->have_posts() ){
		$loop->the_post();
		$id_post = get_the_ID();
		$title_post = get_the_title($id_post);
		$short_name = get_the_title($id_post);
		if(strlen($short_name) > 60) $short_name = substr($short_name, 0, 60).'...';

		$link_post = get_the_permalink($id_post);
		$date_post = get_the_date('d F Y', $id_post);

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id_post), 'medium' );
		if($thumb){
			$urlphoto = $thumb['0'];
		}else{
			$urlphoto = get_template_directory_uri().'/library/images/sorry.png';
		}
		$alt = get_post_meta(get_post_thumbnail_id($id_post), '_wp_attachment_image_alt', true);
		// if(count($alt));

		$category_post = get_the_category($id_post);

		$content = substr(get_the_excerpt($id_post), 0,150);

		$html .= '<div class="col-md-6 col_v_listRubrik">
			<div class="bxsm_listRubrik">
				<div class="left_listRubrik">
					<div class="mg_sm_rubrik">
						<img src="'.$urlphoto.'" alt="'.$alt.'">
					</div>
				</div>
				<div class="right_listRubrik">
					<a href="'.$link_post.'" title="Lihat '.$title_post.'">
						<h5 class="ht_sm_listRubrik">'.$short_name.'</h5>
					</a>
					<div class="info_sm_listRubrik">'.$content.'...</div>
					<a class="a_nextRubrik a_detailRubrik" href="'.$link_post.'">Selengkapnya »</a>
				</div>
				<div class="bx_sm_rubrikCat">
					<span class="l_cat">'.$date_post.'</span>
					<span class="r_cat">'.$category_post[0]->name.'</span>
				</div>
			</div>
		</div>';
		
	}wp_reset_postdata(); 

	$content = 'Sukses Bro'; 
    echo json_encode($html);
    die();
} 
add_action('wp_ajax_ajax_showBerita','ajax_showBerita');
add_action('wp_ajax_nopriv_ajax_showBerita', 'ajax_showBerita');
?>