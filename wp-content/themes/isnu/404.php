<?php get_header(); ?>

			<div id="content" class="row_globalPage row_homeBerita">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<article id="post-not-found" class="hentry clearfix">
							<div class="row">	
							<div class="col-md-1"></div>
							<div class="col-md-10" style="margin-top: 50px;margin-bottom: 30px;"> 
									<div class="mg_rocketLogin">
										<img src="<?php bloginfo('template_directory'); ?>/library/images/login_animate.png">
									</div>
									<header class="article-header">
										<h1>Oops, Anda Nyasar!</h1>
									</header>
									<section class="entry-content">
										<p>Sepertinya halaman yang Anda cari sudah tidak aktif, atau mungkin Anda nyasar ?</p>
									</section>
							</div>
							<div class="col-md-1"> </div>
							</div>
						</article>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
