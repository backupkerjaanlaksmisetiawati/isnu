<?php
/**
 * implement 'MVC' in wordpress --"
 * 'function-datas' works as 'Model' and 'Controller' because wordpress doesnt have them
 * let 'View' stay clean from fetch database
 */

/**
 * Get Setting
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getSetting() {
  global $wpdb;

  $return = array();
  
  $siteurl = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'siteurl'",
    OBJECT
  );
  if(!empty($siteurl)) {
    $return['siteurl'] = $siteurl[0]->option_value;
  }
  
  $blogname = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'blogname'",
    OBJECT
  );
  if(!empty($blogname)) {
    $return['blogname'] = $blogname[0]->option_value;
  }
  
  $tagline = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'blogdescription'",
    OBJECT
  );
  if(!empty($tagline)) {
    $return['tagline'] = $tagline[0]->option_value;
  }
  
  $site_description = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'site_description'",
    OBJECT
  );
  if(!empty($site_description)) {
    $return['site_description'] = $site_description[0]->option_value;
  }
  
  $setting_email = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_email'",
    OBJECT
  );
  if(!empty($setting_email)) {
    $return['setting_email'] = $setting_email[0]->option_value;
  }
  
  $setting_email = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_email'",
    OBJECT
  );
  if(!empty($setting_email)) {
    $return['setting_email'] = $setting_email[0]->option_value;
  }
  
  $setting_phone = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_phone'",
    OBJECT
  );
  if(!empty($setting_phone)) {
    $return['setting_phone'] = $setting_phone[0]->option_value;
  }
  
  $setting_address = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_address'",
    OBJECT
  );
  if(!empty($setting_address)) {
    $return['setting_address'] = $setting_address[0]->option_value;
  }
  
  $setting_facebook_link = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_facebook_link'",
    OBJECT
  );
  if(!empty($setting_facebook_link)) {
    $return['setting_facebook_link'] = $setting_facebook_link[0]->option_value;
  }
  
  $setting_instagram_link = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_instagram_link'",
    OBJECT
  );
  if(!empty($setting_instagram_link)) {
    $return['setting_instagram_link'] = $setting_instagram_link[0]->option_value;
  }
  
  $setting_twitter_link = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_twitter_link'",
    OBJECT
  );
  if(!empty($setting_twitter_link)) {
    $return['setting_twitter_link'] = $setting_twitter_link[0]->option_value;
  }
  
  $setting_youtube_link = $wpdb->get_results(
    "SELECT option_value FROM unsi_options
    WHERE option_name = 'setting_youtube_link'",
    OBJECT
  );
  if(!empty($setting_youtube_link)) {
    $return['setting_youtube_link'] = $setting_youtube_link[0]->option_value;
  }
  
  return $return;
}
add_action( 'getSetting', 'getSetting', 0 );

/**
 * Get Publikasi
 * 
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1'),
 * 		'cat'			=> 5
 * );
 * 
 * param detail:
 * paged		: (string - optional)		current page, if you want to use pagination.
 * 																	integer also works, idk why, dont asked me. I HATE WORDPRESS
 * exclude	: (array - optional)		excluded publikasi id
 * cat			: (integer - optional)	category of publikasi
 *
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getPublikasi($limit='-1', $param=array()) {
  global $wpdb;
  $prefix = $wpdb->prefix;

  $limit_pagination = ($limit != '-1') ? " LIMIT " . $limit : '';
  if(isset($param['paged']) && $limit != '-1') {
    $offset = ($param['paged'] - 1) * $limit;
    $limit_pagination = " LIMIT " . $offset . "," . $limit;
  }

  $select_post = "unsi_posts.ID,
      unsi_posts.post_author,
      unsi_posts.post_date,
      unsi_posts.post_date_gmt,
      unsi_posts.post_title,
      unsi_posts.post_excerpt,
      unsi_posts.post_name,
      unsi_posts.comment_count,
      unsi_posts.menu_order";

  $where = "unsi_posts.post_type = 'publikasi'";
  $where .= " AND unsi_posts.post_status = 'publish'";	

  if(isset($param['search']) && isset($param['search_by'])) {
    if($param['search_by'] === 'semua') {
      $where .= " AND (unsi_posts.post_title LIKE '%" . $param['search'] . "%' OR unsi_posts.post_content LIKE '%" . $param['search'] . "%')";
    }
    
    if($param['search_by'] === 'judul') {
      $where .= " AND unsi_posts.post_title LIKE '%" . $param['search'] . "%'";
    }
    
    if($param['search_by'] === 'konten') {
      $where .= " AND unsi_posts.post_content LIKE '%" . $param['search'] . "%'";
    }
  }
  
  if(isset($param['kategori'])) {
    $where .= " AND type_slug='".$param['kategori']."'";	
  }
  
  if(isset($param['from_tahun'])) {
    $where .= " AND year_slug>='".$param['from_tahun']."' AND year_slug<='".$param['to_tahun']."'";	
  }

  if(isset($param['kategori']) && isset($param['from_tahun'])) {

    $datas = $wpdb->get_results(
      "SELECT unsi_publikasi_ilmiah_year.post_slug, 
        unsi_publikasi_ilmiah_year.year_slug, 
        unsi_publikasi_ilmiah_type.type_slug, 
        $select_post 
      FROM unsi_publikasi_ilmiah_year 
      JOIN unsi_publikasi_ilmiah_type ON unsi_publikasi_ilmiah_year.post_id = unsi_publikasi_ilmiah_type.post_id 
      JOIN unsi_posts ON unsi_posts.ID = unsi_publikasi_ilmiah_type.post_id 
      WHERE $where 
      $limit_pagination",
      OBJECT
    );
  
  } elseif(isset($param['kategori']) && !isset($param['from_tahun'])) {

    $datas = $wpdb->get_results(
      "SELECT unsi_publikasi_ilmiah_type.post_slug, 
        unsi_publikasi_ilmiah_type.type_slug, 
        $select_post 
      FROM unsi_publikasi_ilmiah_type 
      JOIN unsi_posts ON unsi_posts.ID = unsi_publikasi_ilmiah_type.post_id 
      WHERE $where 
      $limit_pagination",
      OBJECT
    );

  } elseif(!isset($param['kategori']) && isset($param['from_tahun'])) {	

    $datas = $wpdb->get_results(
      "SELECT unsi_publikasi_ilmiah_year.post_slug, 
        unsi_publikasi_ilmiah_year.year_slug, 
        $select_post 
      FROM unsi_publikasi_ilmiah_year 
      JOIN unsi_posts ON unsi_posts.ID = unsi_publikasi_ilmiah_year.post_id 
      WHERE $where 
      $limit_pagination",
      OBJECT
    );

  } else {
	
    $datas = $wpdb->get_results(
      "SELECT $select_post FROM unsi_posts
      WHERE $where
      $limit_pagination",
      OBJECT
    );

  }

  if(!empty($datas)) {
    foreach($datas as $key => $data) {
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($data->ID), 'medium' );
      if($thumb){
        $datas[$key]->foto = $thumb['0'];
      }else{
        $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
      }
      
      $alt = get_post_meta(get_post_thumbnail_id($data->ID), '_wp_attachment_image_alt', true);
      if(empty($alt)) {
        $alt = $data->post_title;
      }
      $datas[$key]->alt_foto = $alt;

      $category_post = get_the_terms( $data->ID, 'tipe-publikasi' );
      $datas[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';
      
      $post_year = get_the_terms( $data->ID, 'tahun-publikasi' );
      $datas[$key]->year = ( !empty($post_year[0]) ) ? $post_year[0]->name : '';
    }
  }
  
  return $datas;
}
add_action( 'getPublikasi', 'getPublikasi', 0 );

/**
 * Get popular opini
 * temporary use highlight as popular
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getHighlightOpini() {
  global $wpdb;

  $datas = $wpdb->get_results(
    "SELECT unsi_posts.* FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'popularopini'
        AND unsi_postmeta.meta_value = '1'
        AND unsi_posts.post_status = 'publish'
        AND unsi_posts.post_type = 'opini'
      ORDER BY unsi_posts.post_date DESC
      LIMIT 6",
    OBJECT
  );

  if(!empty($datas)) {
    foreach($datas as $key => $data) {
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($data->ID), 'medium' );
      if($thumb){
        $datas[$key]->foto = $thumb['0'];
      }else{
        $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
      }
      
      $alt = get_post_meta(get_post_thumbnail_id($data->ID), '_wp_attachment_image_alt', true);
      if(empty($alt)) {
        $alt = $data->post_title;
      }
      $datas[$key]->alt_foto = $alt;

      $category_post = get_the_terms( $data->ID, 'category' );
      $datas[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';
    }
  }
  
  return $datas;
}
add_action( 'getHighlightOpini', 'getHighlightOpini', 0 );

/**
 * Get Opini
 * how to use please check page-opini.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1'),
 * 		'cat'			=> 5
 * );
 * 
 * param detail:
 * paged		: (string - optional)		current page, if you want to use pagination.
 * 																	integer also works, idk why, dont asked me. I HATE WORDPRESS
 * exclude	: (array - optional)		excluded publikasi id
 * cat			: (integer - optional)	category of publikasi
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getOpini($limit='-1', $param=array()) {
  global $wpdb;

  $args = array(
    'post_type'				=> 'opini',
    'post_status'			=> 'publish',
    'posts_per_page'	=> $limit,
    'orderby'					=> 'date',
    'order'						=> 'DESC',
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  if(!empty($param) && isset($param['exclude'])) {
    $args['post__not_in'] = $param['exclude'];
  }

  if(!empty($param) && isset($param['cat'])) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'kategori-opini',
        'field'    => 'term_id',
        'terms'    => $param['cat'],
        'operator' => 'IN',
      ),
    );
  }

  $datas = new WP_Query($args);
  $datas = $datas->posts;

  foreach($datas as $key => $data) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($data->ID), 'medium' );
    if($thumb){
      $datas[$key]->foto = $thumb['0'];
      $datas[$key]->style_emptyphoto = 'style=""';
    }else{
      $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
      $datas[$key]->style_emptyphoto = 'style="width: auto; height: 100%; opacity: 0.2;"';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($data->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $data->post_title;
    }
    $datas[$key]->alt_foto = $alt;

    $category_post = get_the_terms( $data->ID, 'kategori-opini' );
    $datas[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';
  }
  
  return $datas;
}
add_action( 'getOpini', 'getOpini', 0 );

/**
 * Get Opini Categories
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getKategoriOpini($slug='') {
  global $wpdb;

  $where = "taxonomy = 'kategori-opini'";
  if(!empty($slug)) {
    $where .= " AND unsi_terms.slug = '$slug'";
  }

  $categories = $wpdb->get_results(
    "SELECT unsi_term_taxonomy.term_id, 
      unsi_term_taxonomy.taxonomy,
      unsi_terms.name,
      unsi_terms.slug
    FROM unsi_term_taxonomy 
    JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
    WHERE $where
    ORDER BY unsi_terms.sort ASC",
    OBJECT
  );

  return $categories;
}
add_action( 'getKategoriOpini', 'getKategoriOpini', 0 );

/**
 * Get kategori berita
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getPostsCategories($slug='') {
  global $wpdb;

  $categories = $wpdb->get_results(
    "SELECT unsi_term_taxonomy.term_id, 
      unsi_term_taxonomy.taxonomy,
      unsi_terms.name,
      unsi_terms.slug
    FROM unsi_term_taxonomy 
    JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
    WHERE unsi_term_taxonomy.taxonomy = 'category'
    ORDER BY unsi_terms.sort ASC",
    OBJECT
  );

  return $categories;
}
add_action( 'getPostsCategories', 'getPostsCategories', 0 );

/**
 * Get popular berita
 * temporary use highlight as popular
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getHighlightBerita($limit='') {
  global $wpdb;

  if($limit==='') {
    $limit = 6;
  }

  $datas = $wpdb->get_results(
    "SELECT unsi_posts.* FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'popularnews'
        AND unsi_postmeta.meta_value = '1'
        AND unsi_posts.post_status = 'publish'
      ORDER BY unsi_posts.post_date DESC
      LIMIT $limit",
    OBJECT
  );
  //ORDER BY unsi_posts.menu_order DESC

  foreach($datas as $key => $data) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($data->ID), 'medium' );
    if($thumb){
      $datas[$key]->foto = $thumb['0'];
    }else{
      $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($data->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $data->post_title;
    }
    $datas[$key]->alt_foto = $alt;

    $category_post = get_the_terms( $data->ID, 'category' );
    $datas[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';
  }
  
  return $datas;
}
add_action( 'getHighlightBerita', 'getHighlightBerita', 0 );

/**
 * Get Berita Posts
 * how to use please check content-berita.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1'),
 * 		'cat'			=> 5
 * );
 * 
 * param detail:
 * paged		: (string - optional)		current page, if you want to use pagination. 
 * 																	integer also works, idk why, dont asked me. I HATE WORDPRESS
 * exclude	: (array - optional)		excluded publikasi id
 * cat			: (integer - optional)	category of publikasi
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getBerita($limit='-1', $param=array()) {
  global $wpdb;

  $args = array(
    'post_type'				=> 'post',
    'post_status'			=> 'publish',
    'posts_per_page'	=> $limit,
    // 'orderby'			=> 'menu_order',
    'orderby'					=> 'date',
    'order'						=> 'DESC',
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  if(!empty($param) && isset($param['exclude'])) {
    $args['post__not_in'] = $param['exclude'];
  }

  if(!empty($param) && isset($param['cat'])) {
    $args['category__in'] = $param['cat'];
  }

  $datas = new WP_Query($args);
  $datas = $datas->posts;

  foreach($datas as $key => $val) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($val->ID), 'full' );
    if($thumb){
      $datas[$key]->foto = $thumb['0'];
      $datas[$key]->have_img = true;
      $datas[$key]->style_emptyphoto = '';
    }else{
      $datas[$key]->foto = get_template_directory_uri().'/library/images/main_logo.png';
      $datas[$key]->have_img = false;
      $datas[$key]->style_emptyphoto = 'width: auto; height: 100%; opacity: 0.2;';
    }
    
    $alt = get_post_meta($val->ID, '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $val->post_title;
    }
    $datas[$key]->alt_foto = $alt;

    $category_post = get_the_category($val->ID);
    $datas[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';

    $datas[$key]->link_post = get_the_permalink($val->ID);
  }
  
  return $datas;
}
add_action( 'getBerita', 'getBerita', 0 );

/**
 * Get video categories
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getVideoCategories($slug='') {
  global $wpdb;

  $categories = $wpdb->get_results(
    "SELECT unsi_term_taxonomy.term_id, 
      unsi_term_taxonomy.taxonomy,
      unsi_terms.name,
      unsi_terms.slug
    FROM unsi_term_taxonomy 
    JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
    WHERE taxonomy = 'kategori-video'
    ORDER BY unsi_terms.sort ASC",
    OBJECT
  );

  return $categories;
}
add_action( 'getVideoCategories', 'getVideoCategories', 0 );

/**
 * Get Image Gallery
 * how to use please check page-media-foto.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1'),
 * 		'cat'			=> 5
 * );
 * 
 * param detail:
 * paged		: (string - optional)		current page, if you want to use pagination.
 * 																	integer also works, idk why, dont asked me. I HATE WORDPRESS
 * exclude	: (array - optional)		excluded publikasi id
 * cat			: (integer - optional)	category of publikasi
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getGaleri($limit='-1', $param=array()) {
  global $wpdb;

  $args = array(
    'post_type'				=> 'gallery',
    'post_status'			=> 'publish',
    'posts_per_page'	=> $limit,
    // 'orderby'			=> 'menu_order',
    'orderby'					=> 'date',
    'order'						=> 'DESC',
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  if(!empty($param) && isset($param['exclude'])) {
    $args['post__not_in'] = $param['exclude'];
  }

  $datas = new WP_Query($args);
  $datas = $datas->posts;

  foreach($datas as $key => $ft) {
    $galeri_foto = get_post_meta($ft->ID, 'galeri_foto', true);
    $thumb = wp_get_attachment_image_src( $galeri_foto, 'full' );
    if($thumb){
      $datas[$key]->foto = $thumb['0'];
    }else{
      $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta($galeri_foto, '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = "Foto " . $ft->post_title;
    }
    $datas[$key]->alt_foto = $alt;

    $datas[$key]->short_description = get_post_meta($ft->ID, 'galeri_short_description', true);
  }
  
  return $datas;
}
add_action( 'getGaleri', 'getGaleri', 0 );

/**
 * Get video
 * how to use please check page-media.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1'),
 * 		'cat'			=> 5
 * );
 * 
 * param detail:
 * paged		: (string - optional)		current page, if you want to use pagination. 
 * 																	integer also works, idk why, dont asked me. I HATE WORDPRESS
 * exclude	: (array - optional)		excluded publikasi id
 * cat			: (integer - optional)	category of publikasi
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getVideos($limit='-1', $param=array()) {
  global $wpdb;

  $args = array(
    'post_type'				=> 'video',
    'post_status'			=> 'publish',
    'posts_per_page'	=> $limit,
    // 'orderby'			=> 'menu_order',
    'orderby'					=> 'date',
    'order'						=> 'DESC',
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  if(!empty($param) && isset($param['exclude'])) {
    $args['post__not_in'] = $param['exclude'];
  }

  if(!empty($param) && isset($param['cat'])) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'kategori-video',
        'field'    => 'term_id',
        'terms'    => $param['cat'],
        'operator' => 'IN',
      ),
    );
  }

  $geVideos = new WP_Query($args);
  $geVideos = $geVideos->posts;

  foreach($geVideos as $key => $vid) {		
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($vid->ID), 'full' );
    if($thumb){
      $geVideos[$key]->foto = $thumb['0'];
    }else{
      $geVideos[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($vid->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $vid->post_title;
    }
    $geVideos[$key]->alt_foto = $alt;
  }
  
  return $geVideos;
}
add_action( 'getVideos', 'getVideos', 0 );

/**
 * Get highlight video for right video in all detail article
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getArticleHighlightVideos() {
  global $wpdb;

  $highlight_video = $wpdb->get_results(
    "SELECT unsi_posts.* FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'highlight_video_article'
        AND unsi_postmeta.meta_value = '1'
        AND unsi_posts.post_status = 'publish'
      ORDER BY unsi_posts.post_date DESC
      LIMIT 1",
    OBJECT
  );
  $highlight_video = (!empty($highlight_video) && !empty($highlight_video[0])) ? $highlight_video[0] : null;

  if($highlight_video !== null) {		
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($highlight_video->ID), 'full' );
    if($thumb){
      $highlight_video->foto = $thumb['0'];
    }else{
      $highlight_video->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($highlight_video->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $highlight_video->post_title;
    }
    $highlight_video->alt_foto = $alt;
    
    $highlight_video->big_video_information = get_field('big_video_information', $highlight_video->ID);
  }
  
  return $highlight_video;
}
add_action( 'getArticleHighlightVideos', 'getArticleHighlightVideos', 0 );

/**
 * Get highlight video for footer banner
 *
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getFooterHighlightVideos() {
  global $wpdb;

  $highlight_video = $wpdb->get_results(
    "SELECT unsi_posts.* FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'highlight_video_home'
        AND unsi_postmeta.meta_value = '1'
        AND unsi_posts.post_status = 'publish'
      ORDER BY unsi_posts.post_date DESC
      LIMIT 4",
    OBJECT
  );

  foreach($highlight_video as $key => $vid) {		
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($vid->ID), 'full' );
    if($thumb){
      $highlight_video[$key]->foto = $thumb['0'];
    }else{
      $highlight_video[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($vid->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $vid->post_title;
    }
    $highlight_video[$key]->alt_foto = $alt;

    $highlight_video[$key]->sort = get_post_meta($vid->ID, 'highlight_video_home_sort', true);	
  }

  usort($highlight_video, function($a, $b) {
    return $a->sort <=> $b->sort;
  });
  
  return $highlight_video;
}
add_action( 'getFooterHighlightVideos', 'getFooterHighlightVideos', 0 );

/**
 * Get highlight video for media page
 *
 * @param  integer	$limit		limit per page, for pagination use
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getHighlightVideos($limit) {
  global $wpdb;

  $highlight_video = $wpdb->get_results(
    "SELECT unsi_posts.* FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'highlight_video'
        AND unsi_postmeta.meta_value = '1'
        AND unsi_posts.post_status = 'publish'
      ORDER BY unsi_posts.post_date DESC
      LIMIT $limit",
    OBJECT
  );

  foreach($highlight_video as $key => $vid) {		
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($vid->ID), 'full' );
    if($thumb){
      $highlight_video[$key]->foto = $thumb['0'];
    }else{
      $highlight_video[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($vid->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $vid->post_title;
    }
    $highlight_video[$key]->alt_foto = $alt;
  }
  
  return $highlight_video;
}
add_action( 'getHighlightVideos', 'getHighlightVideos', 0 );

/**
 * Get Tokoh list
 * how to use please check page-tokoh.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1'
 * );
 * 
 * param detail:
 * paged	: (string - optional)	current page, if you want to use pagination.
 * 															integer also works, idk why, dont asked me. I HATE WORDPRESS
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getTokohList($limit='-1', $param=array()) {
  global $wpdb;

  $args = array(
    'post_type'				=> 'tokoh',
    'post_status'			=> 'publish',
    'posts_per_page'	=> $limit,
    'orderby'					=> 'menu_order',
    'order'						=> 'ASC',
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  $datas = new WP_Query($args);
  $datas = $datas->posts;

  foreach($datas as $key => $data) {
    $datas[$key]->nama_lengkap = get_post_meta($data->ID, 'nama_lengkap_tokoh', true);
    $datas[$key]->tempat_lahir = get_post_meta($data->ID, 'tempat_lahir', true);
    $datas[$key]->tanggal_lahir = get_post_meta($data->ID, 'tanggal_lahir', true);
    $datas[$key]->tempat_meninggal_dunia = get_post_meta($data->ID, 'tempat_meninggal_dunia', true);
    $datas[$key]->tanggal_meninggal_dunia = get_post_meta($data->ID, 'tanggal_meninggal_dunia', true);
    $datas[$key]->cuplikan_tokoh = get_post_meta($data->ID, 'cuplikan_tokoh', true);
    $datas[$key]->jabatan_di_indonesia = get_post_meta($data->ID, 'jabatan_di_indonesia', true);
    
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($data->ID), 'full' );
    if($thumb){
      $datas[$key]->foto = $thumb['0'];
    }else{
      $datas[$key]->foto = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($data->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = "Foto " . $datas[$key]->nama_lengkap;
    }
    $datas[$key]->alt_foto = $alt;
  }
  
  return $datas;
}
add_action( 'getTokohList', 'getTokohList', 0 );

/**
 * Get Tokoh list
 * how to use please check archive-kategori-program.php
 *
 * param reference example:
 * $param = array(
 * 		'paged'		=> '1',
 * 		'exclude'	=> array('1')
 * );
 * 
 * param detail:
 * paged	: (string - optional)	current page, if you want to use pagination.
 * 															integer also works, idk why, dont asked me. I HATE WORDPRESS
 * cat		: (integer - optional) category of publikasi
 * 
 * @param string	$limit			limit per page, for pagination use
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getPrograms($limit='-1', $param=array()) {
  $args = array(
    'post_type'				=> 'program',
    'posts_per_page'	=> $limit,
    'post_status'			=> 'publish',
    'orderby'					=> 'date',
    'order'						=> 'DESC'
  );

  if(!empty($param) && isset($param['paged'])) {
    $args['paged'] = $param['paged'];
  }

  if(!empty($param) && isset($param['cat']) && $param['cat'] != 0) {
    $args['tax_query'] =  array(
      array(
        'taxonomy' => 'kategori-program',
        'field' => 'term_id',
        'terms' => $param['cat']
      )
      );
  }

  $getProgram = new WP_Query( $args );
  $getProgram = $getProgram->posts;

  foreach($getProgram as $key => $program) {
    $check_len = strlen( strip_tags($program->post_content) );
    if( $check_len > 300 ) {
      $getProgram[$key]->cropped_content = substr(strip_tags($program->post_content), 0, 300)  . '...';
    } else {
      $getProgram[$key]->cropped_content = strip_tags($program->post_content);
    }
    
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($program->ID), 'full' );
    if($thumb){
      $getProgram[$key]->photo = $thumb['0'];
    }else{
      $getProgram[$key]->photo = get_template_directory_uri().'/library/images/sorry.png';
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($program->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = $program->post_title;
    }
    $getProgram[$key]->alt_photo = $alt;

    $category_post = get_the_terms( $program->ID, 'kategori-program' );
    $getProgram[$key]->category_name = ( !empty($category_post[0]) ) ? $category_post[0]->name : '';
  }

  return $getProgram;
}
add_action( 'getPrograms', 'getPrograms', 0 );

/**
 * Get Profile Tokoh
 * how to use please check page-tokoh.php
 *
 * @param string		$sc				use ID or slug of post
 * @param boolean		$is_slug	if slug is true, get tokoh from slug, if not get tokoh from id
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getProfileTokoh($sc, $is_slug=false) {
  $args = array(
    'post_type'				=> 'tokoh',
    'posts_per_page'	=> '1',
    'post_status'			=> 'publish'
  );

  if($is_slug === true) {
    $args['name'] = $sc;
  }
  if($is_slug === false) {
    $args['p'] = $sc;
  }

  $getProfileTokoh = new WP_Query( $args );
  $getProfileTokoh = (!empty($getProfileTokoh->posts)) ? $getProfileTokoh->posts[0] : null;

  if($getProfileTokoh !== null) {
    $getProfileTokoh->nama_lengkap_tokoh = get_post_meta($getProfileTokoh->ID, 'nama_lengkap_tokoh', true);
    $getProfileTokoh->tempat_lahir = get_post_meta($getProfileTokoh->ID, 'tempat_lahir', true);
    $getProfileTokoh->tanggal_lahir = get_post_meta($getProfileTokoh->ID, 'tanggal_lahir', true);
    $getProfileTokoh->tempat_meninggal_dunia = get_post_meta($getProfileTokoh->ID, 'tempat_meninggal_dunia', true);
    $getProfileTokoh->tanggal_meninggal_dunia = get_post_meta($getProfileTokoh->ID, 'tanggal_meninggal_dunia', true);
    $getProfileTokoh->cuplikan_tokoh = get_post_meta($getProfileTokoh->ID, 'cuplikan_tokoh', true);
    $getProfileTokoh->jabatan_di_indonesia = get_post_meta($getProfileTokoh->ID, 'jabatan_di_indonesia', true);
    
    $foto_profile_id = get_post_meta($getProfileTokoh->ID, 'foto_profile_tokoh', true);
    if(!empty($foto_profile_id)) {
      $getProfileTokoh->foto = wp_get_attachment_url( $foto_profile_id );
    } else {
      $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($tokoh->ID), 'full' );
      if($thumb){
        $getProfileTokoh->foto = $thumb['0'];
      }else{
        $getProfileTokoh->foto = get_template_directory_uri().'/library/images/sorry.png';
      }
    }
    
    $alt = get_post_meta(get_post_thumbnail_id($getProfileTokoh->ID), '_wp_attachment_image_alt', true);
    if(empty($alt)) {
      $alt = "Foto " . $getProfileTokoh->nama_lengkap_tokoh;
    }
    $getProfileTokoh->alt_foto = $alt;
  }

  return $getProfileTokoh;
}
add_action( 'getProfileTokoh', 'getProfileTokoh', 0 );

/**
 * Get Highlight Tokoh
 * how to use please check page-tokoh.php
 *
 * @param string	$posisi		position where banner will be displayed
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getHighlightTokoh($posisi) {
  global $wpdb;

  if($posisi === 'banner') {
    $select = "unsi_posts.ID, unsi_posts.post_date, unsi_posts.post_title, 
      unsi_posts.post_name, unsi_posts.ID, unsi_posts.ID, unsi_posts.ID ";
  }

  if($posisi === 'artikel') {
    $select = "unsi_posts.* ";
  }

  $query = "SELECT $select 
      FROM unsi_postmeta
      JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID
      WHERE unsi_postmeta.meta_key = 'highlight_tokoh' 
        AND unsi_postmeta.meta_value = '1' 
        AND unsi_posts.post_type = 'tokoh' 
        AND unsi_posts.post_status = 'publish' 
      ORDER BY unsi_posts.post_date DESC
      LIMIT 1";

  $hightlightTokoh = $wpdb->get_results(
    $query,
    OBJECT
  );

  $hightlightTokoh = (!empty($hightlightTokoh)) ? $hightlightTokoh[0] : null;

  if($hightlightTokoh !== null && $posisi != 'artikel') {
    //banner halaman index tokoh
    $banner_image_id = get_post_meta($hightlightTokoh->ID, 'tokoh_banner_image', true);
    $hightlightTokoh->banner_image = wp_get_attachment_url( $banner_image_id );
    $hightlightTokoh->banner_title = get_post_meta($hightlightTokoh->ID, 'tokoh_banner_title', true);
    $hightlightTokoh->banner_text = get_post_meta($hightlightTokoh->ID, 'tokoh_banner_text', true);

    //banner footer
    $footer_banner_image_id = get_post_meta($hightlightTokoh->ID, 'tokoh_image', true);
    $hightlightTokoh->footer_banner_image = wp_get_attachment_url( $footer_banner_image_id );
    $hightlightTokoh->footer_banner_title1 = get_post_meta($hightlightTokoh->ID, 'tokoh_info_1', true);
    $hightlightTokoh->footer_banner_title2 = get_post_meta($hightlightTokoh->ID, 'tokoh_info_2', true);
    $hightlightTokoh->footer_banner_text = get_post_meta($hightlightTokoh->ID, 'tokoh_mini_description', true);
  }

  return $hightlightTokoh;
}
add_action( 'getHighlightTokoh', 'getHighlightTokoh', 0 );

/**
 * Get posisi kepengurusan
 * 
 * param reference example:
 * $param = array(
 * 		'noanggota'		=> TRUE,
 * 		'is_pusat'		=> TRUE,
 * 		'parent'			=> 5,
 * 		'limit'				=> 5
 * );
 * 
 * param detail:
 * noanggota	: (boolean - optional)	if noanggota is TRUE, don't select 'anggota' position
 * is_pusat		: (boolean - optional)	select only position under ISNU Pusat
 * parent			: (integer - optional)	select position from this parent
 * limit			: (integer - optional)	limit selecting
 *
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getPosisiKepengurusan($param=array()){
  global $wpdb;

  $where = "unsi_term_taxonomy.taxonomy = 'posisi-kepengurusan' ";

  if(isset($param['noanggota']) && $param['noanggota'] === TRUE) {
    $where .= " AND unsi_terms.name NOT LIKE '%anggota%' ";
  }
  
  if(isset($param['is_pusat']) && $param['is_pusat'] === TRUE) {
    $where .= "AND unsi_terms.slug = 'isnu-pusat' ";
  }
  
  if(isset($param['parent'])) {
    $where .= "AND unsi_term_taxonomy.parent = ".$param['parent']." ";
  } else {
    $where .= "AND unsi_term_taxonomy.parent = 0 ";
  }

  $query = "SELECT unsi_terms.*,
        unsi_term_taxonomy.taxonomy,
        unsi_term_taxonomy.count
      FROM unsi_term_taxonomy
      JOIN unsi_terms ON unsi_term_taxonomy.term_id = unsi_terms.term_id
      WHERE $where
      ORDER BY unsi_terms.sort ASC";
  
  if(isset($param['limit'])) {
    $query .= " LIMIT " . $param['limit'];
  }

  $categoryPosisi = $wpdb->get_results(
    $query,
    OBJECT
  );

  if(isset($param['parent'])) {
    $categoryPosisi = (!empty($categoryPosisi)) ? $categoryPosisi : null;
  } else {
    $categoryPosisi = (!empty($categoryPosisi)) ? $categoryPosisi[0] : null;
  }
  
  return $categoryPosisi;
}
add_action( 'getPosisiKepengurusan', 'getPosisiKepengurusan', 0 );

/**
 * Get list kepengurusan
 * 
 * param reference example:
 * $param = array(
 * 		'category'	=> 2,
 * 		'limit'			=> 5
 * );
 * 
 * param detail:
 * category	: (integer - optional)	category of anggota kepengurusan
 * limit		: (integer - optional)	limit selecting
 *
 * @param array		$param			other parameter
 * @return void
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function getListKepengurusan($param=array()){
  global $wpdb;

  $where = "unsi_posts.post_status = 'publish' ";
  $where .= "AND unsi_posts.post_type = 'kepengurusan' ";
  // $limit='', $category=0
  if(!empty($param) && isset($param['category'])) {
    $where .= "AND unsi_term_relationships.term_taxonomy_id = " . $param['category'] . " ";
  }

  $query = "SELECT unsi_posts.* 
      FROM unsi_term_relationships 
      JOIN unsi_posts ON unsi_term_relationships.object_id = unsi_posts.ID  
      WHERE $where 
      ORDER BY unsi_posts.post_title ASC ";
  
  if(!empty($param) && isset($param['limit'])) {
    $query .= "LIMIT " . $param['limit'] . " ";
  }

  $listPengurus = $wpdb->get_results(
    $query,
    OBJECT
  );
  $listPengurus = (!empty($listPengurus)) ? $listPengurus : null;
  
  return $listPengurus;
}
add_action( 'getListKepengurusan', 'getListKepengurusan', 0 );

/**
 * Get list ads
 *	
  * @return void
  * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
  */
function getAds($show_in='') {
  global $wpdb;

  $return = array(
    'ads_top' => array(),
    'ads_right' => array(),
    'ads_left' => array(),
    'ads_bottom' => array()
  );

  if(empty($show_in) || $show_in === 'detail_article') {
    $ads_top_query = "SELECT * 
        FROM unsi_postmeta 
        JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID  
        WHERE unsi_postmeta.meta_key = 'advertising_position' 
          AND unsi_postmeta.meta_value = 'top_ads' 
          AND unsi_posts.post_status = 'publish' 
        ORDER BY unsi_posts.post_date DESC
        LIMIT 1";
    $ads_top = $wpdb->get_results(
      $ads_top_query,
      OBJECT
    );
    if(!empty($ads_top)) {
      $return['ads_top'] = $ads_top[0];
      $ads_top_image_id = get_post_meta($ads_top[0]->ID, 'advertising_img', true);
      $return['ads_top']->banner = wp_get_attachment_url( $ads_top_image_id );
      $return['ads_top']->url = get_post_meta($ads_top[0]->ID, 'advertising_url', true);
    }

    $ads_bottom_query = "SELECT * 
        FROM unsi_postmeta 
        JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID  
        WHERE unsi_postmeta.meta_key = 'advertising_position' 
          AND unsi_postmeta.meta_value = 'bottom_ads' 
          AND unsi_posts.post_status = 'publish' 
        ORDER BY unsi_posts.post_date DESC
        LIMIT 1";
    $ads_bottom = $wpdb->get_results(
      $ads_bottom_query,
      OBJECT
    );
    if(!empty($ads_bottom)) {
      $return['ads_bottom'] = $ads_bottom[0];
      $ads_bottom_image_id = get_post_meta($ads_bottom[0]->ID, 'advertising_img', true);
      $return['ads_bottom']->banner = wp_get_attachment_url( $ads_bottom_image_id );
      $return['ads_bottom']->url = get_post_meta($ads_bottom[0]->ID, 'advertising_url', true);
    }

    $ads_right_query = "SELECT * 
        FROM unsi_postmeta 
        JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID  
        WHERE unsi_postmeta.meta_key = 'advertising_position' 
          AND unsi_postmeta.meta_value = 'right_ads' 
          AND unsi_posts.post_status = 'publish' 
        ORDER BY unsi_posts.post_date DESC
        LIMIT 1";
    $ads_right = $wpdb->get_results(
      $ads_right_query,
      OBJECT
    );
    if(!empty($ads_right)) {
      $return['ads_right'] = $ads_right[0];
      $ads_right_image_id = get_post_meta($ads_right[0]->ID, 'advertising_img', true);
      $return['ads_right']->banner = wp_get_attachment_url( $ads_right_image_id );
      $return['ads_right']->url = get_post_meta($ads_right[0]->ID, 'advertising_url', true);
    }

    $ads_left_query = "SELECT * 
        FROM unsi_postmeta 
        JOIN unsi_posts ON unsi_postmeta.post_id = unsi_posts.ID  
        WHERE unsi_postmeta.meta_key = 'advertising_position' 
          AND unsi_postmeta.meta_value = 'left_ads' 
          AND unsi_posts.post_status = 'publish' 
        ORDER BY unsi_posts.post_date DESC
        LIMIT 1";
    $ads_left = $wpdb->get_results(
      $ads_left_query,
      OBJECT
    );
    if(!empty($ads_left)) {
      $return['ads_left'] = $ads_left[0];
      $ads_left_image_id = get_post_meta($ads_left[0]->ID, 'advertising_img', true);
      $return['ads_left']->banner = wp_get_attachment_url( $ads_left_image_id );
      $return['ads_left']->url = get_post_meta($ads_left[0]->ID, 'advertising_url', true);
    }
  }

  return $return;
}
add_action( 'getAds', 'getAds', 0 );
?>