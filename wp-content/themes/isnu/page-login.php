<?php
/*
Template Name: Login Page
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<style type="text/css">
		header, footer{
			display: none !important;
		}
	</style>

	<?php 
		$current_user = wp_get_current_user();
		$u_id = $current_user->ID;

		if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != '') {
			$referer = $_SESSION['rfr'];
		} else {
			$rfr = wp_get_referer();
			if(isset($rfr) AND $rfr != '') {
				$referer = wp_get_referer();
				$_SESSION['rfr'] = $referer;
			} else {
				$referer = home_url();
			}
		}
	?>

	<div class="row"></div>

	<div class="row row_login">
		<div class="col-md-4"></div>
		<div class="col-md-4 col_login">

			<div class="box_toplogin">
				<div class="mg_topLogo">
					<img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
				</div>
				<h1 class="ht_login">Masuk Akun</h1>
			</div>

			<div class="box_login">

				<?php if( isset($u_id) AND $u_id != '') { ?>

					<?php /** reason why didnt use user_status in users table: https://wordpress.org/support/topic/what-is-the-status-of-user_status/ */ ?>
					<?php
						$all_meta_for_user = get_user_meta( $u_id );
						$is_active = (isset($all_meta_for_user['userstatus'])) ? $all_meta_for_user['userstatus'][0] : 0;
					?>

					<?php if($is_active == 0) { ?>
						<div class="ok_login">
							<span class="glyphicon glyphicon-remove remove_icon"></span>
							<div class="ht_okLogin">Akun Anda belum aktif, silahkan hubungi admin.</div>
						</div>

						<div id="redirect_notactive" data-hurl="<?php echo wp_logout_url(home_url('/')); ?>"></div>

						<script type="application/javascript">
							var plant = document.getElementById('redirect_notactive');
							var hurl = plant.getAttribute('data-hurl'); 
								setTimeout(function(){
										location.replace(hurl); 
									}, 3000);
						</script>
					<?php } else { ?>
						<div class="ok_login">
							<span class="glyphicon glyphicon-ok ok_icon"></span>
							<div class="ht_okLogin">Selamat Datang di ISNU</div>
							<span>Mohon menunggu...</span>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
						</div>

						<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>"></div>

						<script>
							var plant = document.getElementById('redirect_ok');
							var hurl = plant.getAttribute('data-hurl'); 
								setTimeout(function(){
										location.replace(hurl); 
									}, 2000);
						</script>
					<?php } ?>
				<?php } else { ?>
					<form id="formLogin" method="post" autocomplete="nope" 
						action="<?php bloginfo('url'); ?>/wp-login.php/" 
						class="wp-user-form formLogin" 
						onsubmit="submit_LoginNow(event);"
					>
							<input autocomplete="nope" name="hidden" type="text" style="display:none;">

							<?php if(isset($_GET['login']) AND $_GET['login'] == 'failed') { ?>
								<div class="alert alert-danger">
									<strong>Maaf,</strong> email atau password salah. Silahkan coba kembali.
								</div>
							<?php } ?>

							<div class="f_ad_input">
								<label>Email</label>
								<input type="email" ng-model="username" name="log" id="user_login" class="ad_input" placeholder="" value="" required="required"  maxlength="50" />
								<div id="err_email" class="f_err">*Silahkan isi email</div>
							</div>

							<div class="f_ad_input">
								<label>Password</label>
								<div class="p_input">
									<input type="password" name="pwd" id="user_pass" class="ad_input" placeholder="	" value="" required="required" autocomplete="nope" />
									<div class="hidden_pass" id="btnpass" onclick="seePassword(event);">
										<span id="show_pass" style="display:flex">
											<i class="fas fa-eye"></i>
										</span>
										<span id="hide_pass" style="display:none">
											<i class="fas fa-eye-slash"></i>
										</span>
									</div>
								</div>
								<div id="err_pwd" class="f_err">*Silahkan isi password</div>
							</div>
											
							<div class="f_ad_input">
								<span>
									<a href="<?php echo home_url(); ?>/lupa-password/">Lupa Password?</a>
								</span>
							</div>
											
							<div class="f_errLogin h_errLogin"></div>
									
							<div class="login_fields">
								<?php do_action('login_form'); ?>
								<input type="submit" name="user-submit" class="sub_login" value="Masuk"/>
								<input type="hidden" name="redirect_to" value="<?php echo esc_attr($referer); ?>" />
								<input type="hidden" name="user-cookie" value="1" />
								<?php wp_nonce_field( 'login_submit' ); ?>
							</div>
					</form>
					<a class="a_bergabung" href="<?php echo home_url(); ?>/daftar/" title="Bergabung Sekarang!">
						<u>Belum punya akun? <b>Bergabung Sekarang</b></u>
					</a>
				<?php } ?>

			</div>

		</div>
		<div class="col-md-4"></div>
	</div>

	<?php // ============= cannot back ============= ?>
	<script type="text/javascript">
		(function (global) { 

			if(typeof (global) === "undefined") {
				throw new Error("window is undefined");
			}

			var _hash = "!";
			var noBackPlease = function () {
				global.location.href += "#";

				// making sure we have the fruit available for juice (^__^)
				global.setTimeout(function () {
						global.location.href += "!";
				}, 50);
			};

			global.onhashchange = function () {
				if (global.location.hash !== _hash) {
					global.location.hash = _hash;
				}
			};

			global.onload = function () {            
				noBackPlease();

				// disables backspace on page except on input fields and textarea..
				document.body.onkeydown = function (e) {
					var elm = e.target.nodeName.toLowerCase();
					if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
							e.preventDefault();
					}
					// stopping event bubbling up the DOM tree..
					e.stopPropagation();
				};          
			}

		})(window);
	</script>

<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>