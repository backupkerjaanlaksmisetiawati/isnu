
<?php
	$req_uri = explode( "/", $_SERVER['REQUEST_URI'] );
	$total_uri = count($req_uri);
	$last_uri = $req_uri[$total_uri-2];
	$taxonomy_uri = $req_uri[$total_uri-3];
?>

<?php if( $taxonomy_uri === 'kategori-program' ) { ?> 
	<?php
		get_template_part(
			'template/archive/archive',
			'kategori-program'
		);
	?>
<?php } else if( $taxonomy_uri === 'kategori-video' ) { ?> 
	<?php
		get_template_part(
			'template/archive/archive',
			'kategori-video'
		);
	?>
<?php } else if( $taxonomy_uri === 'category' ) { ?> 
	<?php
		get_template_part(
			'template/archive/archive',
			'post-category'
		);
	?>
<?php } else if( $taxonomy_uri === 'tipe-publikasi' ) { ?>
	<?php
		get_template_part(
			'template/archive/archive',
			'tipe-publikasi'
		);
	?>
<?php } else if( $taxonomy_uri === 'kategori-opini' ) { ?> 
	<?php
		get_template_part(
			'template/archive/archive',
			'kategori-opini'
		);
	?>
<?php } else if( $taxonomy_uri === 'posisi-kepengurusan' ) { ?> 
	<?php
		get_template_part(
			'template/archive/archive',
			'posisi-kepengurusan'
		);
	?>
<?php } else { ?>
	<?php get_template_part( 'archive', 'default' ); ?>
<?php } ?>