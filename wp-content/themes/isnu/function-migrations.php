<?php
/**
 * migration database from each dev's local to staging or prod
 * haven't figured out how to do migrate like laravel migration,
 * so please call each function on whatever page or header.php
 * please add "migration_" on option to make it more easier to check on unsi_options table
 */

function createTblPublikasiIlmiahYear() {
  global $wpdb;
  $initialVersion = get_option( 'migration_create_tbl_publikasi_ilmiah_year', 1 );
  $currentVersion = 2;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'publikasi_ilmiah_year';
  if ( $initialVersion < $currentVersion) { 
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `post_id` int(11) DEFAULT NULL,
        `post_slug` varchar(255) DEFAULT NULL,
        `year_id` int(11) DEFAULT NULL,
        `year_slug` varchar(255) DEFAULT NULL,
        PRIMARY KEY (`id`) 
      ) $charset_collate ;";
    update_option( 'migration_create_tbl_publikasi_ilmiah_year', $currentVersion );
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
  }
}
add_action( 'createTblPublikasiIlmiahYear', 'createTblPublikasiIlmiahYear', 0 );

function createTblPublikasiIlmiahType() {
  global $wpdb;
  $initialVersion = get_option( 'migration_create_tbl_publikasi_ilmiah_type', 1 );
  $currentVersion = 2;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'publikasi_ilmiah_type';
  if ( $initialVersion < $currentVersion) { 
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `post_id` int(11) DEFAULT NULL,
        `post_slug` varchar(255) DEFAULT NULL,
        `type_id` int(11) DEFAULT NULL,
        `type_slug` varchar(255) DEFAULT NULL,
        PRIMARY KEY (`id`) 
      ) $charset_collate ;";
    update_option( 'migration_create_tbl_publikasi_ilmiah_type', $currentVersion );
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
  }
}
add_action( 'createTblPublikasiIlmiahType', 'createTblPublikasiIlmiahType', 0 );

function createTblUserEducation() {
  global $wpdb;
  $initialVersion = get_option( 'migration_create_tbl_user_education', 1 );
  $currentVersion = 2;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'user_education';
  if ( $initialVersion < $currentVersion) { 
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `degree` varchar(50) DEFAULT NULL,
        `university_name` varchar(100) DEFAULT NULL,
        `major` varchar(250) DEFAULT NULL,
        `entering_year` year DEFAULT NULL,
        `graduated_year` year DEFAULT NULL,
        PRIMARY KEY (`id`) 
      ) $charset_collate ;";
    update_option( 'migration_create_tbl_user_education', $currentVersion );
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
  }
}
add_action( 'createTblUserEducation', 'createTblUserEducation', 0 );

function createTblUserExperienceOrganization() {
  global $wpdb;
  $initialVersion = get_option( 'migration_create_tbl_user_experience_organization', 1 );
  $currentVersion = 2;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'user_experience_organization';
  if ( $initialVersion < $currentVersion) { 
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `organization_name` varchar(100) DEFAULT NULL,
        `organization_detail` text DEFAULT NULL,
        `organization_start` year DEFAULT NULL,
        `organization_end` year DEFAULT NULL,
        PRIMARY KEY (`id`) 
      ) $charset_collate ;";
    update_option( 'migration_create_tbl_user_experience_organization', $currentVersion );
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
  }
}
add_action( 'createTblUserExperienceOrganization', 'createTblUserExperienceOrganization', 0 );

function createTblUserReferensiPublikasi() {
  global $wpdb;
  $initialVersion = get_option( 'migration_create_tbl_user_referensi_publikasi', 1 );
  $currentVersion = 2;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'user_referensi_publikasi';
  if ( $initialVersion < $currentVersion) { 
    $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `organization_name` varchar(100) DEFAULT NULL,
        `organization_detail` text DEFAULT NULL,
        `organization_start` year DEFAULT NULL,
        `organization_end` year DEFAULT NULL,
        PRIMARY KEY (`id`) 
      ) $charset_collate ;";
    update_option( 'migration_create_tbl_user_referensi_publikasi', $currentVersion );
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta( $sql );
  }
}
add_action( 'createTblUserReferensiPublikasi', 'createTblUserReferensiPublikasi', 0 );
?>