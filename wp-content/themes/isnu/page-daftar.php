<?php
/*
Template Name: Daftar Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
?>

<?php if(isset($u_id) AND $u_id != ''){ ?>
	<style>
		#container {
			display: none;
		}
	</style>
	<script> 
		setTimeout(function(){
			location.replace('<?php echo home_url(); ?>'); 
		}, 5);
	</script>
<?php } ?>

<?php
	global $wpdb;

	$pekerjaan = $wpdb->get_results(
		"SELECT ID, post_title, post_name, post_status, post_type 
		FROM unsi_posts 
		WHERE post_status='publish'
		AND post_type='pekerjaan'
		ORDER BY post_title ASC",
		OBJECT
	);	
?>

<div class="row row_globalPage row_daftarPage">
	<div class="col-md-4 col_daftarPage1">
		<h1 class="ht_daftarPage">IKUT BERDISKUSI<br/> BERSAMA ISNU</h1>
		<div class="info_daftarPage">
			Assalamu’alaikum warahmatullahi wabarakaatuh <br/><br/>
			Turut serta menguatkan ISNU, bangun diskusi cerdas nan syarat ilmu.
		</div>
	</div>
	<div class="col-md-8 col_daftarPage2">
		<div class="row">
			<div class="col-md-12">
				<div class="hide bg-success success_daftar clearfix">
					<span class="pull-left text-success">
						Success
					</span>

					<span class="pull-right text-success close_success_daftar">
						x
					</span>
				</div>
			</div>
		</div>

		<form id="formDaftar">
			<div class="mg_pleasewait">
				<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
				<p>Mohon menunggu...</p>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-6 f_daftar">
					<label>Nama Lengkap <span>*</span></label>
					<input type="text" name="namalengkap" class="txt_daftar">
					<div class="f_err err_daftar namalengkap_err"></div>
				</div>
				<div class="col-md-6 f_daftar">
					<label>Nomor Sertifikat MKNU <span>*</span></label>
					<input type="text" name="no_sertifikat_mknu" class="txt_daftar">
					<div class="f_err err_daftar no_sertifikat_mknu_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-6 f_daftar">
					<label>Email <span>*</span></label>
					<input type="email" name="email" class="txt_daftar">
					<div class="f_err err_daftar email_err"></div>
				</div>
				<div class="col-md-6 f_daftar">
					<label>Telepon <span>*</span></label>
					<input type="text" name="phone" class="txt_daftar">
					<div class="f_err err_daftar phone_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-6 f_daftar">
					<label>Tempat Lahir <span>*</span></label>
					<input type="text" name="tempatlahir" class="txt_daftar">
					<div class="f_err err_daftar tempatlahir_err"></div>
				</div>
				<div class="col-md-6 f_daftar">
					<label>Tanggal Lahir <span>*</span></label>
					<input type="text" name="tanggallahir" readonly class="txt_daftar datepicker_daftar">
					<div class="f_err err_daftar tanggallahir_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-6 f_daftar">
					<label>Jenis Kelamin <span>*</span></label>
					<div class="bx_radio">
						<label><input type="radio" name="jeniskelamin" checked class="rad_daftar" value="1">Laki-laki</label>
						<label><input type="radio" name="jeniskelamin" class="rad_daftar" value="2">Perempuan</label>
					</div>
					<div class="f_err err_daftar jeniskelamin_err"></div>
				</div>
				<div class="col-md-6 f_daftar">
					<label>Status Pernikahan <span>*</span></label>
					<div class="bx_radio">
						<label><input type="radio" name="statuspernikahan" class="rad_daftar" checked value="1">Menikah</label>
						<label><input type="radio" name="statuspernikahan" class="rad_daftar" value="2">Tidak Menikah</label>
					</div>
					<div class="f_err err_daftar statuspernikahan_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-12 f_daftar">
					<label>Pendidikan Terakhir <span>*</span></label>
					<div class="gelar_pendidikan nomargin">
						<div class="row list_pendidikan">
							<div class="col-md-2 nopadding_right nopadding_left">
								<label>Gelar</label>
								<select name="pendidikans1" id="pendidikans1" onchange="changeJurusanLabel('pendidikans1')" class="sel_daftar">
									<option value="S1">S1 (Sarjana)</option>
									<option value="S2">S2 (Magister)</option>
									<option value="S3">S3 (Doktor)</option>
									<option value="Guru Besar">Guru Besar</option>
								</select>
								<div class="f_err err_daftar pendidikans1_err"></div>
							</div>
							<div class="col-md-3 nopadding_right">
								<label>Universitas</label>
								<input type="text" name="universitass1" class="txt_daftar">
								<div class="f_err err_daftar universitass1_err"></div>
							</div>
							<div class="col-md-3 nopadding_right">
								<label id="labelJurusans1">Jurusan</label>
								<input type="text" name="jurusans1" class="txt_daftar">
								<div class="f_err err_daftar jurusans1_err"></div>
							</div>
							<div class="col-md-3 nopadding_right tahun_kelulusan">
								<label>Tahun</label>
								<input type="text" name="tahuns1from" class="txt_daftar" maxlength="4"> - 
								<input type="text" name="tahuns1to" class="txt_daftar" maxlength="4">
								<div class="f_err err_daftar tahuns1_err"></div>
							</div>
							<div class="col-md-1 nopadding_left nopadding_right btn_addremove" style="padding-top: 30px;">
								<a class="add_pendidikan btn_yellow" onclick="add_riwayat_pendidikan()">Add</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-12 f_daftar">
					<label>Pengalaman Organisasi <span>*</span></label>
					<div class="pengalaman_organisasi nomargin">
						<div class="row list_organisasi">
							<div class="col-md-4 nopadding_right">
								<label>Nama Organisasi</label>
								<input type="text" name="organisasi_name[]" class="txt_daftar">
								<div class="f_err err_daftar organisasi_name_0_err"></div>
							</div>
							<div class="col-md-4 nopadding_right">
								<label>Keterangan Organisasi</label>
								<input type="text" name="organisasi_detail[]" class="txt_daftar">
								<div class="f_err err_daftar organisasi_detail_0_err"></div>
							</div>
							<div class="col-md-3 nopadding_right tahun_kelulusan">
								<label>Tahun</label>
								<input type="text" name="organisasi_from[]" class="txt_daftar" maxlength="4"> - 
								<input type="text" name="organisasi_to[]" class="txt_daftar" maxlength="4">
								<div class="f_err err_daftar organisasi_tahun_0_err"></div>
							</div>
							<div class="col-md-1 nopadding_left nopadding_right btn_addremove" style="padding-top: 30px;">
								<a class="add_pengalaman btn_yellow" onclick="add_pengalaman_organisasi()">Add</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-12 f_daftar">
					<label>Pekerjaan Sekarang <span>*</span></label>
					<select name="pekerjaan" class="sel_daftar">
						<option value="">(Pilih Pekerjaan)</option>
						<?php foreach($pekerjaan as $pekerjaan) { ?>
						<option value="<?php echo $pekerjaan->post_name; ?>"><?php echo $pekerjaan->post_title; ?></option>
						<?php } ?>
					</select>
					<div class="f_err err_daftar pekerjaan_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-12 f_daftar">
					<label>Alamat KTP <span>*</span></label>
					<textarea name="alamat" class="area_daftar" placeholder=""></textarea>
					<div class="f_err err_daftar alamat_err"></div>
				</div>
			</div>

			<?php /*<div class="row wrap_daftar">				
				<div class="col-md-12 f_daftar">
					<label>Link Referensi Publikasi <span>*</span></label>
					<textarea name="link_referensi_publikasi" class="area_daftar" placeholder=""></textarea>
					<div class="f_err err_daftar link_referensi_publikasi_err"></div>
				</div>
			</div>*/ ?>

			<div class="row wrap_daftar">
				<div class="col-md-12 f_daftar">
					<label>Link Referensi Publikasi <span>*</span></label>
					<div class="link_referensi_publikasi nomargin">
						<div class="row list_link_referensi_publikasi">
							<div class="col-md-11 nopadding_left">
								<input type="text" name="link_referensi_publikasi[]" class="txt_daftar">
								<div class="f_err err_daftar link_referensi_publikasi_0_err"></div>
							</div>
							<div class="col-md-1 nopadding_left nopadding_right btn_addremove" style="padding-top: 7px;">
								<a class="add_pengalaman btn_yellow" onclick="add_link_referensi_publikasi()">Add</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-6 f_daftar">
					<label>Upload Sertifikat MKNU</label>
					<input type="file" class="file_daftar" id="file_daftar" name="scan_sertifikat" />
					<div class="f_err err_daftar scan_sertifikat_err"></div>
				</div>
				<div class="col-md-6 f_daftar">
					<label>Pas Foto</label>
					<input type="file" class="file_daftar" id="file_foto" name="pas_foto" />
					<div class="f_err err_daftar pas_foto_err"></div>
				</div>
			</div>

			<div class="row wrap_daftar">
			</div>

			<div class="row wrap_daftar">
				<div class="col-md-12 f-daftar clearfix">
					<button type="submit" class="btn_yellow a_daftarNU" id="submitDaftar">
						Daftar
					</button>
				</div>
			</div>

			</div>
		</form>

	</div>
</div>



<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>