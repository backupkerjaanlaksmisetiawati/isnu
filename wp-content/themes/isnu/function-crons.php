<?php

/**
 * Cron insert and sycn publikasi_ilmiah_meta
 *
 * @author Ryu Amy <ryuamy.mail@gmail.com>		PLEASE DO ADD NEW AUTHOR WHEN YOU MADE EDIT OF IT AND DON'T TAKE OUT THIS AUTHOR
 */
function sync_publikasiIlmiahMeta() {
  global $wpdb;
  $prefix = $wpdb->prefix;

  $get_publikasi = $wpdb->get_results("SELECT ID, post_name FROM ".$prefix."posts WHERE post_type='publikasi' AND post_status='publish'");

  foreach($get_publikasi as $publikasi) {
    /** tahun publikasi */
      //delete previous meta, im too lazy to check one by one T_T
      //just keep it fresh lmao
      $check_meta_tahun_publikasi = $wpdb->get_results("SELECT * FROM ".$prefix."publikasi_ilmiah_year 
        WHERE post_id=".$publikasi->ID." 
          AND post_slug='".$publikasi->post_name."'");

      if(!empty($check_meta_tahun_publikasi)) {
        $wpdb->query("DELETE FROM ".$prefix."publikasi_ilmiah_year 
        WHERE post_id=".$publikasi->ID." 
          AND post_slug='".$publikasi->post_name."'");
      }

      //check tahun-publikasi meta
      $check_term_tahun_publikasi = $wpdb->get_results("SELECT 
        ".$prefix."term_relationships.object_id AS term_relationships_post_id, 
        ".$prefix."term_relationships.term_taxonomy_id AS term_relationships_taxonomy_id, 
        ".$prefix."term_taxonomy.term_taxonomy_id AS term_taxonomy_id, 
        ".$prefix."term_taxonomy.term_id AS term_taxonomy_term_id, 
        ".$prefix."term_taxonomy.taxonomy AS term_taxonomy_name, 
        ".$prefix."terms.term_id AS term_id, 
        ".$prefix."terms.name AS term_name, 
        ".$prefix."terms.slug AS term_slug, 
        ".$prefix."terms.sort AS term_sort
      FROM ".$prefix."term_relationships 
      JOIN ".$prefix."term_taxonomy ON ".$prefix."term_relationships.term_taxonomy_id = ".$prefix."term_taxonomy.term_id
      JOIN ".$prefix."terms ON ".$prefix."term_taxonomy.term_id = ".$prefix."terms.term_id 
      WHERE ".$prefix."term_relationships.object_id=".$publikasi->ID."
        AND ".$prefix."term_taxonomy.taxonomy='tahun-publikasi'");

      foreach($check_term_tahun_publikasi as $term_tahun_publikasi) {
        $wpdb->query("INSERT INTO ".$prefix."publikasi_ilmiah_year (
          `post_id`, 
          `post_slug`, 
          `year_id`, 
          `year_slug`
        ) VALUES (
          ".$publikasi->ID.", 
          '".$publikasi->post_name."',
          ".$term_tahun_publikasi->term_id.", 
          '".$term_tahun_publikasi->term_slug."'
        )");
      }
    /** tahun publikasi */
    
    /** tipe publikasi */
      //delete previous meta, im too lazy to check one by one T_T
      //just keep it fresh lmao
      $check_meta_tipe_publikasi = $wpdb->get_results("SELECT * FROM ".$prefix."publikasi_ilmiah_type 
        WHERE post_id=".$publikasi->ID." 
          AND post_slug='".$publikasi->post_name."'");
          
      if(!empty($check_meta_tipe_publikasi)) {
        $wpdb->query("DELETE FROM ".$prefix."publikasi_ilmiah_type 
        WHERE post_id=".$publikasi->ID." 
          AND post_slug='".$publikasi->post_name."'");
      }

      //check tipe-publikasi meta
      $check_term_tipe_publikasi = $wpdb->get_results("SELECT 
        ".$prefix."term_relationships.object_id AS term_relationships_post_id, 
        ".$prefix."term_relationships.term_taxonomy_id AS term_relationships_taxonomy_id, 
        ".$prefix."term_taxonomy.term_taxonomy_id AS term_taxonomy_id, 
        ".$prefix."term_taxonomy.term_id AS term_taxonomy_term_id, 
        ".$prefix."term_taxonomy.taxonomy AS term_taxonomy_name, 
        ".$prefix."terms.term_id AS term_id, 
        ".$prefix."terms.name AS term_name, 
        ".$prefix."terms.slug AS term_slug, 
        ".$prefix."terms.sort AS term_sort
      FROM ".$prefix."term_relationships 
      JOIN ".$prefix."term_taxonomy ON ".$prefix."term_relationships.term_taxonomy_id = ".$prefix."term_taxonomy.term_id
      JOIN ".$prefix."terms ON ".$prefix."term_taxonomy.term_id = ".$prefix."terms.term_id 
      WHERE ".$prefix."term_relationships.object_id=".$publikasi->ID."
        AND ".$prefix."term_taxonomy.taxonomy='tipe-publikasi'");

      foreach($check_term_tipe_publikasi as $term_tipe_publikasi) {
        $wpdb->query("INSERT INTO ".$prefix."publikasi_ilmiah_type (
          `post_id`, 
          `post_slug`, 
          `type_id`, 
          `type_slug`
        ) VALUES (
          ".$publikasi->ID.", 
          '".$publikasi->post_name."',
          ".$term_tipe_publikasi->term_id.", 
          '".$term_tipe_publikasi->term_slug."'
        )");
      }
    /** tipe publikasi */
  }
}
add_action('sync_publikasiIlmiahMeta', 'sync_publikasiIlmiahMeta');
?>