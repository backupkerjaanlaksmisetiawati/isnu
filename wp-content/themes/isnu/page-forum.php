<?php
/*
Template Name: Forum
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	header, footer{
		display: none !important;
	}
</style>

<?php
$page = get_page_by_title( 'community' );
$content = apply_filters('the_content', $page->post_content); 
echo $content;
?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>