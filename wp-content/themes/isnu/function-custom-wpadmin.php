<?php
/**
 * funtion for custom wp-admin
 */

/* ------------------------------ Publikasi ------------------------------ */
/**
 * add custom table head di post_type publikasi
 * takeout 'berita_populer' T_T
 */
function add_wck_meta_box_table_head( $defaults ) {
  unset($defaults['berita_populer']);
  return $defaults;
}
add_filter('manage_wck-meta-box_posts_columns', 'add_wck_meta_box_table_head');
/* ------------------------------ Publikasi ------------------------------ */

/* ------------------------------ Gallery ------------------------------ */
/**
 * add custom table head di post_type gallery
 * takeout 'berita_populer' T_T
 */
function add_gallery_table_head( $defaults ) {
  unset($defaults['berita_populer']);
  return $defaults;
}
add_filter('manage_gallery_posts_columns', 'add_gallery_table_head');
/* ------------------------------ Gallery ------------------------------ */

/* ------------------------------ Tokoh ------------------------------ */
/**
 * add custom table head di post_type tokoh
 * takeout 'berita_populer' T_T
 */
function add_tokoh_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['sort'] = 'Sort';
  unset($new_columns['berita_populer']);
  return $new_columns;
}
add_filter('manage_tokoh_posts_columns', 'add_tokoh_table_head');

function add_tokoh_table_content( $column_name, $post_id ) {
  $tokoh = get_post($post_id);
  if ($column_name == 'sort') {
    echo $tokoh->menu_order;
  }
}
add_action( 'manage_tokoh_posts_custom_column', 'add_tokoh_table_content', 10, 2 );

function add_tokoh_table_sorting( $columns ) {
  $columns['sort']	= 'sort';
  return $columns;
}
add_filter( 'manage_edit-tokoh_sortable_columns', 'add_tokoh_table_sorting' );

function ft_tokoh_sort_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'sort' == $vars['orderby'] ) {
    $vars['orderby'] = 'menu_order';
  }
  return $vars;
}
add_filter( 'request', 'ft_tokoh_sort_column_orderby' );
/* ------------------------------ Tokoh ------------------------------ */

/* ------------------------------ Program ------------------------------ */
/**
 * add custom table head di post_type program
 * takeout 'berita_populer' T_T
 */
function add_program_table_head( $defaults ) {
  unset($defaults['berita_populer']);
  return $defaults;
}
add_filter('manage_program_posts_columns', 'add_program_table_head');
/* ------------------------------ Program ------------------------------ */

/* ------------------------------ Publikasi ------------------------------ */
/**
 * add custom table head di post_type publikasi
 * takeout 'berita_populer' T_T
 */
function add_publikasi_table_head( $defaults ) {
  unset($defaults['berita_populer']);
  return $defaults;
}
add_filter('manage_publikasi_posts_columns', 'add_publikasi_table_head');
/* ------------------------------ Publikasi ------------------------------ */

/* ------------------------------ Kepengurusan ------------------------------ */
/**
 * add custom table head di post_type kepengurusan
 * takeout 'berita_populer' T_T
 */
function add_kepengurusan_table_head( $defaults ) {
  unset($defaults['berita_populer']);
  return $defaults;
}
add_filter('manage_kepengurusan_posts_columns', 'add_kepengurusan_table_head');
/* ------------------------------ Kepengurusan ------------------------------ */

/* ------------------------------ Banner ------------------------------ */
/**
 * add custom table head di post_type kepengurusan
 * takeout 'berita_populer' T_T
 */
// function add_banner_table_head( $defaults ) {
// 	unset($defaults['berita_populer']);
// 	return $defaults;
// }
// add_filter('manage_banner_posts_columns', 'add_banner_table_head');
/* ------------------------------ Banner ------------------------------ */

/* ------------------------------ Video ------------------------------ */
/**
 * add custom table head di post_type video
 */
function add_video_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['highlight_video']					= 'Highlight<br />di Media Video';
  $new_columns['highlight_video_article']	= 'Highlight<br />di Detail Artikel';
  $new_columns['highlight_video_home']		= 'Highlight<br />di Homepage';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  //takeout 'berita_populer' T_T
  unset($new_columns['berita_populer']);
  return $new_columns;
}
add_filter('manage_video_posts_columns', 'add_video_table_head');

function add_video_table_content( $column_name, $post_id ) {
  if ($column_name == 'highlight_video') {
    $highlight_video = get_post_meta( $post_id, 'highlight_video', true );
    echo ($highlight_video == 1) ? 'Yes' : 'No';
  }

  if ($column_name == 'highlight_video_article') {
    $highlight_video_article = get_post_meta( $post_id, 'highlight_video_article', true );
    echo ($highlight_video_article == 1) ? 'Yes' : 'No';
  }

  if ($column_name == 'highlight_video_home') {
    $highlight_video_home = get_post_meta( $post_id, 'highlight_video_home', true );
    echo ($highlight_video_home == 1) ? 'Yes' : 'No';
  }
}
add_action( 'manage_video_posts_custom_column', 'add_video_table_content', 10, 2 );

function add_video_table_sorting( $columns ) {
  $columns['highlight_video']					= 'highlight_video';
  $columns['highlight_video_article']	= 'highlight_video_article';
  $columns['highlight_video_home']		= 'highlight_video_home';
  return $columns;
}
add_filter( 'manage_edit-video_sortable_columns', 'add_video_table_sorting' );

function ft_highlight_video_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'highlight_video' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'highlight_video',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_highlight_video_column_orderby' );

function ft_highlight_video_article_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'highlight_video_article' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'highlight_video_article',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_highlight_video_article_column_orderby' );

function ft_highlight_video_home_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'highlight_video_home' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'highlight_video_home',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_highlight_video_home_column_orderby' );
/* ------------------------------ Video ------------------------------ */

/* ------------------------------ Opini ------------------------------ */
/**
 * add custom table head di post_type opini
 */
function add_opini_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['opini_populer'] = 'Opini Populer';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  //takeout 'berita_populer' T_T
  unset($new_columns['berita_populer']);
  return $new_columns;
}
add_filter('manage_opini_posts_columns', 'add_opini_table_head');

function add_opini_table_content( $column_name, $post_id ) {
  if ($column_name == 'opini_populer') {
    $popularopini = get_post_meta( $post_id, 'popularopini', true );
    echo ($popularopini == 1) ? 'Yes' : 'No';
  }
}
add_action( 'manage_opini_posts_custom_column', 'add_opini_table_content', 10, 2 );

function add_opini_table_sorting( $columns ) {
  $columns['opini_populer']	= 'opini_populer';
  return $columns;
}
add_filter( 'manage_edit-opini_sortable_columns', 'add_opini_table_sorting' );

function ft_opini_populer_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'opini_populer' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'popularopini',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_opini_populer_column_orderby' );
/* ------------------------------ Opini ------------------------------ */

/* ------------------------------ Posts / Berita ------------------------------ */
/**
 * add custom table head di post_type posts
 * berimbas ke semua post_type T_T
 */
function add_berita_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['berita_populer'] = 'Berita Populer';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  return $new_columns;
}
add_filter('manage_posts_columns', 'add_berita_table_head');

function add_berita_table_content( $column_name, $post_id ) {
  if ($column_name == 'berita_populer') {
    $popularopini = get_post_meta( $post_id, 'popularnews', true );
    echo ($popularopini == 1) ? 'Yes' : 'No';
  }
}
add_action( 'manage_posts_custom_column', 'add_berita_table_content', 10, 2 );

function add_berita_table_sorting( $columns ) {
  $columns['berita_populer']	= 'berita_populer';
  return $columns;
}
add_filter( 'manage_edit-post_sortable_columns', 'add_berita_table_sorting' );

function ft_berita_populer_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'berita_populer' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'popularnews',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_berita_populer_column_orderby' );
/* ------------------------------ Posts / Berita ------------------------------ */

/* ------------------------------ Advertising ------------------------------ */
/**
 * add custom table head di post_type advertising
 * takeout 'berita_populer' T_T
 */
function add_advertising_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['advertising_position']	= 'Position';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  //takeout 'berita_populer' T_T
  unset($new_columns['berita_populer']);
  return $new_columns;
}
add_filter('manage_advertising_posts_columns', 'add_advertising_table_head');

function add_advertising_table_content( $column_name, $post_id ) {
  if ($column_name == 'advertising_position') {
    $advertising_position = get_post_meta( $post_id, 'advertising_position', true );
    echo ucwords(str_replace('_', ' ', $advertising_position));
  }
}
add_action( 'manage_advertising_posts_custom_column', 'add_advertising_table_content', 10, 2 );

function add_advertising_table_sorting( $columns ) {
  $columns['advertising_position']	= 'advertising_position';
  return $columns;
}
add_filter( 'manage_edit-advertising_sortable_columns', 'add_advertising_table_sorting' );

function ft_advertising_position_column_orderby( $vars ) {
  if ( isset( $vars['orderby'] ) && 'advertising_position' == $vars['orderby'] ) {
    $vars = array_merge( $vars, array(
        'meta_key'	=> 'advertising_position',
        'orderby'		=> 'meta_value'
    ) );
  }
  return $vars;
}
add_filter( 'request', 'ft_advertising_position_column_orderby' );
/* ------------------------------ Advertising ------------------------------ */

/* ------------------------------ Pekerjaan ------------------------------ */
/**
 * add custom table head di post_type pekerjaan
 * takeout 'berita_populer' T_T
 */
function add_pekerjaan_table_head( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  // $new_columns['advertising_position']	= 'Position';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  //takeout 'berita_populer' T_T
  unset($new_columns['berita_populer']);
  return $new_columns;
}
add_filter('manage_pekerjaan_posts_columns', 'add_pekerjaan_table_head');
/* ------------------------------ Pekerjaan ------------------------------ */

/* ------------------------------ User ------------------------------ */
// function new_contact_methods( $contactmethods ) {
// 	$contactmethods['phone'] = 'Phone Number';
// 	return $contactmethods;
// }
// add_filter( 'user_contactmethods', 'new_contact_methods', 10, 1 );

function new_modify_user_table( $defaults ) {
  $total = count($defaults);
  $takeout = $total - 3;
  $new_columns = array_slice( $defaults, 0, $takeout, true );
  $new_columns['education']	=  'Education';
  $new_columns['sertifikat']	=  'Sertifikat';
  $last = array_slice( $defaults, -3, 3, true );
  foreach($last as $key => $val) {
    $new_columns[$key] = $val;
  }
  return $new_columns;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

function new_modify_user_table_row( $val, $column_name, $user_id ) {
  global $wpdb;

  $check_education = $wpdb->get_results(
    "SELECT * FROM unsi_user_education WHERE user_id = " . $user_id . " ORDER BY degree DESC",
    OBJECT
  );

  $check_sertifikat = get_user_meta($user_id, 'upload_sertifikat');

  switch ($column_name) {
    case 'education' :
      if(!empty($check_education)) {
        return $check_education[0]->degree;
      } else {
        return '-';
      }
    case 'sertifikat' :
      if(!empty($check_sertifikat[0])) {
        return "<span style='background-color:green;display:flex;width:15px;height:15px;border-radius:50%;font-size:1px;color:green;' title='Sudah melampirkan sertifikat'>yes</span>";
      } else {
        return "<span style='background-color:red;display:flex;width:15px;height:15px;border-radius:50%;font-size:1px;color:red;' title='Belum melampirkan sertifikat'>no</span>";
      }
    default:
  }
  return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

function add_user_sertifikat_table_sorting( $columns ) {
	$columns['sertifikat']	= 'sertifikat';
	return $columns;
}
add_filter( 'manage_users_sortable_columns', 'add_user_sertifikat_table_sorting' );
 
//set instructions on how to sort the new column
if(is_admin()) {//prolly not necessary, but I do want to be sure this only runs within the admin
  add_action('pre_user_query', 'my_user_query');
}
function my_user_query($userquery){
  if('sertifikat'==$userquery->query_vars['orderby']) {//check if church is the column being sorted
    global $wpdb;
    $userquery->query_from .= " LEFT OUTER JOIN $wpdb->usermeta AS alias ON ($wpdb->users.ID = alias.user_id) ";//note use of alias
    $userquery->query_where .= " AND alias.meta_key = 'upload_sertifikat' ";//which meta are we sorting with?
    $userquery->query_orderby = " ORDER BY alias.meta_value ".($userquery->query_vars["order"] == "ASC" ? "asc " : "desc ");//set sort order
  }
}

// function add_pendidikan_filter() {
// 	if ( isset( $_GET[ 'pendidikan' ]) ) {
// 		$section = $_GET[ 'pendidikan' ];
// 		$section = !empty( $section[ 0 ] ) ? $section[ 0 ] : $section[ 1 ];
// 	} else {
// 		$section = -1;
// 	}
// 	echo '<select name="pendidikan[]" style="float:none;">';
// 		echo '<option value="">Filter by Pendidikan Terakhir</option>';
// 		for ( $i = 1; $i <= 3; ++$i ) {
// 			$selected = $i == $section ? ' selected="selected"' : '';
// 			echo '<option value="' . $i . '"' . $selected . '>S' . $i . '</option>';
// 		}
// 	echo '</select>';
// 	echo '<input type="submit" class="button" value="Filter">';
// }
// add_action( 'restrict_manage_users', 'add_pendidikan_filter' );

// function filter_users_by_pendidikan( $query ) {
// 	global $pagenow;

// 	if ( is_admin() && 'users.php' == $pagenow && isset( $_GET[ 'pendidikan' ] ) && is_array( $_GET[ 'pendidikan' ] ) ) {
// 		$section = $_GET[ 'pendidikan' ];
// 		// echo '<pre>';print_r($section); echo '</pre>'; exit;
// 		$section = !empty( $section[ 0 ] ) ? $section[ 0 ] : $section[ 1 ];
// 		if($section === 3) {
// 			$meta_query = array(
// 				array(
// 					'key'		=> 's3universitas',
// 					'value'	=> $section
// 				)
// 			);
// 			$query->set( 'meta_key', 's3universitas' );
// 			$query->set( 'meta_query', $meta_query );
// 		} elseif($section === 2) {
// 			$meta_query = array(
// 				array(
// 					'key'		=> 's2universitas',
// 					'value'	=> $section
// 				)
// 			);
// 			$query->set( 'meta_key', 's2universitas' );
// 			$query->set( 'meta_query', $meta_query );
// 		} elseif($section === 1) {
// 			$meta_query = array(
// 				array(
// 					'key'		=> 's1universitas',
// 					'value'	=> $section
// 				)
// 			);
// 			$query->set( 'meta_key', 's1universitas' );
// 			$query->set( 'meta_query', $meta_query );
// 		}
// 	}
// }
// add_filter( 'pre_get_users', 'filter_users_by_pendidikan' );
/* ------------------------------ User ------------------------------ */
?>