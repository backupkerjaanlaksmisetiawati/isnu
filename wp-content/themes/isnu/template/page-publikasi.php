<?php /* Template Name: Publikasi Ilmiah ISNU */ ?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/page/page',
			'publikasi'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/page/page',
			'publikasi'
		);
	?>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>