<?php /* Template Name: Media Foto ISNU */ ?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/page/page',
			'media-foto'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/page/page',
			'media-foto'
		);
	?>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_footer(); ?>