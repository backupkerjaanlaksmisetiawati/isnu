<?php /* Template Name: Opini ISNU */ ?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/page/page',
			'opini'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/page/page',
			'opini'
		);
	?>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>