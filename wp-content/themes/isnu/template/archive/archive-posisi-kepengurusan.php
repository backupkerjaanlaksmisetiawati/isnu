<?php get_header(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/archive/archive',
			'posisi-kepengurusan'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/archive/archive',
			'posisi-kepengurusan'
		);
	?>
</div>

<?php get_footer(); ?>