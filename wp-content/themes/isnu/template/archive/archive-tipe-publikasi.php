<?php get_header(); ?>

<article class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/archive/archive',
			'tipe-publikasi'
		);
	?>
</article>

<article class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/archive/archive',
			'tipe-publikasi'
		);
	?>
</article>

<?php get_footer(); ?>