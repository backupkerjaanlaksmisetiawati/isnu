<?php get_header(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/archive/archive',
			'post-category'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/archive/archive',
			'post-category'
		);
	?>
</div>

<?php get_footer(); ?>