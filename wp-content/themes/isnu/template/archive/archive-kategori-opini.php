<?php get_header(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/archive/archive',
			'kategori-opini'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/archive/archive',
			'kategori-opini'
		);
	?>
</div>

<?php get_footer(); ?>