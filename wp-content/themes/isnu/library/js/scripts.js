// jquery script disini....
function validateEmail($email) {
	var emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	return emailReg.test($email);
}

function validate_email(e) {
	$this = $(e);
	$email_val = $(e).val();
	emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	$email = emailReg.test($email_val);
	if ($email == false) {
		$('.error_email').removeClass('hide');
	} else {
		$('.error_email').addClass('hide');
	}
}

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

$(document).ready(function () {

	let hmHeight = $(".header_mobile").innerHeight();
	// $(".mobile_topMenu").css({
	// 	'top': hmHeight
	// });

	$(".btn_mobileTop button").click(function (e) {
		e.preventDefault()
		if ($(this).hasClass("act")) {
			$(this).removeClass("act");
			$(".mobile_topMenu").animate({
				right: '-100%'
			}, 900);
		} else {
			$(this).addClass("act");
			$(".mobile_topMenu").animate({
				right: 0
			}, 900);
		}
	})

	if ($("#mobileListMimpi").length > 0) {
		$('#mobileListMimpi').owlCarousel({
			center: true,
			autoWidth: true,
			items: 2,
			loop: true,
			margin: 15,
			nav: false,
			dots: false
		})
	}

	if ($("#slide_homeBanner").length > 0) {
		$('#slide_homeBanner').owlCarousel({
			center: true,
			loop: true,
			margin: 20,
			nav: false,
			items: 1,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				},
			}
		})

		var owl = $('#slide_homeBanner');
		owl.owlCarousel();
		// Go to the next item
		$('.slide_NextBtn').click(function () {
			owl.trigger('next.owl.carousel');
		})
		// Go to the previous item
		$('.slide_PrevBtn').click(function () {
			// With optional speed parameter
			// Parameters has to be in square bracket '[]'
			owl.trigger('prev.owl.carousel', [300]);
		})

	}

	if ($("#slide_rubrik").length > 0) {
		$('#slide_rubrik').owlCarousel({
			loop: true,
			margin: 10,
			nav: false,
			dots: true,
			autoplay: true,
			autoplayTimeout: 5000,
			autoplayHoverPause: true,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				},
				1000: {
					items: 2
				}
			}
		})
	}

	if ($("#slide_rubrikBeritaMobile").length > 0) {
		$('#slide_rubrikBeritaMobile').owlCarousel({
			center: true,
			items: 1,
			loop: true,
			margin: 20,
			nav: false,
			dots: true,
			autoplay: true,
			autoplayTimeout: 5000,
			autoplayHoverPause: true
		})
	}

	if ($("#slide_rubrikMobile").length > 0) {
		$('#slide_rubrikMobile').owlCarousel({
			center: true,
			items: 1,
			loop: true,
			margin: 20,
			nav: false,
			dots: true,
			autoplay: true,
			autoplayTimeout: 5000,
			autoplayHoverPause: true
		})
	}

	if ($(".select-basic-hide-search").length > 0) {
		$(".select-basic-hide-search").select2({
			minimumResultsForSearch: Infinity
		});
	}

	$(".select-option").on('change', function () {
		let dropdownVal = $(this).val();
		location.replace(dropdownVal);
	});

	if ($('.datepicker_daftar').length > 0) {
		var dpDateToday = new Date();
		var dpyrRange = (dpDateToday.getFullYear() - 50) + ":" + (dpDateToday.getFullYear() - 22);
		var firstYear = (dpDateToday.getFullYear() - 50);
		var defaultDate = '01-Jan-' + firstYear;
		$('.datepicker_daftar').datepicker({
			yearRange: dpyrRange,
			changeYear: true,
			changeMonth: true,
			defaultDate: defaultDate,
			dateFormat: 'dd-M-yy'
		});
	}

	$('.tipe-publikasi').click(function () {
		$('.tipe-publikasi').removeClass('active');
		$(this).addClass('active');
	});

	$('.btn_kepengurusan').click(function (e) {
		e.preventDefault();

		let btnslug = $(this).data('btnslug');

		if ($(this).hasClass('inactive')) {
			$(this).removeClass('inactive').addClass('active');
			$(this).html("Tutup <span>&rsaquo;</span>");
			$(".newwrap_subPengurus[data-slug='" + btnslug + "']").slideDown();
		} else {
			$(this).removeClass('active').addClass('inactive');
			$(this).html("Lihat Selengkapnya <span>&rsaquo;</span>");
			$(".newwrap_subPengurus[data-slug='" + btnslug + "']").slideUp();
		}
	});

	$('.btn_kepengurusan_mobile').click(function (e) {
		e.preventDefault();

		let btnslug_mobile = $(this).data('btnslug');

		if ($(this).hasClass('inactive')) {
			$(this).removeClass('inactive').addClass('active');
			$(this).html("<div>Tutup<br /><span>&rsaquo;</span></div >");
			$(".newwrap_subPengurus_mobile[data-slug='" + btnslug_mobile + "']").slideDown();
		} else {
			$(this).removeClass('active').addClass('inactive');
			$(this).html("<div>Lihat Selengkapnya <br /><span>&rsaquo;</span></div >");
			$(".newwrap_subPengurus_mobile[data-slug='" + btnslug_mobile + "']").slideUp();
		}
	});

	$('#formDaftar').on('submit', function (e) {
		e.preventDefault();

		var datas = new FormData();
		$('#formDaftar').serializeArray().reduce(function (obj, item) {
			datas.append(item.name, item.value);
			return obj;
		}, {});
		var file_data = $('#file_daftar').prop('files')[0];
		datas.append('filesertifikat', file_data);
		var file_foto = $('#file_foto').prop('files')[0];
		datas.append('filefoto', file_foto);
		datas.append('action', 'ajax_daftar_user');

		$('input, button, select, textarea').attr('disabled', 'disabled');
		$('.mg_pleasewait').show();
		$('.wrap_daftar').css('opacity', '0.3');
		$('.err_daftar').html("");

		$('html, body').animate({
			scrollTop: $("body").offset().top
		}, 1000);

		$.ajax({
			url: ajaxscript.ajaxurl,
			type: "POST",
			contentType: false,
			processData: false,
			data: datas,
			enctype: 'multipart/form-data',
			success: function (res) {
				var res = $.parseJSON(res);

				$('.mg_pleasewait').hide();
				$('.wrap_daftar').css('opacity', '1');
				$('input, button, select, textarea').removeAttr('disabled');

				console.log(res);

				if (res.status === 400) {
					$.each(res.data.message, function (key, value) {
						if (key === 'pendidikan') {

						} else {
							if (value !== '') {
								$('.' + key + '_err').html(value);
							} else {
								$('.' + key + '_err').html("valid").css({ "text-indent":"100%", "overflow":"hidden" });
							}
						}
					});
				}
				if (res.status === 200) {
					console.log(res);

					$('.success_daftar').addClass('bg-' + res.data.message_type)
						.removeClass('hide')
						.html(res.data.message);
					$('.success_daftar .pull-left').addClass('text-' + res.data.message_type);
					$('.close_success_daftar').click(function () {
						$('.success_daftar').addClass('hide');
					});
					
					$(".txt_daftar, .area_daftar").val("");
					$(".err_daftar").hide();

					$.ajax({
						url: ajaxscript.ajaxurl,
						type: "POST",
						data: {
							email: res.data.member_email,
							name: res.data.member_name,
							action: "ajax_email_daftar_user",
						},
						success: function (resmail) {
							console.log("success send email");
							console.log(resmail);
						},
						error: function (resmail) {
							console.log("error send email");
							console.log(resmail);
						}
					});
				}
			},
			error: function (res) {
				// console.log("error");
				// console.log(res);
			}
		});
	});

	if ($("#wrap_load_foto").length > 0) {
		foto_fancybox();
	};

	if ($("#bigFoto").length > 0) {
		var bigFoto = $("#bigFoto");
		var thumbsFoto = $("#thumbsFoto");
		//var totalslides = 10;
		var syncedSecondary = true;

		bigFoto
			.owlCarousel({
				items: 1,
				slideSpeed: 2000,
				nav: true,
				autoplay: false,
				dots: false,
				loop: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplayHoverPause: true,
				responsiveRefreshRate: 200,
				navText: [
					'<div></div>',
					'<div></div>'
				],
			})
			.on("changed.owl.carousel", syncPositionFoto);

		thumbsFoto
			.on("initialized.owl.carousel", function () {
				thumbsFoto
					.find(".owl-item")
					.eq(0)
					.addClass("current");
			})
			.owlCarousel({
				items: 6,
				dots: false,
				nav: false,
				// loop: true,
				// navText: [
				// 	'<div><img src="../images/arrow_right.svg" /></div>',
				// 	'<div><img src="../images/arrow_right.svg" /></div>'
				// ],
				smartSpeed: 200,
				slideSpeed: 500,
				slideBy: 1,
				margin: 10,
				responsiveRefreshRate: 100
			})
			.on("changed.owl.carousel", syncPositionFoto2);

		function syncPositionFoto(el) {
			//if loop is set to false, then you have to uncomment the next line
			//var current = el.item.index;

			//to disable loop, comment this block
			var count = el.item.count - 1;
			var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

			if (current < 0) {
				current = count;
			}
			if (current > count) {
				current = 0;
			}
			//to this
			thumbsFoto
				.find(".owl-item")
				.removeClass("current")
				.eq(current)
				.addClass("current");
			var onscreen = thumbsFoto.find(".owl-item.active").length - 1;
			var start = thumbsFoto
				.find(".owl-item.active")
				.first()
				.index();
			var end = thumbsFoto
				.find(".owl-item.active")
				.last()
				.index();

			if (current > end) {
				thumbsFoto.data("owl.carousel").to(current, 100, true);
			}
			if (current < start) {
				thumbsFoto.data("owl.carousel").to(current - onscreen, 100, true);
			}
		}

		function syncPositionFoto2(el) {
			if (syncedSecondary) {
				var number = el.item.index;
				bigFoto.data("owl.carousel").to(number, 100, true);
			}
		}

		thumbsFoto.on("click", ".owl-item", function (e) {
			e.preventDefault();
			var number = $(this).index();
			bigFoto.data("owl.carousel").to(number, 300, true);
		});
	};

	if ($("#bigFotoMobile").length > 0) {
		var bigFotoMobile = $("#bigFotoMobile");
		var thumbsFotoMobile = $("#thumbsFotoMobile");
		//var totalslides = 10;
		var syncedSecondary = true;

		bigFotoMobile
			.owlCarousel({
				items: 1,
				slideSpeed: 2000,
				nav: true,
				autoplay: false,
				dots: false,
				loop: true,
				responsiveRefreshRate: 200,
				navText: [
					'<div></div>',
					'<div></div>'
				],
			})
			.on("changed.owl.carousel", syncPositionFoto);

		thumbsFotoMobile
			.on("initialized.owl.carousel", function () {
				thumbsFotoMobile
					.find(".owl-item")
					.eq(0)
					.addClass("current");
			})
			.owlCarousel({
				items: 4,
				dots: false,
				nav: false,
				// loop: true,
				// navText: [
				// 	'<div><img src="../images/arrow_right.svg" /></div>',
				// 	'<div><img src="../images/arrow_right.svg" /></div>'
				// ],
				smartSpeed: 200,
				slideSpeed: 500,
				slideBy: 1,
				margin: 10,
				responsiveRefreshRate: 100
			})
			.on("changed.owl.carousel", syncPositionFoto2);

		function syncPositionFoto(el) {
			//if loop is set to false, then you have to uncomment the next line
			//var current = el.item.index;

			//to disable loop, comment this block
			var count = el.item.count - 1;
			var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

			if (current < 0) {
				current = count;
			}
			if (current > count) {
				current = 0;
			}
			//to this
			thumbsFotoMobile
				.find(".owl-item")
				.removeClass("current")
				.eq(current)
				.addClass("current");
			var onscreen = thumbsFotoMobile.find(".owl-item.active").length - 1;
			var start = thumbsFotoMobile
				.find(".owl-item.active")
				.first()
				.index();
			var end = thumbsFotoMobile
				.find(".owl-item.active")
				.last()
				.index();

			if (current > end) {
				thumbsFotoMobile.data("owl.carousel").to(current, 100, true);
			}
			if (current < start) {
				thumbsFotoMobile.data("owl.carousel").to(current - onscreen, 100, true);
			}
		}

		function syncPositionFoto2(el) {
			if (syncedSecondary) {
				var number = el.item.index;
				bigFotoMobile.data("owl.carousel").to(number, 100, true);
			}
		}

		thumbsFotoMobile.on("click", ".owl-item", function (e) {
			e.preventDefault();
			var number = $(this).index();
			bigFotoMobile.data("owl.carousel").to(number, 300, true);
		});
	};

});

function remove_riwayat_pendidikan(sarjana) {
	$('#list_pendidikan_' + sarjana).remove();
};

function changeJurusanLabel(selectID) {
	var valPendidikan = $("#" + selectID).val();
	var sJurusan = selectID.replace("pendidikan", "");
	if (valPendidikan == "Guru Besar") {
		$("#labelJurusan" + sJurusan).html("Bidang");
	} else {
		$("#labelJurusan" + sJurusan).html("Jurusan");
	}
}

function add_riwayat_pendidikan() {
	let pendidikanLength = $('.list_pendidikan').length;
	let next_pendidikanLength = pendidikanLength + 1;

	if (pendidikanLength < 4) {
		var html = "";
		html += '<div class="row list_pendidikan" id="list_pendidikan_s' + next_pendidikanLength +'">';
			html += '<div class="col-md-2 nopadding_right nopadding_left">';
				html += '<label style="display:block">Gelar</label>';
				html += '<select onchange="changeJurusanLabel(\'pendidikans' + next_pendidikanLength + '\')" name="pendidikans' + next_pendidikanLength + '" id="pendidikans' + next_pendidikanLength + '" class="sel_daftar">';
				html += '<option value="S1">S1 (Sarjana)</option>';
				if (pendidikanLength == 1) {
					html += '<option value="S2" selected>S2 (Magister)</option>';
				} else {
					html += '<option value="S2">S2 (Magister)</option>';
				}
				if (pendidikanLength == 2) {
					html += '<option value="S3" selected>S3 (Doktor)</option>';
				} else {
					html += '<option value="S3">S3 (Doktor)</option>';
				}
				if (pendidikanLength == 3) {
					html += '<option value="Guru Besar" selected>Guru Besar</option>';
				} else {
					html += '<option value="Guru Besar">Guru Besar</option>';
				}
				html += '</select>';
			html += '</div>';
			html += '<div class="col-md-3 nopadding_right">';
				html += '<label>Universitas</label>';
				html += '<input type="text" name="universitass' + next_pendidikanLength + '" class="txt_daftar">';
				html += '<div class="f_err err_daftar universitass' + next_pendidikanLength + '_err"></div>';
			html += '</div>';
			html += '<div class="col-md-3 nopadding_right">';
				if (next_pendidikanLength == 3) {
					html += '<label id="labelJurusans' + next_pendidikanLength + '">Bidang</label>';
				} else {
					html += '<label id="labelJurusans' + next_pendidikanLength + '">Jurusan</label>';
				}
				html += '<input type="text" name="jurusans' + next_pendidikanLength + '" class="txt_daftar">';
				html += '<div class="f_err err_daftar jurusans' + next_pendidikanLength + '_err"></div>';
			html += '</div>';
			html += '<div class="col-md-3 nopadding_right tahun_kelulusan">';
				html += '<label>Tahun</label>';
				html += '<input type="text" name="tahuns' + next_pendidikanLength + 'from" class="txt_daftar" maxlength="4"> -';
				html += '<input type="text" name="tahuns' + next_pendidikanLength + 'to" class="txt_daftar" maxlength="4">';
				html += '<div class="f_err err_daftar tahuns' + next_pendidikanLength + '_err"></div>';
			html += '</div>';
			if (pendidikanLength != 3) {
				html += '<div class="col-md-1 nopadding_left nopadding_right btn_addremove">';
			} else {
				html += '<div class="col-md-1 nopadding_left nopadding_right btn_addremove" style="padding-top: 30px;">';
			}
					if (pendidikanLength != 3) {
						html += '<a class="add_pendidikan btn_yellow" onclick="add_riwayat_pendidikan()">Add</a>';
					}
					html += '<a class="remove_pendidikan btn_black" onclick="remove_riwayat_pendidikan(\'s' + next_pendidikanLength + '\')">Remove</a>';
				html += '</div>';
		html += '</div>';

		$(".gelar_pendidikan").append(html);
	}
}

function remove_pengalaman_organisasi(sarjana) {
	$('#list_organisasi_' + sarjana).remove();
};

function add_pengalaman_organisasi() {
	let pengalamanLength = $('.list_organisasi').length;
	let next_pengalamanLength = pengalamanLength + 1;

	var html = "";
	html += '<div class="row list_organisasi" id="list_organisasi_s' + next_pengalamanLength + '">';
		html += '<div class="col-md-4 nopadding_right">';
			html += '<label>Nama Organisasi</label>';
			html += '<input type="text" name="organisasi_name[]" class="txt_daftar">';
			html += '<div class="f_err err_daftar organisasi_name_' + next_pengalamanLength + '_err"></div>';
		html += '</div>';
		html += '<div class="col-md-4 nopadding_right">';
			html += '<label>Keterangan Organisasi</label>';
			html += '<input type="text" name="organisasi_detail[]" class="txt_daftar">';
			html += '<div class="f_err err_daftar organisasi_detail_' + next_pengalamanLength + '_err"></div>';
		html += '</div>';
		html += '<div class="col-md-3 nopadding_right tahun_kelulusan">';
			html += '<label>Tahun</label>';
			html += '<input type="text" name="organisasi_from[]" class="txt_daftar" maxlength="4"> - ';
			html += '<input type="text" name="organisasi_to[]" class="txt_daftar" maxlength="4">';
			html += '<div class="f_err err_daftar organisasi_tahun_' + next_pengalamanLength + '_err"></div>';
		html += '</div>';
		html += '<div class="col-md-1 nopadding_left nopadding_right btn_addremove">';
			html += '<a class="add_pengalaman btn_yellow" onclick="add_pengalaman_organisasi()">Add</a>';
			html += '<a class="remove_pengalaman btn_black" onclick="remove_pengalaman_organisasi(\'s' + next_pengalamanLength + '\')">Remove</a>';
		html += '</div>';
	html += '</div>';

	$(".pengalaman_organisasi").append(html);
}

function remove_link_referensi_publikasi(pId) {
	$('#list_link_referensi_publikasi_' + pId).remove();
};

function add_link_referensi_publikasi() {
	let publikasiLength = $('.list_link_referensi_publikasi').length;
	let next_PublikasiLength = publikasiLength + 1;

	var html = "";
	html += '<div class="row list_link_referensi_publikasi" id="list_link_referensi_publikasi_s' + next_PublikasiLength + '">';
	html += '<div class="col-md-11 nopadding_left">';
	html += '<input type="text" name="link_referensi_publikasi[]" class="txt_daftar">';
	html += '<div class="f_err err_daftar link_referensi_publikasi_' + next_PublikasiLength + '_err"></div>';
	html += '</div>';
	html += '<div class="col-md-1 nopadding_left nopadding_right btn_addremove">';
	html += '<a class="add_pengalaman btn_yellow" onclick="add_link_referensi_publikasi()">Add</a>';
	html += '<a class="remove_pengalaman btn_black" onclick="remove_link_referensi_publikasi(\'s' + next_PublikasiLength + '\')">Remove</a>';
	html += '</div>';
	html += '</div>';

	$(".link_referensi_publikasi").append(html);
}

$('.tabsTentang li').click(function () {

	$('.tabsTentang li').removeClass('act');
	$('.col_content_tentang').removeClass('act');
	var tabs = $(this).data('id');
	// console.log(tabs);

	if ($(this).hasClass('act')) {

	} else {
		$(this).addClass('act');
		$('#' + tabs).addClass('act');
	}

});

function seePassword(e) {
	e.preventDefault();
	if ($("#btnpass").hasClass("hidden_pass")) {
		$("#show_pass").css('display', 'none');
		$("#hide_pass").css('display', 'flex');
		$("#btnpass").removeClass("hidden_pass").addClass("showed_pass");
		$("#user_pass").attr("type", "text");
	} else {
		$("#show_pass").css('display', 'flex');
		$("#hide_pass").css('display', 'none');
		$("#btnpass").removeClass("showed_pass").addClass("hidden_pass");
		$("#user_pass").attr("type", "password");
	}

}

function submit_LoginNow(ev) {
	ev.preventDefault();

	var email = $('input[name="log"]').val();
	var password = $('input[name="pwd"]').val();

	if (email == '') {
		$('#err_email').empty().append("*Please insert your email").fadeIn();
	} else if (!validateEmail(email)) {
		$('#err_email').empty().append("*Wrong email format, please").fadeIn();
	} else if (password == '') {
		$('#err_pwd').empty().append("*Please insert your password").fadeIn();
	} else {
		//check flag
		// -------

		//$('#err_pwd').empty().append("*Akun anda belum aktif").fadeIn();
		//

		$('.f_err').empty().fadeOut();
		$('#formLogin')[0].submit();
	}

}

function submit_LupaPassword(ev) {
	ev.preventDefault();

	var email = $('input[name="user_login"]').val();

	if (email == '') {
		$('#err_email').empty().append("*Please insert your email").fadeIn();
	} else if (!validateEmail(email)) {
		$('#err_email').empty().append("*Wrong email format, please check again").fadeIn();
	} else {
		$('.f_err').empty().fadeOut();
		$('#lostpasswordform')[0].submit();
	}

}


if ($('.allow_ajax_load_berita').length > 0) {
	$('.listRubrikCat li').click(function () {

		$('.listRubrikCat li').removeClass('act');
		$('.box_v_listRubrik').removeClass('act').removeClass('finished');
		var tabs = $(this).data('id');
		var cat_id = $(this).data('catid');
		var status = $(this).data('status');
		$('.ht_notfound').hide();

		if ($(this).hasClass('act')) {

		} else {
			$(this).addClass('act');
			$('#' + tabs).addClass('act');
		}

		if (status == 'page') { // for berita page

			if (!$.trim($('#show_berita_' + cat_id).html()).length) {
				// if ($('#show_berita_'+cat_id).length > 0){ // cek div is null / empty to field the berita
				// console.log('jika kosong nih');

				if (cat_id != '') {

					$('.mg_pleasewait').show();

					$.ajax({

						url: ajaxscript.ajaxurl,
						data: {
							'action': 'ajax_showBerita',
							'cat_id': cat_id,
							'status': status
						},
						success: function (result) {
							// console.log(cat_id);
							var obj = $.parseJSON(result);
							if (obj != '') {
								$('#show_berita_' + cat_id).html(obj);
							} else {
								// console.log('kosong nih');
								$('.ht_notfound').show();
							}
							$('.mg_pleasewait').hide();
						},
					});
				}

			} else {
				// console.log('jika ada isi nih');



			}

		} else {
			if (cat_id != '') {
				$('.mg_pleasewait').show();
				$('#show_berita').empty();

				$.ajax({

					url: ajaxscript.ajaxurl,
					data: {
						'action': 'ajax_showBerita',
						'cat_id': cat_id,
					},
					success: function (result) {
						// console.log(result);
						var obj = $.parseJSON(result);
						if (obj != '') {
							$('#show_berita').empty().html(obj);
						} else {
							// console.log('kosong nih');
							$('.ht_notfound').show();
						}
						$('.mg_pleasewait').hide();
					},
				});
			}
		}



	});
}

function foto_fancybox() {
	$('[data-fancybox="gallery-foto"]').fancybox({
		// Options will go here
		infobar: false,
		toolbar: false,
		baseTpl:
			'<div class="fancybox-container" role="dialog" tabindex="-1">' +
			'<div class="fancybox-bg"></div>' +
			'<div class="fancybox-inner">' +
			'<div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div>' +
			'<div class="fancybox-toolbar">{{buttons}}</div>' +
			'<div class="fancybox-navigation">{{arrows}}</div>' +
			'<div class="fancybox-stage"></div>' +
			'<div class="fancybox-caption"><div class="fancybox-caption__body"></div></div>' +
			'</div>' +
			'</div>',
	});
}

// if ($('.allow_ajax_load_publikasi_caregory').length > 0) {
// 	$(window).bind('scroll', function () {
// 		let is_hitListRubikPublikasiCategory = $('.box_v_listRubrik.act').offset().top + $('.box_v_listRubrik.act').outerHeight() - window.innerHeight;

// 		if ($(window).scrollTop() >= is_hitListRubikPublikasiCategory && $(".box_v_listRubrik.act").hasClass('proccessing') === false && $(".box_v_listRubrik.act").hasClass('finished') === false) {
// 			$(".box_v_listRubrik.act").addClass('proccessing');
// 			$(".box_v_listRubrik.act.proccessing").children('.mg_pleasewait').show();

// 			let containerAjaxLoadPublikasiCategory = $(".box_v_listRubrik.act.proccessing").children('.load_ajax_publikasi_category');
// 			let nextPagePublikasiCategory = $(containerAjaxLoadPublikasiCategory).attr("data-next-page");
// 			let perPagePublikasiCategory = $(containerAjaxLoadPublikasiCategory).attr("data-per-page");
// 			let categoryIDPublikasiCategory = $(containerAjaxLoadPublikasiCategory).attr("data-category");

// 			loadPublikasiCategory(nextPagePublikasiCategory, perPagePublikasiCategory, categoryIDPublikasiCategory);
// 		}
// 	});
// }

// function loadPublikasiCategory(nextPagePublikasiCategory, perPagePublikasiCategory, categoryIDPublikasiCategory) {
// 	// change value type from string to integer
// 	nextPagePublikasiCategory = parseInt(nextPagePublikasiCategory);
// 	perPagePublikasiCategory = parseInt(perPagePublikasiCategory);
// 	categoryIDPublikasiCategory = parseInt(categoryIDPublikasiCategory);

// 	$.ajax({
// 		url: ajaxscript.ajaxurl,
// 		type: "POST",
// 		data: {
// 			'action': 'load_ajax_publikasi_category',
// 			'posts_per_page': perPagePublikasiCategory,
// 			'paged': nextPagePublikasiCategory,
// 			'category__in': categoryIDPublikasiCategory
// 		},
// 		success: function (res) {
// 			var res = $.parseJSON(res);
// 			// console.log(res);

// 			if (res.data.html != '') {
// 				$("#show_publikasi").append(res.data.html);

// 				nextPagePublikasiCategory = nextPagePublikasiCategory + 1;
// 				$(".load_ajax_publikasi_category").attr("data-next-page", nextPagePublikasiCategory);
// 			} else {
// 				$(".box_v_listRubrik.act").addClass('finished');
// 			}

// 			$(".box_v_listRubrik.act").removeClass('proccessing');
// 			$('.mg_pleasewait').hide();
// 		}
// 	});

// }