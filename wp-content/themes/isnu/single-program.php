<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="desktop-template">
	<?php
		get_template_part(
			'template-desktop/single/single',
			'program'
		);
	?>
</div>

<div class="wrap_utama_mobile mobile-template">
	<?php
		get_template_part(
			'template-mobile/single/single',
			'program'
		);
	?>
</div>

<?php endwhile; ?>
<?php else : ?>
<article id="post-not-found" class="hentry clearfix">
		<header class="article-header">
			<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
		</header>
		<section class="entry-content">
			<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
		</section>
		<footer class="article-footer">
				<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
		</footer>
</article>
<?php endif; ?>
<?php get_footer(); ?>