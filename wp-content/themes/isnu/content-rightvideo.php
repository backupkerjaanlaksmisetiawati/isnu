<?php 
  //highlight video di detail article
  $video = getArticleHighlightVideos();
?>
<?php if($video !== null) { ?>
  <div class="bx_post_video">
    <h2 class="ht_relatedPost">Video ISNU</h2>
    <a href="<?php echo get_the_permalink($video->ID); ?>"
      class="frame_video mg_postvideo">
      <img src="<?php echo $video->foto; ?>"
        alt="<?php echo $video->alt_foto; ?>" />
      <div class="iconplay">
        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-play.png" />
      </div>
    </a>
    <a href="<?php echo get_the_permalink($video->ID); ?>" title="Lihat <?php echo $video->post_title; ?>">
      <h3 class="ht_videoisnu_detail"><?php echo $video->post_title; ?></h3>
      <div class="info_videoisnu_detail"><?php echo $video->big_video_information; ?></div>
    </a>
  </div>
<?php } ?>