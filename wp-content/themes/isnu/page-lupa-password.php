<?php
/*
Template Name: Lupa Password Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	header, footer{
		display: none !important;
	}
</style>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
	$referer = $_SESSION['rfr'];
}else{

	$rfr = wp_get_referer();
	if(isset($rfr) AND $rfr != ''){
		$referer = wp_get_referer();
		$_SESSION['rfr'] = $referer;
	}else{
		$referer = home_url();
	}

}

$lostpassword_redirect = ! empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
$redirect_to = apply_filters( 'lostpassword_redirect', $lostpassword_redirect );

?>

<div class="row"></div>

<div class="row row_login">
	<div class="col-md-4"></div>
	<div class="col-md-4 col_login">

		<div class="box_toplogin">
			<div class="mg_topLogo">
				<img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
			</div>
			<h1 class="ht_login">Lupa Password Akun</h1>
		</div>

		<div class="box_login">

				<?php 
					if(isset($u_id) AND $u_id != ''){
				?>
					<div class="ok_login">
						<span class="glyphicon glyphicon-ok ok_icon"></span>
						<div class="ht_okLogin">Selamat Datang di ISNU</div>
						<span>Mohon menunggu...</span>
						<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
					</div>

					<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>"></div>
					<script>
						var plant = document.getElementById('redirect_ok');
						var hurl = plant.getAttribute('data-hurl'); 
					    setTimeout(function(){
				          location.replace(hurl); 
				        }, 2000);
					</script>
				<?php
					}else{
				?>
					    <form id="lostpasswordform" name="lostpasswordform" method="post" autocomplete="nope" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" class="wp-user-form formLogin" onsubmit="submit_LupaPassword(event);">
					        <input autocomplete="nope" name="hidden" type="text" style="display:none;">

                  <?php 
                    if(isset($_GET['submit']) AND $_GET['submit'] == 'failed'){
                  ?>
                    <div class="alert alert-danger">
                      <strong>Maaf,</strong> email atau password salah. Silahkan coba kembali.
                    </div>
                  <?php
                    }
                  ?>

                  <div class="f_ad_input">
                    <label>Email</label>
                    <input type="email" ng-model="username" name="user_login" id="user_login" class="ad_input" placeholder="" value="" required="required"  maxlength="50" />
                    <div id="err_email" class="f_err">*Silahkan isi email</div>
                  </div>
                  
                  <div class="f_errLogin h_errLogin"></div>
              
                  <div class="login_fields">
                    <?php do_action('lostpassword_form'); ?>
                    <input type="submit" name="wp-submit" id="wp-submit" class="sub_login" value="Reset Password"/>
                    <input type="hidden" name="redirect_to" value="<?php echo esc_attr( $referer ); ?>" />
                    <input type="hidden" name="user-cookie" value="1" />
                    <?php wp_nonce_field( 'login_submit' ); ?>
                  </div>
					    </form>
					    <a class="a_bergabung" href="<?php echo home_url(); ?>/daftar/" title="Bergabung Sekarang!">
					      <u>Belum punya akun? <b>Bergabung Sekarang</b></u>
					    </a>
				<?php
					}
				 ?>
         
		</div>

	</div>
	<div class="col-md-4"></div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
  (function (global) { 

      if(typeof (global) === "undefined") {
          throw new Error("window is undefined");
      }

      var _hash = "!";
      var noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
          global.location.href += "!";
        }, 50);
      };

      global.onhashchange = function () {
        if (global.location.hash !== _hash) {
          global.location.hash = _hash;
        }
      };

      global.onload = function () {            
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
          var elm = e.target.nodeName.toLowerCase();
          if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
            e.preventDefault();
          }
          // stopping event bubbling up the DOM tree..
          e.stopPropagation();
        };          
      }

  })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>