	<div class="desktop-template">
		<?php
			get_template_part(
				'template-desktop/global/global',
				'footer'
			);
		?>
	</div>

	<div class="mobile-template">
		<?php
			get_template_part(
				'template-mobile/global/global',
				'footer'
			);
		?>
	</div>

</article> <?php // end of div container ?>

<?php wp_footer(); ?>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>

	<?php 
		$page_id = get_the_ID();
		$page_post = get_post($page_id);
	?>

	<?php if( !empty($page_post) && $page_post->post_name === 'publikasi-ilmiah' ) { ?>
		<?php
			$get_tahun_publikasi = get_terms(
				array(
					'taxonomy'    => 'tahun-publikasi',
					'orderby'     => 'name',
					'hide_empty'  => false,
				)
			);

			$min_thnPublikasi = ( !empty($get_tahun_publikasi) ) ? $get_tahun_publikasi[0]->slug : 0 ;
			$max_thnPublikasi = ( !empty($get_tahun_publikasi) ) ? $get_tahun_publikasi[count($get_tahun_publikasi) - 1]->slug : 0 ;
		?>

		<script type="application/javascript">
			$( function() {
				$('#filterTahunPublikasi').slider({
					range: true,
					min: <?php echo $min_thnPublikasi; ?>,
					max: <?php echo $max_thnPublikasi; ?>,
					<?php if(isset($_GET['from_tahun'])) { ?>
						values: [ <?php echo $_GET['from_tahun']; ?>, <?php echo $_GET['to_tahun']; ?> ],
					<?php } else { ?>
						values: [ <?php echo $min_thnPublikasi; ?>, <?php echo $max_thnPublikasi; ?> ],
					<?php } ?>
					create: function() {
						$("#amount").val("<?php echo(isset($_GET['from_tahun'])) ? $_GET['from_tahun'] : $min_thnPublikasi; ?>");
						$("#amount_to").val("<?php echo(isset($_GET['to_tahun'])) ? $_GET['to_tahun'] : $max_thnPublikasi; ?>");
					},
					slide: function (event, ui) {
						<?php if( !empty($get_tahun_publikasi) ) { ?>
							// $("#amount").val( ui.value );
						<?php } else { ?>
							$('#filterTahunPublikasi').slider('disable');					
						<?php } ?>

						$("#amount").val( ui.values[ 0 ] );
						$("#amount_to").val( ui.values[ 1 ] );
					}
				})
				
				$('#filterTahunPublikasiMobile').slider({
					range: true,
					min: <?php echo $min_thnPublikasi; ?>,
					max: <?php echo $max_thnPublikasi; ?>,
					<?php if(isset($_GET['from_tahun'])) { ?>
						values: [ <?php echo $_GET['from_tahun']; ?>, <?php echo $_GET['to_tahun']; ?> ],
					<?php } else { ?>
						values: [ <?php echo $min_thnPublikasi; ?>, <?php echo $max_thnPublikasi; ?> ],
					<?php } ?>
					create: function() {
						$("#amountMobile").val("<?php echo(isset($_GET['from_tahun'])) ? $_GET['from_tahun'] : $min_thnPublikasi; ?>");
						$("#amountMobile_to").val("<?php echo(isset($_GET['to_tahun'])) ? $_GET['to_tahun'] : $max_thnPublikasi; ?>");
					},
					slide: function (event, ui) {
						<?php if( !empty($get_tahun_publikasi) ) { ?>
							// $("#amountMobile").val( ui.value );
						<?php } else { ?>
							$('#filterTahunPublikasiMobile').slider('disable');					
						<?php } ?>

						$("#amountMobile").val( ui.values[ 0 ] );
						$("#amountMobile_to").val( ui.values[ 1 ] );
					}
				})
			});
		</script>
	<?php } ?>

</body>

</html>