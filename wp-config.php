<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'isnu_db2019' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J=A85v#L.<L7QlL[uR.M0gLCrFEZd=pVm2}(B~|Ua|1k8[BTy{%({TM6Ix8/v|xg' );
define( 'SECURE_AUTH_KEY',  'L;Xz//ymNdWXB2tmDA9ToYI/;I7?cPauc*aj:!PV8v#Lz]b3zlY|=FjELvanSjS0' );
define( 'LOGGED_IN_KEY',    'OpL@FC8=:OAf|WQX>ZeO2;pzi{DC_O;fEO#;{(qot_I!lEuw9jx*r:e*~X_W^J(}' );
define( 'NONCE_KEY',        'HzWl| FG 3IK#l.T^ P@2I~zH4d)J#Rs}G1q}$hPsEjWM4`Ls4(@5X>G(vAE|,`]' );
define( 'AUTH_SALT',        '9rmw=Zr.G[x>[fc=J7jHC ?C5@XeQY.cGw72(-/F7i1,e|PdAq#/eVO+T7V9ZAUh' );
define( 'SECURE_AUTH_SALT', 'np#?j%.cAAwD9YkfotUTLj.-0|@Z&Et%+z8cj5Y7Z0#r4(wtGM&4{{,BjoN-&`G]' );
define( 'LOGGED_IN_SALT',   'e&v0!/QiGb*Y64l#YT!lW!Iy/T-|Gn]Q{`Ud>ak:MG6MxtBhO$K52RF{?$Rgm:2a' );
define( 'NONCE_SALT',       '3.i?h@p(e`<a jv;ck#Y/<jK$E&LwMp[<y.$Aee3Us/v}wb*jUf >StPh?8=f-9=' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'unsi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
