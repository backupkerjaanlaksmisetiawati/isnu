UPDATE das_options SET option_value = replace(option_value, 'http://localhost/astrido', 'http://staging.toyotaastrido.co.id') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE das_posts SET guid = replace(guid, 'http://localhost/astrido','http://staging.toyotaastrido.co.id');

UPDATE das_posts SET post_content = replace(post_content, 'http://localhost/astrido', 'http://staging.toyotaastrido.co.id');

UPDATE das_postmeta SET meta_value = replace(meta_value,'http://localhost/astrido','http://staging.toyotaastrido.co.id');